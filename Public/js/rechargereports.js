
function getrechargereports(from,to){
  var data ={};
  data.fromdate = from;
  data.todate = to;
  $('#example1').dataTable({
  processing: true,
  destroy:true,
  autoWidth:false,
  jQueryUI: true,
  sAjaxDataProp: 'data',
  ordering:false,
  ajax:{
  url: "/GetRcReports_Agent",
  type: 'POST',
  data: data,
  dataSrc: ""
  },
    columns: [
      {
            data: "orderid",
            className: "center",
            render: function(data, type, row, meta){
              // if(type === 'display'){
                 data='<td><div class="row" style=" margin-left: -8px;"><a href="#" onclick = ALLRechargeReports("'+row.orderid+'")>'+row.orderid+'</a></div></td>';
            //   }
               return data;
            }
      },
      { data : "operatorid" },
      { data : "MobileNumber" },
      { data : "servicekey"},
      { data : "TransactionAmount" },
      {
      data: "_id",
      className: "center",
      render: function(data, type, row, meta){
      //     if(type === 'display'){
             data='<td>'+moment(row.RechargeTime).format("DD MMM YYYY HH:mm A")+'</td>';
      //   }
       return data;
      }
      },
      {
         data: "_id",
         className: "center",
         render: function(data, type, row, meta){
            //   if(type === 'display'){
                   data='<td><div class="row"><div class="col-sm-5"><h6 style = "margin-top: -3px;">'+row.Status+'</h6><a  href="#" onclick = RechargeReports("'+row._id+'")>ViewDetails</a></div></td>';
            // }
           return data;

        }
     },

]
});

 }
//     ]
//   });
//
// }
function RechargeReports(id){
$.ajax({
  type: 'POST',
   data: {rechargeId:id},
   dataType:'json',
   url: '/GetAllRcReportsID',
    success: function(res) {
        if(res.Status == "SUCCESS"){
              walletID(res);
              $("#WalletNumber").show();
        }else if(res.Status == "Fail"){
           $("#WalletNumber").hide();
        }
      $("#myModal11").modal('show');
      $("#OperatorId").html(res.orderid);
      $("#MobileNumber").html(res.MobileNumber);
      $("#ChargedAmount").html(res.TransactionAmount);
      $("#ResponseMessage").html(res.responsemsg);
      var data = moment(res.RechargeTime).format("DD MMM  YYYY, h:mm a");
        $("#ResponseTime").html(data);
    }
});
}
function ALLRechargeReports(order){
$.ajax({
  type: 'POST',
   data: {orderid:order},
   dataType:'json',
   url: '/GetAllOrderReports',
    success: function(res) {

    if(res.Status == "SUCCESS"){
          walletID(res);
          $("#WalletNumber").show();
    }else if(res.Status == "Fail"){
       $("#WalletNumber").hide();
    }
      $("#myModal11").modal('show');
      $("#OperatorId").html(res.orderid);
      $("#MobileNumber").html(res.MobileNumber);
      $("#ChargedAmount").html(res.TransactionAmount);
      $("#ResponseMessage").html(res.responsemsg);
      var data = moment(res.RechargeTime).format("DD MMM  YY, h:mm a");
        $("#ResponseTime").html(data);
        $("#myModal11").modal('hide');


    }
});
}
function walletID(res){
  //alert("iii");
  $("#TransactionId").val(res.orderid);
  $("#ServicessName").val(res.servicekey)
  $("#myModalwallet").modal('hide');
}
$("#sendEmail").click(function(){
  var data = {};
  data.ToAddress = $("#mailtoagent").val();
  data.Subject = $("#subjectmail").val();
  data.ServiceName = $("#ServicessName").val();
  data.TransactionId = $("#TransactionId").val();
  data.Email_Body = $('#bodymail').val();
  console.log(data);
  if(data.ToAddress == ""){
      $("#mailtoagent").notify("Please Enter AgentEmail..", { className: "error", position:"top" });
      return false;
  }
  if(data.Subject == ""){
      $("#subjectmail").notify("Please fill this field", { className: "error", position:"top" });
      return false;
  }
//return false;
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/Sendmailtocustomerservices',
      async:true,
      crossDomain:true,
      success: function(res) {
        if(res == "1"){
          $("#sendEmail").notify("Message Sent Successfully..", { className: "success", position:"left" });
          setTimeout(function() {
           location.reload();
         },3000);
          // $("#subjectmail").val("");
          // $('#bodymail').val("");
          return false;
        }
        else{
          $("#sendEmail").notify("Message Not Sent..", { className: "error", position:"left" });
           $('#sendemailform')[0].reset();
          return false;
        }
      }
  });
})
