function checkPassword(str)
 {
	 // at least one number, one lowercase and one uppercase letter
	 // at least six characters
	 var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
	 return re.test(str);
 }
// $(function (){
// 	setAgents();
// 	setLenders();
// 	$("#cms_nav_bars").load("/cmsmenu", function () {
// 		$("#menu-toggle").click(function(e) {
// 	        //$("#wrapper").toggleClass("toggled");
// 	        $("#sidebar-wrapper").show();
// 		    if($("#sidebar-wrapper").hasClass("fadeInLeft")){
// 		        $("#sidebar-wrapper").removeClass ("fadeInLeft");
// 				$("#sidebar-wrapper").addClass ("fadeOutLeft");
// 		    }else{
// 		        $("#sidebar-wrapper").removeClass ("fadeOutLeft");
// 		        $("#sidebar-wrapper").addClass ("fadeInLeft");
// 		    }
// 	    });
// 	    $("#cmspost").click(function(){
// 			$('.post_sub-menu').toggle();
// 			if($('#cmspost').hasClass("open")){
// 				$('#cmspost').removeClass("open");
// 			} else {
// 				$('#cmspost').addClass("open");
// 			}
// 		});
// 		$("#cmssetting").click(function(){
// 			$('.setting_sub-menu').toggle();
// 			if($('#cmssetting').hasClass("open")){
// 				$('#cmssetting').removeClass("open");
// 			} else {
// 				$('#cmssetting').addClass("open");
// 			}
// 		});
// 	});
//
// 	$("#nav_bars").load("/sidebar", function () {
//
// 		$("#menu-toggle").click(function(e) {
// 	        //$("#wrapper").toggleClass("toggled");
// 	        $("#sidebar-wrapper").show();
// 		    if($("#sidebar-wrapper").hasClass("fadeInLeft")){
// 		        $("#sidebar-wrapper").removeClass ("fadeInLeft");
// 				$("#sidebar-wrapper").addClass ("fadeOutLeft");
// 		    }else{
// 		        $("#sidebar-wrapper").removeClass ("fadeOutLeft");
// 		        $("#sidebar-wrapper").addClass ("fadeInLeft");
// 		    }
// 	    });
//
// 	    $("#report").click(function(){
// 			$('.report_sub-menu').toggle();
// 			if($('#report').hasClass("open")){
// 				$('#report').removeClass("open");
// 			} else {
// 				$('#report').addClass("open");
// 			}
// 		});
// 	  	$("#market").click(function(){
// 			$('.market_sub-menu').toggle();
// 			if($('#market').hasClass("open")){
// 				$('#market').removeClass("open");
// 			} else {
// 				$('#market').addClass("open");
// 			}
// 		});
// 	  	$("#tools").click(function(){
// 			$('.tools_sub-menu').toggle();
// 			if($('#tools').hasClass("open")){
// 				$('#tools').removeClass("open");
// 			} else {
// 				$('#tools').addClass("open");
// 			}
// 		});
// 		$("#admin").click(function(){
// 			$('.admin_sub-menu').toggle();
// 			if($('#admin').hasClass("open")){
// 				$('#admin').removeClass("open");
// 			} else {
// 				$('#admin').addClass("open");
// 			}
// 		});
// 	});
//
// })
// function setAgents(){
// 	//set agents
//     $.ajax({
//           url : '/getTotalAgents',
//           type : 'POST',
//           dataType : 'json',
//           data : {},
//           success : function(data){
//               	$('#agentsel option').remove();
//               	$('#agentsel1 option').remove();
//               	$('#selectagent option').remove();
//               	$('#agentsel').append('<option value=0>* Select Agent</option>');
//               	$('#agentsel1').append('<option value=0>* Select Agent</option>');
//               	$('#selectagent').append('<option value=0>* Select Agent</option>');
//               	$.each(data,function(i,v){
//                   	$('#agentsel').append('<option value='+v.agentId+'>'+v.firstName+' '+v.lastName+'</option>');
//                   	$('#agentsel1').append('<option value='+v.agentId+'>'+v.firstName+' '+v.lastName+'</option>');
//                   	$('#selectagent').append('<option value='+v.agentId+'>'+v.firstName+' '+v.lastName+'</option>');
//               	});
//               	$('#agentsel').selectpicker('refresh');
//               	$('#agentsel1').selectpicker('refresh');
//               	$('#selectagent').selectpicker('refresh');
//           }
//     })
//
// }
// function setLenders(){
// 	//set lenders
//     $.ajax({
//           url : '/getTotalLenders',
//           type : 'POST',
//           dataType : 'json',
//           data : {},
//           success : function(data){
//               	$('#lendersel option').remove();
//               	$('#lenderselect option').remove();
//               	$('#lendersel').append('<option value=0>Select Lender</option>');
//               	$('#lenderselect').append('<option value=0>Select Lender</option>');
//               	$.each(data,function(i,v){
//                 	$('#lendersel').append('<option value='+v.lenderId+'>'+v.firstName+' '+v.lastName+'</option>');
//                 	$('#lenderselect').append('<option value='+v.lenderId+'>'+v.firstName+' '+v.lastName+'</option>');
//              	});
//                	$('#lendersel').selectpicker('refresh');
//                	$('#lenderselect').selectpicker('refresh');
//           }
//     })
// }

function toTitleCase(str) {
	return str.replace(/\w\S*/g, function (txt) {
    	return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

function priceFormat(price){
	if(price){
		var LP = price.toString();
		if(LP.length>=4){
			LP = LP.replace(/(\d)(?=(\d{3})+$)/g, '$1,');
		}
	} else {
		var LP = 0;
	}
	return LP;
}

function priceReduce(a,b){

	if(a!=''){
		if(parseInt(a)>parseInt(b)){
			var diff = parseInt(a)-parseInt(b);
        	var per = parseInt(diff)/parseInt(a)*100;
        	return {d:priceFormat(diff),p:per.toFixed(2)};
		} else {
			return {d:'',p:''};
		}
	} else {
		return {d:'',p:''};
	}
}

function checkInputAlpha(v,id){
	var email = v.trim();
	var pat = /^[A-Za-z\s]+$/;
	if(email.match(pat)){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter alphabets only", { className: "error", position:"bottom", });
		return false;
    }
}

function checkInputAlphaNum(v,id){
	var email = v.trim();
	var pat=/^[a-zA-Z]+[A-Za-z0-9\s]+$/;
	if(email.match(pat) && Number(email) != 0){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter alphanumerics only", { className: "error", position:"bottom", });
		return false;
    }
}


function checkInputEmail(v,id){
	var email = v.toLowerCase().trim();
	var pat=/^([a-zA-Z])+([a-zA-Z0-9_\.\-])+\@([a-zA-Z])+(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9\-]{2,4})$/g
	if(email.match(pat)){
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	} else {
		$('#'+id).val('').focus().notify("Enter correct email", { className: "error", position:"bottom" });
		return false;
    }
}

function checkInputPhNum(v,id){
	var d = v.trim();
	var pat=/^[0-9]+$/;
	if(d.charAt(1)==0 || !/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/.test(d)){
		$('#'+id).val('').focus().notify("Enter correct phone no", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
	}
}
function checkInputZip(v,id){
	var email = v.trim();
	var pat=/^[0-9]+$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0 || email.length!=5){
		$('#'+id).val('').focus().notify("Enter correct zip", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
    }
}

function checkInputSelect(v,id){

	if(v=='' || v=='0'){
		$('#'+id).focus().notify("Please select option", { className: "error", position:"bottom", });
		return false;
	} else {
		$('.notifyjs-wrapper').trigger('notify-hide');
		return true;
    }
}

function checkInputUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
	var pat = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
    }
}


function checkInputReq(v){
	var email = v.trim();
	if(email!=''){
		return true;
	} else {
		return false;
    }
}


function checkInputNum(v){
	var email = v.trim();
	var pat=/^[0-9]+$/;
	if(!email.match(pat) || Number(email) == 0 || email.charAt(0)==0){
		return false;
	} else {
		return true;
    }
}

function checkgoogleUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}plus\.google\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checkfbUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}facebook\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checktwitterUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}twitter\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checklinkedinUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}linkedin\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checkinstagramUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}instagram\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
function checkyoutubeUrl(v){
	var email = v.trim();
	//var pat = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;/^(https?:\/\/){0,1}(www\.){0,1}facebook\.com/
	var pat = new RegExp("^(https?:\/\/){0,1}(www\.){0,1}youtube\.com(/(.)*)?(\\?(.)*)?");
	if(email.match(pat)){
		return true;
	} else {
		return false;
	}
}
