var mysql = require('mysql');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
const config = require('config');
const DB_CONFIG = config.get('dbConfig');
const EMOVIES_CONFIG = config.get('appFamily').emovies;
const INVOICE_CONFIG = config.get('appFamily').invoices;

const CUSTOMERRAISETOKEN_CONFIG = config.get('appFamily').customercareraisetoken;
const CUSTOMERTICKETSBYBP_CONFIG = config.get('appFamily').customercaregetticketsbybp;
const CUSTOMERTICKETSALL_CONFIG = config.get('appFamily').customercaregetticketsall;
const CUSTOMERTICKETSTATUSUPDATE_CONFIG = config.get('appFamily').customercareticketstatusupdate;
var express = require('express')
var app = express()


var postSchema = new mongoose.Schema({

});
var DigitalGoldCustomerSchema = new mongoose.Schema({
  agent_id: "string",
  outletid: "string",
  name: "string",
  MobileNumber:"string",
  pincode:"string",
  dob:"string",
  Status:"string",
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var webhookshistorySchema = new mongoose.Schema({
  ipay_id:"string",
  agent_id: "string",
  opr_id: "string",
  status:"string",
  res_code: "string",
  res_msg: "string",
  optional_1: "string",
  optional_2: "string",
  optional_3: "string",
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var tokenhistorySchema = new mongoose.Schema({
  tokenid:"string",
  user: "string",
  statusfrom: "string",
  statusto:"string",
  status: "string",
  description: "string",
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var tokenstatusschema = new mongoose.Schema({
  user: "string",
  current_status: "string",
  next_status: "string",
  Status:"String",
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var ItDukaanordersschema = new mongoose.Schema({
   AgentId: "string",
   AgentName: "string",
   Products: [],
   TotalAmount: Number,
   Status: "string",
   created_at: { type: Date, default: Date.now },
   updated_at: { type: Date, default: Date.now },
});
var expresscartOrderschema = new mongoose.Schema({
  AgentId: "string",
  ProductId: "string",
  ProductName: "string",
  Products: [],
  Quantity: "string",
  Store: "string",
  CartValue: Number,
  Status: "string",
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var emovies = new mongoose.Schema({
  movie_id: "string",
  uniq: "string",
  genre_id: "string",
  movie_name: "string",
  seo_keywords: "string",
  seo_description: "string",
  Music: "string",
  description: "string",
  stars: "string",
  director: "string",
  trailer_id: "string",
  video_id: "string",
  image: "string",
  banner: "string",
  is_slide: "string",
  movie_length: "string",
  release_date: "string",
  language: "string",
  price: Number,
  industry:'string',
  status: "string",
  movie_views: "string",
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  metatitle: "String",
  downloadUrl: "string",
  thumbnailUrl:"string",
  fileName: "string",
  fileSize: "string",
  purchases:{ type: Array, default: [] },
  resolutions:{ type: Array, default: [] }
});
var hotelsBookingSchema = mongoose.Schema({
  agentId: "String",
  booking_identifier: "String",
  booking_modification_allowed: Boolean,
  booking_status_id: "String",
  checkin: "String",
  checkout: "String",
  country: "String",
  country_name: "String",
  currency_symbol: "String",
  external_reference_id: "String",
  TotalAmount: "String",
  final_amount: Number,
  get_payment_status_id: Number,
  gstn_enabled: Boolean,
  guest_email: "String",
  guest_name: "String",
  guest_phone: "String",
  id: Number,
  invoice: "String",
  no_of_guest: Number,
  roomCount: Number,
  status: "String",
  CancellationStatus: "String",
  Created_Date: { type: Date, default: Date.now },
  updated_Date: { type: Date, default: Date.now },
});

var hotelsBookingCancellationsSchema = mongoose.Schema({
  AgentId: "String",
  external_reference_id: "String",
  id: "String",
  status: "String",
  cancellation_charge: "String",
  guest_name: "String",
  guest_phone: "String",
  guest_email: "String",
  totalcancellation: "String",
  Status: "String",
  Created_Date: { type: Date, default: Date.now },
  updated_Date: { type: Date, default: Date.now },
});
var niharAdminSchema = mongoose.Schema({
  Name: "String",
  Email: "String",
  Mobile: "String",
  Password: "String",
  EmployeeType: "String",
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  Status: "String",
})
const addrlines = mongoose.Schema({
  careof:{type:String,required:true},
  location:{type:String,required:true},
  city:{type:String,required:true},
  state:{type:String,required:true},
  country:{type:String,required:true},
  pincode:{type:Number,required:true},
  created: { type: Date},
  Updated: { type: Date, default: Date.now },
  Status: { type: Number },
});
const CinnerAddressSchema = mongoose.Schema({
  cname:{type: String,required: true},
  AgentId: [{ type: String,required: true}],
  cmobile: [{ type: String,required: true}],
  BillingAddress: [{ type: addrlines,required: true}],
  ShippingAddress: [{ type: addrlines,required: true}],
  created: { type: Date, default: Date.now },
  Updated: { type: Date, default: Date.now },
  Status: { type: Number },
});
var CustomerServicesSchema = mongoose.Schema({
  AgentId: "string",
  AgentName: "string",
  AgentEmail: "string",
  Subject: "string",
  ServiceName: "String",
  TransactionId: "String",
  Message: "string",
  Status: "string",
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var DMTRemittersSchema = mongoose.Schema({
  AgentId: "string",
  AgentName: "string",
  MobileNumber: "string",
  PinCode: "string",
  RemittterId: "String",
  is_verified:"String",
  Status: "string",
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var DMTBeneficiarySchema = mongoose.Schema({
  AgentId: "string",
  RemitterId: "string",
  BenfId: "string",
  CustId: "string",
  Bnf_FirstName: "string",
  Bnf_LaststName: "string",
  Bnf_Mobile: "string",
  Bnf_Email: "string",
  Bnf_Bank: "string",
  Bnf_Branch: "string",
  Bnf_AccountNumber: "string",
  Bnf_IfscCode: "string",
  Otpverified: "String",
  Status: { type: Number, min: 1, max: 1 },
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

var DMTTransfersSchema = mongoose.Schema({
  AgentId: "string",
  CustId: "string",
  BnfcId: "string",
  Name: "string",
  AccountNumber: "string",
  Bank: "string",
  Branch: "string",
  Ifsc: "string",
  Amount: "string",
  PaymentType: "string",
  ReferenceNumber: "String",
  Status: { type: Number, min: 1, max: 1 },
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var DMTCustomersSchema = mongoose.Schema({
  AgentId: "string",
  Cust_FirstName: "string",
  Cust_LastName: "string",
  Cust_Mobile: "string",
  Cust_Email: "string",
  Cust_Address: "string",
  Cust_city: "string",
  Cust_state: "string",
  Cust_pincode: "string",
  Status: { type: Number, min: 1, max: 1 },
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var add_aggregatorschema = mongoose.Schema({
  name: 'string',
  code: 'string',
  Status: 'String',
  Registered_Date: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var NiharMessagesSchema = mongoose.Schema({
  ToAgentId: "string",
  Subject: "string",
  Message: "string",
  Status: "string",
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var AgentSchema = mongoose.Schema({
  UserId:'string',
  UserName: 'string',
  email: 'string',
  password: 'string',
  Mobile: 'string',
  HNo: 'string',
  Street: 'string',
  Village: 'string',
  City: 'string',
  State: 'string',
  PinCode: 'string',
  RemitterId: 'string',
  PanNumber: 'string',
  GSTNumberDigitalGold: 'string',
  PANCard: "string",
  Aadhar: "string",
  Photo: "string",
  Shipping_DoorNumber: "string",
  Shipping_Street: "string",
  Shipping_City: "string",
  Shipping_DigitalGoldState: "string",
  Shipping_PinCode: "string",
  Billing_DoorNumber: "string",
  Billing_Street: "string",
  Billing_City: "string",
  Billing_State: "string",
  Billing_PinCode: "string",
  Status: { type: Number },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});
var addsubservSchema = mongoose.Schema({
  aggregatorId: 'string',
  aggregatorName: 'string',
  aggregatorCode: 'string',
  servicesName: 'string',
  servicesId: 'string',
  serviceCode: 'string',
  sub_servicesName: 'string',
  subservicesCode: 'string',
  Aggregator_Margin: { type: Number },
  Margintype: 'string',
  Nihar_Margin: { type: Number },
  Agent_Margin: { type: Number },
  Icon: 'string',
  optional_1: 'string',
  optional_2: 'string',
  optional_3: 'string',
  Status: { type: Number },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

var addservSchema = mongoose.Schema({
  aggregatorId: 'string',
  aggregatorName: 'string',
  aggregatorCode: 'string',
  servicesName: 'string',
  serviceCode: 'string',
  Status: { type: Number },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});
var RTISchema = mongoose.Schema({
  AgentId: 'string',
  FirstName: 'string',
  LastName: 'string',
  Email: 'string',
  Mobile: 'string',
  Address: 'string',
  PinCode: 'string',
  Description: 'string',
  RTIStatus: 'string',
  ApplicationId: 'string',
  Payment: 'string',
  Status: { type: Number },
  createdAt: { type: Date, default: Date.now },
});
var PantokensSchema = mongoose.Schema({
  agentId: 'string',
  app_name: 'string',
  app_number: 'string',
  outletid: 'string',
  ipay_id: 'string',
  opr_id: 'string',
  account_no: 'string',
  trans_amt: 'string',
  charged_amt: 'string',
  datetime: 'string',
  resstatus: 'string',
  res_mesg: 'string',
  CreatedDate: { type: Date, default: Date.now },
  Status: { type: Number },
});
var PanOutletSchema = mongoose.Schema({
  agentId: 'string',
  PanNumber: 'string',
  OutLetId: 'string',
  utilodinid: 'string',
  Updated: { type: Date, default: Date.now },
  Status: { type: Number },
});
var btobewalletSchema = mongoose.Schema({
  agentId: 'string',
  agentName: 'string',
  agentEmail:'string',
  Amount:{ type: Number },
  BankReferenceId: 'string',
  RbiReferenceId: 'string',
  ReferenceName: 'string',
  ReferenceMobile: 'string',
  DateofTransaction: 'string',
  Status: 'string',
  optional_1: 'string',
  optional_2: 'string',
  optional_3: 'string',
  created: { type: Date, default: Date.now },
  Updated: { type: Date, default: Date.now },
});
var RechargeSchema = mongoose.Schema({
  agentId: 'string',
  agentName: 'string',
  orderid: 'string',
  operatorid: 'string',
  MobileNumber: 'string',
  servicekey: 'string',
  TransactionAmount: { type: Number },
  ChargedAmount: { type: Number },
  RechargeTime: 'string',
  Status: 'string',
  Responcecode: 'string',
  responsemsg: 'string',
  CreateDate: { type: Date, default: Date.now },
});
var ele_Schema = mongoose.Schema({
  agentId: 'string',
  orderNum: 'string',
  agentName: 'string',
  customername: 'string',
  bill_number: 'string',
  service_number: 'string',
  subservicesCode: 'string',
  ChargedAmount: { type: Number },
  Status: 'string',
  statusdes: 'string',
  CreateDate: { type: Date, default: Date.now },
});
var Addm_Schema = mongoose.Schema({
  agentId: 'string',
  Enteredamount: 'string',
  Amount: 'string',
  TransactionID: 'string',
  status: 'string',
  Paymentfrom: 'string',
  mihpayid: 'String',
  CreateDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
var transitionSchema = mongoose.Schema({
  AgentId: 'string',
  bpname: 'string',
  Store:'string',
  AggregatorCode:'string',
  serviceCode:'string',
  subservicesCode:'string',
  Amount: { type: Number },
  discount:{ type: Number },
  bp_commission:{ type: Number },
  nihar_commission:{ type: Number },
  bp_earn:{ type: Number },
  nihar_earn:{ type: Number },
  TransactionDate: { type: Date, default: Date.now },
  TransactionStatus: 'string',
  pay_id:'string',
  servicesName:'string',
  checked:'string',
  Status: { type: Number },
});

var WalletSchema = mongoose.Schema({
  agentId: 'string',
  Wallet: { type: Number },
  Updated: { type: Date, default: Date.now },
  Status: { type: Number },
});

var EarningsSchema = mongoose.Schema({
  AgentId: 'string',
  Earning: { type: Number },
  Updated: { type: Date, default: Date.now },
  Status: { type: Number },
});

var Earningshistory = mongoose.Schema({
  AgentId: 'string',
  ServiceName: 'string',
  EarningAmount: { type: Number },
  OrderAmount: { type: Number },
  AgentWallet:{ type: Number },
  date: 'string',
  time: 'string',
  Updated: { type: Date, default: Date.now },
  Status: { type: Number },
});
var cancelschema = mongoose.Schema({
  AgentId: 'string',
  BookingID: 'string',
  cancelID:'string',
  statuscode: 'string',
  booking_status: 'string',
  rerund_to_agent: 'string',
  rerund_to_customer: 'string',
  RefundAmount: {type:Number},
  status: 'string',
  canceledAt: { type: Date, default: Date.now },
});
var bookingschema = mongoose.Schema({
  AgentId: 'string',
  BookingID: 'string',
  bookingstatus: 'string',
  Source: 'string',
  Destination: 'string',
  booking_status: 'string',
  Travels: 'string',
  Passenger: 'string',
  Pass_Email: 'string',
  Pass_Mobile: 'string',
  Fare: 'string',
  Date_jou: 'string',
  Seats: 'string',
  status: 'string',
  bookedAt: { type: Date, default: Date.now },
});
var hotelSchema = new mongoose.Schema({
  page: "String",
  id: "String",
  name: "String",
  small_address: { type: String, uppercase: true, lowercase: true },
  city: { type: String, uppercase: true, lowercase: true },
  state: { type: String, uppercase: true, lowercase: true },
  country: "String",
  zipcode: "String",
  description: "String",
  images: [],
  landing_url: "String",
  latitude: "String",
  longitude: "String",
  oyo_home: "String",
  amenities: {
    ComplimentaryVegBreakfast: Boolean,
    AC: Boolean,
    Geyser: Boolean,
    RoomHeater: Boolean,
    MiniFridge: Boolean,
    TV: Boolean,
    HairDryer: Boolean,
    BathTub: Boolean,
    SwimmingPool: Boolean,
    Kitchen: Boolean,
    LivingRoom: Boolean,
    ComplimentaryBreakfast: Boolean,
    InhouseRestaurant: Boolean,
    ParkingFacility: Boolean,
    CardPayment: Boolean,
    FreeWifi: Boolean,
    Powerbackup: Boolean,
    LiftElevator: Boolean,
    Conference_Room: Boolean,
    BanquetHall: Boolean,
    Gym: Boolean,
    WheelchairAccessible: Boolean,
    Laundry: Boolean,
    Bar: Boolean,
    Kindle: Boolean,
    Spa: Boolean,
    WellnessCentre: Boolean,
    Netflix: Boolean,
    PetFriendly: Boolean,
    CCTVCameras: Boolean,
    Dining_Area: Boolean,
    HDTV: Boolean,
    ElectricityChargesIncluded: Boolean,
    cricketstadium: Boolean,
    farzi: Boolean,
    PreBookMeals: Boolean
  },
  category: "String",
  policies: [],
  status: "String",
  optional_1: "String",
  optional_2: "String",
  optional_3: "String",
  updated_Date: { type: Date, default: Date.now },
});
var FSC_LCCsschema = mongoose.Schema({
  Owner: 'string',
  Code: 'string',
  Flight_Type: 'string',
  status: 'string',
  updatedAt: { type: Date, default: Date.now }
});
var flightschema = mongoose.Schema({
  AgentId: 'string',
  // Flight_Type: 'string',
  pnr: { type: Array, default: [] },
  flightDetails: { type: Array, default: [] },
  PassengerList: { type: Array, default: [] },
  Email: 'string',
  phone: 'string',
  Fare: 'string',
  status: 'string',
  bookedAt: { type: Date, default: Date.now }
});
var buyersSchema = mongoose.Schema({
  name: "string",
  phone: "string",
  purchases: [{
    isExpired: Boolean,
    _id: { type: mongoose.Schema.Types.ObjectId },
    movie: { type: mongoose.Schema.Types.ObjectId },
    movieName: 'string',
    securityCode: "string",
    agentId:{type: mongoose.Schema.Types.ObjectId},
    created: { type: Date, default: Date.now },
  }],
});
var webhookshistory = mongoose.model('webhookshistory',webhookshistorySchema)
var DigitalGoldCustomers = mongoose.model('digitalgoldcustomers',DigitalGoldCustomerSchema)
var earnings = mongoose.model('Earnings', EarningsSchema);
var earningshistory = mongoose.model('Earningshistory', Earningshistory);
var tokenhistorys = mongoose.model('tokenhistory', tokenhistorySchema);
var tokenstatussettings = mongoose.model('tokenstatussettings', tokenstatusschema);
var flightbookings = mongoose.model('flightbookings', flightschema, 'flightbookings');
var buyers = mongoose.model('buyers', buyersSchema);
var FSC_LCCs = mongoose.model('FSC_LCCs', FSC_LCCsschema, 'FSC_LCCs');
var hotelcancellations = mongoose.model('hotelsBookingcancellations', hotelsBookingCancellationsSchema);
var hotelsbookingdetails = mongoose.model('hotelsbookingdetails', hotelsBookingSchema);
var agentmessages = mongoose.model('agentmessages', postSchema);
var agents = mongoose.model('agents', AgentSchema);
var nihar_admins = mongoose.model('nihar_admins', niharAdminSchema);
var wallets = mongoose.model('wallets', WalletSchema);
var cancelbustickets = mongoose.model('cancelbustickets', cancelschema);
var transfers = mongoose.model('transfers', DMTTransfersSchema);
var transaction_reports = mongoose.model('transaction_reports', transitionSchema);
var sub_services = mongoose.model('sub_services', addsubservSchema);
var services = mongoose.model('services', addservSchema);
var rtis = mongoose.model('rtis', RTISchema);
var emovies = mongoose.model('emovies', emovies);
var rechargereports = mongoose.model('rechargereports', RechargeSchema);
var banktobankewallets = mongoose.model('banktobankewallets', btobewalletSchema);
var products = mongoose.model('products', postSchema);
var orderproducts = mongoose.model('orderproducts', postSchema);
var pantokens = mongoose.model('pantokens', PantokensSchema);
var panoutlets = mongoose.model('panoutlets', PanOutletSchema);
var CustAddress = mongoose.model('CustAddress', CinnerAddressSchema,'CustAddress');


var operators = mongoose.model('operators', postSchema);
var operatorroles = mongoose.model('operatorroles', postSchema);
var niharmsgs = mongoose.model('niharmsgs', NiharMessagesSchema);
var logins = mongoose.model('logins', postSchema);
var itdukaanorders = mongoose.model('itdukaanorders', ItDukaanordersschema);

var goldnsilverorders = mongoose.model('goldnsilverorders', postSchema);
var esubcategories = mongoose.model('esubcategories', postSchema);
var electricity_reports = mongoose.model('electricity_reports', ele_Schema);
var ecategories = mongoose.model('ecategories', postSchema);
var dmtremitters = mongoose.model('dmtremitters', DMTRemittersSchema);

var dmtcustomers = mongoose.model('dmtcustomers', DMTCustomersSchema);
var dmtbeneficiaries = mongoose.model('dmtbeneficiaries', DMTBeneficiarySchema);
var customerservices = mongoose.model('customerservices', CustomerServicesSchema);
var bustickets = mongoose.model('bustickets', bookingschema);
var aggregators = mongoose.model('aggregators', add_aggregatorschema);

var agentmsgs = mongoose.model('agentmsgs', postSchema);
var agent_history_table = mongoose.model('agent_history_table', postSchema);
var add_money_reports = mongoose.model('add_money_reports', Addm_Schema);
// var Nihar_Admins = mongoose.model('Nihar_Admins', postSchema);
var Dmt_Orders = mongoose.model('Dmt_Orders', postSchema);

var Agent_Dashboard_LoginDetails = mongoose.model('Agent_Dashboard_LoginDetails', postSchema);
var AgentWallet = mongoose.model('AgentWallet', postSchema);
var hoteldata = mongoose.model('hoteldata', hotelSchema);
var expressorders = mongoose.model('expressorders', expresscartOrderschema);
var emovies_latest = mongoose.model('expressorders', emovies_latest);

module.exports = {
  port: process.env.PORT || 8081,
  url: DB_CONFIG.url,
  token: DB_CONFIG.token,
  mongo: mongoose,
  emoviesurl: EMOVIES_CONFIG.domain,
  invoiceurl: INVOICE_CONFIG.domain,
  customercareraiseticketurl : CUSTOMERRAISETOKEN_CONFIG.url,
  customercaregetticketsbybp : CUSTOMERTICKETSBYBP_CONFIG.url,
  customercaregetticketsall : CUSTOMERTICKETSALL_CONFIG.url,
  customercareticketstatusupdate : CUSTOMERTICKETSTATUSUPDATE_CONFIG.url,
  cmsImgPath: DB_CONFIG.cmsImgPath,
  newsletters: '/app/Public2/images/Newsletters',
  // kycdocs: '/opt/Projects/NiharMarketEcenter/Public2/images/bpkyc',
  adminurl: 'https://www.niharecenter.com/',
  webhookshistory:webhookshistory,
  DigitalGoldCustomers:DigitalGoldCustomers,
  tokenhistory:tokenhistorys,
  FSC_LCCs: FSC_LCCs,
  agents: agents,
  earnings:earnings,
  earningshistory:earningshistory,
  //authtoken:authtoken,
  CustAddress:CustAddress,
  emovies_latest:emovies_latest,
  //Nihar_Admins : Nihar_Admins,
  expressorders: expressorders,
  buyers: buyers,
  emovies: emovies,
  hotelcancellations: hotelcancellations,
  hotelsbookingdetails: hotelsbookingdetails,
  agentmessages: agentmessages,
  nihar_admins: nihar_admins,
  wallets: wallets,
  transfers: transfers,
  transaction_reports: transaction_reports,
  sub_services: sub_services,
  services: services,
  rtis: rtis,
  rechargereports: rechargereports,
  banktobankewallets:banktobankewallets,
  products: products,
  pantokens: pantokens,
  panoutlets: panoutlets,
  orderproducts: orderproducts,
  operators: operators,
  operatorroles: operatorroles,
  niharmsgs: niharmsgs,
  logins: logins,
  itdukaanorders: itdukaanorders,
  goldnsilverorders: goldnsilverorders,
  esubcategories: esubcategories,
  electricity_reports: electricity_reports,
  ecategories: ecategories,
  dmtremitters: dmtremitters,
  dmtcustomers: dmtcustomers,
  dmtbeneficiaries: dmtbeneficiaries,
  customerservices: customerservices,
  bustickets: bustickets,
  aggregators: aggregators,
  agentmsgs: agentmsgs,
  agent_history_table: agent_history_table,
  add_money_reports: add_money_reports,
  Dmt_Orders: Dmt_Orders,
  Agent_Dashboard_LoginDetails: Agent_Dashboard_LoginDetails,
  AgentWallet: AgentWallet,
  cancelbustickets: cancelbustickets,
  hoteldata: hoteldata,
  flightbookings: flightbookings,
  tokenstatussettings:tokenstatussettings,
  hotel_bp_commision:0.80,
  hotel_nihar_commision:0.20,
  hotel_agg_commision:0.25,
  transporter11: nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'niharinfo321@gmail.com',
      pass: 'Nihar@321'
    }
  }),
  transporter: nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'customerservice@niharecenter.com',
      pass: 'Niharmarket@786'
    }
  })

}
