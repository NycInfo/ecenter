// this file is to get offers in particular flight
const crypto = require('crypto');
const hart= require('../hart');
const utlity = require('./utlity');
const moment = require('moment');
// const session = require('express-session');
const mongoose = require('mongoose');
const mongoXlsx = require('mongo-xlsx');
const url = hart.url;
const request = require('request');
const http = require("http");
const async=require("async");
const ObjectId = require('mongodb').ObjectID;
const fs = require('fs');
const xmlReader = require('read-xml');
// const parser = require('xml2json');
const parseString = require('xml2js').parseString;
const flightservices = require('./flightservices');
const AirlineTechAPI_Endpoint = 'http://atip.airtechapi.com/airtecht/callApi';
const XmlType = 'application/xml';
const AirlineTechToken = 'Basic bXVydGh5a25AbmloYXJpbmZvLmNvbTptdXJ0aHlrbiU3LW5paGFyITIzNA==';
const refver = "1.0";
const ndc_name = "NDC GATEWAY";
const pname = "Test sender";
const pemail = "airlinestechnology@tc.com";
const psucity = "AH7H";
const agncid = "test";
const origindkey1 = "OD1";
const origindkey2 = "OD2";
const aid = "AI";
const psngrtype = "ADT";
const psngrtype1 = "CHD";
const psngrtype2 = "INF";
const txnident = "894b10e5-1017-4fc0-a887-f61513be";
const remarks = ["CASH"];
const paymentType = "0";
const offerversion = "17.2";
const offertoken ="b9888df6-17c0-4026-8";
const surname_init = "";
const iatanumber = "35201165";
const xmlParser = require('xmldom').DOMParser;
var parser_xml = new xmlParser();
module.exports = function(app){
// app.use(session({
//     secret: 'I9Admin-Flag',
//     resave: true,
//     saveUninitialized: true,
//     name : 'I9Admin',
// }));

//API for offers, returns offers of particular flight
app.post('/getoffers',function(req,res){
      console.log(req.body);

      var body1 = flightservices.OfferPriceRQTag(offerversion,offertoken);
      body1 += flightservices.DocumentTag_offer();
      body1 += flightservices.partytagPseudoCity(pname,pemail,iatanumber,agncid,psucity);
      body1 += flightservices.QueryStartTag();
      //checking trip type
      if (req.body.triptype=="AirshoppingRQ_round") {
        //condition for bundle type
        if (req.body.bundled=='bundled') {

          body1 += flightservices.offerTag(req.body.onwards[0].OfferID,req.body.onwards[0].Owner,req.body.onwards[0].ResponseID,'','');
          // loop for in onwards trip
          for (var i = 0; i < req.body.onwards[0].OfferItemID.length; i++) {
            body1 +=flightservices.offerItemTag(req.body.onwards[0].OfferItemID[i].OfferItemID,req.body.onwards[0].OfferItemID[i].pass_ref);
          }
          body1+=flightservices.offerEndTag();
          //else condition for bundle type
        }else {
          body1 += flightservices.offerTag(req.body.inreturn[0].OfferID,req.body.inreturn[0].Owner,req.body.inreturn[0].ResponseID,'','');
          // loop for in return trip
          for (var i = 0; i < req.body.inreturn[0].OfferItemID.length; i++) {
            body1 +=flightservices.offerItemTag(req.body.inreturn[0].OfferItemID[i].OfferItemID,req.body.inreturn[0].OfferItemID[i].pass_ref);
          }
          body1+=flightservices.offerEndTag();
          body1 += flightservices.offerTag(req.body.onwards[0].OfferID,req.body.onwards[0].Owner,req.body.onwards[0].ResponseID,'','');

          for (var i = 0; i < req.body.onwards[0].OfferItemID.length; i++) {
            body1 +=flightservices.offerItemTag(req.body.onwards[0].OfferItemID[i].OfferItemID,req.body.onwards[0].OfferItemID[i].pass_ref);
          }
          body1+=flightservices.offerEndTag();
        }
        //else condition for trip
      }else {
          body1 += flightservices.offerTag(req.body.onwards[0].OfferID,req.body.onwards[0].Owner,req.body.onwards[0].ResponseID,'','');
          // loop for onwards trip
          for (var i = 0; i < req.body.onwards[0].OfferItemID.length; i++) {
            body1 +=flightservices.offerItemTag(req.body.onwards[0].OfferItemID[i].OfferItemID,req.body.onwards[0].OfferItemID[i].pass_ref);
          }
          body1+=flightservices.offerEndTag();
      }
      body1 += flightservices.QueryEndTag();
      body1 += flightservices.DataListsStartTag();

      // loop for number of adults
      for (var i = 0; i < req.body.adt.length; i++) {
        body1 += flightservices.Offer_new_PassengerTag(req.body.adt[i].ID,req.body.adt[i].type,req.body.adt[i].date,req.body.adt[i].title,req.body.adt[i].fname,req.body.adt[i].lname);
      }
      body1 += flightservices.DataListsEndTag();
      body1+= flightservices.offerpriceRQEndTag();
      // object of parameters for API call
      fs.writeFile('OfferRq.xml', body1, function (err) {
        if (err) throw err;
         console.log('Saved!');
       });
    var options = {
      // method type of API callApi
       method: 'POST',
    // URL of API
      url: 'http://atip.airtechapi.com/airtecht/callApi',
      // headers of API call
      headers:
      //declaring Content-type as xml
       { 'Content-Type': 'application/xml',
         Authorization: 'Basic bXVydGh5a25AbmloYXJpbmZvLmNvbTptdXJ0aHlrbiU3LW5paGFyITIzNA=='
       },
       // sending request to API call
         body: body1,
       };
       // request call for API response by passing object of parameters
    request(options, function (error, response, body) {
      //condition for error
      if (error){
        console.log(error);
        // sending response to client side
        res.json('err');
        // if not error
      }else {
        fs.writeFile('OfferRs.xml', body, function (err) {
          if (err) throw err;
           console.log('Saved!');
         });
        var xmlDoc = parser_xml.parseFromString(body,"text/xml");
        // condition that response is success or fail
          if (xmlDoc.getElementsByTagName("DataLists").length !== 0 ) {
            parseString(body, function (err, s) {
              if (err) {
                console.log(err);
                // sending error response to client
                res.json('err');
              }else {
                //sending response to client
                res.json(s);
              }
            });
          // condition that response is success or fail
          }else {
            // sending error response to client
            res.json('err');
          }
      }
    });
 })
}
