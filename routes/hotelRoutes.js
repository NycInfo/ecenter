var crypto = require('crypto');
var hart= require('../hart');
var utlity = require('./utlity');
var moment = require('moment');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var token = hart.token;
var request = require('request');
var http = require("http");
var ObjectId = require('mongodb').ObjectID;
var request = require("request");
const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss');
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";
const findwalletbal_err = "Error In Find Wallet Balance";
const errorinapireq_err = "Error In Requesting API";
const transactiion_suc_msg = "Transaction Success";
const transactiion_fail_msg = "Transaction Fail";
const Insufficientbalance = "In Insufficient Balance";
const sessionout = "Session Out";
const service_name =  "Hotel Booking";
module.exports = function(app){

app.post('/hotellistsss',function(req,res){
  var options = { method: 'GET',
    //url: 'http://affiliate-staging.ap-southeast-1.elasticbeanstalk.com/api/v2/hotels/listing?no_of_entry=50&page=1',
    url: 'https://api.oyorooms.com/api/v2/hotels/listing?no_of_entry=50&page=2',
    headers:
     { 'Postman-Token': '16ffd946-ebc0-463b-8df3-4f7d5caae59c',
       'Cache-Control': 'no-cache',
       'Content-Type': 'application/json',
       //'ACCESS-TOKEN': 'NWtzMVhzNTlKeHZ1eGFtbm5zdHk6TVdIVHNGa3huRE1nZmRka0tLeGQ=' } };  //Test Token
       'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' } };

      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        var doc1 = JSON.parse(body);
        res.json(doc1);
        var Hoteldata = hart.hoteldata;
           Hoteldata.findOne({"page" :doc1.page},function(err,doc12){
             if(err){
               console.log(err);
             }
               if(doc12 == null){
                 var vaaa = "Complimentary Veg Breakfast";
                 for (var i= 0; i < doc1.hotels.length; i++) {
                // console.log(i);
                 var hotel = new Hoteldata({
                   page:doc1.page,
                   id:doc1.hotels[i].id,
                   name:doc1.hotels[i].name,
                   small_address:doc1.hotels[i].small_address,
                   city:doc1.hotels[i].city,
                   state:doc1.hotels[i].state,
                   country:doc1.hotels[i].country,
                   zipcode:doc1.hotels[i].zipcode,
                   description:doc1.hotels[i].description,
                   images:doc1.hotels[i].images,
                   landing_url:doc1.hotels[i].landing_url,
                   latitude:doc1.hotels[i].latitude,
                   longitude:doc1.hotels[i].longitude,
                   oyo_home:doc1.hotels[i].oyo_home,
                   amenities:{
                     ComplimentaryVegBreakfast:doc1.hotels[i].amenities["Complimentary Veg Breakfast"],
                     AC:doc1.hotels[i].amenities.AC,
                     Geyser: doc1.hotels[i].amenities.Geyser,
                     RoomHeater:doc1.hotels[i].amenities["Room Heater"],
                     MiniFridge :doc1.hotels[i].amenities["Mini Fridge"],
                     TV:doc1.hotels[i].amenities.TV,
                     HairDryer:doc1.hotels[i].amenities["Hair Dryer"],
                     BathTub:doc1.hotels[i].amenities["Bath Tub"],
                     SwimmingPool:doc1.hotels[i].amenities["Swimming Pool"],
                     Kitchen:doc1.hotels[i].amenities.Kitchen,
                     LivingRoom:doc1.hotels[i].amenities["Living Room"],
                     ComplimentaryBreakfast:doc1.hotels[i].amenities["Complimentary Breakfast"],
                     Inhouse_Restaurant:doc1.hotels[i].amenities["In house Restaurant"],
                     ParkingFacility:doc1.hotels[i].amenities["Parking Facility"],
                     CardPayment:doc1.hotels[i].amenities["Card Payment"],
                     FreeWifi: doc1.hotels[i].amenities["Free Wifi"],
                     Powerbackup:doc1.hotels[i].amenities["Power backup"],
                     LiftElevator:doc1.hotels[i].amenities["Lift Elevator"],
                     ConferenceRoom:doc1.hotels[i].amenities["Conference Room"],
                     BanquetHall:doc1.hotels[i].amenities["Banquet Hall"],
                     Gym:doc1.hotels[i].amenities.Gym,
                     WheelchairAccessible:doc1.hotels[i].amenities["Wheelchair Accessible"],
                     Laundry:doc1.hotels[i].amenities.Laundry,
                     Bar:doc1.hotels[i].amenities.Bar,
                     Kindle:doc1.hotels[i].amenities.Kindle,
                     Spa:doc1.hotels[i].amenities.Spa,
                     WellnessCentre:doc1.hotels[i].amenities["Wellness Centre"],
                     Netflix:doc1.hotels[i].amenities.Netflix,
                     PetFriendly:doc1.hotels[i].amenities["Pet Friendly"],
                     CCTVCameras:doc1.hotels[i].amenities["CCTV Cameras"],
                     DiningArea:doc1.hotels[i].amenities["Dining Area"],
                     HDTV:doc1.hotels[i].amenities.HDTV,
                     ElectricityChargesIncluded:doc1.hotels[i].amenities["Electricity Charges Included"],
                     cricketstadium:doc1.hotels[i].amenities["cricket stadium"],
                     farzi:doc1.hotels[i].amenities.farzi,
                     PreBookMeals:doc1.hotels[i].amenities["Pre Book Meals"]},
                     category:doc1.hotels[i].category,
                     policies:doc1.hotels[i].policies,
                     status:1,
                 });
                 hotel.save(function(err,results) {
                   console.log("Saved ")
                 });
               }
             }
             });

      });
})

app.post('/GetHotelsData',function(req,res){
     var Hoteldata = hart.hoteldata;
     Hoteldata.find({}).exec(function(err,data) {
       res.json(data);
     });
});

app.get('/search_hotel', function(req, res) {
    var regex = new RegExp(req.query["term"], 'i');
    var Hoteldata  = hart.hoteldata;
    var query = Hoteldata.find({$or : [ {small_address : regex},{ 'small_address': 1 },{city: regex},{ 'city': 1 },{state:regex},{ 'state': 1 }]});
  //var query = Hoteldata.find({small_address: regex},{ 'small_address': 1 },{city: regex},{ 'city': 1 },).limit(1);
   query.exec(function(err, users) {
       if (!err){
          var result = users;
          res.send(result, {
             'Content-Type': 'application/json'
          }, 200);
       } else {
          res.send(JSON.stringify(err), {
             'Content-Type': 'application/json'
          }, 404);
       }
    });
});

app.get('/search_hotelAddress', function(req, res) {
    var regex = new RegExp(req.query["term"], 'i');
    var Hoteldata  = hart.hoteldata;
  //  var query = Hoteldata.find({$or : [ {city : regex},{ 'city': 1 },{"state" : regex},{ 'state': 1 },{"country":regex},{ 'country': 1 },{"small_address":regex},{ 'small_address': 1 }]});
  var query = Hoteldata.find({small_address: regex},{ 'small_address': 1 });
   query.exec(function(err, users) {
       if (!err){
          var result = users;
          res.send(result, {
             'Content-Type': 'application/json'
          }, 200);
       } else {
          res.send(JSON.stringify(err), {
             'Content-Type': 'application/json'
          }, 404);
       }
    });
});

app.post('/hotelsdatawithcity',function(req,res){
     var Hoteldata = hart.hoteldata;
     var oyotype = {};
     if(req.body.oyotype != ""){
       oyotype = { category: { $in: req.body.oyotype }}
     }
     var Query = { "$and": [{"city": req.body.city},oyotype] };
       Hoteldata.find(Query).sort({_id:-1}).exec(function(err,doc1){
         if(err){
           console.log(err);
         }else{
           console.log(doc1.length);
           res.json(doc1);
         }
     });
});

app.post('/hotelfulldetails',function(req,res){
     var Hoteldata = hart.hoteldata;
     Hoteldata.findOne({ id : req.body.hotelid },function(err,data){
       res.json(data);
     });
});


app.post('/hotelavailability',function(req,res){
  var options = { method: 'POST',
    url: 'https://api.oyorooms.com/api/v2/hotels/get_availability',
    qs: { cp: 'true' },
    headers:
     { 'Postman-Token': '1b24b7eb-acb3-4956-bb7d-61df6bcd1d1d',
       'Cache-Control': 'no-cache',
       'Content-Type': 'application/json',
       'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' },
    body:
     { HotelAvailability:
        { HotelID: [ req.body.hotelid ],
          City: '',
          checkInDate: req.body.checkin,
          checkOutDate: req.body.checkout,
          adults: parseInt(req.body.adults),
          children: parseInt(req.body.children),
          child_1_age: 0,
          child_2_age: 0,
          rooms: parseInt(req.body.numberofrooms) } },
    json: true };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.json(body);
  });
});


app.post('/hotelbooking',function(req,res){
  try{
  utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,ser_sart_msg,"Customer Number = "+req.body.mobilenumber+" ");
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,service_name,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
    var collection = hart.wallets;
    collection.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,findwalletbal_err+err);
        res.json("Unable to process your request");
        return false;;
      }
      else{
        var balance = data.Wallet;
        var Amount = req.body.totalamount   //for Prepaid
        //var Amount = 25;
        if(balance > Amount){
          var options = { method: 'POST',
            //url: 'http://affiliate-staging.ap-southeast-1.elasticbeanstalk.com/api/v2/vendor/bookings',
            url: 'https://api.oyorooms.com/api/v2/vendor/bookings',
            headers:
             { 'Postman-Token': '87cf3113-7817-4ccd-bfc0-ddfc4ba6d4de',
               'Cache-Control': 'no-cache',
               'Content-Type': 'application/json',
               'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' },
            body:
            { guest:
               { first_name: req.body.firstname,
                 last_name: req.body.lastname,
                 email: req.body.emailid,
                 country_code: '+91',
                 phone: req.body.mobilenumber },
              booking:
               { single: 0,
                 double: parseInt(req.body.DoubleRoom),
                 extra: 0,
                 checkin: req.body.checkin,
                 checkout: req.body.checkout,
                 hotel_id: req.body.hotelid,
                 is_provisional: 'true',
                 external_reference_id: req.body.orderid },
                 payments:
                 { bill_to_affiliate : "False"} },
            json: true };
          request(options, function (error, response, body) {
              if(error){
                utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,errorinapireq_err+error);
                res.json("Unable to process your request");
                return false;
              }
              if(body.status == "Saved"){
                res.json(body);
              }else{
                res.json("Unable to process your request..");
                return false;
              }
          });
          }else {
            utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,Insufficientbalance);
            res.json("0")
          }
      }
     })
   }
   catch (err) {
     utlity.loggermessage('Unknown error in hotel booking - hotel API:', err);
     return (err, { statusCode: '9999' });
   }
});

app.post('/hotelbooking_update',function(req,res){
    var collection = hart.wallets;
    collection.findOne({ agentId : req.session.id1 },function(err,data){
    var options = { method: 'POST',
      url: 'https://api.oyorooms.com/api/v2/vendor/bookings/'+req.body.id+'/update',
      qs: { cp: 'true' },
      headers:
       { 'Content-Type': 'application/json',
         'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' },
      body:
      {
        "booking": {
          "status": "Confirm Booking",
          "external_reference_id": req.body.external_reference_id
         },
         "payments": {
            "bill_to_affiliate": "true"
         }
      },
      json: true };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
      if (body.status == "Confirm Booking") {
        savehotelreports(req.session.id1,
          body.booking_identifier,
          body.booking_modification_allowed,
          body.booking_status_id,
          body.checkin,
          body.checkout,
          body.country,
          body.country_name,
          body.currency_symbol,
          req.body.external_reference_id,
          req.body.Amount,
          body.minimum_prepay_amount,
          body.get_payment_status_id,
          body.gstn_enabled,
          body.guest_email,
          body.guest_name,
          body.guest_phone,
          body.id,
          body.invoice,
          body.is_modifiable,
          body.no_of_guest,
          body.roomCount,
          "1");
          var NiharPrice = body.minimum_prepay_amount              //Update this amount in Wallets
          var NiharPercentage = body.minimum_prepay_amount*hart.hotel_agg_commision; //Total commision : 0.25
          var bpp = NiharPercentage*hart.hotel_bp_commision;  //Bp Margin:0.80              //Update bp commision in
          var nmp = NiharPercentage*hart.hotel_nihar_commision;  //Nihar Margin:0.20           //Update nihar commision
          var balance = data.Wallet;
          var Amount = body.minimum_prepay_amount;
          var UpdateWallet = parseInt(balance) - parseInt(Amount);
          utlity.transactions(req.session.id1,req.session.username,'ecenter','OYO','ROOMS','ROOMS',UpdateWallet,NiharPrice,NiharPercentage,hart.hotel_bp_commision,hart.hotel_nihar_commision,bpp,nmp,"Success",body.booking_identifier,'Hotels');
        res.json(body);
      }else {
        res.json("Booking Failed")
      }
    });
  })
})

function hotelbookingmail(){
  var mailOptions = {
    from: 'customerservice@niharecenter.com',
    to: email,
    subject: "Hotel Booking Confirmation",
    html: "<h4>Hotel Booking Confirmation</h4>"
  };
  hart.transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}


function savehotelreports(agentId,booking_identifier,booking_modification_allowed,booking_status_id,checkin,checkout,country,country_name,currency_symbol,external_reference_id,totalamount,final_amount,get_payment_status_id,gstn_enabled,guest_email,guest_name,guest_phone,id,invoice,is_modifiable,no_of_guest,roomCount,status){
  var hotelBooking = hart.hotelsbookingdetails;
   var hotel = new hotelBooking({
     agentId :agentId,
     booking_identifier : booking_identifier,
     booking_modification_allowed : booking_modification_allowed,
     booking_status_id :booking_status_id,
     checkin : checkin,
     checkout : checkout,
     country : country,
     country_name : country_name,
     currency_symbol : currency_symbol,
     external_reference_id : external_reference_id,
     TotalAmount:totalamount,
     final_amount : final_amount,
     get_payment_status_id :get_payment_status_id,
     gstn_enabled :gstn_enabled,
     guest_email : guest_email,
     guest_name : guest_name,
     guest_phone : guest_phone,
     id : id,
     invoice : invoice,
     is_modifiable : is_modifiable,
     no_of_guest : no_of_guest,
     roomCount : roomCount,
     status:status,
     CancellationStatus:"",
   });
   hotel.save(function(err,results) {
     if(err){
       console.log(err);
     }else{
       console.log(results);
     }
   });
}



app.post('/getcancellationcharges',function(req,res){
  var options = { method: 'GET',
    url: 'https://api.oyorooms.com/api/v2/vendor/bookings/'+req.body.id+'/cancellation_charge',
    headers:
     { 'Postman-Token': '0618fa47-5bcd-4359-8c27-0a1da6b9197e',
       'Cache-Control': 'no-cache',
       'Content-Type': 'application/json',
       'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' } };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      var dd = JSON.parse(body);
      var calcelcharges = parseInt(dd.applicable_cancellation_charge) + 20
      res.json(calcelcharges);
    });
})

  app.post('/ConfirmCancel',function(req,res){
    var collection = hart.wallets;
    collection.findOne({ agentId : req.session.Agentid },function(err,data){
      if(err){
        console.log(err);
      }
      else{
        var balance = data.Wallet;
        var options = { method: 'POST',
          url: 'https://api.oyorooms.com/api/v2/vendor/bookings/'+req.body.id+'/update',
          headers:
           { 'Postman-Token': '47af1308-6807-450f-b07f-7917f3137341',
             'Cache-Control': 'no-cache',
             'Content-Type': 'application/json',
             'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' },
          body: { booking: { status: 'Cancelled Booking' } },
          json: true };
        request(options, function (error, response, body) {
          if (error) throw new Error(error);
          if(body.status == 'Cancelled Booking'){
            res.json("1");
            // var updamnt = parseInt(data.Wallet)+parseInt(req.body.totalcancellation);  //for prepaid
            var updamnt = parseInt(data.Wallet)+15;

            utlity.transactions(req.session.Agentid,"Hotel Cancellation",updamnt,25,15,0,"Success",0);

            savehotelcancellations(req.session.Agentid,req.body.id,body.id,body.status,body.cancellation_charge,body.guest_name,body.guest_phone,body.guest_email,req.body.totalcancellation);
            utlity.sendmessages(body.guest_phone,"Dear, "+body.guest_name+" Your Hotel Booking Order "+body.id+" is cancelled..");
          }else{
            console.log(body);
            res.json(body.error.message);
          }
        });
      }
    })
    })

    function savehotelcancellations(agentid,refid,id,status,cancellation_charge,guest_name,guest_phone,guest_email,totalcancellation)
    {
      hart.hotelsbookingdetails.updateOne({ 'id': refid },{ $set:{ CancellationStatus: "Cancelled" }}, function(err, response) {
      if(err){
        console.log(err);
      }
      else{
        var HotelCancellations = hart.hotelcancellations;
          var hotelbookingcancels = new HotelCancellations({
            AgentId:agentid,
            external_reference_id:refid,
            id:id,
            status:status,
            cancellation_charge:cancellation_charge,
            guest_name:guest_name,
            guest_phone:guest_phone,
            guest_email:guest_email,
            totalcancellation:totalcancellation,
            Status:"1",
          });
          hotelbookingcancels.save(function (err, results) {
            if(err){

            }
            else{

            }
          });
      }
      });
    }
}
