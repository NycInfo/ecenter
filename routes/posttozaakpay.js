var express = require('express');
var checksum = require('../lib/checksum');
var router = express.Router();
var auth1 = function(req, res, next) {
  if (req.session && req.session.user != "" && req.session.admin1)
    return next();
  else
    return res.redirect('/');
  //  return res.status(401).sendFile('/home/narayanamurthy/Desktop/eCenterNew/Public2/images/401.jpeg');
};
router.get('/transact',auth1,function(req,res,next) {
  res.render('test_merchant_input');
});

router.post('/transact', function(req, res, next) {

  var checksumstring = checksum.getChecksumString(req.body);
  console.log("checksum string:"+checksumstring);
  var calculatedchecksum = checksum.calculateChecksum(checksumstring);
  console.log("checksum "+calculatedchecksum);
  res.render('transact', {
        data: req.body,
        checksum: calculatedchecksum
    });
  res.end();
});


//router.post('/payutransact', function(req, res, next) {
  // var checksumstring = checksum.getChecksumString(req.body);
  // console.log("checksum string:"+checksumstring);
  // var calculatedchecksum = checksum.calculateChecksum(checksumstring);
  // console.log("checksum "+calculatedchecksum);
  // res.render('transact', {
  //       data: req.body,
  //       checksum: calculatedchecksum
  //   });
  // res.end();
//});

module.exports = router;
