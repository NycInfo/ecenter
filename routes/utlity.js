var crypto = require('crypto');
var hart= require('../hart');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var request = require('request');
var http = require("http");     //For Otp service
var sha512 = require('sha512');
var btoa = require('btoa');
var ObjectId = require('mongodb').ObjectID;
var execPhp = require('exec-php');
var sha512 = require('js-sha512');
var fs = require('fs');

var Transactions = hart.transaction_reports;
module.exports={
  transactions:function(Agentid,BPname,store,AggregatorCode,serviceCode,subservicesCode,updatewallet,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,trans_status,pay_id,servicesName){

  // transactions:function(Agentid,service,updatewallet,Amount,agent_commission,nihar_com,trans_status,NCom_amount){
            if (updatewallet!==undefined) {
              var wallets = hart.wallets;
              wallets.updateOne({'agentId':Agentid },{ $set:{ Wallet: updatewallet }}, function(err, response) {
                if(err){
                  console.log(err);
                }
                else{
                  console.log("Wallet Updated");
                  return "updated";
                }
              });
            }
            var tsreports = new Transactions({
              AgentId:Agentid,
              bpname:BPname,
              Store:store,
              AggregatorCode:AggregatorCode,
              serviceCode:serviceCode,
              subservicesCode:subservicesCode,
              Amount:Amount,
              discount:discount,
              bp_commission:bp_commission,
              nihar_commission:nihar_commission,
              bp_earn:bp_earn,
              nihar_earn:nihar_earn,
              TransactionStatus:trans_status,
              pay_id:pay_id,
              servicesName:servicesName,
              checked:'No',
              Status:1,
            });
            tsreports.save(function (err, results) {
              if(err){
                console.log(err);
              }else{
                console.log("transactions updated");
              }
            });
      },

     Commissionfun:function(networkcode,callback)
      {
        var sub_services = hart.sub_services;
        sub_services.findOne({$and:[{subservicesCode : networkcode},{Status : 1}]},function(err,comm){
          if(err){
            console.log(err);
          }else {
          // Agg_margin=comm.Agent_Margin;
          // Ni_mar=comm.Nihar_Margin;
          // var agent_commission=Agg_margin/100;
          // var nihar_com=Ni_mar/100; //sai
          callback(comm);
          }
        });
    },

    walletloading:function(sessionid,deductablebal){
      var wallets = hart.wallets;
      var amount = parseInt(deductablebal);
      wallets.findOne({ agentId : sessionid },function(err,data){
        console.log("My Wallet:"+data.Wallet);
        if(!err){
          if(data.Wallet < amount){
            return "Insufficient Balance";
          }else{
            var TotalAMount = data.Wallet - amount;
            wallets.updateOne({ 'agentId':sessionid },{ $set:{ Wallet: TotalAMount }}, function(err, response) {
              if(!err){
                console.log("wallet updated");
                return 200;
              }else{
                console.log("Wallet Not Updated");
                return 500;
              }
            });
          }
        }else{
          return err;
        }
       });
    },
    updatewallet:function(sessionid,updatewallet){
        var wallets = hart.wallets;
        wallets.updateOne({ 'agentId':sessionid },{ $set:{ Wallet: updatewallet }}, function(err, response) {
        if(err){
          console.log(err);
        }
        else{
          console.log("Wallet Updated");
          return "updated";
          //res.json(1);
        }
        });
    },
    toTitleCase:function(str)
    {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    },
    timeSince:function(date) {
        var dt = new Date(date);
        var seconds = Math.floor((new Date() - dt) / 1000);

        var interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours";
        }
        interval = Math.floor(seconds / 60);
        if (interval >= 1) {
            return interval + " minutes";
        }
        return Math.floor(seconds) + " seconds";

    },
    timeFormat: function(date){
        var dt = new Date(date);
        var hours = dt.getHours();
        var minutes = dt.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    },
    dateFormat: function(date){
        var dt = new Date(date);
        var d = (dt.getDate()<10)?'0'+dt.getDate():dt.getDate();
        var m = dt.getMonth();
        if(m==0){
            m = 'Jan';
        } else if(m==1){
            m = 'Feb';
        } else if(m==2){
            m = 'Mar';
        } else if(m==3){
            m = 'Apr';
        } else if(m==4){
            m = 'May';
        } else if(m==5){
            m = 'Jun';
        } else if(m==6){
            m = 'Jul';
        } else if(m==7){
            m = 'Aug';
        } else if(m==8){
            m = 'Sep';
        } else if(m==9){
            m = 'Oct';
        } else if(m==10){
            m = 'Nov';
        } else {
            m = 'Dec';
        }

        var y = dt.getFullYear();
        return m+" "+d+", "+y;
    },
    dateFormat2 : function(date){
        var dt = new Date(date);
        var d = (dt.getDate()<10)?'0'+dt.getDate():dt.getDate();
        var m = ((dt.getMonth()+1)<10?'0'+(dt.getMonth()+1):(dt.getMonth()+1));
        var y = dt.getFullYear();
        return m+"-"+d+"-"+y;
    },
    getLastweek : function(date){
        var dt = new Date(date);
        var d = (dt.getDate()<10)?'0'+dt.getDate():dt.getDate();
        var m = ((dt.getMonth()+1)<10?'0'+(dt.getMonth()+1):(dt.getMonth()+1));
        var y = dt.getFullYear();
        return y+"-"+m+"-"+d;
    },
    sendmessages : function(number,message){
      var txt = message;
      var options = { method: 'GET',
      url: 'https://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
      qs:
       { type: 'smsquicksend',
         user: 'niharinfo',
         pass: 'Nihar@123',
         sender: 'NIHAAR',
         to_mobileno: number,
         sms_text: txt },
      headers:
       {
         'cache-control': 'no-cache' } };
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        console.log(body);
      });
    },
    sendemail: function(mail,subject,body){
      var mailOptions = {
        from: 'niharinfo321@gmail.com',
        to: email,
        subject: subject,
        html: body
      };
      hart.transporter11.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
    },
    loggermessage : function(agentid,agentname,service,date,status,message){

      var message = "#"+agentid+"#"+agentname+"#"+service+"#"+date+"#"+status+"#"+message+"\n";
      fs.appendFile('logfile.txt', message, function(err, data){
        if (err) console.log(err);
        console.log("Successfully Written to File.");
      });
      return message;
    },
    addoneday : function(todate){
      var new_date = moment(todate, "YYYY-MM-DD").add('days', 1);
      var day = new_date.format('DD');
      var month = new_date.format('MM');
      var year = new_date.format('YYYY');
      var tmrw = year + '-' + month + '-' +day;
      return tmrw;
    }
    // alltransactionssave : function(Agentid,Amount,agent_commission,nihar_com,NCom_amount,trans_status){
    //   var tsreports = new Transactions({
    //     AgentId:Agentid,
    //     ServiceName:service,
    //     Amount:Amount,
    //     Agent_Commission:agent_commission,
    //     NiharCommission:nihar_com,
    //     Nihar_com_Amount:NCom_amount,
    //     TransactionStatus:trans_status,
    //     Status:1,
    //   });
    //   tsreports.save(function (err, results) {
    //     if(err){
    //       console.log(err);
    //     }else{
    //       console.log("All transactions added..ExpressEcart");
    //     }
    //   });
    // }


// "url": "mongodb://MongoUser:NiharECenter@13.127.152.244:27017/nihartest",
}
