var hart = require('../hart');
var utlity = require('./utlity');
var movieservices = require('./movieservices');
var moment = require('moment');
var mongoose = require('mongoose');
var url = hart.url;
var request = require('request');
var ObjectId = require('mongodb').ObjectID;
var wallets = hart.wallets;
var Transactions = hart.transaction_reports;
const authService = require('../_helpers/auth.service');
const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss')
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";
const findwalletbal_err = "Error In Find Wallet Balance";
const errorinapireq_err = "Error In Requesting API";
const transactiion_suc_msg = "Transaction Success";
const transactiion_fail_msg = "Transaction Fail";
const Insufficientbalance = "In Insufficient Balance";
const sessionout = "Session Out";
module.exports = function(app) {
  app.post('/WalletCheckECart123',async function(req, res, next) {
    console.log(req.body);
    const auth = await authService.validateToken(req.body.Token, req.body.AgentId);
    if(auth.success == "fail"){
      var data = {};
      data.Message = "Token miss match"
      data.Status = "500";
      res.json(data);
      return false;
    }
    if (!req.body.AgentId) {
      next({ type: "InputError", message: "AgentId missing" });
      return;
    }
    if(req.body.Token != ""){
      var CartValue = parseFloat(req.body.TotalCartValue);
      var agentId = req.body.AgentId;
      wallets.findOne({ agentId: agentId }, function(err, data) {
        if (err) {
          console.log(err);
        }
        else {
          if (data.Wallet > CartValue) {
            var data1 = {};
            data1.Message = data.Wallet
            data1.Status = "True";
            res.json(data1);
          }
          else {
            var data2 = {};
            data2.Message = data.Wallet
            data2.Status = "False";
            res.json(data2);
          }
        }
      });
    }
  });
  app.post('/Checkout123', async function(req, res, next) {
    console.log(req.body);
    // const auth = await authService.validateToken(req.body.Token, req.body.AgentId);
    // if(auth.success == "fail"){
    //   console.log("Authenticated failed");
    //   var data = {};
    //   data.Message = "Token miss match"
    //   data.Status = "500";
    //   res.json(data);
    //   return false;
    // }
    utlity.loggermessage(req.session.id1, req.session.username, req.body.Store, datenow, ser_sart_msg, "Amount = " + req.body.TotalAmount + " ");
    if (!req.body.AgentId || !req.body.TotalAmount || !req.body.Store || !req.body.products) {
      next({ type: "InputError", message: "AgentId or TotalAmount or Store or products missing" });
      return;
    }
    var agentId = req.body.AgentId;
    var agentname=req.body.AgentName;
    var CartValue = req.body.TotalAmount;
    var Store = req.body.Store;
    if(typeof req.body.products=="object"){
      var products = req.body.products;
    }else{
      var products = JSON.parse(req.body.products);
    }
    wallets.findOne({ agentId: agentId }, function(err, data) {
      if (!err) {
        var value = parseFloat(CartValue);
        if (data.Wallet < value) {
          var data = {};
          data.Status = "100";
          data.Message = "In Sufficient Balance"
          res.json(data);
          utlity.loggermessage(agentId, "", req.body.Store, datenow, fail_msg, "Insufficient Balance");
          return false;
        }
        else {
          var TotalAMount = data.Wallet - value;
          wallets.updateOne({ 'agentId': agentId }, { $set: { Wallet: TotalAMount } }, function(err, response) {
            if (!err) {
             utlity.loggermessage(agentId, "", Store, datenow, suc_msg, "Product Added..");
             alltransactionssave(agentId, Store, value, 0, 0, 0, "Success");
              if (Store=='itdukaan.in') {
                saveItDukaanorders(agentId, products, Store, value, agentname);
              }else {
                saveexpressorders(agentId, products, Store, value);
              }
              var data = {};
              data.Status = "1";
              data.Message = "Success"
              res.json(data);
              return false;
            }
            else {
              var data = {};
              data.Status = "150";
              data.Message = "Error in wallet update"
              res.json(data);
              utlity.loggermessage(req.session.id1, req.session.username, req.body.Store, datenow, fail_msg, "Error:" + err);
              return false;
            }
          });
        }
      }
      else {
        return err;
      }
    });
  });
}
function saveItDukaanorders(agentId, products, store, totalAmount,agentname) {
  // utlity.loggermessage(agentId + " " + products + " " + store + " " + totalAmount + " For checking all ItDukaanorders");
  var expressorderss = hart.itdukaanorders;
  var total=parseFloat(totalAmount);
  var exreports = new expressorderss({
    AgentId: agentId,
    AgentName: agentname,
    Products: products,
    TotalAmount: total,
    Status: "1",
  });
  exreports.save(function(err, results) {
    if (err) {
      console.log(err);
    }
    else {
      console.log("saveItDukaanorders updated");
    }
  });

}
 function saveexpressorders(agentId, products, store, totalAmount) {
  utlity.loggermessage(agentId + " " + products + " " + store + " " + totalAmount + " For checking all saveexpressorders");
  var expressorderss = hart.expressorders;
  var exreports = new expressorderss({
    AgentId: agentId,
    Products: products,
    Store: store,
    TotalAmout: parseFloat(totalAmount),
    Status: "1",
  });
  console.log("saveexpressorders");
  console.log(exreports);
  exreports.save(function(err, results) {
    if (err) {
      console.log(err);
    }
    else {
      console.log("transactions updated");
    }
  });
}

function alltransactionssave(Agentid, Store, value, agent_commission, nihar_com, NCom_amount, trans_status) {
  utlity.loggermessage(Agentid + " " + NCom_amount + " " + Store + " " + value + " For all transactions expresscart");
  var Transactions = hart.transaction_reports;
  var tsreports = new Transactions({
    AgentId: Agentid,
    ServiceName: Store,
    Amount: value,
    Agent_Commission: agent_commission,
    NiharCommission: nihar_com,
    Nihar_com_Amount: NCom_amount,
    TransactionStatus: trans_status,
    Status: 1,
  });
  console.log(tsreports);
  tsreports.save(function (err, results) {
    if(err){
      console.log(err);
    }else{
      console.log("transactions updated");
    }
  });
}
