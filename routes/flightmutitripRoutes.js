//this page is to get flights list , saving records in db, generating pnr and eTicketID, wallet loading, calculating earnings
const crypto = require('crypto');
const hart= require('../hart');
const utlity = require('./utlity');
const flightservices = require('./flightservices');
const moment = require('moment');
const mongoose = require('mongoose');
const mongoXlsx = require('mongo-xlsx');
const url = hart.url;
const request = require('request');
const http = require("http");
const async=require("async");
const ObjectId = require('mongodb').ObjectID;
const fs = require('fs');
const xmlReader = require('read-xml');
const xmlParser = require('xmldom').DOMParser;
var parser_xml = new xmlParser();
const parseString = require('xml2js').parseString;

const refver = "1.0";
const ndc_name = "NDC GATEWAY";
const pname = "Test sender";
const pemail = "abc@tc.com";
const psucity = "AH7H";
const agncid = "test";
const origindkey1 = "OD1";
const origindkey2 = "OD2";
const origindkey3 = "OD3";
const origindkey4 = "OD4";
const origindkey5 = "OD5";
const origindkey6 = "OD6";
const aid = "AI";
const psngrtype = "ADT";
const ADULT_code='ADT';
const CHILD_code='CHD';
const INF_code='INF';
const DomParser = require('dom-parser');
var parser = new DomParser();

module.exports = function(app){
  //flight listing API
app.post('/getmultitripflights',function(req,res){
  console.log("Getting Flights")
  //making request xml
  var body1=flightservices.AirShoppingRQStart()+flightservices.documentTag(ndc_name,refver) ;
  body1 += flightservices.partytagPseudoCity(pname,pemail,psucity,agncid);
  body1 += flightservices.CoreQueryStatrt();
  body1 += flightservices.origindestination(origindkey1,req.body.source,req.body.departing,req.body.destination);
  //round trip logic goes here
  if (req.body.triptype=='AirshoppingRQ_round') {
    body1 += flightservices.origindestination(origindkey2,req.body.destination,req.body.returning,req.body.source);
  }
  if(req.body.sourcemulti2){
  body1 += flightservices.origindestination(origindkey2,req.body.sourcemulti2,req.body.departing_2,req.body.destinationmulti2);
  }
  if(req.body.sourcemulti3){
  body1 += flightservices.origindestination(origindkey3,req.body.sourcemulti3,req.body.departing_3,req.body.destinationmulti3);
  }
  if(req.body.sourcemulti4){
  body1 += flightservices.origindestination(origindkey4,req.body.sourcemulti4,req.body.departing_4,req.body.destinationmulti4);
  }
  if(req.body.sourcemulti5){
  body1 += flightservices.origindestination(origindkey5,req.body.sourcemulti5,req.body.departing_5,req.body.destinationmulti5);
  }
  if(req.body.sourcemulti6){
  body1 += flightservices.origindestination(origindkey6,req.body.sourcemulti6,req.body.departing_6,req.body.destinationmulti6);
  }
  body1 += flightservices.CoreQueryEnd();
  body1 += flightservices.PreferenceTag(aid,req.body.class,req.body.triptype);
  body1 += flightservices.DataListsStartTag();
  // adult passengers loop code goes here
   for (var i = 0; i < req.body.adults; i++) {
     body1 += flightservices.PassengerTag(psngrtype+i,ADULT_code,i);
   }
   // child passengers loop code goes here

   for (var i = 0; i < req.body.child; i++) {
     body1 += flightservices.PassengerTag(CHILD_code+i,CHILD_code,i);
   }
   // infant passengers loop code goes here

   for (var i = 0; i < req.body.infant; i++) {
     body1 += flightservices.PassengerTag(INF_code+i,INF_code,i);
   }

   body1 += flightservices.DataListsEndTag();
   body1 += flightservices.AirShoppingEndTag();
   console.log(body1);
   fs.writeFile('AirshoppingRq.xml', body1, function (err) {
     if (err) throw err;
      console.log('Saved!');
    });
   //object to make API call
    var options = { method: 'POST',
    url: 'http://atip.airtechapi.com/airtecht/callApi',
      headers:
       { 'Content-Type': 'application/xml',
         Authorization: 'Basic bXVydGh5a25AbmloYXJpbmZvLmNvbTptdXJ0aHlrbiU3LW5paGFyITIzNA=='
       },
         body: body1,
       };
       //calling api by using request module
    request(options, function (error, response, body) {
      console.log(body);
      if (error) throw new Error(error);
        //if no error response sending to client side
        fs.writeFile('AirshoppingRS.xml', body, function (err) {
          if (err) throw err;
           console.log('Saved!');
         });
        res.send(body);
    });
 })
//API for making payment and booking flight
  app.post('/ordercreate',function(req,res){
    console.log(req.body);
    //getting wallet balance
    hart.wallets.findOne({ agentId : req.session.id1 },function(err,data){
      console.log(1);
      if(err){
        console.log(err);
      }else {
        var balance = data.Wallet;
        var Amount = parseFloat(req.body.offerprice[0].price);
        //checking balance is sufficent
        if(balance > Amount){
          var request = require("request");
          //making request xml
          var body1='<OrderCreateRQ Version="17.2" TransactionIdentifier="9234a126-7769-4e16-8"><Document id="document"/><Party><Sender><TravelAgencySender><PseudoCity>AE9L</PseudoCity><AgencyID>test</AgencyID></TravelAgencySender></Sender></Party><Query><Order><Offer OfferID="'+req.body.offerprice[0].OfferID+'" Owner="'+req.body.offerprice[0].Owner+'" ResponseID="'+req.body.offerprice[0].ResponseID+'">';
          //loop for offer item id
          for (var i = 0; i < req.body.offerprice[0].OfferItemID.length; i++) {
            body1+='<OfferItem OfferItemID="'+req.body.offerprice[0].OfferItemID[i].id+'"><PassengerRefs>'+req.body.offerprice[0].OfferItemID[i].passref+'</PassengerRefs></OfferItem>';
          }
          body1+='</Offer></Order><Payments><Payment><Type>CA</Type><Method><Other><Remarks><Remark>CASH</Remark></Remarks></Other></Method><Amount Code="INR">'+req.body.offerprice[0].price+'</Amount></Payment></Payments><DataLists><PassengerList>';
          var agender;
          //loop for number of passengers
          for (var i = 0; i < req.body.adt.length; i++) {
            if (req.body.adt[i].title=='Mr'||req.body.adt[i].title=='Master') {
              agender='Male';
            }
            if (req.body.adt[i].title=='Ms'||req.body.adt[i].title=='Mrs') {
              agender='Female';
            }
            body1+='<Passenger PassengerID="'+req.body.adt[i].ID+'"><PTC>'+req.body.adt[i].type+'</PTC><ResidenceCountryCode>IN</ResidenceCountryCode><Individual><Birthdate>'+req.body.adt[i].date+'</Birthdate><Gender>'+agender+'</Gender><NameTitle>'+req.body.adt[i].title+'</NameTitle><GivenName>'+req.body.adt[i].fname+' '+req.body.adt[i].mname+'</GivenName><Surname>'+req.body.adt[i].lname+'</Surname></Individual><ContactInfoRef>CID1</ContactInfoRef></Passenger>';
          }
          body1+='</PassengerList><ContactList><ContactInformation ContactID="CID1"><PostalAddress><Label>A</Label><Street>Home</Street><PostalCode>PostalCode</PostalCode><CityName>Delhi</CityName><CountrySubdivisionName>IN</CountrySubdivisionName><CountryName>IN</CountryName><CountryCode>IN</CountryCode></PostalAddress>';
          body1+='<ContactProvided><EmailAddress><Label>Personal</Label><EmailAddressValue>'+req.body.email+'</EmailAddressValue></EmailAddress></ContactProvided><ContactProvided><Phone><Label>Home</Label><PhoneNumber>'+req.body.phone+'</PhoneNumber></Phone></ContactProvided>';
          body1+='</ContactInformation></ContactList></DataLists></Query></OrderCreateRQ>';
          console.log(body1);
          fs.writeFile('OrderCreate.xml', body1, function (err) {
            if (err) throw err;
             console.log('Saved!');
           });
          //object to make API call
        var options = { method: 'POST',
          url: 'http://atip.airtechapi.com/airtecht/callApi',
          headers:
           { 'Content-Type': 'application/xml',
             Authorization: 'Basic bXVydGh5a25AbmloYXJpbmZvLmNvbTptdXJ0aHlrbiU3LW5paGFyITIzNA=='
           },
             body: body1,
         };
         //calling api by using request module
         request(options, function (error, response, body) {
           if (error) throw new Error(error);
           fs.writeFile('OrderCreateRS.xml', body, function (err) {
             if (err) throw err;
              console.log('Saved!');
            });
             var xmlDoc = parser_xml.parseFromString(body,"text/xml");
             //checking response is not with error
               if (xmlDoc.getElementsByTagName("DataLists").length !== 0 ) {
                 parseString(body, function (err, s) {
                       if (!err) {
                         //getting commission for flights
                         utlity.Commissionfun('FLT', function(result) {
                           var bp_commission=result.Agent_Margin/100;
                           var nihar_commission=result.Nihar_Margin/100;
                           var discount=Amount*result.Aggregator_Margin/100;
                           var bp_earn=discount*bp_commission;
                           var nihar_earn=discount*nihar_commission;
                           var updamnt = balance-Amount;
                           //recording transactions in db
                         utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",0,result.servicesName);
                         var datalist= s.OrderViewRS.Response[0].DataLists[0];
                         var tno=[];var flt=[];var fltname=[];var PassengerList=[];
                         //saving flight details to store in db
                         for (var i = 0; i < datalist.FlightSegmentList[0].FlightSegment.length; i++) {
                           flt.push({number:datalist.FlightSegmentList[0].FlightSegment[i].MarketingCarrier[0].FlightNumber[0],
                             name:datalist.FlightSegmentList[0].FlightSegment[i].MarketingCarrier[0].Name[0],
                             Departure:{date:datalist.FlightSegmentList[0].FlightSegment[i].Departure[0].Date[0],
                               time:datalist.FlightSegmentList[0].FlightSegment[i].Departure[0].Time[0],
                               Airportname:datalist.FlightSegmentList[0].FlightSegment[i].Departure[0].AirportName[0],
                               Terminal:datalist.FlightSegmentList[0].FlightSegment[i].Departure[0].Terminal[0].Name[0]},
                             Arriaval:{date:datalist.FlightSegmentList[0].FlightSegment[i].Arrival[0].Date[0],
                               Time:datalist.FlightSegmentList[0].FlightSegment[i].Arrival[0].Time[0],
                               AirportName:datalist.FlightSegmentList[0].FlightSegment[i].Arrival[0].AirportName[0],
                               Terminal:datalist.FlightSegmentList[0].FlightSegment[i].Arrival[0].Terminal[0].Name[0]},
                           });
                           // console.log(flt);
                         }
                         // saving eticket id to store in db
                         var eTicketinviewrs='';
                         if (s.OrderViewRS.Response[0].TicketDocInfos!==undefined&&s.OrderViewRS.Response[0].TicketDocInfos[0].TicketDocInfo!==undefined&&s.OrderViewRS.Response[0].TicketDocInfos[0].TicketDocInfo!=='') {
                             for (var Tc = 0; Tc < s.OrderViewRS.Response[0].TicketDocInfos[0].TicketDocInfo.length; Tc++) {
                                 eTicketinviewrs+=s.OrderViewRS.Response[0].TicketDocInfos[0].TicketDocInfo[Tc].TicketDocument[0].TicketDocNbr[0]+' ';
                              }
                         }else {
                            eTicketinviewrs='';
                         }
                         //saving passengers data by using loop for storing in db
                         for (var i = 0; i < datalist.PassengerList[0].Passenger.length; i++) {
                           // console.log(datalist.PassengerList[0].Passenger[i]);
                           PassengerList.push({name:datalist.PassengerList[0].Passenger[i].Individual[0].GivenName[0],
                             Surname:datalist.PassengerList[0].Passenger[i].Individual[0].Surname[0],
                             Gender:datalist.PassengerList[0].Passenger[i].Individual[0].Gender[0],
                             Birthdate:datalist.PassengerList[0].Passenger[i].Individual[0].Birthdate[0],
                             ptc:datalist.PassengerList[0].Passenger[i].PTC[0],
                             id:datalist.PassengerList[0].Passenger[i].$.PassengerID,
                             eTicketID:eTicketinviewrs,
                           });
                         }
                         //saving pnr to store in db
                         var pnr_=[];
                         for (var vr = 0; vr < s.OrderViewRS.Response[0].Order.length; vr++) {
                           for (var vr1 = 0; vr1 < s.OrderViewRS.Response[0].Order[vr].BookingReferences[0].BookingReference.length; vr1++) {
                             if (s.OrderViewRS.Response[0].Order[vr].BookingReferences[0].BookingReference[vr1].OtherID!==undefined) {
                               pnr_[vr]=s.OrderViewRS.Response[0].Order[vr].BookingReferences[0].BookingReference[vr1].ID[0];
                             }
                           }
                         }
                         var flightbuk = hart.flightbookings;
                         //creating object to store in db
                         var fltrec = new flightbuk({
                         AgentId : req.session.id1,
                         pnr:pnr_,
                         flightDetails:flt,
                         PassengerList:PassengerList,
                         Email:xmlDoc.getElementsByTagName("EmailAddressValue")[0].childNodes[0].nodeValue,
                         phone:xmlDoc.getElementsByTagName("PhoneNumber")[0].childNodes[0].nodeValue,
                         Fare: req.body.offerprice[0].price,
                         status:'1'
                       });
                       //saving flightbookings in db
                       fltrec.save(function (err, results) {
                         console.log(results);
                         if(err){
                           console.log(err);
                         }else {
                           //if no error in saving data in db sending response to client
                           console.log('recorded');
                         res.json(s);
                        }
                       });
                       });
                       }else {
                         //if error sending error msg
                         console.log('err1');
                         res.json('err');
                       }
                 });

               }else {
                 //if response error then sending error to clent
                 res.send('err');
               }
         });
        }else {
          //if no balance sending code to add balance
            res.json("10");
        }
      }
  })
})

}
