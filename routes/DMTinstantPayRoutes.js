var crypto = require('crypto');
var hart= require('../hart');
var utlity = require('./utlity');
var moment = require('moment');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var token = hart.token;
var request = require('request');
var http = require("http");
var ObjectId = require('mongodb').ObjectID;
const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss');
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";
const findwalletbal_err = "Error In Find Wallet Balance";
const errorinapireq_err = "Error In Requesting API";
const transactiion_suc_msg = "Transaction Success";
const transactiion_fail_msg = "Transaction Fail";
const Insufficientbalance = "In Insufficient Balance";
const sessionout = "Session Out";
const service_name =  "Money Transfer";
module.exports = function(app){

app.post('/RemitterRegistration',function(req,res){
  hart.panoutlets.findOne({ agentId : req.body.agentid },function(err,data1){
    if(!err){
      if (data1 == null) {
        res.json("0")
      }else{
        var outlet = data1.OutLetId;
        var options = { method: 'POST',
        url: 'https://www.instantpay.in/ws/dmi/remitter',
        headers:
         {
           'content-type': 'application/json' },
        body:
         { token: token,
           request:
            { mobile: req.body.mobilenumber,
              name: req.body.nameofremmiter,
              surname: req.body.surnameofremmiter,
              pincode: req.body.pincode,
              outletid:outlet }
            },
        json: true };
        request(options, function (error, response, body) {
          if (error) throw new Error(error);
          console.log(body);
          if(body.statuscode == "TXN"){
            res.json(body);
            saveremitterdet(req.body.agentid,req.body.mobilenumber,req.body.username,req.body.pincode,body.data.remitter.id,body.data.remitter.is_verified);
          }
          else{
            res.json(body);
          }
        //  console.log(body);
        });
      }
    }
  })
  return false;
})

function saveremitterdet(Aid,mobile,name,pincode,remtrid,is_verified){
    var DmtRemitters = hart.dmtremitters;
      var dmtremitters = new DmtRemitters({
        AgentId:Aid,
        AgentName:name,
        MobileNumber:mobile,
        PinCode:pincode,
        RemittterId:remtrid,
        is_verified:is_verified,
        Status:"1",
      });
      dmtremitters.save(function (err, results) {
        if(err){
          console.log(err);
        }
        else{
          //res.json(1);
        }
      });
      var agentid = ObjectId(Aid);
       hart.agents.updateOne({ '_id': agentid },{ $set:{ RemitterId: remtrid }}, function(err, response) {
       if(err){
         console.log(err);
       }
       else{
         console.log("Wallet Updated");
       }
       });

}

app.post('/RemitterRegistration_validation',function(req,res){
  hart.panoutlets.findOne({ agentId : req.body.agentid },function(err,data1){
    if(!err){
      if (data1 == null) {
        res.json("0")
      }else{
        var outlet = data1.OutLetId;
        var options = { method: 'POST',
        url: 'https://www.instantpay.in/ws/dmi/remitter_validate',
        headers:
         {
           'content-type': 'application/json' },
        body:
         { token: token,
           request:
            { remitterid: req.body.remmiterid,
              mobile: req.body.mobilenumber,
              otp: req.body.otp_rem,
              outletid:outlet }
            },
        json: true };
        request(options, function (error, response, body) {
          if (error) throw new Error(error);
          console.log(body);
          if(body.statuscode == "TXN"){
            res.json("1")
          }
          else{
            res.json("0")
          }
        });
      }
    }
  })
})

app.post('/BeneficiaryRegistration',function(req,res){
  hart.dmtbeneficiaries.find({$and: [{ 'AgentId':req.session.id1},{ 'RemitterId':req.session.Remitterid},{ 'Bnf_AccountNumber':req.body.AccountNumber}]}).exec(function(err, doc1) {
    var x = JSON.parse(JSON.stringify(doc1));
    console.log(x.length);
    if(x.length>0){
      savebenfacc(req.session.id1,req.session.Remitterid,x[0].BenfId,req.body.CustId,req.body.firstName,req.body.lastName,req.body.mobile,req.body.emailAddress,req.body.Bank,req.body.Branch,x[0].Bnf_AccountNumber,req.body.IfscCode,"Yes");
      res.json("3");
      return false;
    }else{
      hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
        if (!err) {
          var outlet = data1.OutLetId;
          console.log(data1);
          var options = { method: 'POST',
          url: 'https://www.instantpay.in/ws/dmi/beneficiary_register',
          headers:
           {
             'content-type': 'application/json'
           },
          body:
           { token: token,
             request:
              { remitterid: req.session.Remitterid,
                name: req.body.firstName+' '+req.body.lastName,
                ifsc: req.body.IfscCode,
                account: req.body.AccountNumber,
                outletid:outlet } },
          json: true };
          request(options, function (error, response, body) {
            if (error) throw new Error(error);
            if(body.statuscode == "TXN"){
              res.json(body);
              ///res.json("1");
              savebenfacc(req.session.id1,req.session.Remitterid,body.data.beneficiary.id,req.body.CustId,req.body.firstName,req.body.lastName,req.body.mobile,req.body.emailAddress,req.body.Bank,req.body.Branch,req.body.AccountNumber,req.body.IfscCode,"No");
            }
            else{
              res.json("0");
            }
            console.log(body);
          });
        }else {
          res.json("0");
        }
        });
    }
  });

})


function savebenfacc(agentid,rid,bnfid,customerid,fname,lname,mobile,email,bankname,branch,accountnumber,ifsc,otpv){
  var Beneficiaries = hart.dmtbeneficiaries;
    var BeneficiaryAcc = new Beneficiaries({
      AgentId:agentid,
      RemitterId:rid,
      BenfId:bnfid,
      CustId:customerid,
      Bnf_FirstName:fname,
      Bnf_LaststName:lname,
      Bnf_Mobile:mobile,
      Bnf_Email:email,
      Bnf_Bank:bankname,
      Bnf_Branch:branch,
      Bnf_AccountNumber:accountnumber,
      Bnf_IfscCode:ifsc,
      Otpverified:otpv,
      Status:1,
    });
    BeneficiaryAcc.save(function (err, results) {
      if(err){
        console.log(err);
      //  res.json(0);
      }
      else{
      //  res.json(1);
      }
    });
}


app.post('/BeneficiaryRegistrationValidate',function(req,res){

    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/dmi/beneficiary_register_validate',
    headers:
     {
       'content-type': 'application/json' },
    body:
     { token: token,
       request:
        { remitterid: req.session.Remitterid,
          beneficiaryid: req.body.benfid,
          otp: req.body.otpentered
          } },
    json: true };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      if(body.statuscode == "TXN"){
        updateotpbenf(req.session.id1,req.body.benfid);
        res.json(body);
      }
      else{
        res.json(body);
      }
    //  console.log(body);
    });
})


function updateotpbenf(id,benfid){
   hart.dmtbeneficiaries.updateOne({$and:[{AgentId:id},{BenfId:benfid}]},{ $set:{ Otpverified: "Yes" }}, function(err, response) {
   if(err){
     console.log(err);
     //res.json(0);
   }
   else{
     console.log("OTP Yes");
     //res.json(1);
   }
 });
}
app.post('/BeneficiaryRegistrationResendOTP',function(req,res){

    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/dmi/beneficiary_resend_otp',
    headers:
     {
       'content-type': 'application/json' },
    body:
     { token: token,
       request:
        { remitterid: req.session.Remitterid,
          beneficiaryid: req.body.benfid
           } },
    json: true };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      if(!error){
        res.json(body);
      }
      else{
        res.json("error occured");
      }
    //  console.log(body);
    });
})

app.post('/FundTransfer', function(req,res){
  utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,ser_sart_msg,"Amount = "+req.body.Amount+" ");
      if(!req.session.id1){
        utlity.loggermessage(sessionout,sessionout,service_name,datenow,fail_msg,sessionout);
        next({ type: "UnauthorizedError", message: "Session not found" });
        return;
      }
      var options = { method: 'POST',
      url: 'https://www.instantpay.in/ws/dmi/transfer',
      headers:
       {
         'content-type': 'application/json' },
      body:
       { token: token,
         request:
          { remittermobile: req.session.Mobile,
            beneficiaryid: req.body.ipbenfcid,
            agentid: req.session.id1,
            amount: req.body.Amount,
            mode: req.body.PaymentType } },
      json: true };
      request(options, function (error, response, body) {
        if(error){
          utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,errorinapireq_err+error);
          res.json("Unable to process your request");
          return false;;
        }
      //  console.log(body);
        if(!error){
          if(body.statuscode == "TXN"){
            res.json(body);
            savemoneytransfer(req.session.id1,req.body.CustId,req.body.Name,req.body.AccountNumber,req.body.Bank,req.body.Branch,req.body.Ifsc,req.body.Amount,req.body.PaymentType,body.data.ref_no);
            var balance = req.body.wmoney;
            var Amount = parseFloat(req.body.Amount);
            var instantcommision = 6.1;     //beofre it was 4.5
            var bp_commission = 0.666666667;
            var nihar_commission = 0.333333333;
            if(Amount <= 1500){
              var discount=15
              var agentdeductamount = Amount + instantcommision + discount;   //Deduct Amount From AGent Wallet  //for 26.1 for 20
              var bp_earn=discount*bp_commission;
              var nihar_earn=discount*nihar_commission;
              // var niharcommision = ;
              // var agentcommision = 10;
            }else{
              var discount=Amount*0.10;
              var agentdeductamount = Amount + instantcommision + discount;   //Deduct Amount From AGent Wallet
              var bp_earn=discount*bp_commission;
              var nihar_earn=discount*nihar_commission;
              // var agentdeductamount = Amount +instantcommision+Amount * 0.34/100;
              // var niharcommision = Amount * 0.34/100;
              // var agentcommision = Amount * 0.66/100;
            }
            var updamnt = parseFloat(balance) - parseFloat(agentdeductamount);
            utlity.transactions(req.session.id1,req.session.username,'ecenter','INP','DMT','DMT',updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",body.data.ref_no,'DMT');
            senddmtmessage(req.body.Name,req.body.benfmobile,req.body.Amount,body.data.ref_no);
            utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,suc_msg,transactiion_suc_msg);
          }else{
            res.json(body);
            utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,"Api Responce : "+transactiion_fail_msg);
          //  savemoneytransfer();
          }
        }
        else{
          res.json("error occured");
        }
      });
})
function senddmtmessage(Name,number,amount,refno){
  var msg = "Dear "+Name+", Fund transfer of Rs. "+amount+" has been successfull with reference id "+refno;
    var options = { method: 'GET',
    url: 'http://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
    qs:
     { type: 'smsquicksend',
       user: 'niharinfo',
       pass: 'Nihar@123',
       sender: 'NIHAAR',
       to_mobileno: number,
       sms_text: msg },
    headers:
     {
       'cache-control': 'no-cache' } };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);

  });

}




function savemoneytransfer(aid,cid,name,AccountNumber,Bank,Branch,Ifsc,Amount,PaymentType,refno){
  var Transfers = hart.transfers;
    var MoneyTransfers = new Transfers({
      AgentId:aid,
      CustId:cid,
      Name:name,
      AccountNumber:AccountNumber,
      Bank:Bank,
      Branch:Branch,
      Ifsc:Ifsc,
      Amount:Amount,
      PaymentType:PaymentType,
      ReferenceNumber:refno,
      Status:1,
    });
    MoneyTransfers.save(function (err, results) {
      if(err){
        console.log(err);
        //res.json(0);
      }
      else{
      //  res.json(1);
      }
    });
}


app.post('/benfaccverify',function(req,res){
    //console.log(req.body);
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/imps/account_validate',
    headers:
     {
       'content-type': 'application/json' },
    body:
     { token: token,
       request:
        { remittermobile: req.session.Mobile,
          account: req.body.accno,
          ifsc: req.body.ifsc,
          agentid:req.session.id1
          } },
    json: true };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      if(body.statuscode == "TXN"){
        //updateotpbenf(req.session.id1);
        res.json(body);
      }
      else{
        res.json(body);
      }
    //  console.log(body);
    });
})

app.post('/editDmtBeneficeries',function(req,res){
    var docs={
      Bnf_FirstName: req.body.firstName,
      Bnf_LaststName:req.body.lastName,
      Bnf_Mobile:req.body.mobile,
      Bnf_Email:req.body.emailAddress,
      Bnf_Bank:req.body.Bank,
      Bnf_Branch:req.body.Branch,
      Bnf_AccountNumber:req.body.AccountNumber,
      Bnf_IfscCode:req.body.IfscCode,
      Status:1,
    };
    var ObjectId = require('mongodb').ObjectID;
    var beneficiaryid = ObjectId(req.body.beneficiaryid);
    hart.dmtbeneficiaries.updateOne({ '_id':beneficiaryid },{ $set:docs}, function(err, data) {
      if(err){
        res.json("0");
        console.log(err)
      }
      res.json("1");
    })
})
app.post('/AgentProfileDetails12', function(req,res){
    var ObjectId = require('mongodb').ObjectID;
    var beneficiaryid= ObjectId(req.body.id);
    hart.dmtbeneficiaries.findOne({ _id : beneficiaryid },function(err,data){
      res.json(data);
     });
})

app.post('/DeleteDmtBeneficeriesDetails', function(req,res){
    var beneficiaryid= ObjectId(req.body.aid);
    hart.dmtbeneficiaries.updateOne( { '_id':beneficiaryid },{ $set:{Status:0}}, function(err, doc) {
      if (err) {
         console.log(err);
         res.json("0");
      }
      else{
        res.json("1");
      }
    });
})

app.post('/editDmtBeneficeriesDetails',function(req,res){
    var docs={
      Bnf_FirstName: req.body.FirstName,
      Bnf_LaststName:req.body.LastName,
      Bnf_Mobile:req.body.Mobilenumber,
      Bnf_Email:req.body.email,
      Bnf_Bank:req.body.bank,
      Bnf_Branch:req.body.branch,
      Bnf_AccountNumber:req.body.accountNumber,
      Bnf_IfscCode:req.body.ifscCode,
      Status:1,
    };
    var ObjectId = require('mongodb').ObjectID;
    var beneficiaryid = ObjectId(req.body.beneficiaryid);
    hart.dmtbeneficiaries.updateOne({ '_id':beneficiaryid },{ $set:docs}, function(err, data) {
      if(err){
        res.json("0");
        console.log(err)
      }
      res.json("1");
    })
})



  app.post('/editDmtCustomer',function(req,res){
      var docs={
        //AgentId:req.session.id1,
        Cust_FirstName:req.body.FirstName,
        Cust_LastName:req.body.LastName,
        Cust_Mobile:req.body.Mobilenumber,
        Cust_Email:req.body.email,
        Cust_Address:req.body.address,
        Cust_city:req.body.City,
        Cust_state:req.body.State,
        Cust_pincode:req.body.Pincode,
        Status:1,
      };
      var ObjectId = require('mongodb').ObjectID;
      var customerid = ObjectId(req.body.customerid);
      hart.dmtcustomers.updateOne({ '_id':customerid },{ $set:docs}, function(err, data) {
        if(err){
          res.json("0");
          console.log(err)
        }
        res.json("1");
      })

  })

  app.post('/DeleteDmtCustomerDetails', function(req,res){
      var beneficiaryid= ObjectId(req.body.aid);
      hart.dmtcustomers.updateOne( { '_id':beneficiaryid },{ $set:{Status:0}}, function(err, doc) {
        if (err) {
           console.log(err);
           res.json("0");
        }
        else{
          res.json("1");
        }
      });
  })
app.post('/BeneficiaryRegistrationResendOTP',function(req,res){

    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/dmi/beneficiary_resend_otp',
    headers:
     {
       'content-type': 'application/json' },
    body:
     { token: token,
       request:
        { remitterid: req.session.Remitterid,
          beneficiaryid: req.body.benfid
        } },
    json: true };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      if(!error){
        res.json(body);
      }
      else{
        res.json("error occured");
      }
    //  console.log(body);
    });
});
app.post('/DeleteBeneficiariesDetails',function(req,res){

  var obj = ObjectId(req.body.obid);
  hart.dmtbeneficiaries.updateOne( { '_id':obj },{ $set:{Status:0}}, function(err, doc) {
    if (err) {
       console.log(err);
       res.json("0");
    }
    else{
      res.json("1");
    }
  });
  // var options ={ method : 'POST',
  //    url: 'https://www.instantpay.in/ws/dmi/beneficiary_remove',
  //    headers:{

  //      'Content-Type': 'application/json',
  //    },
  //    body :{

  //       token: token,
  //       request:{

  //          beneficiaryid: req.body.benfid,remitterid: req.body.remtrid}},
  //          json:true};
  //        request(options,function(error,response,body){
  //          if(error)throw new Error(error);
  //          res.json(body)
  //        })
})
app.post('/adddmtcustomer', function(req,res){
  var Dmtcstmrs = hart.dmtcustomers;
    var dmtcustomers = new Dmtcstmrs({
      AgentId:req.session.id1,
      Cust_FirstName:req.body.firstName,
      Cust_LastName:req.body.lastName,
      Cust_Mobile:req.body.mobile,
      Cust_Email:req.body.emailAddress,
      Cust_Address:req.body.Address,
      Cust_city:req.body.city,
      Cust_state:req.body.state,
      Cust_pincode:req.body.pincode,
      Status:1,
    });
    dmtcustomers.save(function (err, results) {
      if(err){
        console.log(err);
        res.json(0);
      }
      else{
        res.json(1);
      }
    });
})
app.post('/gettransferdet', function(req,res){

    var obj = ObjectId(req.body.bnfcdet);
    hart.dmtbeneficiaries.findOne({ _id : obj },function(err,data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
   });

   app.post('/checkdmtcustomernumber', function(req,res){
         hart.dmtcustomers.count({$and: [{ 'AgentId':req.session.id1},{ 'Cust_Mobile':req.body.custmobile},{ 'Status':"1"}]}).exec(function(err, doc1) {
         if(err){
           console.log(err);
         }
         else{
           res.json(doc1);
         }
        });
      });
      app.post('/checkdmtcustomeremail', function(req,res){
            hart.dmtcustomers.count({$and: [{ 'AgentId':req.session.id1},{ 'Cust_Email':req.body.email},{ 'Status':"1"}]}).exec(function(err, doc1) {
            if(err){
              console.log(err);
            }
            else{
              res.json(doc1);
            }
           });
         });
         app.post('/checkdmtaccountnumber', function(req,res){
               hart.dmtbeneficiaries.count({$and: [{ 'AgentId':req.session.id1},{ 'CustId':req.body.custId},{ 'Bnf_AccountNumber':req.body.benAccountNumber},{ 'Status':1}]}).exec(function(err, doc1) {
               if(err){
                 console.log(err);
               }
               else{
                 console.log("count:"+doc1);
                 res.json(doc1);
               }
              });
            });

}
