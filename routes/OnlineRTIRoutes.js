var crypto = require('crypto');
var hart= require('../hart');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var request = require('request');
var qs = require("querystring");  //For Otp service
var http = require("http");     //For Otp service
var ObjectId = require('mongodb').ObjectID;
var utlity = require('./utlity');
module.exports = function(app){

// app.use(session({
//     secret: 'I9Admin-Flag',
//     resave: true,
//     saveUninitialized: true,
//     name : 'I9Admin',
// }));

app.post('/RTIRequest',function(req,res){
    if(!req.session.id1){
      next({ type: "UnauthorizedError", message: "Session not found" });
      return;
    }
    var options = {
    method: 'POST',
    url: 'https://onlinerti.com/api/applyRTI',
    headers:
     {
       'content-type': 'application/json' },
    body:
        { firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.Email,
          mobileNumber: req.body.Mobile,
          address: req.body.Address,
          pincode: req.body.PinCode,
          rtiDescription: req.body.Description,
        //  apiKey: 'niharinfo-sandbox'},
          apiKey: 'niharinfo-prod'},
    json: true };
    request(options, function (error, response, body) {
      if (error) {
        console.log(error);
        res.json("Unable to process your request");
        return false;;
      }
      res.json(response);
      insertrtidb(req.session.id1,req.body.firstName,req.body.lastName,req.body.Email,req.body.Mobile,req.body.Address,req.body.PinCode,req.body.Description,body.status,body.applicationID,"No");
    //  console.log(body);
    });
})
app.post('/RTIPayment',function(req,res){
    if(req.body.paymenttype == "1"){
      var Amount=249;
    }else{
      var Amount = 349;
    }
    var discount=50;
    var bp_commission=0.5;
    var nihar_commission=0.5;
    var bp_earn=discount*bp_commission;
    var nihar_earn=discount*nihar_commission;
    hart.wallets.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        console.log(err);
      }else{
        var balance = data.Wallet;
        if(balance>Amount){
          var options = {
          method: 'POST',
          url: 'https://onlinerti.com/api/makePaid',
          headers:
           {
             'content-type': 'application/json' },
          body:
              { id: req.body.appnumber,
                payment: req.body.paymenttype,
              },
          json: true };
          request(options, function (error, response, body) {
            if(error){
              console.log(error);
              res.json("Unable to process your request");
              return false;;
            }
            if(body.status == "PAID"){
                res.json('1');
                var updamnt=balance-Amount;
                utlity.transactions(req.session.id1,req.session.username,'ecenter','INP','RTI','RTI',updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",0,'RTI');
                // utlity.transactions(req.session.id1,"RTI",parseFloat(updamnt),249,parseFloat(agentcommision),parseFloat(niharcommision),"Success",0);
                updatepayment(req.body.appnumber,req.body.paymenttype,req.session.id1);
            }else{
              console.log("0")
              res.json('0');
            }
          });
        }else{
          res.json("3")
        }
      }
     });
})


  function updatepayment(appnumber,payment,sessionid){
    hart.rtis.updateOne({ 'ApplicationId': appnumber },{ $set:{ Payment: "Yes" }}, function(err, response) {
    if(err){
      console.log(err);
    }
    else{
      console.log("Updated");
    }
    });
  }

  function insertrtidb(id1,fname,lname,email,mobile,address,pincode,description,status,appId,payment){
     var rti = hart.rtis;
       var rtirecordes = new rti({
       AgentId:id1,
       FirstName: fname,
       LastName: lname,
       Email:email,
       Mobile:mobile,
       Address:address,
       PinCode:pincode,
       Description:description,
       RTIStatus:status,
       ApplicationId:appId,
       Payment:payment,
       Status:1,
     });
     rtirecordes.save(function (err, results) {
       if(!err){
         console.log("RTI Saved");
       }else{
         console.log("RTI Error");
       }
     });
  }
}
