var crypto = require('crypto');
var hart= require('../hart');
var utlity = require('./utlity');
var WalletRoutes = require('./WalletRoutes');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = hart.url;
var token = hart.token;
var request = require('request');
var http = require("http");     //For Otp service
var parseString = require('xml2js').parseString;  // To convert the xml to json
module.exports = function(app){

  app.post('/getagentelectrictresults',function(req,res){
        hart.electricity_reports.find({agentId:req.session.Agentid}).sort({_id:-1}).exec(function(err, data){
          if(err){
            console.log(err);
          }
          else{
            res.json(data);
          }
         });
  });

app.post('/getele_AD',function(req,res){

    hart.electricity_reports.find({agentId:req.session.Agentid}).exec(function(err, data){
      res.json(data);
    });
})
app.post('/ele_getbill123', function(req,res, next){
    if(!req.session.id1){
      next({ type: "UnauthorizedError", message: "Session not found" });
      return;
    }
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
      if(err){
        console.log(err);
        res.json("Unable to process your request");
        return false;;
      }
      else{
        var outlet = x.OutLetId;
        var date = moment().format('YYYY-MM-DD hh:mm:ss');
        var options = { method: 'POST',
          url: 'https://www.instantpay.in/ws/api/transaction',
          headers:
           { 'Cache-Control' : 'no-cache' },
          formData:
           { token: token,
             agentid: req.session.id1,
             amount: '10',
             spkey: req.body.spkey,
             // account: '115750H829000149',
             account: req.body.account,
             mode: 'VALIDATE',
             optional8: 'Bill Payment',
             optional9: '17.400672,78.488174|500029',
             outletid: outlet,
             paymentmode: 'CASH',
             paymentchannel: 'AGT' } };
        request(options, function (error, response, body) {
          if(error){
            console.log(error);
            res.json("Unable to process your request");
            return false;;
          }
          res.send(body);
        });
        }
     });
})


app.post('/ele_getbill', function(req,res, next){
    if(!req.session.id1){
      next({ type: "UnauthorizedError", message: "Session not found" });
      return;
    }
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
      if(err){
        console.log(err);
        res.json("Unable to process your request");
        return false;;
      }
      else{
        var outlet = x.OutLetId;
        var date = moment().format('YYYY-MM-DD hh:mm:ss');
        var request = require("request");
              var options = { method: 'POST',
              url: 'https://www.instantpay.in/ws/bbps/bill_fetch',
              headers:
               { 'Cache-Control': 'no-cache',
                 'Content-Type': 'application/json' },
              body:
               { token: token,
                 request:
                  { sp_key: req.body.spkey,                         //"MDE", //req.body.spkey
                    agentid: req.session.id1,                       //"MAHA00000MAH01",   //req.session.id1
                    customer_mobile: req.body.mobile,               //"9909999999",   //req.body.mobile
                    customer_params: [req.body.account],            //["021660000094", "4405"  ], //consumer number req.body.account   // and 4405 is Bu for Maharastra only
                    init_channel: 'AGT',
                    endpoint_ip: '13.233.92.94',                    //ip of end
                    mac: 'AD-fg-12-78-GH',
                    payment_mode: 'WALLET',
                    payment_info: 'WALLET|'+req.body.mobile,
                    amount: '10',
                    reference_id: '',
                    latlong: '17.400672,78.488174',
                    outletid: outlet }
                  },  //status: 'Payment mode not allowed for this outlet',
              json: true };
              request(options, function (error, response, body) {
                console.log(body);
                if(error){
                  console.log(error);
                  res.json("Unable to process your request");
                  return false;
                }else{
                  res.send(body);
                }
              });
        }
     });
})
app.post('/ele_billpay', function(req,res){
try{
  if(!req.session.id1){
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  utlity.Commissionfun(req.body.Network, async function(result) {

     var wallets = hart.wallets;
     var panoutlets = hart.panoutlets;
     var outlet;
     var data1 = await panoutlets.findOne({ agentId : req.session.id1 });
       outlet=data1.OutLetId;
    wallets.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        console.log(err);
      }
      else{
        console.log(req.body);
        var balance = data.Wallet;
        var Amount = req.body.rcamount
        if(balance > Amount){
          var date = moment().format('YYYY-MM-DD hh:mm:ss')
          var options = { method: 'POST',
          url: 'https://www.instantpay.in/ws/bbps/bill_pay',
          headers:
           { 'Cache-Control': 'no-cache',
             'Content-Type': 'application/json' },
             body:
              { token: token,
                request:
                 { sp_key: req.body.Network,
                   agentid: req.session.id1,
                   customer_mobile: req.body.mobilenumber,
                   customer_params: [ req.body.acnumber,req.body.mobilenumber ],
                   init_channel: 'AGT',
                   endpoint_ip: '13.233.92.94', //ip of end
                   mac: 'AD-fg-12-78-GH',
                   payment_mode: 'Cash',
                   payment_info: 'bill',
                   amount: req.body.rcamount,
                   reference_id: req.body.ref_number,
                   latlong: '17.400672,78.488174',
                   outletid: outlet } },
          json: true };
          request(options, function (error, response, body) {
            console.log(body);
            if (error)
            {
              console.log(error);
            }else {
              if (body.statuscode == "TXN") {
                var bp_commission=result.Agent_Margin/100;
                var nihar_commission=result.Nihar_Margin/100;
                var discount=Amount*result.Aggregator_Margin/100;
                var bp_earn=discount*bp_commission;
                var nihar_earn=discount*nihar_commission;
                var updamnt = balance-Amount;

               saveelectricitydb(req.session.id1,
                 req.session.username,
                 req.body.custname,
                 body.ipay_id,
                 req.body.account,
                 req.body.Network,
                 req.body.rcamount,
                 'success',
                 'success');
               utlity.transactions(req.session.id1,
                 req.session.username,
                 'ecenter',result.aggregatorCode,
                 result.serviceCode,
                 result.subservicesCode,
                 updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,
                 "Success",
                 final.ipay_id,
                 result.servicesName);
                 res.json(1);
              }else {
                res.json("Please try later");
              }
            }

            // parseString(body, function (err, s) {
            //   var final = s.xml;
            //   console.log(final);
            //   var bp_commission=result.Agent_Margin/100;
            //   var nihar_commission=result.Nihar_Margin/100;
            //   var discount=Amount*result.Aggregator_Margin/100;
            //   var bp_earn=discount*bp_commission;
            //   var nihar_earn=discount*nihar_commission;
            //   var updamnt = balance-Amount;
            //   if(final.res_code){
            //     res.json(1);
            //     saveelectricitydb(req.session.id1,req.session.username,req.body.custname,final.ipay_id[0],req.body.account,req.body.Network,req.body.rcamount,'success',final.ipay_errordesc[0]);
            //
            //     utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",final.ipay_id[0],result.servicesName);
            //
            //   }else {
            //     res.json(final.ipay_errordesc[0]);
            //       utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Fail",final.ipay_errordesc[0],result.servicesName);
            //       saveelectricitydb(req.session.id1,req.session.username,req.body.custname,"Un Bill",req.body.account,req.body.Network,req.body.rcamount,'Fail',final.ipay_errordesc[0]);
            //   }
            // });
          });
        }
        else {
          console.log("ok 5");
          res.json("Insufficient Balance in your Wallet");
        }
      }
     });

  });
}
catch (err) {
  utlity.loggermessage('Unknown error in ele_billpay - utility API:', err);
  return (err, { statusCode: '9999' });
}
})

app.post('/landlinebillfetch', function(req,res){
  if(!req.session.id1){
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
    var x = JSON.parse(JSON.stringify(data1));
    if(err){
      console.log(err);
      res.json("Unable to process your request");
      return false;;
    }
    else{
      if (req.body.Network == "BGL") {
        var custparams = req.body.acnumber+","+req.body.stdcode+req.body.mobilenumber
      }else {
        var custparams =req.body.stdcode+req.body.mobilenumber
      }
      console.log(custparams);
      var outlet = x.OutLetId;
      var date = moment().format('YYYY-MM-DD hh:mm:ss');
      var request = require("request");
            var options = { method: 'POST',
            url: 'https://www.instantpay.in/ws/bbps/bill_fetch',
            headers:
             { 'Cache-Control': 'no-cache',
               'Content-Type': 'application/json' },
            body:
             { token: token,
               request:
                { sp_key: req.body.Network,
                  agentid: req.session.id1,
                  customer_mobile: req.body.ref_number,
                  customer_params: [ req.body.acnumber,req.body.stdcode+req.body.mobilenumber ],
                  init_channel: 'AGT',
                  endpoint_ip: '13.233.92.94', //ip of end
                  mac: 'AD-fg-12-78-GH',
                  payment_mode: 'Cash',
                  payment_info: 'bill',
                  amount: '10',
                  reference_id: '',
                  latlong: '17.400672,78.488174',
                  outletid: outlet } },
            json: true };
            request(options, function (error, response, body) {
              if(error){
                console.log(error);
                res.json("Unable to process your request");
                return false;
              }else{
                console.log(body);
                res.send(body);
              }
            });
      }
   });
})


app.post('/landlinebillpay', function(req,res){
try{
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,req.body.Network,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  utlity.Commissionfun(req.body.Network, function(result) {
    var collection = hart.wallets;
    collection.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,findwalletbal_err+err);
        res.json("Unable to process your request");
        return false;;
      }else{
        var balance = data.Wallet;
        var Amount = req.body.rcamount
        var network = req.body.Network;
        if(balance > Amount){
          var date = moment().format('YYYY-MM-DD hh:mm:ss')
          hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
            var x = JSON.parse(JSON.stringify(data1));
            console.log(x);
            if(err){
              console.log(err);
              res.json("Unable to process your request");
              return false;;
            }
            else{
              var outlet = x.OutLetId;
              var date = moment().format('YYYY-MM-DD hh:mm:ss');
                    var options = { method: 'POST',
                    url: 'https://www.instantpay.in/ws/bbps/bill_pay',
                    headers:
                     { 'Cache-Control': 'no-cache',
                       'Content-Type': 'application/json' },
                       body:
                        { token: token,
                          request:
                           { sp_key: req.body.Network,
                             agentid: req.session.id1,
                             customer_mobile: req.body.ref_number,
                             customer_params: [ req.body.acnumber,req.body.stdcode+req.body.mobilenumber],
                             init_channel: 'AGT',
                             endpoint_ip: '13.233.92.94', //ip of end
                             mac: 'AD-fg-12-78-GH',
                             payment_mode: 'Cash',
                             payment_info: 'bill',
                             amount: req.body.rcamount,
                             reference_id: req.body.ref_id,
                             latlong: '17.400672,78.488174',
                             outletid: outlet } },
                    json: true };
                    request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        var bp_commission=result.Agent_Margin/100;
                        var nihar_commission=result.Nihar_Margin/100;
                        var discount=Amount*result.Aggregator_Margin/100;
                        var bp_earn=discount*bp_commission;
                        var nihar_earn=discount*nihar_commission;
                        var updamnt = balance-Amount;
                        if (body.statuscode == "TXN") {
                          res.json("1");
                          utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",body.data.ipay_id,result.servicesName);
                          saverechargesdb(req.session.id1,req.session.username,body.data.ipay_id,body.data.opr_id,body.data.account_no,body.data.sp_key,body.data.trans_amt,body.data.charged_amt,body.data.datetime,body.data.status,body.statuscode,body.status);
                          utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,suc_msg,transactiion_suc_msg);
                        }else{
                          console.log(body);
                          res.send(body.status);
                        }
                      }
                    });
              }
           });
        }
        else {
          utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,Insufficientbalance);
          res.json("Insufficient Balance in your Wallet");
        }
      }
     });

  });
}
catch (err) {
  utlity.loggermessage('Unknown error in landline bill pay - landline API:', err);
  return (err, { statusCode: '9999' });
}
});




function saveelectricitydb(agentId,agentname,custname,ipayid,serv_id,subservicesCode,amount,status,statusdes) {
  var ele_Reports = hart.electricity_reports;
        var Ereports = new ele_Reports({
          agentId:agentId,
          agentName:agentname,
          customername:custname,
          bill_number:ipayid,
          service_number:serv_id,
          subservicesCode: subservicesCode,
          ChargedAmount:amount,
          Status:status,
          statusdes:statusdes,
        });
        Ereports.save(function (err, results) {
          if(err){
            console.log(err);
          }else{
            console.log("Recharge Status updated");
          }
        });
}

app.post('/getele_Electricity_Agent',function(req,res) {
  hart.electricity_reports.find({agentId:req.session.Agentid }).exec(function(err, data){
    res.json(data);
  });
});

app.post('/getele_ND',function(req,res) {
    hart.electricity_reports.find({}).exec(function(err,data) {
      res.json(data);
    });
  });

app.post('/getElectricityId',function(req,res){
  var obj = ObjectId(req.body.electricityId);
  hart.electricity_reports.findOne({_id:obj},function(err,doc){
    if(err){
      console.log(err);
    }else {
      res.json(doc);
    }
  });
});

app.post('/GetAllElecOrderReports',function(req,res){
  var doc = req.body.orderNum;
    hart.electricity_reports.findOne({orderNum:doc},function(err,doc){
    if(err){
      console.log(err);
    }else {
      res.json(doc)
    }
    });
  });
}




//
// var options = { method: 'POST',
// url: 'https://www.instantpay.in/ws/bbps/bill_fetch',
// headers:
//  { 'Cache-Control': 'no-cache',
//    'Content-Type': 'application/json' },
// body:
//  { token: token,
//    request:
//     { sp_key: "MDE", //req.body.spkey
//       agentid: "MAHA00000MAH01",   //req.session.id1
//       customer_mobile: "9909999999",   //req.body.mobile
//       customer_params: ["021660000094", "4405"  ], //consumer number req.body.account   // and 4405 is Bu for Maharastra only
//       init_channel: 'AGT',
//       endpoint_ip: '13.233.92.94', //ip of end
//       mac: 'AD-fg-12-78-GH',
//       payment_mode: 'Cash',
//       payment_info: '11',
//       amount: '10',
//       reference_id: '',
//       latlong: '17.400672,78.488174',
//       outletid: outlet } },
// json: true };
