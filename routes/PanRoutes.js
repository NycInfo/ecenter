var hart= require('../hart');
var token = hart.token;
var utlity = require('./utlity');
var moment = require('moment');
var mongoose = require('mongoose');
var url = hart.url;
var request = require('request');
var http = require("http");     //For Otp service
var ObjectId = require('mongodb').ObjectID;

const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss')
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";



module.exports = function(app){

app.post('/VerifyOutlet',function(req,res){
      var options = { method: 'POST',
        //url: 'https://www.instantpay.in/ws/outlet/sendOTP',  //old API url
        url: 'https://www.instantpay.in/ws/outlet/registrationOTP',
        // qs:
        //  { token: token,
        //    mobile: req.body.MobileNumber },
        headers:
         {
           'cache-control': 'no-cache',
           accept: 'application/json',
           'content-type': 'application/json' },
        body:
         { token: token,
           request: { mobile: req.body.MobileNumber } },
        json: true };
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.json(body);
        console.log(body);
      });
})


app.post('/RegisterOulet',function(req,res){
    var options = { method: 'POST',
    // url: 'https://www.instantpay.in/ws/outlet/register',
    url: 'https://www.instantpay.in/ws/outlet/registration',
    headers:
     {
       'content-type': 'application/json' },
    body:
     { token: token,
       request:
        { mobile: req.body.mobile,
          email: req.body.Email,
          company: req.body.Company,
          name: req.body.Name,
          pan: req.body.pannumber,
          pincode: req.body.PinCode,
          address: req.body.Address,
          otp: req.body.otp
          // store_type: req.body.Email,
          } },
    json: true };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      if (body.statuscode == "TXN") {
          saveoutlets(req.session.id1,body.data.outlet_id,req.body.pannumber);
      }
      res.json(body);
      console.log(body);
    });
})

app.post('/Getkyc',function(req,res){
      var options = { method: 'POST',
        url: 'https://www.instantpay.in/ws/outlet/requiredDocs',
        headers:
         {
           accept: 'application/json',
           'content-type': 'application/json'
         },
        body:
         { token: token,
           request: {
             outletid: req.body.outletId,
             pan_no: req.body.pannumber
           }
          },
        json: true };

      request(options, function (error, response, body) {
        if(error){
          console.log(error);
        }else{
          console.log(body);
          res.json(body);
        }
      });
})
app.post('/UploadDocument',function(req,res){
  console.log(req.body);
  var agentid = req.body.agentid;
  var pannumber = req.body.pannumberdp;
  var outletid = req.body.outletdisdp;
  var obj= ObjectId(agentid);
  hart.agents.findOne({ '_id':obj },function(err,data1) {
    if(!err){
      var adarfileame = data1.Aadhar;
      var photofilename = data1.Photo;
      var panfilename = data1.PANCard;
      var adarfilelink = "https://www.niharecenter.com/Public2/images/EImages/"+adarfileame;
      var photofilelink = "https://www.niharecenter.com/Public2/images/EImages/"+photofilename;
      var panfilelink = "https://www.niharecenter.com/Public2/images/EImages/"+panfilename;
      if (req.body.status == "twoneed") {
        console.log("Two Need");
        var adarid  = req.body.requiredidforadar;
        var photoid = req.body.requiredidforphoto;
        var options = { method: 'POST',
          url: 'https://www.instantpay.in/ws/outlet/uploadDocs',
          headers:
           {
             'content-type': 'application/json'
           },
          body:
           { token: token,
             request:
               { outletid: outletid,
                pan_no: pannumber,
                document:
                 { id: adarid,
                   link:adarfilelink,
                   filename: adarfileame
                 }
               }
             },
          json: true };
          request(options, function (error, response, body) {
            if (!error) {
              console.log(body);
              var options = { method: 'POST',
                url: 'https://www.instantpay.in/ws/outlet/uploadDocs',
                headers:
                 {
                   'content-type': 'application/json'
                 },
                body:
                 { token: token,
                   request:
                     { outletid: outletid,
                      pan_no: pannumber,
                      document:
                       { id: photoid,
                         link:photofilelink,
                         filename: photofilename
                       }
                     }
                   },
                json: true };
                request(options, function (error, response, body1) {
                  if (!error) {
                    console.log(body1);
                    res.json(body1);
                  }else{
                    console.log(error);
                  }
                });
            }else{
              console.log(error);
            }
          });
      }else {
        console.log("One Need");
        if (req.body.requiredfile == "photofile") {
          var docid = req.body.requiredid;
          var docfilelink = photofilelink;
          var docfileame = photofilename;
        }else if (req.body.panfilefile == "panfile") {
          var docid = req.body.requiredid;
          var docfilelink = panfilelink;
          var docfileame = panfilename;
        }
        else {
          var docid = req.body.requiredid;
          var docfilelink = adarfilelink;
          var docfileame = adarfileame;
        }
        console.log(req.body.requiredfile);
        console.log("file link: "+docfilelink);
        var options = { method: 'POST',
          url: 'https://www.instantpay.in/ws/outlet/uploadDocs',
          headers:
           {
             'content-type': 'application/json'
           },
          body:
           { token: token,
             request:
               { outletid: outletid,
                pan_no: pannumber,
                document:
                 { id: docid,
                   link:docfilelink,
                   filename: docfileame
                 }
               }
             },
          json: true };
          request(options, function (error, response, body1) {
            if (!error) {
              console.log(body1);
              res.json(body1)
            }else{
              console.log(error);
            }
          });
      }
    }else{
      res.json("0");
    }

  })
})




app.post('/PanServcieAgent',function(req,res){
  console.log(req.body);
      var panoutlets = hart.panoutlets;
      panoutlets.findOne({ agentId : req.body.aid},function(err,resp){
        if (resp == null || resp == "" || resp == undefined) {
          res.json("5");
          return false;
        }
        else if(resp.utilodinid == ""){
          var options = { method: 'POST',
            url: 'https://www.instantpay.in/ws/utipan/psa_registration',
            headers:
             { 'Postman-Token': 'fd0ecc3a-f5a5-4c79-bd56-8b2cdd914a94',
               'Cache-Control': 'no-cache',
               'Content-Type': 'application/json' },
            body:
             { token: token,
               request: { outletid: resp.OutLetId }
             },
            json: true };
          request(options, function (error, response, body) {
            if (error) throw new Error(error);

            if(body.statuscode == "TXN"){
              var loginutiid = body.data.psa_uti_login_id;
              var outletid = resp.OutLetId;
              var agentid = req.body.aid;
              panoutlets.updateOne({ 'agentId':agentid },{ $set:{ utilodinid: loginutiid }}, function(err, response) {
                if(!err){
                  res.json("1");
                  console.log("Uti updated");
                }else{
                  res.json("0");
                  console.log("Uti Not Updated");
                }
              });
            }
          });
        }
        else{
          //console.log(resp);
          res.json(resp);
        }
      })
})

app.post('/Buypantoken', function(req,res){
    if(!req.session.id1){
      next({ type: "UnauthorizedError", message: "Session not found" });
      return;
    }
    var wallets = hart.wallets;
    wallets.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        console.log(err);
        res.json("Unable to process your request");
        return false;;
      }
      else{
      //  console.log(data.Wallet);
        var balance = parseFloat(data.Wallet);
        var Amount = parseFloat(200);
        if(balance > Amount){
          var request = require("request");
          var options = { method: 'GET',
            url: 'https://www.instantpay.in/ws/api/transaction',
            qs:
             { token: token,
               agentid: req.session.id1,
               spkey: "PAN",
               account:req.body.outletid,
               outletid:req.body.outletid,
               amount:"107",
               //mode:"VALIDATE"
            },
            headers:
             { accept: 'application/json',
               'content-type': 'application/json',
               'cache-control': 'no-cache' } };

          request(options, function (error, response, body) {
            console.log(body);
            if(!error){
              var string = JSON.stringify(body);
              var daata = JSON.parse(body);
              if(daata.res_code == "TXN"){
                savepanreports(req.session.id1,req.body.applicantname,req.body.applicantnumber,req.body.outletid,daata.ipay_id,daata.opr_id,daata.account_no,daata.trans_amt,daata.charged_amt,daata.datetime,daata.status,daata.res_mesg,req.body.applicantname,req.body.applicantnumber);
                var updamnt = balance - Amount;
                var discount=90;
                var bp_commission=0.8;
                var nihar_commission=0.2;
                var bp_earn=discount*bp_commission;
                var nihar_earn=(discount*nihar_commission)+3; //3rs is instant giving commission for nihar
                // var agentcommision = 60;
                // var niharcommision = 33;
                utlity.transactions(req.session.id1,req.session.username,'ecenter','INP','PAN','PAN',updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",daata.ipay_id,'PAN');

                // utlity.transactions(req.session.id1,"Pan Service",parseFloat(TotalAMount),200,parseFloat(agentcommision),parseFloat(niharcommision),"Success",0);
              }else{
                res.json("Please check with cc team");
              }
            }else{
              if(error){
                console.log(error);
                res.json("Unable to process your request");
                return false;;
              }
            }
          });
        }
        else {
          res.json("Insufficient Balance in your Wallet");
        }
      }
     });
})



function savepanreports(agentId,app_name,app_number,outletid,ipay_id,opr_id,account_no,trans_amt,charged_amt,datetime,resstatus,res_mesg,applicantname,applicantnumber){
  var Pantokens = hart.pantokens;
  var pantokens = new Pantokens({
    agentId:agentId,
    app_name:app_name,
    app_number:app_number,
    outletid:outletid,
    ipay_id:ipay_id,
    opr_id:opr_id,
    account_no:account_no,
    trans_amt:trans_amt,
    charged_amt:charged_amt,
    datetime:datetime,
    resstatus:resstatus,
    res_mesg:res_mesg,
    applicantname:applicantname,
    applicantnumber:applicantnumber,
    Status:1,
  });
  pantokens.save(function (err, results) {
    if(err){
      console.log(err);
    }
   // res.json(1);
  });

}
app.post('/GetUTIDetails',function(req,res){
      hart.panoutlets.findOne({ agentId : req.session.id1},function(err,resp){
        if(!err){
          res.json(resp);
        }else{
          res.json("0");
        }
      })
})

function sendmessage(mobile,txt){
    var options = { method: 'GET',
    url: 'http://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
    qs:
     { type: 'smsquicksend',
       user: 'niharinfo',
       pass: 'Nihar@123',
       sender: 'NIHAAR',
       to_mobileno: mobile,
       sms_text: txt },
    headers:
     {
       'cache-control': 'no-cache' } };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    console.log(body);
  });

}




function saveoutlets(x,y,z){
  var PanOutlets = hart.panoutlets;
    PanOutlets.findOne({ agentId : x},function(err,resp){
      if(resp == null){
        var panoutlets = new PanOutlets({
          agentId:x,
          PanNumber:y,
          OutLetId:z,
          utilodinid:"",
          Status:1,
        });
        PanOutlets.save(function (err, results) {
          if(err){
            console.log(err);
          }
         // res.json(1);
        });
      }
      else{
        console.log("Already There");
      }
   });
}


}
