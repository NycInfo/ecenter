const crypto = require('crypto');
const hart= require('../hart');
const moment = require('moment');
//const session = require('express-session');
const mongoose = require('mongoose');
const mongoXlsx = require('mongo-xlsx');
const MongoClient = require('mongodb').MongoClient;
const url = hart.url;
const emovies =hart.emovies;
//console.log(emovies);
const request = require('request');
const qs = require("querystring");  //For Otp service
const http = require("http");     //For Otp service
const ObjectId = require('mongodb').ObjectID;
const utlity = require('./utlity');
const movieservices = require('./movieservices');

const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss')
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";
const findwalletbal_err = "Error In Find Wallet Balance";
const errorinapireq_err = "Error In Requesting API";
const transactiion_suc_msg = "Transaction Success";
const transactiion_fail_msg = "Transaction Fail";
const Insufficientbalance = "In Insufficient Balance";
const sessionout = "Session Out";


module.exports = function(app){

app.post('/AddMovie',function(req,res){
  var EMovies = hart.emovies;
  hart.emovies.count({},function(err,doc){
    if (err) {
      console.log(err);
      res.json('err');
    }else {
      var emovies = new EMovies({
        movie_id: doc+1,
        uniq: "",
        genre_id: req.body.Genre,
        movie_name: req.body.Name,
        seo_keywords: "",
        seo_description: "",
        Music: "",
        description: req.body.Description,
        stars: req.body.Casting,
        director: req.body.Director,
        trailer_id: req.body.Trailer,
        video_id: "",
        image: "",
        banner: req.body.Banner,
        is_slide: "",
        movie_length: req.body.Movie_Length,
        release_date: req.body.Release_Date,
        language: req.body.Language,
        price: req.body.Price,
        industry:req.body.Industry,
        status: "active",
        movie_views: '',
        metatitle: "",
        downloadUrl: req.body.Download_URL,
        thumbnailUrl: req.body.Thumbnail,
        fileName: '',
        fileSize: "",
        purchases:{ type: Array, default: [] },
        resolutions:{ type: Array, default: [] }
      });
      emovies.save(function(err1,results) {
        if (err1) {
          console.log(err1);
          res.json('err');
        }else {
          console.log("Saved ");
          res.json('1');
        }
      });
    }

  });

})

app.post('/MovieProfileDetails1', function(req,res){
    var id= ObjectId(req.body.id);
    hart.emovies.findOne({ _id : id },function(err,data){
      res.json(data);
     });
})
app.post('/Deletemovie', function(req,res){
    var id= ObjectId(req.body.aid);
    hart.emovies.remove( { _id:id }, function(err, doc) {
      if (err) {
         console.log(err);
         res.json("0");
      }
      else{
        res.json("1");
      }
    });

})
app.post('/Editmovie', function(req,res){
        var docs={
          genre_id: req.body.Genre,
          movie_name: req.body.Name,
          description: req.body.Description,
          stars: req.body.Casting,
          director: req.body.Director,
          trailer_id: req.body.Trailer,
          banner: req.body.Banner,
          movie_length: req.body.Movie_Length,
          release_date: req.body.Release_Date,
          language: req.body.Language,
          price: req.body.Price,
          industry:req.body.Industry,
          downloadUrl: req.body.Download_URL,
          thumbnailUrl: req.body.Thumbnail,
        };
        var id = ObjectId(req.body.Movieid);
        hart.emovies.updateOne({ '_id':id },{ $set:docs}, function(err, data) {
          if(err){
            res.json("0");
            console.log(err)
          }else {
            res.json("11");
          }
        })
  })
app.post('/savemovies',function(req,res){
  const EMovies = hart.emovies;
  const emovies = new EMovies({
    movie_id:"0001",
    uniq:"00001",
    genre_id:"1",
    movie_name:"Attharintiki Daredhi",
    seo_keywords:"string",
    seo_description:"string",
    Music : "Anub",
    description:"string",
    stars:"string",
    director:"Puru",
    trailer_id:"string",
    video_id:"string",
    image:"string",
    banner:"string",
    is_slide:"string",
    movie_length:"string",
    release_date:"string",
    language:"string",
    price:150,
    status:"string",
    movie_views:"string",
  });
  emovies.save(function(err,results) {
    console.log("Saved ")
  });
})
app.post('/getMovies_dash',function(req,res){
         hart.emovies.find({}).exec(function(err, data){
           res.json(data);
           // console.log(data);
         });
})
app.post('/GetMovies',function(req,res){

  //console.log(req.session.auth_token);
  //req.session.auth_token = emoviesservices.getauth_token(req.session.user,req.session.password)
//path: "/api/web/index.php/v1/message/save-message",
//https://learn-angular-mittu-spidey.c9users.io/api/emovies/list
  var request = require("request");
  var options = { method: 'GET',
  //  url: 'http://13.126.135.52:8080/api/emovies/all',
   url:hart.emoviesurl+'/list',
    headers:
     { 'Postman-Token': '72a9475c-8540-4d7d-96d2-6179896c4157',
       'Cache-Control': 'no-cache',
       token: '987654321' } };
  request(options, function (error, response, body) {
      if (error) throw new Error(error);
      res.json(JSON.parse(body));
  });
})

app.post('/GetMovieDetails',function(req,res){
  utlity.loggermessage(req.session.id1,req.session.username,"Getting Movie list",datenow,ser_sart_msg,"list of movies API ");
  const movieid = req.body.movieid;
  const options = { method: 'GET',
  url: hart.emoviesurl+"/"+movieid,
  headers: movieservices.headersforapi() };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.json(JSON.parse(body))
  });
})

app.post('/Purchasetokenfor_movie',function(req,res){
  var options = { method: 'POST',
    url:hart.emoviesurl+'/purchase/make',
    headers:
     { 'Postman-Token': '83895890-467f-4d19-a0ac-9dc87b1f002f',
       'Cache-Control': 'no-cache',
       Authorization: 'Bearer '+req.session.auth_token,
       'Content-Type': 'application/json' },
    body:
     { name: req.body.cust_name,
       phone: req.body.cust_number,
       movieId: req.body.movieid,
       agentId: req.session.id1 },
    json: true };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    if(body == true){
      res.json("1")
    }else{
      res.json(body);
    }
  });
});
}
