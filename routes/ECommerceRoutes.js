var crypto = require('crypto');
var hart= require('../hart');
//var sqlcon = hart.sql;
var utlity = require('./utlity');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectID;
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var request = require('request');
var qs = require("querystring");  //For Otp service
var http = require("http");     //For Otp service


module.exports = function(app){


app.post('/LatestProducts',function(req,res){
  hart.mongo.connect(url,function(err,db){
    if(err){
      return console.dir(err);
    }
    var collection = db.collection('products');
    var query = {Status:1};
    collection.find(query).limit(5).toArray(function(err, data){

     res.json(data);
    });
   });
})


app.post('/getECIndex',function(req,res){
  hart.mongo.connect(url,function(err,db){
    if(err){
      return console.dir(err);
    }
    var collection = db.collection('ecategories');
    collection.find({Status:1}).toArray(function(err, data){
      if(!err){
      //console.log(data);
     res.json(data);
     }
     else{
       console.log(err);
     }
    });
   });
})

app.post('/getECSubC',function(req,res){
  hart.mongo.connect(url,function(err,db){
    if(err){
      return console.dir(err);
    }
    var collection = db.collection('esubcategories');
    collection.find({CategoryId:req.body.id}).toArray(function(err, data){
     res.json(data);
    });
   });
})

app.post('/viewallproducts',function(req,res){
  hart.mongo.connect(url,function(err,db){
    if(err){
      return console.dir(err);
    }
    var collection = db.collection('products');
    var query = {SubCategory:req.body.id};
    collection.find(query).toArray(function(err, data){
      //console.log(data);
     res.json(data);
    });
   });
})

app.post('/productdetails',function(req,res){
  hart.mongo.connect(url,function(err,db){
    if(err){return console.dir(err);}
    var collection = db.collection('products');
    var obj= ObjectId(req.body.id);
    collection.findOne({'_id':obj},function(err,doc1){
     if(err){
          res.json(0);
      }else{
        res.json(doc1);
        //console.log(doc1);
      }
     });
   });
})

var ProductOrdersSchema = mongoose.Schema({
  AgentId:"string",
  ProductId:"string",
  ProductName:"string",
  SkuNumber:"string",
  Amount:{ type: Number},
  Quantity:"string",
  BillNumber:"string",
  Status:{ type: Number, min: 1, max: 1 },
  OrderDate:{ type: Date, default: Date.now },
});
app.post('/Orderproduct', function(req,res){

  var OrderProducts = mongoose.model('OrderProducts', ProductOrdersSchema);
  hart.mongo.connect(url,function(err,db){
    var orderproducts = new OrderProducts({
      AgentId:req.session.id1,
      ProductId:req.body.productId,
      ProductName:req.body.productName,
      SkuNumber:req.body.skunumber,
      Amount:req.body.Amount,
      Quantity:req.body.Quantity,
      BillNumber:randpwd(),
      Status:1,
    });
    orderproducts.save(function (err, results) {
      if(err){
        console.log(err);
        res.json(0);
      }
      else{
        res.json(results);
      }
    });
   });
})


app.post('/orderproductdetails',function(req,res){
  hart.mongo.connect(url,function(err,db){
    if(err){return console.dir(err);}
    var collection = db.collection('orderproducts');
    var obj= ObjectId(req.body.id);
    console.log(obj);
    collection.findOne({'_id':obj},function(err,doc1){
     if(err){
          res.json(0);
      }else{
        res.json(doc1);
        //console.log(doc1);
      }
     });
   });
})

function randpwd(){
    //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789@$!";
    var chars = "id";
    var string_length = 8;
    var randomstring = '';
    var charCount = 0;
    var numCount = 0;

    for (var i=0; i<string_length; i++) {
        // If random bit is 0, there are less than 3 digits already saved, and there are not already 5 characters saved, generate a numeric value.
        if((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
            var rnum = Math.floor(Math.random() * 10);
            randomstring += rnum;
            numCount += 1;
        } else {
            // If any of the above criteria fail, go ahead and generate an alpha character from the chars string
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum,rnum+1);
            charCount += 1;
        }
    }
    return randomstring;
}

}
