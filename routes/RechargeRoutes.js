var crypto = require('crypto');
var hart= require('../hart');
var utlity = require('./utlity');
var moment = require('moment');
var mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectID;
var url = hart.url;
var token = hart.token;
var request = require('request');
var http = require("http");     //For Otp service
const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss');
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";
const findwalletbal_err = "Error In Find Wallet Balance";
const errorinapireq_err = "Error In Requesting API";
const transactiion_suc_msg = "Transaction Success";
const transactiion_fail_msg = "Transaction Fail";
const Insufficientbalance = "In Insufficient Balance";
const sessionout = "Session Out";
module.exports = function(app){

app.post('/MobileRecharge', function(req,res){
  try{
  utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,ser_sart_msg,"Amount = "+req.body.rcamount+" ");
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,req.body.Network,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  utlity.Commissionfun(req.body.Network, function(result) {
    var collection = hart.wallets;
    collection.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,findwalletbal_err+err);
        res.json("Unable to process your request");
        return false;
      }else{
        var balance = data.Wallet;
        var Amount = req.body.rcamount
        var network = req.body.Network;
        if(balance > Amount){
          var date = moment().format('YYYY-MM-DD hh:mm:ss')
          var options = { method: 'GET',
            url: 'https://www.instantpay.in/ws/api/transaction',
            qs:
             { token: token,
               agentid: req.session.id1,
               amount: req.body.rcamount,
               spkey: req.body.Network,
               account: req.body.mobilenumber,
               optional1:req.body.stdcode,
               optional2:req.body.acnumber,
             },
            headers:
             { accept: 'application/json',
               'content-type': 'application/json',
               'cache-control': 'no-cache' } };
          request(options, function (error, response, body) {
            if(error){
              utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,errorinapireq_err+error);
              res.json("Unable to process your request");
              return false;;
            }
            var gdgdg = JSON.parse(body);
            var bp_commission=result.Agent_Margin/100;
            var nihar_commission=result.Nihar_Margin/100;
            var discount=Amount*result.Aggregator_Margin/100;
            var bp_earn=discount*bp_commission;
            var nihar_earn=discount*nihar_commission;
            var updamnt = balance-Amount;
            console.log(gdgdg);
            if(gdgdg.ipay_id){
              res.json("1");
              utlity.transactions(req.session.id1,
                req.session.username,
                'ecenter',
                result.aggregatorCode,
                result.serviceCode,
                result.subservicesCode,
                updamnt,
                Amount,
                discount,
                bp_commission,
                nihar_commission,
                bp_earn,
                nihar_earn,
                "Success",
                gdgdg.ipay_id,
                result.servicesName);
              saverechargesdb(req.session.id1,req.session.username,gdgdg.ipay_id,gdgdg.opr_id,gdgdg.account_no,gdgdg.sp_key,gdgdg.trans_amt,gdgdg.charged_amt,gdgdg.datetime,gdgdg.status,gdgdg.res_code,gdgdg.res_msg);
              utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,suc_msg,transactiion_suc_msg);
            }
            else if(gdgdg.ipay_errorcode == "DTX" || gdgdg.ipay_errorcode == "SPE" || gdgdg.ipay_errorcode == "SPD"|| gdgdg.ipay_errorcode == "IAN" || gdgdg.ipay_errorcode == "IRA" || gdgdg.ipay_errorcode == "DTB" || gdgdg.ipay_errorcode == "RPI" || gdgdg.ipay_errorcode == "UAD" || gdgdg.ipay_errorcode == "IAC" || gdgdg.ipay_errorcode == "IAB" || gdgdg.ipay_errorcode == "AAB" || gdgdg.ipay_errorcode == "ISP" || gdgdg.ipay_errorcode == "DID" || gdgdg.ipay_errorcode == "RBT" || gdgdg.ipay_errorcode == "UED" || gdgdg.ipay_errorcode == "IEC" || gdgdg.ipay_errorcode == "IRT" || gdgdg.ipay_errorcode == "ITI" || gdgdg.ipay_errorcode == "TSU" || gdgdg.ipay_errorcode == "IPE" || gdgdg.ipay_errorcode == "ISE" || gdgdg.ipay_errorcode == "TRP" || gdgdg.ipay_errorcode == "SNA"){
              res.json(gdgdg.ipay_errordesc);
          //    utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Fail",0,result.servicesName);
            //  utlity.transactions(req.session.id1,network,updamnt,Amount,0,nihar_com,"Fail",agent_commission);
          //    saverechargesdb(req.session.id1,req.session.username,"ipay_id","opr_id",req.body.mobilenumber,req.body.Network,req.body.rcamount,0,date,"Fail",gdgdg.ipay_errorcode,gdgdg.ipay_errorcode);
              utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,transactiion_fail_msg+gdgdg.ipay_errordesc);
            }
            else{
              res.json("0");
            }
          });
        }
        else {
          utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,Insufficientbalance);
          res.json("Insufficient Balance in your Wallet");
        }
      }
     });

  });
  }
  catch (err) {
    utlity.loggermessage('Unknown error in Mobile Reacharge - Recharge API:', err);
    return (err, { statusCode: '9999' });
  }
})


app.post('/landlinebillfetch123', function(req,res){
  if(!req.session.id1){
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
    var options = { method: 'GET',
      url: 'https://www.instantpay.in/ws/api/transaction',
      qs:
       { token: token,
         agentid: req.session.id1,
         amount: "10",
         spkey: req.body.Network,
         account: req.body.mobilenumber,
         optional1:req.body.stdcode,
         optional2:req.body.acnumber,
         mode:"VALIDATE",
          },
      headers:
       { accept: 'application/json',
         'content-type': 'application/json',
         'cache-control': 'no-cache' } };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      var gdgdg = JSON.parse(body);
      res.json(gdgdg);
    });
})


app.post('/postpaidibllpay', function(req,res){
  try{
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,req.body.Network,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  utlity.Commissionfun(req.body.Network, function(result) {
    var collection = hart.wallets;
    collection.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,findwalletbal_err+err);
        res.json("Unable to process your request");
        return false;;
      }else{
        var balance = data.Wallet;
        var Amount = req.body.rcamount
        var network = req.body.Network;
        if(balance > Amount){
          var date = moment().format('YYYY-MM-DD hh:mm:ss')
          hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
            var x = JSON.parse(JSON.stringify(data1));
            if(err){
              console.log(err);
              res.json("Unable to process your request");
              return false;;
            }
            else{
              var outlet = x.OutLetId;
              var date = moment().format('YYYY-MM-DD hh:mm:ss');
                    var options = { method: 'POST',
                    url: 'https://www.instantpay.in/ws/bbps/bill_pay',
                    headers:
                     { 'Cache-Control': 'no-cache',
                       'Content-Type': 'application/json' },
                    body:
                     { token: token,
                       request:
                        { sp_key: req.body.Network,
                          agentid: req.session.id1,
                          customer_mobile: req.body.mobilenumber,
                          customer_params: [ req.body.mobilenumber ], //consumer number
                          init_channel: 'AGT',
                          endpoint_ip: '13.233.92.94', //ip of end
                          mac: 'AD-fg-12-78-GH',
                          payment_mode: 'Cash',
                          payment_info: '11',   //bill for live  //11 for uat
                          amount: req.body.rcamount,
                          reference_id: '',
                          latlong: '17.400672,78.488174',
                          outletid: outlet } },
                    json: true };
                    request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        var bp_commission=result.Agent_Margin/100;
                        var nihar_commission=result.Nihar_Margin/100;
                        var discount=Amount*result.Aggregator_Margin/100;
                        var bp_earn=discount*bp_commission;
                        var nihar_earn=discount*nihar_commission;
                        var updamnt = balance-Amount;
                        if (body.statuscode == "TXN") {
                          res.json("1");
                          utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",body.data.ipay_id,result.servicesName);
                          saverechargesdb(req.session.id1,req.session.username,body.data.ipay_id,body.data.opr_id,body.data.account_no,body.data.sp_key,body.data.trans_amt,body.data.charged_amt,body.data.datetime,body.data.status,body.statuscode,body.status);
                          utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,suc_msg,transactiion_suc_msg);
                        }else{
                          console.log(body);
                          res.send(body.status);
                        }
                      }
                    });
              }
           });
        }
        else {
          utlity.loggermessage(req.session.id1,req.session.username,req.body.Network,datenow,fail_msg,Insufficientbalance);
          res.json("Insufficient Balance in your Wallet");
        }
      }
     });

  });
}
catch (err) {
  utlity.loggermessage('Unknown error in postpaidibllpay - postpaid  API:', err);
  return (err, { statusCode: '9999' });
}
});


  function saverechargesdb(agentid,agentname,orderid,operatorid,mobilenumber,servicekey,TransactionAmount,ChargedAmount,time,status,responcecode,responsemsg){
      var RechargeReports = hart.rechargereports;
      var Rechargereports = new RechargeReports({
        agentId:agentid,
        agentName:agentname,
        orderid:orderid,
        operatorid:operatorid,
        MobileNumber:mobilenumber,
        servicekey: servicekey,
        TransactionAmount:TransactionAmount,
        ChargedAmount:ChargedAmount,
        RechargeTime:time,
        Status:status,
        Responcecode:responcecode,
        responsemsg:responsemsg,
      });
      Rechargereports.save(function (err, results) {
        if(err){
          console.log(err);
        }else{
          //res.json("ok");
          console.log("Recharge Status updated");
        }
      });
}

app.post('/GetAgentRcReports',function(req,res){
    hart.rechargereports.find({agentId:req.session.Agentid}).sort({_id:-1}).exec(function(err, data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
});

app.post('/getagentbusresults',function(req,res){
    hart.bustickets.find({AgentId:req.session.Agentid}).sort({_id:-1}).exec(function(err, data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
});

app.post('/GetRcReports_Agent',function(req,res){
  if(req.body.fromdate != "" && req.body.todate != ""){
    console.log("condition - 1");
    var fromdate = req.body.fromdate;
    var todate = req.body.todate;
    var enddate = utlity.addoneday(todate);
    hart.rechargereports.find({$and:[{ "agentId": req.session.Agentid },{CreateDate:{$gte:fromdate}},{CreateDate:{$lte:enddate}}]}).exec(function(err,data){
      if (!err) {
        res.json(data)
      }else {
        console.log(err);
      }
    });
  }else {
    console.log("condition-2");
    hart.rechargereports.find({agentId:req.session.Agentid}).sort({CreateDate:-1}).exec(function(err, data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
  }
});



app.post('/GetRcReports_Nihar',function(req,res){
  //console.log(req.body);
    hart.rechargereports.find({'agentId':req.body.id}).exec(function(err, data){
      if(err){
        console.log(err);
      }
      else{console.log(data);
        res.json(data);
      }
     });
});

app.post('/GetNiharAgentBusBookingReports',function(req,res){
      hart.bustickets.find({AgentId:req.body.id}).exec(function(err, data){
        if(err){
          console.log(err);
        }
        else{
          res.json(data);
        }
       });
});
app.post('/GetAllRcReports',function(req,res){
  console.log(req.body);
  if(req.body.fromdate != "" && req.body.todate != ""){
    console.log("condition - 1");
    var fromdate = req.body.fromdate;
    var todate = req.body.todate;
    var enddate = utlity.addoneday(todate);
    hart.rechargereports.find({$and:[{CreateDate:{$gt:fromdate}},{CreateDate:{$lte:enddate}}]}).exec(function(err,data){
      if (!err) {
        res.json(data)
      }else {
        console.log(err);
      }
    });
  }else {
    console.log("condition-2");
    hart.rechargereports.find({}).sort({CreateDate:-1}).exec(function(err, data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
  }
});
app.post('/getallserv',function(req,res){
    hart.sub_services.find({Status:1}).exec(function(err, data){
     res.json(data);
    });
})

app.post('/getsingleSUBsrv',function(req,res){
    var id= ObjectId(req.body.id);
    hart.sub_services.find({$and:[{Status:1},{_id:id}]}).exec(function(err, data){
     res.json(data);
    });
})
app.post('/getPostPaidSingleImages',function(req,res){
    var id= ObjectId(req.body.aid);
    hart.sub_services.findOne({$and:[{Status:1},{_id:id}]}).exec(function(err,data){
      res.json(data);
     });
});

app.post('/GetAllRcReportsID',function(req,res){
  var doc = ObjectId(req.body.rechargeId);
    hart.rechargereports.findOne({_id:doc},function(err,doc){
      if(err){
        console.log(err);
      }else {
        res.json(doc)
      }
    });
});
app.post('/GetAllOrderReports',function(req,res){
  var doc = req.body.orderid;
    hart.rechargereports.findOne({orderid:doc},function(err,doc){
      if(err){
        console.log(err);
      }else {
        res.json(doc)
      }
    });
});
}
