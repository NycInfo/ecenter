//var nodemailer  = require('nodemailer');
var async = require('async');
var hart= require('../hart');
var sqlcon = hart.sql;
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
// var url = 'mongodb://localhost:27017/niharmarket';
//var url = 'mongodb://admin:admin@ds251217.mlab.com:51217/niharinfo';
//var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.cmsImgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });

module.exports = function(app){

// app.use(session({
//     secret: 'I9Admin-Flag',
//     resave: true,
//     saveUninitialized: true,
//     name : 'I9Admin',
// }));

var ECategorySchema = mongoose.Schema({
  CategoryName : "String",
  TaxRate : {type: Number},
  CategoryImg : [],
  Status:{ type: Number, min: 1, max: 1 },
  CreateDate:{ type: Date, default: Date.now },
});
app.post('/insertCategory',upload.any(),function(req,res){
  var fileslength = req.files.length;
  var vfvf = [];
  if(fileslength>0){
    for (var i in req.files) {
      var filddd = req.files[i].filename;
      vfvf.push(filddd);
    }
  }
  var eCategories = mongoose.model('ECategories', ECategorySchema);
    hart.mongo.connect(url,function(err,db){
      var ecategories = new eCategories({
        CategoryName:req.body.CategoryName,
        TaxRate:req.body.Taxrate,
        CategoryImg:vfvf,
        Status:1,
      });
      ecategories.save(function (err, results) {
        if(err){
          console.log(err);
          res.json(0);
        }
        else{
          res.json(1);
        }
      });
     });
})

var ESubCategorySchema = mongoose.Schema({
  SubCategoryName : "String",
  SubCategoryImg : [],
  CategoryId : "String",
  CategoryName : "String",
  Status:{ type: Number, min: 1, max: 1 },
  CreateDate:{ type: Date, default: Date.now },
});
app.post('/insertSubCategory',upload.any(),function(req,res){
  var fileslength = req.files.length;
  var vfvf = [];
  if(fileslength>0){
    for (var i in req.files) {
      var filddd = req.files[i].filename;
      vfvf.push(filddd);
    }
  }
  var ESubCategories = mongoose.model('ESubCategories', ESubCategorySchema);
  hart.mongo.connect(url,function(err,db){
    var ESubcategories = new ESubCategories({
      SubCategoryName : req.body.SubCatName,
      SubCategoryImg:vfvf,
      CategoryId : req.body.CategoryId,
      Status:1,
    });
    ESubcategories.save(function (err, results) {
      if(err){
        console.log(err);
        res.json(0);
      }
      else{
        res.json(1);
      }
    });
   });
})


var productSchema = mongoose.Schema({
    skuid :String,
    productName: String,
    productBrand: String,
    productColor: String,
    productMRP: { type: Number},
    productPrice: { type: Number},
    productDiscount:{ type: Number},
    productQuantity:{ type: Number},
    productTechnicalDetails: String,
    productAdditionalInformation: String,
    productWarrenty: String,
    TaxRate:{ type: Number},
    photo_path:[],
    Category: String,
    SubCategory: String,
    Status: { type: Number},
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
  });
    app.post('/insertProduct',upload.any(),function(req,res){
      var fileslength = req.files.length;
      var vfvf = [];
      if(fileslength>0){
        for (var i in req.files) {
          var filddd = req.files[i].filename;
          vfvf.push(filddd);
        }
      }
      var skuid = req.body.skunum;
      var productName = req.body.product_name;
      var productBrand = "";
      var productColor = "";
      var productMRP = req.body.product_mrp;
      var productPrice = req.body.product_price;
      var productDiscount = req.body.product_discount;
      var productQuantity = req.body.product_quantity;
      var productTechnicalDetails = "";
      var productAdditionalInformation = "";
      var productWarrenty = req.body.product_warranty;
      var TaxRate = req.body.TaxRate;
      var photo_path = vfvf;
      var Category = req.body.product_cat;
      var SubCategory = req.body.product_subcat;
      var Status = 1;
      console.log(req.body.TaxRate);
        var Products = mongoose.model('products', productSchema);
        console.log(req.body);
        hart.mongo.connect(url,function(err,db){
          if(err){return console.dir(err);}
          var collection = db.collection('products');
         collection.findOne({ skuid : req.body.skunum },function(err,data){
           if(err){
                res.json(0);
                console.log(err)
            }
           else{
                if(data = "null"){
                  var product = new Products({
                    skuid :skuid,
                    productName: productName,
                    productBrand: productBrand,
                    productColor: productColor,
                    productMRP: productMRP,
                    productPrice: productPrice,
                    productDiscount:productDiscount,
                    productQuantity:productQuantity,
                    productTechnicalDetails:productTechnicalDetails,
                    productAdditionalInformation:productAdditionalInformation,
                    productWarrenty:productWarrenty,
                    TaxRate:TaxRate,
                    photo_path:photo_path,
                    Category:Category,
                    SubCategory:SubCategory,
                    Status:Status,
                });
                product.save(function (err, results) {
                  if(!err){
                    res.json(1);
                  }
                  else {
                    console.log(err)
                    res.json(0);
                  }

                });
              }
              else{
              res.json(0);
              }
           }
          });
         });

    })



    app.post('/getproducts',function(req,res){
      //console.log("Working");
      hart.mongo.connect(url,function(err,db){
        if(err){
          return console.dir(err);
        }
        var collection = db.collection('products');
        collection.find().toArray(function(err, data){
         res.json(data);

        });
       });

    })
    app.post('/getorderecommerce',function(req,res){
      //console.log("Working");
      hart.mongo.connect(url,function(err,db){
        if(err){
          return console.dir(err);
        }
        var collection = db.collection('orderproducts');
        collection.find().toArray(function(err, data){
         res.json(data);
        });
       });

    })
    app.post('/delete',function(req,res){
      hart.mongo.connect(url, function(err, db) {
        var collection = db.collection('products');
        var del= req.body.id;
        //console.log(del);
        collection.remove( { _id:del }, function(err, doc) {
          if (err) {
               console.log(err);
           }
           else {
             res.json(1);
           }
         });
       });
    })
    app.post('/UpdateDeliveryStatus',function(req,res){
      hart.mongo.connect(url,function(err,db){
          var collection = db.collection('agents');
          var date = Date.now();
          console.log(date);
          console.log(req.body.id);
          var query = { DeliveryStatus: 1 };
          collection.update(query,{ _id : req.body.id },function(err,data){
            if(err){
            console.log(err);}
            else{
              console.log("success");
              res.json(1);
            }

           });
         });
    });



    app.post('/SingleOrderDetails', function(req,res){
      //console.log(req.body.OrderId);
      hart.mongo.connect(url,function(err,db){
        if(err){return console.dir(err);}
        var collection = db.collection('orderproducts');
        var ObjectId = require('mongodb').ObjectID;
        var ordrid= ObjectId(req.body.OrderId);
        collection.findOne({ _id : ordrid },function(err,data){
          //console.log(data);
          res.json(data);
         });
       });
    })
    app.post('/GetProductDetails', function(req,res){
      console.log(req.body.ProductId);
      hart.mongo.connect(url,function(err,db){
        if(err){return console.dir(err);}
        var collection = db.collection('products');
        var ObjectId = require('mongodb').ObjectID;
        var ProductId= ObjectId(req.body.ProductId);
        collection.findOne({ _id : ProductId },function(err,data){
          console.log(data);
          res.json(data);
         });
       });
    })



}
