var hart= require('../hart');
var url = hart.url;
var request = require('request');
var ObjectId = require('mongodb').ObjectID;



module.exports = function(app){


  // app.post('/tokenstatussettings123',function(req,res){
  //   var Tokenstatuss = hart.tokenstatussettings;
  //     var tokenstatuss = new Tokenstatuss({
  //       user: "BP",
  //       current_status: "pending",
  //       next_status: "cancelled",
  //       Status:'1',
  //     });
  //     tokenstatuss.save(function (err, results) {
  //       if(err){
  //         res.json(0);
  //         console.log(err);
  //       }
  //       else{
  //         console.log("Saved");
  //         res.json("1");
  //       }
  //     });
  // })<br>
  app.post('/enquiryform',function(req,res) {
    var body = "Dear Sir/Madam, I am interested in becoming a business partner (BP) of your nihareCenter. ";
    body += " My Contact Details are given below:<br> ";
    body += "<br>Name : "+req.body.name;
    body += "<br>Mobile : "+req.body.mobile;
    body += "<br>Email : "+req.body.email;
    body += "<br>Address : "+req.body.address;
    body += "<br><br><br><br>Looking forward to your response soon."
    body += "<br><br>Thanks";
    body += "<br>"+req.body.name+'<br>';
    var mailOptions = {
      from: req.body.email,
     to: "jagadees.reddi@niharinfo.com",
      subject: "Business Enquiry from "+req.body.name,
      html: body
    };
    hart.transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
        res.json("0");
      } else {
        res.json("1");
        console.log('Email sent: ' + info.response);
      }
    });
  });
  app.post('/getCustAdr_ND',function(req,res) {
    hart.CustAddress.find({}).sort({_id:-1}).exec(function(err,data) {
        res.json(data);
      });
  });
  app.post('/getCustAdr_AD',function(req,res) {
    hart.CustAddress.find({AgentId:req.session.Agentid}).sort({_id:-1}).exec(function(err,data) {
        res.json(data);
      });
  });

  app.post('/Sendmailtocustomerservices',function(req,res){
      var options = { method: 'POST',
      url: hart.customercareraiseticketurl,
      headers:
       { 'Cache-Control': 'no-cache',
         'Content-Type': 'application/x-www-form-urlencoded' },
      form:
       { AgentId: req.session.Agentid,
         AgentName: req.session.username,
         AgentEmail: req.session.useremail,
         Subject: req.body.Subject,
         ServiceName: req.body.ServiceName,
         TransactionId: req.body.TransactionId,
         Message: req.body.Email_Body } };
      request(options, function (error, response, body) {
        if(error){
          res.json(0);
          console.log(err);
        }
        else{
          res.json(1);
        }
      });
  })

  app.post('/Get_AgentTokens',function(req,res){
    console.log(hart.customercaregetticketsbybp);
      var refid = req.session.Agentid;
      var options = { method: 'POST',
        url: hart.customercaregetticketsbybp,
        headers:
         { 'Cache-Control': 'no-cache',
           'Content-Type': 'application/x-www-form-urlencoded' },
        form:
         {
           AgentId: refid
         }
       };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      var dataa = JSON.parse(body);
        res.json(dataa.message);
    });
  })

  app.post('/gettokenstatus',function(req,res){
    hart.tokenstatussettings.find({$and: [{ 'user':req.body.Role},{ 'current_status':req.body.PresentStatus},{'Status':"1"}]}).exec(function(err, doc1) {
      if (!err) {
        res.json(doc1);
      }else{
        console.log(err);
      }
    })
    return false;
  })

  app.post('/Get_AllTokens',function(req,res){
      var options = { method: 'POST',
        url: hart.customercaregetticketsall,
        headers:
         { 'Cache-Control': 'no-cache',
           'Content-Type': 'application/x-www-form-urlencoded' },
        form:
         {
           AgentId: "Admin"
         }
       };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      var dataa = JSON.parse(body);
      res.json(dataa.message);
    });
  })

  app.post('/tokenstatusupdate',function(req,res){
      var options = { method: 'POST',
        url: hart.customercareticketstatusupdate,
        headers:
         { 'Cache-Control': 'no-cache',
           'Content-Type': 'application/x-www-form-urlencoded' },
        form:
         {
           TokenId: req.body.tokenid,
           TokenStatus: req.body.status,
           PreviousStatus:req.body.PreviousStatus,
           Description:req.body.Description,
         }
       };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      var dataa = JSON.parse(body);
      if(dataa.success == true){
        res.json("1");
        savetokenhistory(req.body.role,req.body.tokenid,req.body.PreviousStatus,req.body.status,req.body.Description);
      }
      else{
        res.json("0");
      }
    });
  })
}


function savetokenhistory(role,tokenid,PreviousStatus,status,Description){
    var TokenHistory = hart.tokenhistory;
      var tokenhistry = new TokenHistory({
        tokenid:tokenid,
        user: role,
        statusfrom: PreviousStatus,
        statusto:status,
        status: "1",
        description: Description,
      });
      tokenhistry.save(function (err, results) {
        if(err){
          console.log(err);
        }
        else{

        }
      });
}
