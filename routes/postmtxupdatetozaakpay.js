var express = require('express');
var router = express.Router();
var checksum = require('../lib/checksum');

router.get('/updatetxn',function(req,res,next) {
  res.render('postmtxupdatetozaakpay');
});

router.post('/updatetxn',function(req,res,next) {
  var checksumstring = checksum.getCheckAndUpdateChecksumString(req.body);
  console.log(checksumstring)
  var calculatedchecksum = checksum.calculateChecksum(checksumstring);
  res.render('updatetxn', {
        data: req.body,
        checksum: calculatedchecksum
    });
  res.end();
});
module.exports = router;