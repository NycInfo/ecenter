var async = require('async');
var utlity = require('./utlity');
var hart= require('../hart');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
//var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
// var Date_ = require('node-datetime');
var moment = require('moment');
var ObjectId = require('mongodb').ObjectID;
module.exports = function(app){
  // app.use(session({
  // secret: 'Niharinfo',
  // resave: true,
  // saveUninitialized: true
  // }));

  app.post('/agentWalletAmount',function(req,res){
        var collection = hart.wallets;
        collection.findOne({ agentId : req.body.agentid},function(err,resp){
          if(resp == null){
               var Wallet = collection;
               var AgentWallet = new Wallet({
                 agentId:req.body.agentid,
                 Wallet:req.body.Amount,
                 Status:1,
               });
              AgentWallet.save(function (err, results) {
                res.json(1);
              });
          }
          else{
            hart.banktobankewallets.findOne({ 'BankReferenceId': req.body.utrnumber },function(error1,datsts){
              if (datsts == null) {
                res.json("null");
                return false;
              }else {
                var amount = parseInt(req.body.Amount);
                var updatebal = resp.Wallet+amount;
                var  Wallet= updatebal;
                var docs={
                  agentId: resp.agentId,
                  Wallet:updatebal,
                  Status:1,
                };
                collection.updateOne({ '_id':resp._id },{ $set:docs}, function(err, data) {
                  if(!err){
                    res.json("1");
                    posttozaakpaymentGetWay(resp.agentId,req.body.Amount,req.body.Amount,req.body.utrnumber,"success","b2b",req.body.utrnumber);
                    var obj= ObjectId(req.body.agentid);
                    hart.agents.findOne({ '_id': obj },function(error1,ad){
                      if (!error1) {
                        utlity.sendmessages(ad.Mobile,"Dear "+ad.UserName+" Your nihareCenter Wallet is credited "+req.body.Amount+" by Reference Number "+req.body.utrnumber);
                        var subject = "nihareCenter Wallet Updated Successfully - B2B";
                        var body = "Dear "+ad.UserName+", Your nihareCenter Wallet is credited "+req.body.Amount+" by Reference Number "+req.body.utrnumber+". If you any queries please contact nihar support team<br>Thanks<br>nihareCenter";
                        //utlity.sendemail(ad.email,subject,body);
                        var mailOptions = {
                          from: 'niharinfo321@gmail.com',
                          to: ad.email,
                          subject: subject,
                          html: body
                        };
                        hart.transporter11.sendMail(mailOptions, function(error, info){
                          if (error) {
                            console.log(error);
                          } else {
                            console.log('Email sent: ' + info.response);
                          }
                        });

                      }else {
                        console.log(error1);
                      }
                    })
                  }else{
                    res.json("0");
                    console.log(err);
                  }
                })
              }
              })
          }
         });
  });
  function posttozaakpaymentGetWay(agentId,Enteredamount,Amount,TransactionID,status,Paymentfrom,mihpayid){
      var ADDm_Reports = hart.add_money_reports;
          var Ereports = new ADDm_Reports({
            agentId:agentId,
            Enteredamount:Enteredamount,
            Amount: Amount,
            TransactionID:TransactionID,
            Paymentfrom:Paymentfrom,
            mihpayid:mihpayid,
            status:status,
          });
          Ereports.save(function (err, results) {
            if(err){
              console.log(err);
            }else{
              console.log("Add money Status updated");
            }
          });
  }

  app.post('/get_Agent_All_Earnings',function(req,res) {
    if (req.body.service == 'All') {
      if (req.body.earningduration == "today") {
        var formatted=moment().startOf('day').format('YYYY-MM-DD');
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{TransactionDate:{$gt:formatted}}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }else if (req.body.earningduration == "mtd") {
        var formatted=moment().startOf('month').format('YYYY-MM-DD');
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{TransactionDate:{$gt:formatted}}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }else if (req.body.earningduration == "ytd") {
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid }]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }else if (req.body.datefrom != "") {
        var dfrom = req.body.datefrom;
        var dto = req.body.dateto;
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{TransactionDate:{$gt:dfrom}},{TransactionDate:{$lt:dto}}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }else{
        var formatted=moment().startOf('day').format('YYYY-MM-DD');
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{TransactionDate:{$gt:formatted}}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }
    }else{
      if (req.body.earningduration == "today") {
        var formatted=moment().startOf('day').format('YYYY-MM-DD');
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{servicesName:req.body.service	},{TransactionDate:{$gt:formatted}}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }else if (req.body.earningduration == "mtd") {
        var formatted=moment().startOf('month').format('YYYY-MM-DD');
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{servicesName:req.body.service	},{TransactionDate:{$gt:formatted}}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }else if (req.body.datefrom != "") {
        var dfrom = req.body.datefrom;
        var dto = req.body.dateto;
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{TransactionDate:{$gt:dfrom}},{TransactionDate:{$lt:dto}}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }else{
        hart.transaction_reports.find({$and:[{ "AgentId": req.session.Agentid },{servicesName:req.body.service	}]}).exec(function(err,data){
          res.end(JSON.stringify(data));
        });
      }
    }
  });
  app.post('/getmoney',function(req,res){
    hart.wallets.find({ "agentId": req.session.id1 }).exec(function(err,doc1){
      res.end(JSON.stringify(doc1));
    });
  });
  app.post('/getEarnings_today',function(req,res){
    var formatted=moment().startOf('day').format('YYYY-MM-DD');
    hart.transaction_reports.find({$and:[{ "AgentId": req.session.id1 },{TransactionStatus: /^Success/i},{Status:1},{checked:'No'},{TransactionDate:{$gt:formatted}}]}).exec(function(err,doc1){
      res.end(JSON.stringify(doc1));
    });
  });
  app.post('/getEarnings_month',function(req,res){
    var formatted=moment().startOf('month').format('YYYY-MM-DD');
    hart.transaction_reports.find({$and:[{ "AgentId": req.session.id1 },{TransactionStatus:/^Success/i},{Status:1},{checked:'No'},{TransactionDate:{$gt:formatted}}]}).exec(function(err,doc1){
      res.end(JSON.stringify(doc1));
    });
  });
  app.post('/getsingleagentwallet',function(req,res){
    hart.wallets.find({ "agentId": req.body.id }).exec(function(err,doc1){
      res.end(JSON.stringify(doc1));
    });
  });
  app.post('/walletcheck', function(req,res){
      hart.wallets.findOne({ agentId : req.session.id1 },function(err,data){
        if(err){
          console.log(err);
        }
        else{
        //console.log(data);
          res.json(data);
        }
       });
  })
  app.post('/banktobanksendbp', function(req,res){
    var eWallets = hart.banktobankewallets;
    var btobewalls = new eWallets({
      agentId :req.session.Agentid,
      agentName:req.session.username,
      agentEmail: req.session.useremail,
      Amount:req.body.amount,
      BankReferenceId:req.body.bankrefid,
      RbiReferenceId:req.body.rbirefid,
      ReferenceName:req.body.referencename,
      ReferenceMobile:req.body.referencemobile,
      DateofTransaction:req.body.dateofsend,
      Status:"1",
      //if status 1 means it is in processing
      //if status 2 means it is in verifed
      //if status 3 means it is in Rejected
      //if status 4 means it is in approved
    });
   btobewalls.save(function (err, results) {
     if(!err){
       res.json("1");
     }else{
       res.json("0");
     }
   });
  })
  app.post('/getewallet_bp',function(req,res){
    // if (req.body.earningduration == "today") {
    //   var formatted=moment().startOf('day').format('YYYY-MM-DD');
    //   hart.banktobankewallets.find({$and:[{ "agentId": req.session.Agentid },{created:{$gt:formatted}}]}).exec(function(err,data){
    //     res.end(JSON.stringify(data));
    //   });
    // }
    if (req.body.earningduration == "mtd") {
      var formatted=moment().startOf('month').format('YYYY-MM-DD');
      hart.banktobankewallets.find({$and:[{ "agentId": req.session.Agentid },{created:{$gt:formatted}}]}).exec(function(err,data){
        res.end(JSON.stringify(data));
      });
    }else if (req.body.earningduration == "ytd") {
      hart.banktobankewallets.find({$and:[{ "agentId": req.session.Agentid }]}).exec(function(err,data){
        res.end(JSON.stringify(data));
      });
    }else if (req.body.datefrom != "") {
      var dfrom = req.body.datefrom;
      var dto = req.body.dateto;
      hart.banktobankewallets.find({$and:[{ "agentId": req.session.Agentid },{created:{$gt:dfrom}},{created:{$lt:dto}}]}).exec(function(err,data){
        res.end(JSON.stringify(data));
      });
    }else{
      hart.banktobankewallets.find({agentId:req.session.Agentid}).exec(function(err,doc1){
        res.end(JSON.stringify(doc1));
      });
    }
  });
  app.post('/getewallet_nihar_processing',function(req,res){
    hart.banktobankewallets.find({Status :"1"}).exec(function(err,doc1){
      res.end(JSON.stringify(doc1));
    });
  });
  app.post('/getewallet_nihar_verified',function(req,res){
    hart.banktobankewallets.find({Status :"2"}).exec(function(err,doc1){
      res.end(JSON.stringify(doc1));
    });
  });
  app.post('/getewallet_nihar_approved',function(req,res){
    if (req.body.earningduration == "mtd") {
      var formatted=moment().startOf('month').format('YYYY-MM-DD');
      hart.banktobankewallets.find({$and:[{ "Status": "4" },{created:{$gt:formatted}}]}).exec(function(err,data){
        res.end(JSON.stringify(data));
      });
    }else if (req.body.earningduration == "ytd") {
      hart.banktobankewallets.find({$and:[{ "Status": "4" }]}).exec(function(err,data){
        res.end(JSON.stringify(data));
      });
    }else if (req.body.datefrom != "") {
      var dfrom = req.body.datefrom;
      var dto = req.body.dateto;
      hart.banktobankewallets.find({$and:[{ "Status": "4" },{created:{$gt:dfrom}},{created:{$lt:dto}}]}).exec(function(err,data){
        res.end(JSON.stringify(data));
      });
    }else{
      hart.banktobankewallets.find({ "Status": "4" }).exec(function(err,doc1){
        res.end(JSON.stringify(doc1));
      });
    }
    // hart.banktobankewallets.find({Status :"4"}).exec(function(err,doc1){
    //   res.end(JSON.stringify(doc1));
    // });
  });

  app.post('/updateewallet',function(req,res){
    var obj= ObjectId(req.body.id);
    hart.banktobankewallets.updateOne({'_id':obj },{ $set:{ Status: req.body.value }}, function(err, response) {
      if(!err){
        res.json("1");
        if (req.body.value == "3") {
          var aid= ObjectId(req.body.agentid);
          hart.agents.findOne({ '_id': aid },function(error1,ad){
            utlity.sendmessages(ad.Mobile,"Dear "+ad.UserName+" Your nihareCenter Wallet update request with amount "+req.body.Amount+" by Reference Number "+req.body.utrnumber+"is Rejected. Please contact nihar cc team");
            var subject = "nihareCenter Wallet Update Rejected - B2B";
            var body = "Dear "+ad.UserName+", Your nihareCenter Wallet update request with amount "+req.body.Amount+" by Reference Number "+req.body.utrnumber+"is Rejected. If you any queries please contact nihar support team<br>Thanks<br>nihareCenter";
            //utlity.sendemail(ad.email,subject,body);
            var mailOptions = {
              from: 'niharinfo321@gmail.com',
              to: ad.email,
              subject: subject,
              html: body
            };
            hart.transporter11.sendMail(mailOptions, function(error, info){
              if (error) {
                console.log(error);
              } else {
                console.log('Email sent: ' + info.response);
              }
            });
          });
        }
      }else{
        res.json("0");
      }
    });
  })
}
