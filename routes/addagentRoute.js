var async = require('async');
// var utlity = require('./utlity');
var hart= require('../hart');
var utlity = require('./utlity');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
//var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var request = require('request');
var ObjectId = require('mongodb').ObjectID;
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.cmsImgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });

module.exports = function(app){

  app.post('/UpdateAgentProfile',function(req,res){
        var response={
          UserName: req.body.exampleInputUser,
          email:req.body.exampleInputEmail1,
          Mobile:req.body.exampleInputMobile,
        };
        var doc =response;
        var obj= ObjectId(req.session.Agentid);
        hart.agents.updateOne({ '_id':obj },
        { $set:doc}, function(err, response) {
          if(err){
              console.log(err);
          }
          else{
         res.json(1);
         }
       });
     });

   app.post('/wallet_reports',function(req,res) {
     hart.add_money_reports.find({agentId:req.session.Agentid }).exec(function(err, data){
       if (!err) {
         res.json(data);
       }else {
         res.json("0");
       }
     });
   });
  app.post('/UpdateAgentPwd',function(req,res){
         var doc ={};
         doc.password=req.body.newpassword
         var obj= ObjectId(req.session.Agentid);
         hart.agents.findOne({ '_id':obj },function(err,data1) {
           if (data1.password==req.body.oldpassword) {
             hart.agents.updateOne({ '_id':obj },{ $set:doc}, function(err, response) {
               if(err){
                   console.log(err);
               }
               else{
              res.json(1);
              }
            });
           }else {
             res.json(0);
           }
         })
       })
       app.post('/GetNiharAdminDetails', function(req,res){
          var agntid= ObjectId(req.session.niharid);
          hart.nihar_admins.findOne({ _id : agntid },function(err,data){
            res.json(data);
           });
       })
      app.post('/UpdateAdminProfile',function(req,res){
             var response={
               Name: req.body.exampleInputUser,
               Email:req.body.exampleInputEmail1,
               Mobile:req.body.exampleInputMobile,
              // password:req.body.exampleInputPassword1,
             };
             var doc =response;
             var ObjectId = require('mongodb').ObjectID;
             var obj= ObjectId(req.session.niharid);
             hart.nihar_admins.updateOne({ '_id':obj },
             {$set:doc}, function(err, response) {
               if(err){
                   console.log(err);
               }
               else{
                 res.json(1);
              }
            });
          });
          app.post('/UpdatePassword',function(req,res){
                var doc ={};
                doc.Password=req.body.newpassword
                var obj= ObjectId(req.session.niharid);
                hart.nihar_admins.findOne({ '_id':obj },function(err,data1) {
                  if (data1.Password==req.body.oldpassword) {
                    hart.nihar_admins.updateOne({ '_id':obj },{ $set:doc}, function(err, response) {
                      if(err){
                          console.log(err);
                      }
                      else{
                     res.json(1);
                     }
                   });
                  }else {
                    res.json(0);
                  }
                })
              })
  app.post('/getall_tsAD',function(req,res) {
      hart.transaction_reports.find({AgentId:req.session.Agentid}).sort({_id:-1}).exec(function(err,data) {
        res.json(data);
      });
  });

  app.post('/getBUS_AD',function(req,res){
    console.log(req.body);
    if(req.body.fromdate != "" && req.body.todate != ""){
      var fromdate=req.body.fromdate;
      var todate=req.body.todate;
      var enddate = utlity.addoneday(todate);
      hart.bustickets.find({"$and" : [{bookedAt:{"$gt": fromdate, "$lte": enddate}},{AgentId: req.session.Agentid }]}).exec(function(err,doc){
        if(!err){
          res.json(doc)
        }else{
          console.log(err);
    }
  });
}else{
  hart.bustickets.find({AgentId:req.session.Agentid }).sort({_id:-1}).exec(function(err, data){
    if(err){
      console.log(err);
    }
    else{
      res.json(data);
    }
  });
}

 });


  app.post('/ServiceRequests',function(req,res){
    var Servicerequests = hart.servicerequests;
      var ServiceRequests = new Servicerequests({
        ReferenceId:"SRQ"+randotp(),
        TransactionId:req.body.transactionid,
        AgentId : req.session.Agentid,
        pnr:req.body.pnrnumber,
        ServiceType:req.body.flghtype,
        Fare: req.body.fare,
        status:'1',
      });
      ServiceRequests.save(function (err, results) {
        if(err){
          res.json(0);
          console.log(err);
        }
        else{
          res.json(results);
          bookingsupdatestatus(req.body.pnrnumber);
        }
      });
  })


  app.post('/getflight_AD',function(req,res){
    console.log(req.body);
    if(req.body.fromdate != "" && req.body.todate != ""){
      var fromdate=req.body.fromdate;
      var todate=req.body.todate;
      hart.flightbookings.find({"$and" : [{bookedAt:{"$gte": fromdate, "$lt": todate}},{AgentId: req.session.Agentid }]}).exec(function(err,doc){
        if(!err){
          res.json(doc)
        }else{
          console.log(err);
    }
  });
}else{
      hart.flightbookings.find({AgentId:req.session.Agentid }).sort({_id:-1}).exec(function(err, data){
        if(err){
          console.log(err);
        }
        else{
          res.json(data);
        }
        });
        }
});

  app.post('/getNiharAdminflightDetails',function(req,res){
      hart.flightbookings.find({}).exec(function(err, data){
        if(!err){
          res.json(data);
        }else{
          console.log(err);
        }
      });
  })
  app.post('/gethotel_AD',function(req,res){
    console.log(req.body);
    if(req.body.fromdate != "" && req.body.todate != ""){
      var fromdate=req.body.fromdate;
      var todate=req.body.todate;
      var enddate = utlity.addoneday(todate);
      hart.hotelsbookingdetails.find({"$and" : [{Created_Date:{"$gte": fromdate, "$lte": enddate}},{agentId: req.session.Agentid }]}).exec(function(err,doc){
        if(!err){
          res.json(doc)
        }else{
          console.log(err);
    }
  });
}else{
      hart.hotelsbookingdetails.find({agentId:req.session.Agentid }).sort({_id:-1}).exec(function(err, data){
        if(err){
          console.log(err);
        }
        else{
          res.json(data);
        }
        });
        }
});
  app.post('/GetNiharAgent_Hotel_Booking_Reports',function(req,res){
      hart.hotelsbookingdetails.find({agentId:req.body.id }).exec(function(err, data){
        if(!err){
          res.json(data);
        }else{
          console.log(err);
        }
      });
  });

  app.post('/GetNiharAgent_Flight_Booking_Reports',function(req,res){
      hart.flightbookings.find({AgentId:req.body.id }).exec(function(err, data){
        if(!err){
          res.json(data);
        }else{
          console.log(err);
        }
      });
  });

  function bookingsupdatestatus(pnrnumber){
    hart.flightbookings.updateOne({ 'pnr': pnrnumber },{ $set:{ status: "2" }}, function(err, response) {
      if(err){
        console.log(err);
      }
      else{
        console.log("Request Updated");
      }
    });
  }
  app.post('/Sendmailtocustomerservices123',function(req,res){

    var CustomerServices = hart.customerservices;
      var customerservice = new CustomerServices({
        AgentId:req.session.Agentid,
        AgentName:req.session.username,
        AgentEmail:req.session.useremail,
        Subject:req.body.Subject,
        ServiceName:req.body.ServiceName,
        TransactionId:req.body.TransactionId,
        Message:req.body.Email_Body,
        Status:"1",
      });
      customerservice.save(function (err, results) {
        if(err){
          res.json(0);
          console.log(err);
        }
        else{
          res.json(1);
        }
      });
  })
  app.post('/getmessage',function(req,res){
      hart.agentmsgs.find({ "ToEmail": req.session.user }).sort({_id:-1}).exec(function(err,doc1){
      res.end(JSON.stringify(doc1));
      });

   });


     app.post('/getdetails',function(req,res){
          var agntid= ObjectId(req.session.id1);
         hart.agents.find({ "_id": agntid }).exec(function(err,doc1){
           res.end(JSON.stringify(doc1));
         });
      });
      app.post('/getUpdate',function(req,res){
        var response={
          UserName: req.body.exampleInputUser,
          email:req.body.exampleInputEmail1,
          Mobile:req.body.exampleInputMobile,
          password:req.body.exampleInputPassword1,
        };
        var doc =response;
        var obj= ObjectId(req.session.Agentid);
        hart.agents.updateOne({ '_id':obj },
        { $set:doc}, function(err, response) {
          if(err){
              console.log(err);
          }
          else{
         console.log("success");
         res.json(1);
         }
       });
     });


    app.post('/GetAgentProfileDetails', function(req,res){

        var agntid= ObjectId(req.session.Agentid);
        // hart.agents.find({}).exec(function(err,doc){
        //   res.json(doc);
        // })
        hart.agents.findOne({ "_id" : agntid },function(err,data){
          res.json(data);
         });

    })


    app.post('/agentmessages',function(req,res){
        hart.agentmessages.find({AgentEmail:'steaven5953@gmail.com'}).exec(function(err, x){
        if(!err){
          var data = JSON.parse(JSON.stringify(x));
            res.json(data);
        }else{
          console.log(err);
        }
        });

    })
    var AgentMessagesSchema = mongoose.Schema({
      ToEmail:"string",
      AgentId: "string",
      AgentName:"string",
      Subject:"string",
      Message:"string",
      Status:"string",
      CreateDate:{ type: Date, default: Date.now },
    });
    app.post('/SendEmail',function(req,res){
      var AgentMsg = mongoose.model('AgentMsg', AgentMessagesSchema);

        var agentmessages = new AgentMsg({
          ToEmail:req.body.ToAddress,
          AgentId: "5953",
          AgentName:"Steaven",
          Subject:req.body.Subject,
          Message:req.body.Email_Body,
          Status:"1",
        });
        agentmessages.save(function (err, results) {
          if(err){
            console.log(err);
          }
          else{
            res.json(1);
          }
        });

    })

    app.post('/sendotp',function(req,res){
      var txt = req.body.otp;var mobile = req.body.Mobile;
      var msg = txt+"  is the otp for forget password"
        var options = { method: 'GET',
        url: 'http://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
        qs:
         { type: 'smsquicksend',
           user: 'niharinfo',
           pass: 'Nihar@123',
           sender: 'NIHAAR',
           to_mobileno: mobile,
           sms_text: msg },
        headers:
         { 'postman-token': 'eda138b3-9000-ff1c-56a6-adb5438d46f4',
           'cache-control': 'no-cache' } };
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        //console.log(body);
        res.json(body);
      });
    })
    app.post('/forgototp',function(req,res){
      var txt = "Your Login Credentials is Username:"+req.body.email+" & Password :"+req.body.password;
      var mobile = req.body.Mobile;
      var msg = txt;
        var options = { method: 'GET',
        url: 'http://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
        qs:
         { type: 'smsquicksend',
           user: 'niharinfo',
           pass: 'Nihar@123',
           sender: 'NIHAAR',
           to_mobileno: mobile,
           sms_text: msg },
        headers:
         { 'postman-token': 'eda138b3-9000-ff1c-56a6-adb5438d46f4',
           'cache-control': 'no-cache' } };
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        //console.log(body);
        res.json(body);
      });
    })

    function randpwd(){
        //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789@$!";
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZ0123456789";
        var string_length = 8;
        var randomstring = '';
        var charCount = 0;
        var numCount = 0;

        for (var i=0; i<string_length; i++) {
            // If random bit is 0, there are less than 3 digits already saved, and there are not already 5 characters saved, generate a numeric value.
            if((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
                var rnum = Math.floor(Math.random() * 10);
                randomstring += rnum;
                numCount += 1;
            } else {
                // If any of the above criteria fail, go ahead and generate an alpha character from the chars string
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum,rnum+1);
                charCount += 1;
            }
        }
        return randomstring;
    }
    function randotp(){
     var chars = "0123456789";
     var string_length = 6;
     var randomstring = '';
     var charCount = 0;
     var numCount = 0;
     for (var i=0; i<string_length; i++) {
         if((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
             var rnum = Math.floor(Math.random() * 10);
             randomstring += rnum;
             numCount += 1;
         } else {
             var rnum = Math.floor(Math.random() * chars.length);
             randomstring += chars.substring(rnum,rnum+1);
             charCount += 1;
         }
     }
     return randomstring;
   }
}
