const request = require('request');
//const session = require('express-session');
const http = require("http");
const method_get = 'GET'
const movie_list = [];
const openper = '{';
const cache = 'Cache-Control';
const nocache = ': no-cache';
const cnnttype = 'Content-Type';
const appjson = ': application/json';
const closedper = '}';
const username = 'username';
const password = 'password';
module.exports = {
  headersforapi: function() {
    return openper + cache + nocache + ',' + cnnttype + appjson;
  },
  bodyforapi: function(uname, pswd) {
    return openper + username + ':' + uname + ',' + password + ':' + pswd + closedper;
  },
  getauth_token,
}

async function getauth_token(user, password) {
  var options = {
    method: 'POST',
    url: 'https://www.niharecenter.com/api/auth/authenticate',
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json'
    },
    body: {
      username: user,
      password: password
    },
    json: true
  };
  return await new Promise((resolve) => {
    request(options, (error, response, body) => {
      if (error) {
        resolve({ success: false, message: error.message });
      }
      else if (response.statusCode != 200) {
        console.log('Response : ' + response.body.message);
        resolve({ success: false, message: response.body.message });
      }
      else {
        resolve({ success: true, message: response.body });
      }
    });
  });
}
