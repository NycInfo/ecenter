var async = require('async');
var request = require('request');
var hart= require('../hart');
var sqlcon = hart.sql;
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var ObjectId = require('mongodb').ObjectID;
var checksum = require('../lib/checksum');
var parseString = require('xml2js').parseString;
const authService = require('../_helpers/auth.service');
const mailtemplate = require('./mailtemplates');
const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss')
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.cmsImgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });

module.exports = function(app){

// app.use(session({
//     secret: 'I9Admin-Flag',
//     resave: true,
//     saveUninitialized: true,
//     name : 'I9Admin',
// }));


app.post('/cancelflight_ticketvoid',function(req,res){

  var request = require("request");
  var body1='<TicketVoidRQ TransactionIdentifier="295d295d-295d-4295-295d-295d2">';
  body1 += '<IATA>35201165</IATA>';
  body1 += '<PseudoCityCode>AH7H</PseudoCityCode>';
  body1 += '<TicketNumber>22057472315793</TicketNumber>'; //req.body.eticketnumber
  body1 += '<RecordLocator>DJ4WJU</RecordLocator>'; //req.body.pnrno
  body1 += '</TicketVoidRQ>';
  var options = { method: 'POST',
  url: 'http://35.161.63.245:8081/airtecht/callApi',
  headers:
   { 'Content-Type': 'application/xml',
     Authorization: 'Basic bXVydGh5a25AbmloYXJpbmZvLmNvbTptdXJ0aHlrbiU3LW5paGFyITIzNA=='
   },
     body: body1,
   };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    console.log(body);
    parseString(body, function (err, s) {
      res.send(s);
    });
  });
})

app.post('/cancelflight_order',function(req,res){
  console.log(req.body);
  var request = require("request");
  var body1='<OrderCancelRQ Version="17.2" TransactionIdentifier="acde5a8d-3468-4c12-b"><Document>';
  body += '<Name>NDC GATEWAY</Name><ReferenceVersion>1.0</ReferenceVersion></Document><Party><Sender><TravelAgencySender><Name>ATUI sender</Name><Contacts><Contact><EmailContact><Address>airlinestechnology@tc.com</Address></EmailContact></Contact></Contacts><IATA_Number>35201165</IATA_Number><AgencyID>test</AgencyID></TravelAgencySender></Sender></Party>';
  body += '<Query><Order Owner="EK" OrderID="973103P2"/></Query>';
  body += '</OrderCancelRQ>';
  var options = { method: 'POST',
  url: 'http://35.161.63.245:8081/airtecht/callApi',
  headers:
   { 'Content-Type': 'application/xml',
     Authorization: 'Basic bXVydGh5a25AbmloYXJpbmZvLmNvbTptdXJ0aHlrbiU3LW5paGFyITIzNA=='
   },
     body: body1,
   };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    console.log(body);
    parseString(body, function (err, s) {
      res.send(s);
    });
  });
})


app.post('/UpdatePassword',function(req,res){
   var doc ={};
   doc.Password=req.body.newpassword
   var obj= ObjectId(req.session.niharid);
   hart.nihar_admins.findOne({ '_id':obj },function(err,data1) {
     if (data1.Password==req.body.oldpassword) {
       hart.nihar_admins.updateOne({ '_id':obj },{ $set:doc}, function(err, response) {
         if(err){
             console.log(err);
         }
         else{
        res.json(1);
        }
      });
     }else {
       res.json(0);
     }
   })
 })




app.post('/getele_boards',function(req,res){
    hart.sub_services.find({$and:[{Status:1},{optional_1:req.body.id}]}).exec(function(err, data){
      res.json(data);
    });
})
app.post('/getrecentelec',function(req,res){
    hart.electricity_reports.find({agentId:req.session.id1}).exec(function(err, data){
      res.json(data);
    });
})

app.post('/servProfileDetails', function(req,res){
    var serviceid= ObjectId(req.body.id);
    hart.services.findOne({ _id : serviceid },function(err,data){
      res.json(data);
     });
})

app.post('/add_aggregator',function(req,res) {
  var Aggregators = hart.aggregators;
  Aggregators.findOne({ name : req.body.AggregatorName },function(err,data){
    if(err){
        console.log(err)
        res.json(0);
      }else {
        if (data == null) {
          var agg = new Aggregators({
          name : req.body.AggregatorName,
          code : req.body.AggregatorCode,
          Status : "1",
        });
        agg.save(function (err, results) {
          res.json(1);
        });
      } else {
        res.json(0);
      }
    }
  })
})
app.post('/getaggregaters',function(req,res){
    hart.aggregators.find({}).exec(function(err, data){
      res.json(data);
    });
})
app.post('/ServiceRqs',function(req,res){
    hart.servicerequests.find({}).exec(function(err, data){
      res.json(data);
    });
})

app.post('/NiharAddAdmin',function(req,res){
        var NiharAdmin =hart.nihar_admins;
            var pswd = randpwd(req.body.Name);
            var niharAdmin = new NiharAdmin({
              Name:req.body.Name,
              Email:req.body.Email,
              Mobile:req.body.MobileNumber,
              Password:pswd,
              EmployeeType:req.body.EmployeeType,
              Status:"1",
          });
          niharAdmin.save(function (err, results) {
            if(!err){
              res.json(1);
            }
            else{
              res.json(0);
            }
          });
})

app.post('/SendEmailtoAgent',function(req,res){
      var Niharmessages = hart.niharmsgs;
        var niharmessages = new Niharmessages({
          ToAgentId:req.body.ToAddress,
          Subject:req.body.Subject,
          Message:req.body.Email_Body,
          Status:"1",
        });
        niharmessages.save(function (err, results) {
          if(err){
            res.json(0);
            console.log(err);
          }
          else{
            res.json(1);
          }
        });
    })
app.post('/getCategories',function(req,res){
        hart.ecategories.find({Status:1}).exec(function(err, data){
          res.json(data);
        });
    })
app.post('/getECSubC',function(req,res){
        hart.esubcategories.find({CategoryId:req.body.id}).exec(function(err, data){
         res.json(data);
        });
     })

     app.post('/getAgents',function(req,res){

         hart.agents.find({Status:1}).exec(function(err, data){
           res.json(data);
         });
     })


app.post('/AddAgent',async function(req,res){

    var Agent = hart.agents;
    var UserId = await authService.userrandomod(req.body.PhoneNumber);
    var pswd = randpwd(req.body.firstName+''+req.body.lastName);
    var agents = new Agent({
    UserId: UserId,
    UserName: req.body.firstName+''+req.body.lastName,
    email: req.body.emailAddress,
    password:pswd,
    Mobile:req.body.PhoneNumber,
    HNo:req.body.Address,
    Street:req.body.Street,
    Village:req.body.Village,
    City:req.body.City,
    State:req.body.State,
    PANCard:"",
    Aadhar:"",
    Photo:"",
    Shipping_DoorNumber:req.body.shippingdoornumber,
    Shipping_Street: req.body.shippingstreet,
    Shipping_City: req.body.shippingcity,
    Shipping_State: req.body.shippingstate,
    Shipping_PinCode: req.body.shippingpincode,
    Billing_DoorNumber: req.body.billingdoornumber,
    Billing_Street: req.body.billingstreet,
    Billing_City: req.body.billingcity,
    Billing_State: req.body.billingstate,
    Billing_PinCode: req.body.billingpincode,
    PinCode:req.body.ZipCode,
    RemitterId:"0",
    PanNumber:req.body.pannumber,
    GSTNumber:req.body.gstnumber,
    Status:1,
  });
  agents.save(function (err, results) {
    //console.log(results._id);
    if(!err){
      res.json(1);
      activatewallet_0(req.body.PhoneNumber,req.body.emailAddress);
      sendmessage(1,req.body.firstName+' '+req.body.lastName,req.body.PhoneNumber,encodeURIComponent(req.body.emailAddress),pswd);
      var MailBody = mailtemplate.registrationmailstyles()+mailtemplate.registrationmailhead()+mailtemplate.registrationmaibody(req.body.firstName+' '+req.body.lastName)+mailtemplate.registrationmaibody1()+mailtemplate.registrationmaibody2()+mailtemplate.registrationmaibody3();
      // itdukaanagentreg(req.body.firstName,
      //                 req.body.lastName,
      //                 req.body.emailAddress,
      //                 req.body.PhoneNumber,
      //                 req.body.Address,
      //                 req.body.Street,
      //                 req.body.Village,
      //                 req.body.City,
      //                 req.body.State,
      //                 req.body.shippingdoornumber,
      //                 req.body.shippingstreet,
      //                 req.body.shippingcity,
      //                 req.body.shippingstate,
      //                 req.body.shippingpincode,
      //                 req.body.billingdoornumber,
      //                 req.body.billingstreet,
      //                 req.body.billingcity,
      //                 req.body.billingstate,
      //                 req.body.billingpincode,
      //                 req.body.ZipCode,
      //                 req.body.pannumber,
      //                 req.body.gstnumber)
      var email = req.body.emailAddress;
      var mailOptions = {
        from: 'niharinfo321@gmail.com',
        to: email,
        subject: "Thanks for Registered with us - Nihar eCenter",
        html: MailBody
      };
      hart.transporter11.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
    }else{
      console.log(err);
      res.json('err');
    }
  });
})
function activatewallet_0(PhoneNumber,emailAddress) {
  hart.agents.findOne({$and:[{Mobile:PhoneNumber},{email:emailAddress},{Status:1}]}).exec(function(err1,res1) {
    console.log(res1);
    if (res1!==null||res1!==undefined) {
      var collection = hart.wallets;
      collection.find({$and:[{Mobile:PhoneNumber},{email:emailAddress}]}).exec(function(err,resp){
        if(resp[0] == null||resp[0] == undefined){
            var Wallet = collection;
            var AgentWallet = new Wallet({
              agentId:res1._id,
              Wallet:0.00,
              Status:1,
            });
           AgentWallet.save(function (err, results) {
             if (!err) {
               console.log('wallet created for',emailAddress);
             }else {
               console.log(err);
             }
           });
       }
      })
    }
  })
}
app.post('/agentmobileoremailchecking',function(req,res){
      hart.agents.count({ "$or": [{"email":req.body.emailentered}, {"Mobile": req.body.phoneentered}] },function(err,doc){
        if(!err){
          res.json(doc);
        }else{
          res.json("0");
        }
      });
  })

app.post('/EditAgent', function(req,res){
        // var pswd = randpwd(req.body.firstName+''+req.body.lastName);
        var docs={
          UserName: req.body.firstName+''+req.body.lastName,
          email: req.body.emailAddress,
          // password:pswd,
          Mobile:req.body.PhoneNumber,
          HNo:req.body.Address,
          Street:req.body.Street,
          Village:req.body.Village,
          City:req.body.City,
          State:req.body.State,
          PinCode:req.body.ZipCode,
          Shipping_DoorNumber:req.body.shippingdoornumber,
          Shipping_Street: req.body.shippingstreet,
          Shipping_City: req.body.shippingcity,
          Shipping_State: req.body.shippingstate,
          Shipping_PinCode: req.body.shippingpincode,
          Billing_DoorNumber: req.body.billingdoornumber,
          Billing_Street: req.body.billingstreet,
          Billing_City: req.body.billingcity,
          Billing_State: req.body.billingstate,
          Billing_PinCode: req.body.billingpincode,
          PinCode:req.body.ZipCode,
          PanNumber:req.body.pannumber,
          GSTNumber:req.body.gstnumber,
          Status:1,
        };
        var agentid = ObjectId(req.body.agentid);
        hart.agents.updateOne({ '_id':agentid },{ $set:docs}, function(err, data) {
          if(err){
            res.json("0");
            console.log(err)
          }else {
            res.json("12");
            // sendmessage(2,req.body.firstName,req.body.PhoneNumber,encodeURIComponent(req.body.emailAddress),pswd);
          }

        })
    })
app.post('/AgentProfileDetails1', function(req,res){
    var agntid= ObjectId(req.body.id);
    hart.agents.findOne({ _id : agntid },function(err,data){
      res.json(data);
     });
})
app.post('/AgentECommerceReports', function(req,res){
    var AgentId= req.body.id;
    hart.orderproducts.find({AgentId:AgentId}).exec(function(err, data){
      res.json(data);
     });
})
app.post('/AgentECommerceReports', function(req,res){
    var AgentId= req.body.id;
    hart.orderproducts.find({AgentId:AgentId}).exec(function(err, data){
      res.json(data);
     });
})
app.post('/AgentDMTReports', function(req,res){

    var AgentId= req.body.id;
    hart.transfers.find({AgentId:AgentId}).exec(function(err, data){
      res.json(data);
     });
})
app.post('/DeleteAgent', function(req,res){
    var agntid= ObjectId(req.body.aid);
    hart.agents.remove( { _id:agntid }, function(err, doc) {
      if (err) {
         console.log(err);
         res.json("0");
      }
      else{
        res.json("1");
      }
    });

})
function sendmessage(x,name,mobile,email,pswd){
      if(x == 2){
        var txt = "Dear "+name+" Your Profile updated successfully.. For Login UserName:'"+email+"', Password:"+pswd;
      }else{
        var txt = "Dear "+name+", Thanks for registering with Nihar eCenter. Your login credentials are User Name : Your email id provided in the BP Registration Form, Password: "+pswd+" Regards, Nihar eCenter Admin";
      }
        var options = { method: 'GET',
        url: 'http://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
        qs:
         { type: 'smsquicksend',
           user: 'niharinfo',
           pass: 'Nihar@123',
           sender: 'NIHAAR',
           to_mobileno: mobile,
           sms_text: txt },
        headers:
         { 'postman-token': 'eda138b3-9000-ff1c-56a6-adb5438d46f4',
           'cache-control': 'no-cache' } };
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        console.log(body);
      });
    }

app.post('/GetCustomermails',function(req,res){
      hart.customerservices.find().exec(function(err, data){
        if(err){
          console.log(err);
        }
        else{
          res.json(data);
        }
       });

});
app.post('/getsingemessage', function(req,res){
    var obj = ObjectId(req.body.msgid);
    hart.customerservices.findOne({ _id : obj },function(err,data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
})
app.post('/fgtpwd',function(req,res){
  hart.nihar_admins.find({$or: [ { 'Email':req.body.emails},{'Mobile':req.body.mobilenumber}]}).exec(function(err, doc1) {
     res.json(doc1);
  });
})
app.post('/kyc_editAgent',upload.any(), function(req,res){
      var docs={};
      var files=req.files;
      for (var i = 0; i < files.length; i++) {
        if (files[i].fieldname=='Photo') {
           docs.Photo =req.files[i].filename;
        }if (files[i].fieldname=='PanCard') {
          docs.PANCard =req.files[i].filename;
        }if (files[i].fieldname=='Aadhar') {
          docs.Aadhar=req.files[i].filename;
        }
      }
        var agentid = ObjectId(req.body.agentid);
        hart.agents.updateOne({ '_id':agentid },{ $set:docs}, function(err, data) {
          if(err){
            res.json("0");
            console.log(err)
          }else {
            res.json("1");
            console.log(data);
          }
        })
    })
app.post('/getsubServices_data',function(req,res){
    hart.sub_services.find({Status:1}).exec(function(err, data){
      res.json(data);
    });
})

app.post('/getServices_data',function(req,res){
    hart.services.find({'aggregatorId':req.body.id}).exec(function(err, data){
      res.json(data);
    });

})
app.post('/getallserv',function(req,res){
    hart.sub_services.find({Status:1}).exec(function(err, data){
     res.json(data);
    });
})
app.post('/getsingleSUBsrv',function(req,res){
    var id= ObjectId(req.body.id);
    hart.sub_services.find({$and:[{Status:1},{_id:id}]}).exec(function(err, data){
     res.json(data);
    });
})
app.post('/subservProfileDetails', function(req,res){
    var subserviceid= ObjectId(req.body.id);
    hart.sub_services.findOne({$and:[{Status:1},{_id:subserviceid}]}).exec(function(err,data){
      if (!err) {
        res.json(data);
      }
     });
})
app.post('/deletesubserv', function(req,res){
    var subserviceid= ObjectId(req.body.id);
    hart.sub_services.updateOne({ '_id':subserviceid },{ $set:{'Status':0}}, function(err, data) {
      if (err) {
        console.log(err);
        res.json('err');
      }
      if (!err) {
        res.json('1');
      }
     });
})

app.post('/add_subservices',upload.any(), function(req,res) {
    var subserv = hart.sub_services;
    hart.sub_services.findOne({$and:[{servicesId:req.body.servicesId},{sub_servicesName:req.body.sub_servicesName},{Status:1}]},function(err,data) {
      if (err) {
        console.log(err);res.json(0);
      }else {
        if (data==null) {
          var sServices = new subserv({
            aggregatorId: req.body.aggregatorId,
            aggregatorName: req.body.aggregatorName,
            aggregatorCode: req.body.aggregatorCode,
            servicesId: req.body.servicesId,
            serviceCode: req.body.serviceCode,
            servicesName: req.body.servicesName,
            sub_servicesName: req.body.sub_servicesName,
            subservicesCode: req.body.subservicesCode,
            Aggregator_Margin: req.body.Aggregator_Margin,
            Margintype:req.body.Margintype,
            Nihar_Margin: req.body.Nihar_Margin,
            Agent_Margin: req.body.Agent_Margin,
            Icon:req.files[0].filename,
            optional_1:req.body.optional_1,
            optional_2:req.body.optional_2,
            optional_3:req.body.optional_3,
            Status:1,
          });
          sServices.save(function (err, results) {
            if (!err) {
              res.json(0);
            }else {
              console.log(err);
              res.json('err');
            }
          });
        }else {
          res.json('12');
        }
      }
    });
 })

app.post('/add_services', function(req,res) {
    var services = hart.services;
    services.findOne({servicesName:req.body.services_name},function(err,data) {
      if (err) {console.log(err);res.json(0);}else {
        if (data==null) {
          var Services = new services({
            aggregatorId:req.body.aggregatorId,
            aggregatorName:req.body.aggregatorName,
            aggregatorCode:req.body.aggregatorCode,
            serviceCode:req.body.serviceCode,
            servicesName:req.body.servicesName,
            Status:1,
          });
          Services.save(function (err, results) {
            if (!err) {
              res.json(1);
            }else {
              res.json(0);
            }
          });
        }else {
          res.json(0);
        }
      }

    });

 })


 app.post('/update_subservices',upload.any(), function(req,res){
     var s=req.body;
     var docs={
       aggregatorId: req.body.aggregatorId,
       aggregatorName: req.body.aggregatorName,
       aggregatorCode: req.body.aggregatorCode,
       servicesName: req.body.servicesName,
       servicesId: req.body.servicesId,
       serviceCode: req.body.serviceCode,
       sub_servicesName: req.body.sub_servicesName,
       subservicesCode: req.body.subservicesCode,
       Aggregator_Margin: req.body.Aggregator_Margin,
       Margintype:req.body.Margintype,
       Nihar_Margin: req.body.Nihar_Margin,
       Agent_Margin: req.body.Agent_Margin,
       Icon:req.files[0].filename,
       optional_1:req.body.optional_1,
       optional_2:req.body.optional_2,
       optional_3:req.body.optional_3,
       Status:1,

     //  updatedAt: { type: Date, default: Date.now },
     };
     var id = ObjectId(req.body.subserviceId);
     hart.sub_services.updateOne({ '_id':id },{ $set:docs}, function(err, data) {
       console.log(data);
       if(err){
         res.json("err");
         console.log(err);
       }else {
         res.json("1");
       }
     })
 })

    function randpwd(name){
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789@$!";
        //var chars = name;
        var string_length = 8;
        var randomstring = '';
        var charCount = 0;
        var numCount = 0;

        for (var i=0; i<string_length; i++) {
            // If random bit is 0, there are less than 3 digits already saved, and there are not already 5 characters saved, generate a numeric value.
            if((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
                var rnum = Math.floor(Math.random() * 10);
                randomstring += rnum;
                numCount += 1;
            } else {
                // If any of the above criteria fail, go ahead and generate an alpha character from the chars string
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum,rnum+1);
                charCount += 1;
            }
        }
        return randomstring;
    }
//})

}
