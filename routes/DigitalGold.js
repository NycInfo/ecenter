var utlity = require('./utlity');
var hart= require('../hart');
var ObjectId = require('mongodb').ObjectID;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var request = require('request');
var moment = require('moment');
var hmacsha1 = require('hmacsha1');
var request = require("request");
var token = hart.token;

module.exports = function(app){

  app.post('/buy', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/buyPrice',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id: x.OutLetId,
     mobile:req.session.Mobile
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        // console.log(body);
                        res.json(body)
                      }
                    });
  });
  });
  app.post('/sell', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/sellPrice',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id: x.OutLetId,
     mobile:req.session.Mobile
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        // console.log(body);
                      }
                      res.json(body)
                    });
                  });
  });
  app.post('/verifyCust',function(req,res){
    console.log(req.body);
    hart.DigitalGoldCustomers.findOne({ MobileNumber : req.body.mobile },function(err,data){
      if (!err) {
        if (data == null || data == "") {
          res.json("New User")
        }else{
          hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
            var x = JSON.parse(JSON.stringify(data1));
          var options = { method: 'POST',
          url: 'https://www.instantpay.in/ws/digitalgold/login',
            headers:
              { accept: 'application/json',
                'content-type': 'application/json' },
            body: { token:token,
              request : {
           outlet_id:x.OutLetId,
           mobile:req.body.mobile
         } },
            json: true };
            request(options, function (error, response, body1) {
              if(!error){
                console.log(body1)
                res.json(body1)
              }else{
                console.log(error)
              }

            });
              });
        }
      }else{
        res.json("0")
        console.log(err);
      }
    });
  });

  app.post('/Login', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/login',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:x.OutLetId,
     mobile:req.body.mobile
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        res.json(body)
                        console.log(body);
                      }

                    });
                  });
  });
  app.post('/verifyLogin', function(req,res){
    console.log(req.body)
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
      console.log(data1)
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/verifyLogin',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
          outlet_id:x.OutLetId,
          mobile:req.body.mobile,
          otp:req.body.otp
        } },
      json: true };
      request(options, function (error, response, body) {
         console.log(body)
        if(!error){
          if (body.statuscode == "TXN") {
            res.json({"status":"1","body":body.data})
          }else {
            res.json({"status":body.status,"body":body.status})
          }
        }else {
          console.log("unexpected error in Api");
        }

      });
  });
  });



  app.post('/fetchBalance', function(req,res){
    console.log("Fetch Balance Api")
    console.log(req.body)
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/fetchBalance',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
          outlet_id:x.OutLetId,
          mobile:req.body.mobile
        } },
      json: true };
      request(options, function (error, response, body) {
                if(error){
                  console.log(error);
                  res.json("Unable to process your request");
                  return false;
                }else{
                  console.log(body);
                  res.json(body)
                }
              });
            });
  });
  app.post('/validatePincode', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/validatePincode',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
          outlet_id:x.OutLetId,
           name:req.body.name,
           mobile:req.body.mobile,
           dob:req.body.dob,
           pincode:req.body.pincode
         }
       },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        console.log(body);
                        if(body.statuscode != 'TXN'){
                          res.json({"status":0,"message":body.status})
                        }else{
                          //for login into glod portal
                          var options = { method: 'POST',
                          url: 'https://www.instantpay.in/ws/digitalgold/customerRegistration',
                            headers:
                              { accept: 'application/json',
                                'content-type': 'application/json' },
                            body: { token:token,
                              request : {
                                  "outlet_id":x.OutLetId,
                                  "mobile":req.body.mobile,
                                  "name":req.body.name,
                                  "pincode":req.body.pincode,
                                  "dob":req.body.dob
                                  } },
                            json: true };
                            request(options, function (error, response, body) {
                              console.log(body);
                              if(body.status = "TXN"){
                                goldsavecustomer(req.session.id1,x.OutLetId,req.body.mobile,req.body.name,req.body.pincode,req.body.dob)
                                res.json(1);
                              }else{
                                res.json({"status":0,"message":body.status})
                              }
                            });
                        }
                      }
                    });
                  });
  });

  function goldsavecustomer(agentid,outletid,custnumber,custname,custpin,custdob){

    var custreg = hart.DigitalGoldCustomers;
    var cregistration = new custreg({
      agent_id:agentid,
      outletid:outletid,
      name:custname,
      MobileNumber:custnumber,
      pincode:custpin,
      dob:custdob,
      Status:"1"
    });
    cregistration.save(function (err, results) {
      console.log("Saved Successfully");
    });
  }
  app.post('/buyGold', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/buyGold',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
          outlet_id:x.OutLetId,
         	mobile:req.body.mobile,
           amount:req.body.amount,
           agent_id:req.session.id1,
           buy_type:req.body.buy_type,
           mode:req.body.mode
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        // console.log(body);
                        res.json(body)
                      }
                    });
                  });
  });
  app.post('/getBankList', function(req,res){
    console.log("Hitted")
  //  hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
    //  var x = JSON.parse(JSON.stringify(data1)); x.OutLetId
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/getBankList',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:"19027",
     mobile:"8331094518",
     sell_type:"amount"
   } },
      json: true };
      request(options, function (error, response, body) {
          if(error){
            console.log(error);
            res.json("Unable to process your request");
            return false;
          }else{
            console.log(body);
            res.json(body)
          }
        });
  //    });
  });
  app.post('/sellGoldVerify', function(req,res){
    console.log("Sell Gold Api Hitted")
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/sellGoldVerify',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:x.OutLetId,
     mobile:req.body.mobile,
     sell_type:req.body.sell_type,
     amount:req.body.amount
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                         console.log(body);
                          res.json(body)
                      }
                    });
                  });
  });
  app.post('/sellGoldConfirm', function(req,res){
    console.log(req.body);
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/sellGoldConfirm',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     tx_id:req.body.tx_id,
     outlet_id:x.OutLetId,
     mobile:req.body.mobile,
     sell_type:req.body.sell_type,
     bene_name:req.body.benef_name,
     bank_name:req.body.bankname,
     ifsc_code:req.body.ifsccode,
     account_no:req.body.acc_number,
     amount:req.body.amount,
     otp:req.body.otpenteredsell
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        console.log(body);
                        res.json(body)
                      }
                    });
                  });
  });
  app.post('/goldProducts', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/goldProducts',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:x.OutLetId,
     mobile:"8331094518"
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        console.log(body);
                      }
                      res.json(body)
                    });
                  });
  });
  app.post('/redeemGoldVerify', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/redeemGoldVerify',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:x.OutLetId,
     mobile:"8331094518",
     product_code:"4",
    	state:"andhra pradesh",
    	city:"vizianagaram",
    	pincode:"110015",
    	address:"dwarakanagar,vizianagaram",
    	landmark:"near vijnana bharathi school"
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        console.log(body);
                      }
                      res.json(body)
                    });
                  })
  });
  app.post('/redeemGoldConfirm', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/redeemGoldConfirm',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:x.OutLetId,
     mobile:"8331094518",
     tx_id:"1168784",
     otp:"504352"
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        console.log(body);
                      }
                      res.json(body)
                    });
                  });
  });
  app.post('/transactionHistory', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/transactionHistory',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:x.OutLetId,
     mobile:req.body.mobile,
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        //console.log(body);
                      }
                      res.json(body.data.transactions);
                    });
                  });
  });
  app.post('/fetchInvoice', function(req,res){
    hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
      var x = JSON.parse(JSON.stringify(data1));
    var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/digitalgold/fetchInvoice',
      headers:
        { accept: 'application/json',
          'content-type': 'application/json' },
      body: { token:token,
        request : {
     outlet_id:x.OutLetId,
     mobile:"8331094518",
     tx_id:"2440026"
   } },
      json: true };
      request(options, function (error, response, body) {
                      if(error){
                        console.log(error);
                        res.json("Unable to process your request");
                        return false;
                      }else{
                        console.log(body);
                      }
                      res.json(body)
                    });
                  });
  });
}
