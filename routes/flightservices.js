//this file contains functions that we are using in every flight flies.
var hart= require('../hart');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var url = hart.url;
var request = require('request');
var http = require("http");     //For Otp service
var ObjectId = require('mongodb').ObjectID;
var Transactions = hart.transaction_reports;
const Version = 'Version=';
const airshoppingstart = '<AirShoppingRQ Version="17.2" TransactionIdentifier="acde5a8d-3468-4c12-b">';
const docstart = '<Document>'
const docend = '</Document>'
const partystart = '<Party>'
const senderstart = '<Sender>'
const travelagencysenderstart = '<TravelAgencySender>'
const nameStart = '<Name>'
const nameEnd = '</Name>'
const contactsstart = '<Contacts>'
const contactstart = '<Contact>'
const emailcontactstart = '<EmailContact>'
const addressstart = '<Address>'
const addressend = '</Address>'
const emailcontactend = '</EmailContact>'
const contactend = '</Contact>'
const contactsend = '</Contacts>'
const pseudocitystart = '<PseudoCity>';
const pseudocityend = '</PseudoCity>';
const agencyidstart = '<AgencyID>';
const agencyidend = '</AgencyID>';
const travelagencysenderend = '</TravelAgencySender>';
const senderend = '</Sender>';
const partyend = '</Party>';


const CoreQuerystart = '<CoreQuery>';
const begintag = '<';
const OriginDestinationsstart = '<OriginDestinations>';
const OriginDestinationstart = 'OriginDestination';
const endingtag = '>';
const OriginDestinationKey = 'OriginDestinationKey=';
const Departurestart = '<Departure>';
const AirportCodestart = '<AirportCode>';
const AirportCodeend =  '</AirportCode>';
const DateStart = '<Date>'
const DateEnd = '</Date>';
const DepartureEnd = '</Departure>';
const ArrivalStart = '<Arrival>';
const ArrivalEnd = '</Arrival>';
const OriginDestinationEnd = '</OriginDestination>';
const OriginDestinationsEnd = '</OriginDestinations>';
const CoreQueryEnd = '</CoreQuery>';
const iatanumberstart = '<IATA_Number>'
const iatanumberend = '</IATA_Number>'
const querystart = '<Query>'
const offerstart = 'Offer'
const referenceVersionStart = '<ReferenceVersion>'
const referenceVersionEnd = '</ReferenceVersion>';
const PreferenceStart = '<Preference>';
const AirlinePreferencesStart = '<AirlinePreferences>';
const AirlineStart = '<Airline>'
const AirlineIDStart	= '<AirlineID>'
const AirlineIDEnd	= '</AirlineID>'
const AirlineEnd = '</Airline>'
const AirlinePreferencesEnd = '</AirlinePreferences>';
const CabinPreferencesStart = '<CabinPreferences>';
const CabinTypeStart = '<CabinType>';
const CodeStart = '<Code>';
const CodeEnd = '</Code>';
const OriginDestinationReferencesStart =  '<OriginDestinationReferences>';
const OriginDestinationReferencesEnd = '</OriginDestinationReferences>'
const CabinTypeEnd = '</CabinType>';
const CabinPreferencesEnd = '</CabinPreferences>';
const PreferenceEnd = '</Preference>';
const DatalistsStart = '<DataLists>';
const PassengerListStart = '<PassengerList>';
const PassengerStart = 'Passenger';
const PassengerId = 'PassengerID='
const ptcstart = '<PTC>';
const ptcend = '</PTC>';
const Individualstart = '<Individual>';
const surnamestart = '<Surname>';
const surnameend = '</Surname>';
const IndividualEnd = '</Individual>'
const PassengerEnd = '</Passenger>'
const DatalistsEnd = '</DataLists>';
const PassengerListEnd = '</PassengerList>';
const airshoppingend = '</AirShoppingRQ>';

//TIcketIssue
const TicketIssueRQStart = 'TicketIssueRQ';
const txnIdentifier = "TransactionIdentifier=";
const travelerreqstart = '<TravelerReq>';
const travelerreqend = '</TravelerReq>';
const TktTravelerStart = 'TktTraveler';
const TktTravelerEnd = '</TktTraveler>';
const element = 'Element=';
const TktFlightStart = 'TktFlight';
const TktFlightEnd = '/TktFlight';
const FlightRefInfoStart = 'FlightRefInfo';
const FlightRefInfoEnd = '</FlightRefInfo>';
const AssociationID = 'AssociationID=';
const AirlineCodeStart = '<AirlineCode>';
const AirlineCodeEnd = '</AirlineCode>';
const FlightNumberStart = '<FlightNumber>';
const FlightNumberEnd = '</FlightNumber>';
const RecordLocatorStart = '<RecordLocator>';
const RecordLocatorEnd = '</RecordLocator>';
const QualifierGroupStart = '<QualifierGroup>';
const TravelerStart = '<Traveler>';
const TravelerElementNumberStart = '<TravelerElementNumber>';
const TravelerElementNumberEnd = '</TravelerElementNumber>';
const FareGroupNumberStart = '<FareGroupNumber>';
const FareGroupNumberEnd = '</FareGroupNumber>';
const TravelerEnd = '</Traveler>';
const QualifierGroupEnd = '</QualifierGroup>';
const ReferenceDefinitionsStart = '<ReferenceDefinitions>;'
const ReferenceDefinitionsEnd = '</ReferenceDefinitions>;'
const TicketIssueRQEnd = '</TicketIssueRQ>';


//OrderCreateRQ

const OrderCreateRQStart = '<OrderCreateRQ Version="17.2" TransactionIdentifier="9234a126-7769-4e16-8">';
const orderstart = '<Order>';

const offeritemidstart = 'OfferItem';
const passengerrefsstart = '<PassengerRefs> ';
const passengerrefsend = '</PassengerRefs>';
const offeritemend = '</OfferItem>';
const offerend = '</Offer>';
const orderend = '</Order>';
const paymentsstart = '<Payments>';
const paymentstart = '<Payment>';
const typestart = '<Type>';
const typeend = '</Type>';
const methodstart =	'<Method>';
const otherstart = '<Other>';
const remarksstart = '<Remarks>';
const remarkstart =	'<Remark>';
const remarkend = '</Remark>';
const remarksend = '</Remarks>';
const otherend = '</Other>';
const methodend = '</Method>';
const amountstart = '<Amount>';
const amountend = '</Amount>';
const paymentend = '</Payment>';
const paymentsend = '</Payments>';
const ordercreaterqend = '</OrderCreateRQ>';
const offerID = 'OfferID=';
const owner = 'Owner=';
const responseID = 'ResponseID=';
const offerItemID = 'OfferItemID=';

//OfferRQ

const offerPriceRQStart = 'OfferPriceRQ'
const birtdatestart = '<Birthdate>'
const birtdateend = '</Birthdate>'
const genderstart = '<Gender>'
const genderend = '</Gender>'
const nametitlestart = '<NameTitle>'
const nametitleend = '</NameTitle>'
const givennamestart = '<GivenName>'
const givennameend = '</GivenName>'

const offerpriceRQEnd = '</OfferPriceRQ>'
const documenttgful = '<Document id="document"/>';
const documenttgful_offr = '<Document><Name>NDC GATEWAY</Name><ReferenceVersion>1.0</ReferenceVersion></Document>';
const Querystart = '<Query>';
const QueryEnd = '</Query>';



module.exports={
  // this function returns AirShoppingRQ Start tag
  AirShoppingRQStart:function(){
    return airshoppingstart;
  },
  // this function returns documentTag tag
  documentTag:function(nm,rv){
    return docstart + nameStart + nm + nameEnd + referenceVersionStart + rv + referenceVersionEnd + docend;
  },
  // this function returns partytagPseudoCity tag
  partytagPseudoCity:function(name,partyemail,pscitycode,agencyid){
    return partystart + senderstart + travelagencysenderstart + nameStart + name + nameEnd + contactsstart + contactstart +  emailcontactstart + addressstart + partyemail + addressend + emailcontactend + contactend + contactsend + pseudocitystart + pscitycode + pseudocityend + agencyidstart + agencyid + agencyidend + travelagencysenderend + senderend + partyend;
  },
  // this function returns partytagIATA tag
  partytagIATA:function(name,partyemail,iatanumber,agencyid){
    return partystart + senderstart + travelagencysenderstart + nameStart + name + nameEnd + contactsstart + contactstart +  emailcontactstart + addressstart + partyemail + addressend + emailcontactend + contactend + contactsend + iatanumberstart + iatanumber + iatanumberend + agencyidstart + agencyid + agencyidend + travelagencysenderend + senderend + partyend;
  },
  // this function returns partytagIATA_PseudoCity tag
  partytagIATA_PseudoCity:function(name,partyemail,iatanumber,agencyid,pscitycode){
    return partystart + senderstart + travelagencysenderstart + nameStart + name + nameEnd + contactsstart + contactstart +  emailcontactstart + addressstart + partyemail + addressend + emailcontactend + contactend + contactsend + pseudocitystart + pscitycode + pseudocityend + iatanumberstart + iatanumber + iatanumberend + agencyidstart + agencyid + agencyidend + travelagencysenderend + senderend + partyend;
  },
  // this function returns CoreQueryStatrt tag
  CoreQueryStatrt:function(){
    return CoreQuerystart + OriginDestinationsstart;
  },
  // this function returns origindestination tag
  origindestination:function(odk,sourceairportcode,depdate,destinationairportcode){
     return begintag + OriginDestinationstart+' '+OriginDestinationKey+'"'+odk+'"'+endingtag+Departurestart+AirportCodestart+sourceairportcode+AirportCodeend+DateStart+depdate+DateEnd+DepartureEnd+ArrivalStart+AirportCodestart+destinationairportcode+AirportCodeend+ArrivalEnd+OriginDestinationEnd;
  },
  // this function returns CoreQueryEnd tag
  CoreQueryEnd:function(){
    return OriginDestinationsEnd+CoreQueryEnd;
  },
  // this function returns PreferenceTag tag
  PreferenceTag:function(AirlineId,cabinclass,triptype){
       return PreferenceStart+CabinPreferencesStart+CabinTypeStart+CodeStart+cabinclass+CodeEnd+OriginDestinationReferencesStart+OriginDestinationReferencesEnd+CabinTypeEnd+CabinPreferencesEnd+PreferenceEnd;
  },
  // this function returns DataListsStartTag tag
  DataListsStartTag:function(){
    return DatalistsStart+PassengerListStart;
  },
  // this function returns PassengerTag tag
  PassengerTag:function(psngid,psngtype,surname){
    return begintag+PassengerStart+' '+PassengerId+'"'+psngid+'"'+endingtag+ptcstart+psngtype+ptcend+Individualstart+surnamestart+surname+surnameend+IndividualEnd+PassengerEnd;
  },
  // this function returns DataListsEndTag tag
  DataListsEndTag:function(){
    return PassengerListEnd+DatalistsEnd;
  },
  // this function returns AirShoppingEndTag tag
  AirShoppingEndTag:function(){
    return airshoppingend;
  },

  // this function returns TicketIssueStartTag tag
  TicketIssueStartTag:function(txnid){
    return begintag+TicketIssueRQStart+' '+txnIdentifier+'"'+txnid+'"'+endingtag+travelerreqstart;
  },
  // this function returns TicketTravellerStartTag tag

  TicketTravellerStartTag:function(psngrId){
      return begintag+TktTravelerStart+' '+element+'"'+psngrId+'"'+endingtag
  },
  // this function returns FlightRefInfoStartTag tag
  FlightRefInfoStartTag:function(sgementkey,airlineid,flightnumber,airportcode,date,airportcode2,classofservice){
    return begintag+FlightRefInfoStart+' '+AssociationID+'"'+sgementkey+'"'+endingtag+AirlineCodeStart+airlineid+AirlineCodeEnd+
    FlightNumberStart+
    flightnumber+
    FlightNumberEnd+
    Departurestart+
    AirportCodestart+
    airportcode+
    AirportCodeend+DateStart+date+DateEnd+DepartureEnd+
    ArrivalStart+AirportCodestart+airportcode2+AirportCodeend+ArrivalEnd+ClassOfServiceStart+classofservice+ClassOfServiceEnd+FlightRefInfoEnd;
  },
  // this function returns TktTravelerEndTag tag
  TktTravelerEndTag:function(){
    return TktTravelerEnd
  },
  // this function returns TktTravelerEndTag tag
  TravelerReqEndTag:function(){
    return travelerreqend;
  },
  // this function returns RecordLocatorTag tag

  RecordLocatorTag:function(rlocator){
    return RecordLocatorStart+rlocator+RecordLocatorEnd;
  },
  // this function returns QualifierGroupStartTag tag
  QualifierGroupStartTag:function(){
    return QualifierGroupStart;
  },
  // this function returns TravellerTag tag
TravellerTag:function(i,num){
    return TravelerStart+TravelerElementNumberStart+i+TravelerElementNumberEnd+FareGroupNumberStart+num+FareGroupNumberEnd+TravelerEnd;
  },
  // this function returns QualifierGroupEndTag tag
  QualifierGroupEndTag:function(){
    return QualifierGroupEnd;
  },
  // this function returns ReferenceDefinitionTag tag
ReferenceDefinitionTag:function(){
    return ReferenceDefinitionsStart;
  },
  // this function returns ReferenceDefinitionEndTag tag
  ReferenceDefinitionEndTag:function(){
    return ReferenceDefinitionsEnd+TicketIssueRQEnd;
  },

//OrderTicket
// this function returns OrderCreateRQStart tag

OrderCreateRQStart:function(){
   return OrderCreateRQStart;
 },
 // this function returns OrderCreateRQEnd tag
OrderCreateRQEnd:function(){
   return ordercreaterqend;
 },
 // this function returns orderStart tag
orderStart:function(){
   return orderstart;
 },
 // this function returns orderEnd tag
orderEnd:function(){
   return orderend;
 },
 // this function returns offerTag tag
offerTag:function(oID,ow,rID,itID,passengerRefs){
   return  begintag+offerstart+' '+offerID+ '"' + oID+ '"' + ' ' + owner +'"'+ow+'"'+ ' ' +responseID +'"'+rID+'"'+' '+endingtag;
 },
 // this function returns offerEndTag tag
offerEndTag:function(){
   return offerend;
 },
 // this function returns offerItemTag tag
offerItemTag:function(itID,passengerRefs){
   return begintag+offeritemidstart+ ' '+ offerItemID + '"'+itID+'"'+ endingtag +passengerrefsstart+passengerRefs+passengerrefsend +offeritemend;
 },
 // this function returns paymentsStart tag
paymentsStart:function(){
   return paymentsstart;
 },
 // this function returns paymentsEnd tag
paymentsEnd:function(){
   return paymentsend;
 },
 // this function returns paymentTag tag
paymentTag:function(type,remarks,amount){
   allremarks = '';
   //loop for remarks tag
   for(var i=0;i<remarks.length;i++){
       allremarks += remarkstart;
       allremarks += remarks[i];
       allremarks += remarkend;
   }
   return  paymentstart +
           typestart + type + typeend +
           methodstart +
           otherstart +
           remarksstart +
           allremarks +
           remarksend +
           otherend +
           methodend +
           amountstart + amount + amountend +
           paymentend;
 },
 // this function returns OfferPriceRQTag tag
OfferPriceRQTag:function(versionnumber,transactid){
   return begintag+offerPriceRQStart+' '+Version+'"'+versionnumber+'"'+' '+txnIdentifier+'"'+transactid+'"'+endingtag;
 },
 // this function returns DocumentTag tag
DocumentTag:function(){
   return documenttgful;
 },
 // this function returns DocumentTag_offer tag
DocumentTag_offer:function(){
   return documenttgful_offr;
 },
 // this function returns QueryStartTag tag
 QueryStartTag:function(){
   return Querystart;
 },

 // this function returns QueryEndTag tag
 QueryEndTag:function(){
   return QueryEnd;
 },
 // this function returns Offer_new_PassengerTag tag
Offer_new_PassengerTag:function(psngid,psngtype,adate,atitle,afname,alname){
   return begintag+PassengerStart+' '+PassengerId+'"'+psngid+'"'+endingtag+ptcstart+psngtype+ptcend+PassengerEnd;
 },
 // this function returns OfferPassengerTag tag
OfferPassengerTag:function(psngid,psngtype,adate,agender,atitle,afname,alname){
   return begintag+PassengerStart+' '+PassengerId+'"'+psngid+'"'+endingtag+ptcstart+psngtype+ptcend+Individualstart
   +birtdatestart+adate+birtdateend
   +genderstart+agender+genderend
   +nametitlestart+atitle+nametitleend
   +givennamestart+afname+givennameend
   +surnamestart+alname+surnameend+
   IndividualEnd+PassengerEnd;
 },
 // this function returns offerpriceRQEndTag tag
offerpriceRQEndTag:function(){
   return offerpriceRQEnd;
 }
}
