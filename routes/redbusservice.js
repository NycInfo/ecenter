var nodemailer  = require('nodemailer');
var async = require('async');
var utlity = require('./utlity');
var hart= require('../hart');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var request = require('request');
var bodyParser = require('body-parser');
var moment = require('moment');
var hmacsha1 = require('hmacsha1');
var request = require("request");
var token = hart.token;
const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss')
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";
const findwalletbal_err = "Error In Find Wallet Balance";
const errorinapireq_err = "Error In Requesting API";
const transactiion_suc_msg = "Transaction Success";
const transactiion_fail_msg = "Transaction Fail";
const Insufficientbalance = "In Insufficient Balance";
const sessionout = "Session Out";
const service_name =  "Bus Booking";


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.cmsImgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });

module.exports = function(app){
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.post('/Addbus123',function(req,res){

  try{
  utlity.loggermessage(req.session.id1,req.session.username,'BusSearch',datenow,ser_sart_msg,"Amount = "+req.body.rcamount+" ");
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,req.body.Network,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/buses/sources',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token: token },
    json: true };
    request(options, function (error, response, body) {
      if (error){
          utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,errorinapireq_err+error);
      }else {
        var x1 = JSON.parse(JSON.stringify(body.data));
        res.json(x1);
      }
    });
  }
  catch (err) {
    utlity.loggermessage('Unknown error in bus search - bus API:', err);
    return (err, { statusCode: '9999' });
  }
})

app.post('/bussearch',function(req,res){
  try{
  utlity.loggermessage(req.session.id1,req.session.username,'BusSearch',datenow,ser_sart_msg,"Amount = "+req.body.rcamount+" ");
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,req.body.Network,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/buses/search',
    headers:
     {accept: 'application/json',
       'content-type': 'application/json' },
    body:
     { token: token,
       request: { source: req.body.source, destination: req.body.destination, doj: req.body.date } },
    json: true };

  request(options, function (error, response, body) {
    if (error){
      utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,errorinapireq_err+error);
    }else {
      var x2 = JSON.parse(JSON.stringify(body.data));
      res.json(x2);
    }
  });
}
catch (err) {
  utlity.loggermessage('Unknown error in bus search - Bus API:', err);
  return (err, { statusCode: '9999' });
}
});


app.post('/seatlayouts',function(req,res){
  try{
  utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,ser_sart_msg,"Amount = "+req.body.Fare+" ");
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,service_name,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/buses/seatlayout',
    headers:
     { accept: 'application/json',
       'content-type': 'application/json' },
    body:
     { token: token,
       request: { tripid: req.body.busid } },
    json: true };

  request(options, function (error, response, body) {
    if (error){
      utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,errorinapireq_err+error);
    }else {
      var x3 = JSON.parse(JSON.stringify(body.data));
      res.json(x3);
    }
  });
}
catch (err) {
  utlity.loggermessage('Unknown error in seat layouts - bus API:', err);
  return (err, { statusCode: '9999' });
}

})
app.post('/BusTicketBooking',function(req,res){
  try{
  utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,ser_sart_msg,"Amount = "+req.body.Fare+" ");
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,service_name,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  var s=req.body;
  var text=[];
  if (Array.isArray(s.names1)) {
 	    var names=s.names1;var age= s.age;var gender= s.gender;
  }
  if (!Array.isArray(s.names1)) {
    var names=s.names1.split(",");
    var age= [s.age];var gender= [s.gender];
  }
     var sai=s.seatnumber.split(',');
    for (i = 0; i < names.length; i++) {
      if(gender[i]=='male'){var gen="MR";}else{var gen="MS";}
      text.push({'seatname':sai[i],'passengertitle':gen,'passengername':names[i],'passengerage':age[i],'passengergender':gender[i]});
    }
  s.seats=text;
  var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/buses/seatblock',
    headers:
     { 'Postman-Token': 'e0a95b2b-07be-4dcc-86cf-3ad7699a139e',
       'Cache-Control': 'no-cache',
       accept: 'application/json',
       'Content-Type': 'application/json' },
    body:
     { token: token,
       request:
        { source: req.body.source1,
          destination: req.body.destination1,
          doj: req.body.sourcedate1,
          tripid: req.body.bid,
          bpid: req.body.boarding,
          dpid: req.body.dropping,
          mobile: req.body.phone,
          email: req.body.email,
          idtype: 'aadhar',
          idnumber: '5464621384479841',
          address: 'Hyderabad',
          seats: text
            } },
    json: true };
  request(options, function (error, response, body) {
    if (error){
      utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,errorinapireq_err+error);
    }else {
      res.json(body);
    }

  });
}
catch (err) {
  utlity.loggermessage('Unknown error in bus ticket booking - bus API:', err);
  return (err, { statusCode: '9999' });
}
})



app.post('/confirm_booking',function(req,res){
  try{
  utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,ser_sart_msg,"Amount = "+req.body.Fare+" ");
  if(!req.session.id1){
    utlity.loggermessage(sessionout,sessionout,service_name,datenow,fail_msg,sessionout);
    next({ type: "UnauthorizedError", message: "Session not found" });
    return;
  }
  utlity.Commissionfun(req.body.subservices, function(result) {
    // var agent_commission=result[0];
    // var nihar_com=result[1];
      hart.wallets.findOne({ agentId : req.session.id1 },function(err,data){
        if(err){
          utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,findwalletbal_err+err);
        }
        else{
          var balance = data.Wallet;
          var Amount = req.body.Fare;
          if(balance > Amount){
            var options = { method: 'POST',
              url: 'https://www.instantpay.in/ws/buses/bookticket',
              headers:
               { 'Postman-Token': 'e0a95b2b-07be-4dcc-86cf-3ad7699a139e',
                 'Cache-Control': 'no-cache',
                 accept: 'application/json',
                 'Content-Type': 'application/json' },
              body:
               { token: token,
                 request:
                  { booking_id: req.body.id,
                    agentid: req.session.id1
                      } },
              json: true };

            request(options, function (error, response, body) {
              if (error){
                utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,errorinapireq_err+error);
              }
              var bp_commission=result.Agent_Margin/100;
              var nihar_commission=result.Nihar_Margin/100;
              var discount=Amount*result.Aggregator_Margin/100;
              var bp_earn=discount*bp_commission;
              var nihar_earn=discount*nihar_commission;
              var updamnt = balance-Amount;
              if (body.statuscode=="TXN") {
                res.json(body);
               // var Amount = body.data.charged_amt;
               // var NComm_Amount = nihar_com*Amount;
               // var discount=Amount*agent_commission;
               utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,suc_msg,transactiion_suc_msg);

               utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",body.data.booking_id,result.servicesName);
               // utlity.transactions(req.session.id1,req.body.services,updamnt,Amount,discount,nihar_com,body.status,agent_commission);
             mailticket(body.data.booking_id,req.body.age,req.body.dep,req.body.Ari,req.session.id1,Amount);
             }else {
               res.json(body);
               //res.json(2);

               utlity.transactions(req.session.id1,req.session.username,'ecenter',result.aggregatorCode,result.serviceCode,result.subservicesCode,updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Fail",'none',result.servicesName);
              // utlity.transactions(req.session.id1,req.body.services,updamnt,Amount,0,0,body.status,0);
              utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,"Api Responce : "+transactiion_fail_msg);
             //sendtickettomobile(1,req.body.Name,req.body.Mobile,req.body.Fare,req.body.Seat,req.body.date_jou,body.data.booking_id,body.statuscode);
             }
            });
          }
          else {
            utlity.loggermessage(req.session.id1,req.session.username,service_name,datenow,fail_msg,Insufficientbalance);
            res.json("12");
          }
        }
       });
});
}
catch (err) {
  utlity.loggermessage('Unknown error in confirm booking - bus API:', err);
  return (err, { statusCode: '9999' });
}
});
app.post('/Get_mob_can',function(req,res){
  hart.bustickets.findOne({ 'BookingID':req.body.id },function(err,data1) {
    if(!err){
      if(data1 == null){
        res.json("0");
      }else{
        res.json(data1);
      }
    }else{
      res.json("0")
    }

  })
})
app.post('/Cancellationcharge',function(req,res){
  var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/buses/cancellationcharge',
    headers:
     { 'Postman-Token': 'e0a95b2b-07be-4dcc-86cf-3ad7699a139e',
       'Cache-Control': 'no-cache',
       accept: 'application/json',
       'Content-Type': 'application/json' },
    body:
     { token: token,
       request:
        {booking_id: req.body.id
            } },
    json: true };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.json(body)
  });
})
app.post('/Cancelticket',function(req,res){
  try{
  var sts=req.body.seats.split(' ');
  var sts_str;
  if (sts[1]==undefined) {
    sts_str=sts[0];
  }else {
    sts_str=sts[0]+sts[1];
  }
  var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/buses/cancelticket',
    headers:
     { 'Postman-Token': 'e0a95b2b-07be-4dcc-86cf-3ad7699a139e',
       'Cache-Control': 'no-cache',
       accept: 'application/json',
       'Content-Type': 'application/json' },
    body:
     { token: token,
       request:
        { booking_id: req.body.id,
          seats_to_cancel:sts_str
        } },
    json: true };
  request(options, function (error, response, body) {
    if (error){console.log(error);}
    if (body.statuscode == "TXN") {
        hart.wallets.findOne({ agentId : req.session.id1 },function(err,data){
          if(err){ console.log(err);}
          else {
            var balance = data.Wallet;
            var bp_commission=0.7;
            var nihar_commission=0.3;
            var discount=10;
            var Amount = body.data[0].rerund_to_agent - discount;
            var bp_earn=discount*bp_commission;
            var nihar_earn=discount*nihar_commission;
            // var Amount = body.data[0].rerund_to_customer - 3;
            var updamnt = balance + Amount;
            utlity.transactions(req.session.id1,req.session.username,'ecenter','INP','TBS','BCS',updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success",body.data[0].cr_ipay_id,'Bus cancellation');
                var cant = hart.cancelbustickets;
                var can = new cant({
                  AgentId : req.session.id1,
                  BookingID: req.body.id,
                  cancelID:body.data[0].cr_ipay_id,
                  statuscode : body.statuscode,
                  booking_status: body.status,
                  rerund_to_agent: body.data[0].rerund_to_agent,
                  rerund_to_customer: body.data[0].rerund_to_customer ,
                  RefundAmount:Amount,
                  status:'1'
                });
              can.save(function (err, results) {
                if(err){
                  console.log(err);
                }else {
                  console.log('canceled recorded');
                  sendmsgtocustomer(req.body.Mobile,req.body.seats,req.body.id);
                  sendtickettomobile(1,"",req.session.Mobile,req.body.Fare,req.body.seats,"",req.body.id,body.statuscode,Amount);
                  res.json(body);
                }
              });

              // }else{
              //   console.log("Wallet Not Updated");
              // }
            // });

         }
        });

    }else {
      res.json(body);
    }
  });
}
catch (err) {
  utlity.loggermessage('Unknown error in cancel ticket - bus API:', err);
  return (err, { statusCode: '9999' });
}
});


function mailticket(bookingid,age,dep,arival,aid,total_fare) {
  var options = { method: 'POST',
    url: 'https://www.instantpay.in/ws/buses/ticket',
    headers:
     { 'Postman-Token': 'e0a95b2b-07be-4dcc-86cf-3ad7699a139e',
       'Cache-Control': 'no-cache',
       accept: 'application/json',
       'Content-Type': 'application/json' },
    body:
     { token: token,
       request:
        {booking_id: bookingid} },
    json: true };
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      console.log(body.data.inventoryItems);
      console.log(body.data);
      var names=[];var gender=[];var email=[];var fare=[];var seat=[];var age_res=[];var Mobileno=[];
      if (Array.isArray(body.data.inventoryItems)) {
        for (var i = 0; i < body.data.inventoryItems.length; i++) {
          names.push(body.data.inventoryItems[i].passenger.name);
          gender.push(body.data.inventoryItems[i].passenger.gender);
          email=body.data.inventoryItems[0].passenger.email;
          seat.push(body.data.inventoryItems[i].seatName);
          fare.push(body.data.inventoryItems[i].fare);
          age_res.push(body.data.inventoryItems[i].passenger.age);
          Mobileno=body.data.inventoryItems[0].passenger.mobile;
        }
      }
      if (!Array.isArray(body.data.inventoryItems)) {
          names=body.data.inventoryItems.passenger.name;
          gender=body.data.inventoryItems.passenger.gender;
          email=body.data.inventoryItems.passenger.email;
          seat=body.data.inventoryItems.seatName;
          fare=body.data.inventoryItems.fare;
          age_res=body.data.inventoryItems.passenger.age;
          Mobileno=body.data.inventoryItems.passenger.mobile;
      }
      if (body.statuscode == "TXN") {
          var booking = hart.bustickets;
          var agg = new booking({
          AgentId : aid,
          BookingID:bookingid,
          bookingstatus : body.status,
          Source: body.data.sourceCity,
          Destination:body.data.destinationCity,
          Travels: body.data.travels,
          Passenger: names,
          Pass_Email: email,
          Pass_Mobile:Mobileno,
          Fare: fare ,
          Date_jou: body.data.doj,
          Seats:seat,
          status:'1'
        });
        agg.save(function (err, results) {
          if(err){console.log(err);}else {console.log('booking recorded');}
        });

       var deptime=time(body.data.primeDepartureTime);
       var Picktime=time(body.data.pickupTime);
       console.log(Picktime,deptime);
       Mticket('booked',names,Mobileno,body.data.sourceCity,body.data.destinationCity,body.data.pnr,deptime[0],seat,body.data.pickupLocation,Picktime[0],body.data.pickupLocationLandmark,body.data.pickUpLocationAddress,body.data.travels,body.data.pickUpContactNo,bookingid,body.data.doj,total_fare);
       var Ticket='<body class="hold-transition skin-black sidebar-mini">';
      Ticket+='<div class="wrapper"> <div class="content-wrapper"> <section class="content-header"> <ol class="breadcrumb"> <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> <li class="breadcrumb-item"><a href="#">Examples</a></li> <li class="breadcrumb-item active">Invoice</li> </ol> </section> <div class="pad margin no-print"> <div class="callout callout-success" style="margin-bottom:0!important"> <h4><i class="fa fa-info"></i> Note:</h4> This page has been enhanced for printing. Click the print button at the bottom of the invoice to test. </div> </div> <section class="invoice" id="printableArea"> <div class="row"> <div class="col-12 table-responsive"> <div style="border:solid 1px #666"> <table width="100%" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr style="font-size:24px;color:#444">';
      Ticket+='<td width="45%" style="padding:20px 25px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;padding:20px 25px;color: #2b337d;font-weight: bold;">NihareCenter Bus Ticket </td> <td width="55%" align="right" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;padding:20px"><img src="'+hart.adminurl+'Public2/images/nihar-market-logo.png" alt="Nihar Market" class="CToWUd"></td> </tr> <tr> <td colspan="2" style="text-align:center;padding:8px 25px;font-size:32px;border-top:solid 1px #999;border-bottom:solid 1px #999;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle">'+body.data.sourceCity+' to '+body.data.destinationCity+' <span style="font-size:20px;padding-left:10px"><img src="'+hart.adminurl+'Public2/images/time.png" width="16px" width="16px" alt="" class="CToWUd"> '+moment(body.data.doj).format("DD-MM-YYYY")+'</span></td></tr><tr>';
      Ticket+='<td colspan="2" style="text-align:center;padding:8px 25px;font-size:22px;border-bottom:solid 1px #999;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle">'+body.data.travels+'<span style="display:block;color:#444;font-size:14px;line-height:22px">'+body.data.busType+'</span></td></tr>';
      Ticket+='<tr> <td colspan="2" style="padding:20px 60px 25px 60px;text-align:center;font-size:12px;color:#444;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle"><table width="100%" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr>';
      Ticket+='<td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle">Boarding Point<br> <h4 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;padding-top:5px">'+body.data.pickUpLocationAddress+' (Pickup Address)<br> </h4> <h5 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:13px;font-weight:normal">'+body.data.pickupLocationLandmark+' (Landmark)</h5></td> <td>&nbsp;</td> <td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle">Dropping Point';
      if (body.data.dropLocation==undefined||null||''||' ') {
        Ticket+='<h4 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;padding-top:5px">'+body.data.destinationCity+'(Dropping point is negligible)</h4> ';
      }else {
        Ticket+='<h4 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;padding-top:5px">'+body.data.dropLocation+'</h4> ';
      }

      Ticket+='<h5 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:13px;font-weight:normal"></h5></td></tr><tr><td colspan="3" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;padding-top:15px"><img src="'+hart.adminurl+'Public2/images/journey_points.png" class="CToWUd"> <div>&nbsp; </div></td> </tr> <tr> ';
      Ticket+='<td width="30%" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;padding-top:25px">Reporting Time <h3 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:24px;padding-top:5px"><span class="aBn" data-term="goog_734251630" tabindex="0"><span class="aQJ">15 min Before Boarding time</span></span></h3> <br> Boarding Time <h4 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;padding-top:5px"><span class="aBn" data-term="goog_734251631" tabindex="0">';
      Ticket+='<span class="aQJ">Boarding Time: '+Picktime[0]+'</span></span></h4></td> <td width="30%" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;vertical-align:bottom;line-height:18px"><img src="'+hart.adminurl+'Public2/images/call.png" width="20px" alt="" class="CToWUd"><br>'+body.data.travels+'<br>';
      if (body.data.pickUpContactNo==undefined||null||''||' ') {
        Ticket+='</td>';
      }else {
        Ticket+='<h6 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;font-weight:normal;color:#444;padding-bottom:5px">Pickup Contact : <a href="tel:'+body.data.pickUpContactNo+'" value="'+body.data.pickUpContactNo+'" target="_blank">'+body.data.pickUpContactNo+'</a></h6> <h6 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;font-weight:normal;color:#444;padding-bottom:5px"></h6></td>';
      }
      if (body.data.dropTime==undefined||null||''||' ') {
        Ticket+='<td width="30%" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle">Dropping Time <h3 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:24px;padding-top:5px">Dropping point is negligible</h3> <br> &nbsp; <h4 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;padding-top:5px">&nbsp;</h4></td> </tr> </tbody> </table></td> </tr> <tr> <td colspan="2" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;padding:0 25px">';
      }else {
        var dropdesTime=time(body.data.dropTime);
        var drop_mom;
        if (dropdesTime[1]!==0) {
          drop_mom=moment(body.data.doj).add(dropdesTime[1],'days').format("DD-MM-YYYY");
        }
        if (dropdesTime[1]==0) {
          drop_mom=moment(body.data.doj).format("DD-MM-YYYY");
        }
        Ticket+='<td width="30%" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle">Dropping Time <h3 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:24px;padding-top:5px">'+dropdesTime[0]+'</h3> <br> &nbsp; <h4 style="margin:0;padding:0;font-weight:bold;color:#000;font-size:14px;padding-top:5px">&nbsp;</h4></td> </tr> </tbody> </table></td> </tr> <tr> <td colspan="2" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;padding:0 25px">';
      }
      Ticket+='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:solid 1px #999;border-right:solid 1px #999;margin-bottom:40px"> <tbody> <tr> <th width="5%" style="border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">&nbsp;</th> <th width="30%" style="border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">Travellers</th> <th width="10%" style="border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">Age/Gender</th> <th width="10%" style="border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">Seat </th> <th style="border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">Details</th> </tr>';
      if (Array.isArray(body.data.inventoryItems)) {
        for (var j = 0; j < body.data.inventoryItems.length; j++) {
         Ticket+='<tr><td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222;padding:10px;text-align:center"><img src="'+hart.adminurl+'Public2/images/male.png" alt="" class="CToWUd"> </td> <td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">'+names[j]+'</td>';
         Ticket+=' <td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">'+age_res[j]+'/'+gender[j]+'</td> <td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">'+seat[j]+'</td>';
         Ticket+='<td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222; text-align:center;"><img src="https://ci3.googleusercontent.com/proxy/hSiR5qfItXWk3daAfAmpybhL9x1Ow-cU7TF-tvb8z40RyLL7glQ8RF5mGe-ld2fFZSY06YvqStmIcAPwinvTazk=s0-d-e1-ft#https://www.abhibus.com/img/print/rupee1.png" alt="" class="CToWUd">'+fare[j]+'<br><p style="text-align:center;margin:0;padding:0;font-size:12px;line-height:20px"><strong>Service Tax <img src="https://ci3.googleusercontent.com/proxy/hSiR5qfItXWk3daAfAmpybhL9x1Ow-cU7TF-tvb8z40RyLL7glQ8RF5mGe-ld2fFZSY06YvqStmIcAPwinvTazk=s0-d-e1-ft#https://www.abhibus.com/img/print/rupee1.png" alt="" class="CToWUd">&nbsp;'+body.data.serviceCharge+'</strong></p></td></tr>';
        }
          Ticket+='<tr><td colspan="5" style:"border: 1px solid #999;border-top: 0px;border-right: 0px; padding:0px 10px;"><span style="margin-right:30px;font-size:12px;line-height:20px"><span style="font-weight:bold;">NihareCenter Booking ID:</span> '+bookingid+'</span> <span style="margin-right:30px;font-size:12px;line-height:20px"><span style="font-weight:bold;">Operator PNR:</span> '+body.data.pnr+'</span><span style="margin-right:30px;font-size:12px;line-height:20px;font-size:14px;line-height:30px"> <span style="font-weight:bold;">Booked on :</span> '+moment(body.data.dateOfIssue).format("DD-MM-YYYY h:mm:ss")+'<span class="aBn" data-term="goog_734251632" tabindex="0"><span class="aQJ"></span></span></span></td></tr>';
      }else {
        Ticket+='<tr><td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222;padding:10px;text-align:center"><img src="'+hart.adminurl+'Public2/images/male.png" alt="" class="CToWUd"> </td> <td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">'+names+'</td>';
        Ticket+=' <td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">'+age_res+'/'+gender+'</td> <td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222">'+body.data.inventoryItems.seatName+'</td>';
        Ticket+='<td rowspan="6" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;border-bottom:solid 1px #999;border-left:solid 1px #999;padding:5px;font-size:14px;color:#222;padding:10px;text-align:center"><p style="text-align:center;margin:0;padding:0;font-size:12px;line-height:20px">NihareCenter Booking ID: '+bookingid+'</p> <p style="text-align:center;margin:0;padding:0;font-size:12px;line-height:20px">Operator PNR: '+body.data.pnr+'</p> <span style="border:solid 2px #999;display:inline-block;padding:5px 40px;margin:10px 0;font-size:18px;font-weight:bold"><img src="https://ci3.googleusercontent.com/proxy/hSiR5qfItXWk3daAfAmpybhL9x1Ow-cU7TF-tvb8z40RyLL7glQ8RF5mGe-ld2fFZSY06YvqStmIcAPwinvTazk=s0-d-e1-ft#https://www.abhibus.com/img/print/rupee1.png" alt="" class="CToWUd">'+fare+'</span><br> <p style="text-align:center;margin:0;padding:0;font-size:12px;line-height:20px">';
        Ticket+='<strong>Service Tax <img src="https://ci3.googleusercontent.com/proxy/hSiR5qfItXWk3daAfAmpybhL9x1Ow-cU7TF-tvb8z40RyLL7glQ8RF5mGe-ld2fFZSY06YvqStmIcAPwinvTazk=s0-d-e1-ft#https://www.abhibus.com/img/print/rupee1.png" alt="" class="CToWUd">&nbsp;'+body.data.serviceCharge+'</strong></p> <p style="text-align:center;margin:0;padding:0;font-size:12px;line-height:20px;font-size:14px;line-height:30px"> Booked on : '+moment(body.data.dateOfIssue).format("DD-MM-YYYY h:mm:ss")+'<span class="aBn" data-term="goog_734251632" tabindex="0"><span class="aQJ"></span></span></p></td> </tr>';
      }
      Ticket+=' </tbody> </table></td> </tr> </tbody></table><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:solid 2px #666;border-bottom:solid 2px #666;padding-left:25px;padding-right:5px"><tbody><tr><td colspan="8" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;font-size:11px;color:#444;text-align:left;padding:0 0 20px 0;margin:0">';
      Ticket+='<h2 style="font-size:18px;color:#444;font-weight:normal;margin:0;padding:10px 0 15px 0;padding:10px 0 0 0;padding:10px 0 0 25px">Customer Support and Enquiries</h2></td></tr><tr><td style="font-size:11px;color:#444;text-align:left;padding:0 0 20px 0;margin:0;width:5%;width:2%;padding-left:25px;padding-right:5px"><img src="'+hart.adminurl+'Public2/images/call.png" width="20px" alt="" class="CToWUd"></td>';
      Ticket+='<td style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle;font-size:11px;color:#444;text-align:left;padding:0 0 20px 0;margin:0;width:20%;width:23%">Nihar Market Customer Care (24*7) <br><strong style="display:block;font-size:14px;font-weight:normal;color:#444;padding-top:5px"><a href="tel:1860%20200%209999" value="+9118602009999" target="_blank">18602009999</a></strong></td><td style="font-size:11px;color:#444;text-align:left;padding:0 0 20px 0;margin:0;width:5%;width:2%;padding-left:25px;padding-right:5px">&nbsp;</td><td style="font-size:11px;color:#444;text-align:left;padding:0 0 20px 0;margin:0;width:20%;width:23%">&nbsp;<br><strong style="display:block;font-size:14px;font-weight:normal;color:#444;padding-top:5px">&nbsp;</strong></td><td style="font-size:11px;color:#444;text-align:left;padding:0 0 20px 0;margin:0;width:5%;width:2%;padding-left:25px;padding-right:5px">&nbsp;</td>';
      Ticket+='<td style="font-size:11px;color:#444;text-align:left;padding:0 0 20px 0;margin:0;width:20%;width:23%">&nbsp;</td></tr></tbody></table><table width="100%" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td width="70%" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-weight:normal;vertical-align:middle"><h2 style="font-size:18px;color:#444;font-weight:normal;margin:0;padding:10px 0 15px 0;padding:10px 0 0 0;padding:20px 25px;vertical-align:top!important;margin:0;padding:0">Terms and Conditions</h2><ul style="margin:20px 0;padding:0"><li style="margin-left:25px;margin-bottom:10px;padding:0;font-size:13px;color:#444"> The arrival and departure times mentioned on the ticket are only tentative timings. </li><li style="margin-left:25px;margin-bottom:10px;padding:0;font-size:13px;color:#444"> Passengers are requested to arrive at the boarding point at least 15 minutes before the scheduled time of departure</li>';
      Ticket+='<li style="margin-left:25px;margin-bottom:10px;padding:0;font-size:13px;color:#444"> NihareCenter is not responsible for any accidents or loss of passenger belongings.</li><li style="margin-left:25px;margin-bottom:10px;padding:0;font-size:13px;color:#444"> NihareCenter is not responsible for any delay or inconvenience during the journey due to breakdown of the vehicle or other reasons beyond the control of NihareCenter.</li><li style="margin-left:25px;margin-bottom:10px;padding:0;font-size:13px;color:#444"> If a bus/service is canceled, for tickets booked through NihareCenter, the refund money is paid back to the passenger through cash by the NihareCenter/Franchise/Agent.</li><li style="margin-left:25px;margin-bottom:10px;padding:0;font-size:13px;color:#444"> Cancellation charges are applicable on Original fare but not on the Discounted Fare.</li></ul></td>';
      Ticket+='</tr></tbody></table></td></tr></tbody></table></div></div></div>';
      Ticket+='</section></div></div></body>';
          var mailOptions = {
            from: 'customerservice@niharecenter.com',
            to: email,
            subject: "Bus Ticket",
            html: Ticket
          };
          hart.transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
      }
    });
}
function time(str){
  var t= parseInt(str);
  var min=t%60;
var multiplier=(t-min)/60;
var days=multiplier/24;
var hours=multiplier%24;
var jday='';
if (days >= 1 && days < 2) {
  jday=1;
}
if (days < 1) {
  jday=0;
}
if (days >= 2 && days < 2) {
  jday=2;
}
if (min==0) {
  min='00';
}
return [hours+':'+min,jday];
}
function Mticket(x,name,mobile,source,dorp,pnr,primedep_time,seats,pick_loc,pick_time,pick_landmark,pick_loc_add,travels,pick_Contact,booking_id,doj,total_fare){
    var txt='';
    var doj_f=moment(doj).format("DD-MM-YYYY");
    if (x=='booked') {
      console.log(mobile);
      console.log(doj_f);
      txt+='NihareCenter MTicket.. Please Show this ticket while boarding.\n Route:  '+source+' to '+dorp+',\n Passenger Name: '+name+',\n Date of Journey: '+doj_f+',\n Total-fare: '+total_fare+',\n BookingID: '+booking_id+',\n PNR: '+pnr+',\n Travels: '+travels+',\n Seat Numbers: '+seats+',\n Boarding Point: '+pick_loc+' '+pick_time+'.\n Landmark: '+pick_landmark+',\n Address: '+pick_loc_add+',\n Boarding Point-contact: '+travels+' '+pick_Contact+'.\n Thanks for using NihareCenter.'
    }
    var options = { method: 'GET',
    url: 'https://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
    qs:
     { type: 'smsquicksend',
       user: 'niharinfo',
       pass: 'Nihar@123',
       sender: 'NIHAAR',
       to_mobileno: mobile,
       sms_text: txt },
    headers:
     {
       'cache-control': 'no-cache' } };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    console.log(body);
  });
}
function sendtickettomobile(x,name,mobile,fare,seat,date_jou,bookingid,status,refund){
if(x == 1){
  if (status == "TXN") {
    if (refund == undefined) {
      var txt = "Dear Customer Your Bus Cancellation is success.. with BookingID: "+bookingid+", seat number:"+seat;
    }else {
      var txt = "Dear Agent Your Bus Cancellation is success.. with BookingID: "+bookingid+", seat number:"+seat+" and Amount refund of Rs "+refund+" is added to your wallet.";
    }
  }else {
    var txt = "Dear "+name+" Your Bus Booking is failed.. Please try again";
  }

}else{
  var txt = "Dear "+name+ ", thanks for registering with us. your credentials for login NihareCenter. UserName: "+email+ " Password: "+pswd;
 }

  var options = { method: 'GET',
  url: 'https://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
  qs:
   { type: 'smsquicksend',
     user: 'niharinfo',
     pass: 'Nihar@123',
     sender: 'NIHAAR',
     to_mobileno: mobile,
     sms_text: txt },
  headers:
   {
     'cache-control': 'no-cache' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
  console.log(body);
});

}

function sendmsgtocustomer(mobile,seat,bookingid){
  var txt = "Dear Customer Your Bus Cancellation is success.. with BookingID: "+bookingid+", seat number:"+seat;
  var options = { method: 'GET',
  url: 'https://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
  qs:
   { type: 'smsquicksend',
     user: 'niharinfo',
     pass: 'Nihar@123',
     sender: 'NIHAAR',
     to_mobileno: mobile,
     sms_text: txt },
  headers:
   {
     'cache-control': 'no-cache' } };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    console.log(body);
  });
}


  app.post('/GetCanelReports_AD', function(req,res){
    console.log(req.body);
    if(req.body.fromdate != "" && req.body.todate != ""){
      var fromdate=req.body.fromdate;
      var todate=req.body.todate;
        var enddate = utlity.addoneday(todate);
      hart.cancelbustickets.find({"$and" : [{canceledAt:{"$gte": fromdate, "$lte": enddate}},{AgentId: req.session.Agentid }]}).exec(function(err,doc){
        if(!err){
          res.json(doc)
        }else{
          console.log(err);
    }
  });
}else{
        hart.cancelbustickets.find({'AgentId':req.session.Agentid}).sort({canceledAt:-1}).exec(function(err, data){
          if(err){
            console.log(err);
          }
          else{
            res.json(data);
          }
        });
      }
    });
    app.post('/GetCanelReports_ND', function(req,res){
      console.log(req.body);
      if(req.body.fromdate != "" && req.body.todate != ""){
        var fromdate=req.body.fromdate;
        var todate=req.body.todate;
        var enddate = utlity.addoneday(todate);
        hart.cancelbustickets.find({"$and" : [{canceledAt:{"$gte": fromdate, "$lte": enddate}}]}).exec(function(err,doc){
          if(!err){
            res.json(doc)
          }else{
            console.log(err);
      }
    })
    }else{
      console.log('okokonani');
        hart.cancelbustickets.find({}).sort({canceledAt:-1}).exec(function(err, data){
          if(err){
            console.log(err);
          }
          else{
            res.json(data);
          };
        });
  }
});
}
