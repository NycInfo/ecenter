var crypto = require('crypto');
var hart= require('../hart');
var utlity = require('./utlity');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var token = hart.token;
var request = require('request');
var http = require("http");
var ObjectId = require('mongodb').ObjectID;
module.exports = function(app){
// app.use(session({
//     secret: 'I9Admin-Flag',
//     resave: true,
//     saveUninitialized: true,
//     name : 'I9Admin',
// }));



app.post('/hotellist',function(req,res){
  var options = { method: 'GET',
    //url: 'http://affiliate-staging.ap-southeast-1.elasticbeanstalk.com/api/v2/hotels/listing?no_of_entry=50&page=3',
    url: 'https://api.oyorooms.com/api/v2/hotels/listing?no_of_entry=50&page=8',
    headers:
     { 'Postman-Token': '16ffd946-ebc0-463b-8df3-4f7d5caae59c',
       'Cache-Control': 'no-cache',
       'Content-Type': 'application/json',
       //'ACCESS-TOKEN': 'NWtzMVhzNTlKeHZ1eGFtbm5zdHk6TVdIVHNGa3huRE1nZmRka0tLeGQ=' } };  //Test Token
       'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' } };

      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        var doc1 = JSON.parse(body);

        res.json(doc1);
        var Hoteldata = hart.hoteldata;
           Hoteldata.findOne({"page" :doc1.page},function(err,doc12){
             if(err){
               console.log(err);
             }
               if(doc12 == null){
                 var vaaa = "Complimentary Veg Breakfast";
                 for (var i= 0; i < doc1.hotels.length; i++) {
                // console.log(i);
                 var hotel = new Hoteldata({
                   page:doc1.page,
                   id:doc1.hotels[i].id,
                   name:doc1.hotels[i].name,
                   small_address:doc1.hotels[i].small_address,
                   city:doc1.hotels[i].city,
                   state:doc1.hotels[i].state,
                   country:doc1.hotels[i].country,
                   zipcode:doc1.hotels[i].zipcode,
                   description:doc1.hotels[i].description,
                   images:doc1.hotels[i].images,
                   landing_url:doc1.hotels[i].landing_url,
                   latitude:doc1.hotels[i].latitude,
                   longitude:doc1.hotels[i].longitude,
                   oyo_home:doc1.hotels[i].oyo_home,
                   amenities:{
                     ComplimentaryVegBreakfast:doc1.hotels[i].amenities["Complimentary Veg Breakfast"],
                     AC:doc1.hotels[i].amenities.AC,
                     Geyser: doc1.hotels[i].amenities.Geyser,
                     RoomHeater:doc1.hotels[i].amenities["Room Heater"],
                     MiniFridge :doc1.hotels[i].amenities["Mini Fridge"],
                     TV:doc1.hotels[i].amenities.TV,
                     HairDryer:doc1.hotels[i].amenities["Hair Dryer"],
                     BathTub:doc1.hotels[i].amenities["Bath Tub"],
                     SwimmingPool:doc1.hotels[i].amenities["Swimming Pool"],
                     Kitchen:doc1.hotels[i].amenities.Kitchen,
                     LivingRoom:doc1.hotels[i].amenities["Living Room"],
                     ComplimentaryBreakfast:doc1.hotels[i].amenities["Complimentary Breakfast"],
                     Inhouse_Restaurant:doc1.hotels[i].amenities["In house Restaurant"],
                     ParkingFacility:doc1.hotels[i].amenities["Parking Facility"],
                     CardPayment:doc1.hotels[i].amenities["Card Payment"],
                     FreeWifi: doc1.hotels[i].amenities["Free Wifi"],
                     Powerbackup:doc1.hotels[i].amenities["Power backup"],
                     LiftElevator:doc1.hotels[i].amenities["Lift Elevator"],
                     ConferenceRoom:doc1.hotels[i].amenities["Conference Room"],
                     BanquetHall:doc1.hotels[i].amenities["Banquet Hall"],
                     Gym:doc1.hotels[i].amenities.Gym,
                     WheelchairAccessible:doc1.hotels[i].amenities["Wheelchair Accessible"],
                     Laundry:doc1.hotels[i].amenities.Laundry,
                     Bar:doc1.hotels[i].amenities.Bar,
                     Kindle:doc1.hotels[i].amenities.Kindle,
                     Spa:doc1.hotels[i].amenities.Spa,
                     WellnessCentre:doc1.hotels[i].amenities["Wellness Centre"],
                     Netflix:doc1.hotels[i].amenities.Netflix,
                     PetFriendly:doc1.hotels[i].amenities["Pet Friendly"],
                     CCTVCameras:doc1.hotels[i].amenities["CCTV Cameras"],
                     DiningArea:doc1.hotels[i].amenities["Dining Area"],
                     HDTV:doc1.hotels[i].amenities.HDTV,
                     ElectricityChargesIncluded:doc1.hotels[i].amenities["Electricity Charges Included"],
                     cricketstadium:doc1.hotels[i].amenities["cricket stadium"],
                     farzi:doc1.hotels[i].amenities.farzi,
                     PreBookMeals:doc1.hotels[i].amenities["Pre Book Meals"]},
                     category:doc1.hotels[i].category,
                     policies:doc1.hotels[i].policies,
                     status:1,
                 });
                 hotel.save(function(err,results) {
                   console.log("Saved ")
                 });
               }
             }
             });

      });
})

app.post('/GetHotelsData',function(req,res){
     var Hoteldata = hart.hoteldata;
     Hoteldata.find({}).exec(function(err,data) {
       res.json(data);
     });
});
// app.get('/search_hotel', function(req, res) {
//     var regex = new RegExp(req.query["term"], 'i');
//     var Hoteldata  = hart.hoteldata;
  //  var query = Hoteldata.find({$or : [ {city : regex},{ 'city': 1 },{"state" : regex},{ 'state': 1 },{"country":regex},{ 'country': 1 },{"small_address":regex},{ 'small_address': 1 }]});
//   var query = Hoteldata.find({city: regex},{ 'city': 1 }).limit(1);
//    query.exec(function(err, users) {
//
//        if (!err){
//           var result = users;
//           res.send(result, {
//              'Content-Type': 'application/json'
//           }, 200);
//        } else {
//           res.send(JSON.stringify(err), {
//              'Content-Type': 'application/json'
//           }, 404);
//        }
//     });
// });
app.get('/search_hotel', function(req, res) {
    var regex = new RegExp(req.query["term"], 'i');
    var Hoteldata  = hart.hoteldata;
    var query = Hoteldata.find({$or : [ {small_address : regex},{ 'small_address': 1 },{city: regex},{ 'city': 1 },{state:regex},{ 'state': 1 }]});
  //var query = Hoteldata.find({small_address: regex},{ 'small_address': 1 },{city: regex},{ 'city': 1 },).limit(1);
   query.exec(function(err, users) {

       if (!err){
          var result = users;
          res.send(result, {
             'Content-Type': 'application/json'
          }, 200);
       } else {
          res.send(JSON.stringify(err), {
             'Content-Type': 'application/json'
          }, 404);
       }
    });

});

app.get('/search_hotelAddress', function(req, res) {
    var regex = new RegExp(req.query["term"], 'i');
    var Hoteldata  = hart.hoteldata;
  //  var query = Hoteldata.find({$or : [ {city : regex},{ 'city': 1 },{"state" : regex},{ 'state': 1 },{"country":regex},{ 'country': 1 },{"small_address":regex},{ 'small_address': 1 }]});
  var query = Hoteldata.find({small_address: regex},{ 'small_address': 1 });
   query.exec(function(err, users) {

       if (!err){
          var result = users;
          res.send(result, {
             'Content-Type': 'application/json'
          }, 200);
       } else {
          res.send(JSON.stringify(err), {
             'Content-Type': 'application/json'
          }, 404);
       }
    });
});
app.post('/hotelsdatawithcity',function(req,res){
  console.log(req.body);

     var Hoteldata = hart.hoteldata;
     var oyotype = {};
     if(req.body.oyotype != ""){
       oyotype = { category: { $in: req.body.oyotype }}
     }
     // if(req.body.amenitAC != ""){
     //   aminityac = { amenities.AC: { $in: false }}
     // }
     //var skipby = 0;
     var Query = { "$and": [{"city": req.body.city},oyotype] };
       Hoteldata.find(Query).sort({_id:-1}).exec(function(err,doc1){
         if(err){
           console.log(err);
         }else{
           console.log(doc1.length);
           res.json(doc1);
         }
       });
     });

app.post('/hotelfulldetails',function(req,res){
     var Hoteldata = hart.hoteldata;
     Hoteldata.findOne({ id : req.body.hotelid },function(err,data){
       res.json(data);
     });
});


app.post('/hotelavailability',function(req,res){

  var request = require("request");
  var options = { method: 'POST',
  url: 'http://affiliate-staging.ap-southeast-1.elasticbeanstalk.com/api/v2/hotels/get_availability',
  //url: 'https://api.oyorooms.com/api/v2/hotels/get_availability?cp=true',
  headers:
  { 'Postman-Token': '962cfe41-11b3-406e-af9f-b0018610e73c',
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json',
    'ACCESS-TOKEN': 'NWtzMVhzNTlKeHZ1eGFtbm5zdHk6TVdIVHNGa3huRE1nZmRka0tLeGQ==' },
  body:
  { HotelAvailability:
     { HotelID: [ req.body.hotelid ],
       City: '',
       checkInDate: req.body.checkin,
       checkOutDate: req.body.checkout,
       adults: parseInt(req.body.adults),
       children: parseInt(req.body.children),
       child_1_age: 0,
       child_2_age: 0,
       rooms: parseInt(req.body.numberofrooms) } },
  json: true };

  request(options, function (error, response, body) {
  if (error) throw new Error(error);
  res.json(body);
  //var data = JSON.stringify(body);
  //console.log(data);
  });
});

// var request = require("request");
//
// var options = { method: 'POST',
//   url: 'http://affiliate-staging.ap-southeast-1.elasticbeanstalk.com/api/v2/hotels/get_availability',
//   headers:
//    { 'Postman-Token': '91b2ed1c-8d10-4665-8d90-1ede24bdb064',
//      'Cache-Control': 'no-cache',
//      'Content-Type': 'application/json',
//      'ACCESS-TOKEN': 'NWtzMVhzNTlKeHZ1eGFtbm5zdHk6TVdIVHNGa3huRE1nZmRka0tLeGQ=' },
//   body:
//    { HotelAvailability:
//       { HotelID: [ 'GRG024' ],
//         City: '',
//         checkInDate: '12/08/2018',
//         checkOutDate: '13/08/2018',
//         adults: 2,
//         children: 0,
//         child_1_age: 0,
//         child_2_age: 0,
//         rooms: 2 } },
//   json: true };
//
// request(options, function (error, response, body) {
//   if (error) throw new Error(error);
//
//   console.log(body);
// });




app.post('/hotelbooking',function(req,res){
    console.log(req.body);
    var collection = hart.wallets;
    collection.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        console.log(err);
      }
      else{
        var balance = data.Wallet;
        var Amount = req.body.totalamount
        if(balance > Amount){
          var updamnt = balance - Amount;
        //  console.log("ok Balance is there");
        var commsn = Amount/10;
        var nihar_com  = Amount/2;
        var agent_commission = commsn/2;
        var discount = 0;
          var request = require("request");
          var options = { method: 'POST',
            //url: 'http://affiliate-staging.ap-southeast-1.elasticbeanstalk.com/api/v2/vendor/bookings',
            url: 'https://api.oyorooms.com/api/v2/vendor/bookings',
            headers:
             { 'Postman-Token': '87cf3113-7817-4ccd-bfc0-ddfc4ba6d4de',
               'Cache-Control': 'no-cache',
               'Content-Type': 'application/json',
               'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' },
            body:
            { guest:
               { first_name: req.body.firstname,
                 last_name: req.body.lastname,
                 email: req.body.emailid,
                 country_code: '+91',
                 phone: req.body.mobilenumber },
              booking:
               { single: 0,
                 double: parseInt(req.body.DoubleRoom),
                 extra: 0,
                 checkin: req.body.checkin,
                 checkout: req.body.checkout,
                 hotel_id: req.body.hotelid,
                 //is_provisional: 'true',
                 external_reference_id: req.body.orderid },
                 payments:
                 { bill_to_affiliate : "true"} },
            json: true };
          request(options, function (error, response, body) {
            if (error) throw new Error(error);
              res.json(body);
              if(body.status == "Confirm Booking"){
                savehotelreports(req.session.id1,body.booking_identifier,body.booking_modification_allowed,body.booking_status_id,body.checkin,body.checkout,body.country,body.country_name,body.currency_symbol,body.external_reference_id,Amount,body.final_amount,body.get_payment_status_id,body.gstn_enabled,body.guest_email,body.guest_name,body.guest_phone,body.id,body.invoice,body.is_modifiable,body.no_of_guest,body.roomCount,"1");
                utlity.transactions(req.session.id1,"HotelBooking",updamnt,Amount,agent_commission,nihar_com,"Success",discount);
              }else{
                console.log(body);
              }
          });

        }else {
          res.json("0")
        }
      }
     })
});

function savehotelreports(agentId,booking_identifier,booking_modification_allowed,booking_status_id,checkin,checkout,country,country_name,currency_symbol,external_reference_id,totalamount,final_amount,get_payment_status_id,gstn_enabled,guest_email,guest_name,guest_phone,id,invoice,is_modifiable,no_of_guest,roomCount,status){
  var hotelBooking = hart.hotelsbookingdetails;
   var hotel = new hotelBooking({
     agentId :agentId,
     booking_identifier : booking_identifier,
     booking_modification_allowed : booking_modification_allowed,
     booking_status_id :booking_status_id,
     checkin : checkin,
     checkout : checkout,
     country : country,
     country_name : country_name,
     currency_symbol : currency_symbol,
     external_reference_id : external_reference_id,
     TotalAmount:totalamount,
     final_amount : final_amount,
     get_payment_status_id :get_payment_status_id,
     gstn_enabled :gstn_enabled,
     guest_email : guest_email,
     guest_name : guest_name,
     guest_phone : guest_phone,
     id : id,
     invoice : invoice,
     is_modifiable : is_modifiable,
     no_of_guest : no_of_guest,
     roomCount : roomCount,
     status:status,
   });
   hotel.save(function(err,results) {
     if(err){
       console.log(err);
     }else{
       console.log(results);
     }
   });
}
}

//
// booking_identifier : null
// booking_modification_allowed : true
// booking_status_id : 0
// checkin : "2018-08-22"
// checkout : "2018-08-23"
// country : "India"
// country_name : "India"
// currency_symbol : "₹"
// external_reference_id : "NMECH100896"
// final_amount : 2082
// get_payment_status_id : 2
// gstn_enabled : true
// guest_email : "narayanamurthykk@gmail.com"
// guest_name : "Nani"
// guest_phone : "8331994610"
// id : 37506594
// invoice : "CKVU7502"
// is_modifiable : true
// meals_hash : []
// no_of_guest : 2
// roomCount : 1
// room_category : {room_category_id: 1, room_category_name: "Classic (2X)", room_category_priority: 0, icon_code: null, restricted_amenities: Array(0)}
// status : "Confirm Booking"



// booking_identifier : null
// booking_modification_allowed : true
// booking_status_id : 12
// checkin : "2018-08-23"
// checkout : "2018-08-24"
// country : "India"
// country_name : "India"
// currency_symbol : "₹"
// external_reference_id : "NMECH384316"
// final_amount : 2801
// get_payment_status_id : 0
// gstn_enabled : true
// guest_email : "nani@gmail.com"
// guest_name : "Nani"
// guest_phone :  "8331994610"
// id : 37503680
// invoice : "EVDP5346"
// is_modifiable : true
// meals_hash : []
// minimum_prepaid_payment_time : "Sat, 18 Aug 2018 12:33:14"
// no_of_guest : 2
// roomCount : 1
// room_category : {room_category_id: 1, room_category_name: "Classic (2X)", room_category_priority: 0, icon_code: null, restricted_amenities: Array(0)}
// status : "Saved"



// var request = require("request");
//
// var options = { method: 'POST',
//   url: 'https://api.oyorooms.com/api/v2/vendor/bookings',
//   headers:
//    { 'Postman-Token': 'd725fc42-45b2-4538-99a0-bed32ff2c9bd',
//      'Cache-Control': 'no-cache',
//      'Content-Type': 'application/json',
//      'ACCESS-TOKEN': 'ZW16cU1lTUxQQ3JIX0RCekJwVkQ6RlJ3Y3NzUk5Uc3FtZmZBczhZR20=' },
//   body:
//    { guest:
//       { first_name: 'Abc',
//         last_name: 'Def',
//         email: 'abc@def.com',
//         country_code: '+91',
//         phone: '1234567890' },
//      booking:
//       { single: 0,
//         double: 1,
//         extra: 0,
//         checkin: '28/08/2018',
//         checkout: '29/08/2015',
//         hotel_id: 'HYD091',
//         is_provisional: 'true',
//         external_reference_id: 'VGHVEHWV' } },
//   json: true };
//
// request(options, function (error, response, body) {
//   if (error) throw new Error(error);
//
//   console.log(body);
// });



//
//   var request = require("request");
//
// var options = { method: 'POST',
// url: 'http://affiliate-staging.ap-southeast-1.elasticbeanstalk.com/api/v2/hotels/get_availability',
// headers:
// { 'Postman-Token': '93c5bd8b-1af1-4ec5-912c-4006b84e156e',
//  'Cache-Control': 'no-cache',
//  'Content-Type': 'application/json',
//  'ACCESS-TOKEN': 'NWtzMVhzNTlKeHZ1eGFtbm5zdHk6TVdIVHNGa3huRE1nZmRka0tLeGQ=' },
// body:
// { HotelAvailability:
//   { HotelID: [],
//     City: 'Hyderabad',
//     checkInDate: '16/08/2018',
//     checkOutDate: '17/08/2018',
//     adults: 2,
//     children: 0,
//     child_1_age: 0,
//     child_2_age: 0,
//     rooms: 2 } },
// json: true };
//
// request(options, function (error, response, body) {
// if (error) throw new Error(error);
//   res.json(body);
// console.log(body);
// });
// return false
