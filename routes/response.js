var express = require('express');
var checksum = require('../lib/checksum');
var router = express.Router();
var crypto = require('crypto');
var hart= require('../hart');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var request = require('request');
var qs = require("querystring");  //For Otp service
var http = require("http");     //For Otp service
var sha512 = require('sha512');
var btoa = require('btoa');
var ObjectId = require('mongodb').ObjectID;
var execPhp = require('exec-php');
var sha512 = require('js-sha512');
var querystring = require('querystring');
// var parser = require('xml2json');

router.post('/response', function(req,res,next) {
  //console.log(req.body);
    var checksumstring = checksum.getResponseChecksumString(req.body);
    var calculatedchecksum = checksum.calculateChecksum(checksumstring);

    if(req.body.responseCode == "100"){
      var sessionid = req.body.product1Description;
      var amount1 = req.body.amount/100;
      var enteredamount = parseFloat(req.body.product2Description);
      var wallets = hart.wallets;
      wallets.findOne({ agentId : sessionid },function(err,data){
        if(!err){
            var TotalAMount = data.Wallet + enteredamount;

            wallets.updateOne({ 'agentId':sessionid },{ $set:{ Wallet: TotalAMount }}, function(err, response) {
              if(!err){
                // console.log(data);
                // console.log("wallet updated");
              savewallettransaction(sessionid,enteredamount,"001");
              }else{
                console.log("Wallet Not Updated");
              }
            });

        }else{
          return err;
        }
       });
    }else{
      console.log("Transaction Fail")
    }
    res.render('response', {
        data: req.body,
        isChecksumValid: (calculatedchecksum.toString()) === ((req.body.checksum).toString())
    });
    res.end();
});




function savewallettransaction(sessionid,amount1,txnid) {
  var ADDm_Reports = hart.add_money_reports;
        var Ereports = new ADDm_Reports({
          agentId:sessionid,
          Amount: amount1,
          TransactionID:txnid,
          Paymentfrom:"Mobikwik",
          mihpayid:"",
          status:'1',
        });
        Ereports.save(function (err, results) {
          if(err){
            console.log(err);
          }else{
            console.log("Add money Status updated");
          }
        });
}
module.exports = router;
