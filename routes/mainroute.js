var path = require('path');
//var session = require('express-session');
var hart = require('../hart');
var async = require('async');
var crypto = require('crypto');
var utlity = require('./utlity');
var url = require('url');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var request = require('request');
var qs = require("querystring"); //For Otp service
var http = require("http"); //For Otp service
const authService = require('../_helpers/auth.service');

var auth = function(req, res, next) {
  if (req.session && req.session.user != "" && req.session.admin)
    return next();
  else
    return res.redirect('/');
};

var auth1 = function(req, res, next) {
  if (req.session && req.session.user != "" && req.session.admin1)
    return next();
  else
    return res.redirect('/');
};
var auth2 = function(req, res, next) {
  if (req.session && req.session.user != "" && req.session.admin2)
    return next();
  else
    return res.redirect('/');
};



module.exports = function(app) {

  app.get('/', async function(req, res) {
    var auth = await authService.logout(req.session.auth_token);
    res.clearCookie('token');
    res.clearCookie('userId');
    res.clearCookie('username');
    req.session.logout((err) => {
      if (!err) {
        res.sendFile(path.resolve(__dirname, '../Public2/loginecenter.html'));
      }
    });
  });


  app.get('/Login', async function(req, res) {
    var auth = await authService.logout(req.session.auth_agenttoken);
    req.session.logout((err) => {
      if (!err) {
        // res.redirect('/');
        res.sendFile(path.resolve(__dirname, '../Public2/loginecenter.html'));
      }
    });
  });

  app.get('/header', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/header.html'));
  });
  app.get('/sidebar', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/dashboard.html'));
  });

  app.get('/AgentEmoviesReports', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/emovies.html'));
  });

  app.get('/ForgetPassword',function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/forgot-password.html'));
  });
  app.get('/Dashboard',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/index.html'));
  });
  app.get('/Agents',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/operator.html'));
  });
  app.get('/Topups',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/topups-reports.html'));
  });
  app.get('/Bus',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/bus-services-reports.html'));
  });
  app.get('/DMT', auth, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/direct-money-transfers-reports.html'));
  });

  app.get('/OnlineRTI',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/OnlineRTI.html'));
  });
  app.get('/Pancard',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/Pancard.html'));
  });
  app.get('/Refunds',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/refunds-reports.html'));
  });
  app.get('/Cancellations',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/cancellations-reports.html'));
  });
  app.get('/Earnings',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/earning-reports.html'));
  });
  app.get('/Alltransactions',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/all-transactions.html'));
  });
  app.get('/AgentMyRequest',  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/AgentMyRequest.html'));
  });
  app.get('/QuickMessage',  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/quick-messages.html'));
  });
  app.get('/Help',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/quick-messages.html'));
  });
  app.get('/Electricity_bills',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/electricity_bill.html'));
  });
  app.get('/FlightBookings',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/flightbookings.html'));
  });
  app.get('/FlightCancellation',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/flightcancellations.html'));
  });
  app.get('/HotelBookings',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/hotelbookings.html'));
  });
  app.get('/HotelCancellation',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/hotelcancellations.html'));
  });
  app.get('/RaisedComplaints',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/raisedtokens.html'));
  });
  app.get('/Invoices_reports',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/agentinvoices.html'));
  });
  app.get('/BanyanStore_Invoices_bp',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/banyanstoreinvoices_bp.html'));
  });
  app.get('/GoldenSilver_Invoices_bp',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/goldensilerinvoicesbp.html'));
  });
  app.get('/MyWalletUpdates',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/mywalletupdates.html'));
  });
  app.get('/BpEarnigs',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/bpearnings.html'));
  });
  app.get('/BpCustomers',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/BpCustomers.html'));
  });
  app.get('/eWalletTopup',auth,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public/btobtransfer_bp.html'));
  });


  app.get('/home',auth1,function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/home.html'));
  });
  app.get('/e_Services',auth1,function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/homeone.html'));
  });
  app.get('/BusAvailability',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/Busavailability.html'));
  });
  app.get('/BusSearch',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/redbus.html'));
  });
  app.get('/TicketCancellation',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/cancelticket.html'));
  });
  app.get('/E_Center',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/index.html'));
  });
  app.get('/AddMoney',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/dmt.html'));
  });
  app.get('/Customers',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/epdmtcustomers.html'));
  });
  app.get('/Beneficiary',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/epdmtbeneficeries.html'));
  });
  app.get('/Transfer',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/money-transfer.html'));
  });
  // app.get('/E_Commerce',auth1, function(req, res) {
  //   res.sendFile(path.resolve(__dirname, '../Public2/ecommerce-index.html'));
  // });

  app.get('/Computer&Accessories',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/computer-and-accessories.html'));
  });
  app.get('/Products',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/ecommerce-products.html'));
  });
  app.get('/ProductsDetails',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/ecommerce-products-detail.html'));
  });
  app.get('/ProductsInvoice',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/product-invoice.html'));
  });
  app.get('/PanCardRegistration',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/panindex.html'));
  });
  app.get('/Recharges',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/recharge.html'));
  });
  app.get('/DTHRecharge',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/dthrecharge.html'));
  });
  app.get('/Postpaid',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/postpaidrecharge.html'));
  });
  app.get('/LandLine',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/landlineRecharge.html'));
  });
  app.get('/RtiApplication',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/rti.html'));
  });
  app.get('/RtiUpload',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/rti-uploaddoc.html'));
  });
  app.get('/ep_debit',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/epdmt.html'));
  });

  app.get('/rechargepartners', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/recharge-partners.html'));
  });
  app.get('/Electricity',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/electricity.html'));
  });

  app.get('/SearchFlight',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/flightsearch.html'));
  });

  app.get('/TermsAndConditions', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/TermsAndConditions.html'));
  });
  app.get('/PrivacyPolicy', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/PrivacyPolicy.html'));
  });
  app.get('/Brands', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/brands.html'));
  });
  app.get('/eCenterModelOverview', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/eCenterModelOverview.html'));
  });
  app.get('/CustomerServices', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/CustomerServices.html'));
  });
  app.get('/PartnerWithUs', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/PartnerWithUs.html'));
  });
  app.get('/aboutus', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/about-us.html'));
  });
  app.get('/boardofdirectors', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/board-of-directors.html'));
  });
  app.get('/media', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/media.html'));
  });
  app.get('/contactus', function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/contact-us.html'));
  });

  app.get('/FightResults',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/flightsresults.html'));
  });
  app.get('/SearchHotels',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/hotelindex.html'));
  });
  app.get('/hotel_lists',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/hotellists.html'));
  });
  app.get('/hotelfulldetails',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/HotelFullDetails.html'));
  });
  app.get('/hotelOrder',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/hotelorder.html'));
  });
  app.get('/Hotel_Order_Confirm',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/hotelorderconfirm.html'));
  });
  app.get('/flight_multi_results',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/flight_multitrip.html'));
  });
  app.get('/FightRResults',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/flight-round.html'));
  });
  app.get('/eMovies',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/emovies.html'));
  });
  app.get('/movie_details',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/moviefulldetails.html'));
  });
  app.get('/e_Commerce',auth1, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/ecommerce.html'));
  });
  app.get('/eServices',function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/eservices.html'));
  });
  app.get('/movies',function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/emovieslogin.html'));
  });
  app.get('/eCommerce',function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/ecommercelogin.html'));
  });
  //Digital Gold Routes
    app.get('/DigitalGoldLogin',auth1, function(req, res) {
      res.sendFile(path.resolve(__dirname, '../Public2/digitalgoldlogin.html'));
    });
    app.get('/DigitalGold',auth1, function(req, res) {
      res.sendFile(path.resolve(__dirname, '../Public2/digitalgold.html'));
    });
  app.get('/NiharLogin',async function(req, res) {
    var auth = await authService.logout(req.session.auth_nihartoken);
    req.session.logout((err) => {
      if (!err) {
        // res.redirect('/');
        res.sendFile(path.resolve(__dirname, '../Public3/index.html'));
      }
    });
  });
  app.get('/Buscanellations',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/Buscanellation.html'))
  });
  app.get('/sidebar', auth2,function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/sidemenu.html'));
  });
  app.get('/index',auth2, function(req, res) {
    delete req.session.Niharinfo;
    res.sendFile(path.resolve(__dirname, '../Public3/index.html'));
  });
  app.get('/Nihardashboard',auth2,function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/Dashboard.html'));
  });
  app.get('/NiharAgents',auth2,function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/agents.html'));
  });
  app.get('/RechargesReports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/recharge-reports.html'));
  });
  app.get('/BusBooking',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/bus-booking-reports.html'));
  });
  app.get('/DMTReports', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/dmt-reports.html'));
  });
  app.get('/Online-rti',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/Online-rti.html'));
  });
  app.get('/PAN-reports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/pancard-reports.html'))
  });



  app.get('/E_CommerceReports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/ecommerce-reports.html'));
  });
  app.get('/Aggregators', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/aggregator.html'));
  });
  app.get('/eProducts',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/products.html'));
  });
  app.get('/AddProduct', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/add-products.html'));
  });
  app.get('/Transactions',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/our-transactions.html'));
  });
  app.get('/Messages', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/messages.html'));
  });
  app.get('/AgentFullDetails',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/agent-details.html'));
  });
  app.get('/inbox',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/mailbox.html'));
  });
  app.get('/compose',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/compose.html'));
  });
  app.get('/readmail',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/read-mail.html'));
  });
  app.get('/CareReports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/reportstwo.html'));
  });
  app.get('/ForgotPassword',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/forgot-password.html'));
  });
  app.get('/Itdukan',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/itdukan.html'));
  });
  app.get('/pay',  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public2/paymentgateway.html'));
  });
  app.get('/ElectricityReports', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/electricity_reports.html'));
  });
  app.get('/ServiceRequests', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/servicerequests.html'));
  });
  app.get('/eMoviesAdmin', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/emovies.html'));
  });
  app.get('/eMovies_modify', auth2, function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/eMovies_modify.html'));
  });
  app.get('/HotelBookings_Reports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/Hotel_Bookings_Reports.html'));
  });
  app.get('/HotelCancellations',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/hotelcancellations.html'));
  });

  app.get('/FlightBookingReports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/flightbookings.html'));
  });
  app.get('/FlightCancellations',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/flightcancellations.html'));
  });
  app.get('/RaisedTokens',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/tokens.html'));
  });
  app.get('/ItDukaanReports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/it-dukaan-reports-courier.html'));
  });
  app.get('/BanyanStoreReports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/banyanstoreinvoices_courier.html'));
  });
  app.get('/GoldenSilverReports',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/goldensilverinvoices_courier.html'));
  });
  app.get('/CustomersAddress',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/CustomersAddress.html'));
  });
  app.get('/eWalletRequests',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/ewalletrequests.html'));
  });
  app.get('/eWalletVerified',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/eWalletverified.html'));
  });
  app.get('/eWalletApproved',auth2,  function(req, res) {
    res.sendFile(path.resolve(__dirname, '../Public3/eWalletApproved.html'));
  });

    function randotp() {
      var chars = "0123456789";
      var string_length = 6;
      var randomstring = '';
      var charCount = 0;
      var numCount = 0;
      for (var i = 0; i < string_length; i++) {
        if ((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
          var rnum = Math.floor(Math.random() * 10);
          randomstring += rnum;
          numCount += 1;
        }
        else {
          var rnum = Math.floor(Math.random() * chars.length);
          randomstring += chars.substring(rnum, rnum + 1);
          charCount += 1;
        }
      }
      return randomstring;
    }


    function sendotpItdukaan(mobile, txt) {
      var msg = txt + " is the OTP for accessing your eCenter. PLEASE DO NOT SHARE IT WITH ANY ONE"
      var options = {
        method: 'GET',
        url: 'http://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
        qs: {
          type: 'smsquicksend',
          user: 'niharinfo',
          pass: 'Nihar@123',
          sender: 'NIHAAR',
          to_mobileno: mobile,
          sms_text: msg
        },
        headers: {
          'postman-token': 'eda138b3-9000-ff1c-56a6-adb5438d46f4',
          'cache-control': 'no-cache'
        }
      };
      request(options, function(error, response, body) {
        if (error) throw new Error(error);
      });
    }
    function senddeductamount(amount, agentid) {
      wallets.updateOne({ 'agentId': agentid }, { $set: { Wallet: amount } }, function(err, response) {
        if (err) {
          console.log(err);
        }
        else {
          console.log("Wallet Updated");
          //res.json(1);
        }
      });
    }
}
