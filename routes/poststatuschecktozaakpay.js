var express = require('express');
var router = express.Router();
var checksum = require('../lib/checksum');

router.get('/checktxn',function(req,res,next) {
  res.render('poststatuschecktozaakpay');
});

router.post('/checktxn',function(req,res,next) {
  var checksumstring = checksum.getCheckAndUpdateChecksumString(req.body);
  console.log(checksumstring)
  var calculatedchecksum = checksum.calculateChecksum(checksumstring);
  res.render('checktxn', {
        data: req.body,
        checksum: calculatedchecksum
    });
  res.end();
});


module.exports = router;
