var path = require('path');
//var session = require('express-session');
var hart = require('../hart');
var async = require('async');
var crypto = require('crypto');
var utlity = require('./utlity');
var emoviesservices = require('./movieservices');
var urlget = require('url');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var token = hart.token;
var request = require('request');
var qs = require("querystring"); //For Otp service
var http = require("http"); //For Otp service
var TinyURL = require('tinyurl');
var fs = require('fs');
const logger = require("../_helpers/logger");

const authService = require('../_helpers/auth.service');


module.exports = function(app) {

  app.get('/ping', function(req, res) {
    res.json({ success: true });
  })
//DigitalGold api's
app.post('/buy', function(req,res){

  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/buyPrice',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:req.body.mobile
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      // console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/sell', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/sellPrice',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:req.body.mobile
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/verifyCust',function(req,res){
  hart.DigitalGold.findOne({ MobileNumber : req.body.mobile },function(err,data){
    console.log(data);
});
});
app.post('/customerRegistration', function(req,res){
  console.log(req.body);
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/customerRegistration',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
        "outlet_id":"19027",
          "mobile":req.body.mobile,
          "name":req.body.name,
          "pincode":req.body.pincode,
          "dob":req.body.dob
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      var custreg = hart.DigitalGold;
                      var creg = new custreg({
                        agent_id:req.session.agent_id,
                        name:req.body.name,
                        MobileNumber:req.body.mobile,
                        pincode:req.body.pincode,
                        dob:req.body.dob,
                      });
                      creg.save(function (err, results) {
                        console.log("Saved Successfully");
                      });
                    }
                    res.json(body)
                  });
});
app.post('/Login', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/login',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:req.body.mobile
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      //console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/verifyLogin', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/verifyLogin',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:req.body.mobile,
   otp:req.body.otp

 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      // console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/fetchBalance', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/fetchBalance',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/validatePincode', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/validatePincode',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
        outlet_id:"19027",
         name:req.body.name,
         mobile:req.body.mobile,
         dob:req.body.dob,
         pincode:req.body.pincode

 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/buyGold', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/buyGold',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
        outlet_id:"19027",
       	mobile:"8331094518",
         amount:"5",
         agent_id:"2151654",
         buy_type:"amount",
         mode:"validate"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/getBankList', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/getBankList',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518",
   sell_type:"amount"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/sellGoldVerify', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/sellGoldVerify',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518",
   sell_type:"amount",
   amount:"5"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/sellGoldConfirm', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/sellGoldConfirm',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518",
   sell_type:"1254",
   bene_name:"name",
   bank_name:"yes bank",
   ifsc_code:"ICIC0000626",
   account_no:"123456789",
   amount:"10",
   otp:"317557"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/goldProducts', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/goldProducts',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/redeemGoldVerify', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/redeemGoldVerify',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518",
   product_code:"4",
  	state:"andhra pradesh",
  	city:"vizianagaram",
  	pincode:"110015",
  	address:"dwarakanagar,vizianagaram",
  	landmark:"near vijnana bharathi school"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/redeemGoldConfirm', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/redeemGoldConfirm',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518",
   tx_id:"1168784",
   otp:"504352"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/transactionHistory', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/transactionHistory',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518",
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
app.post('/fetchInvoice', function(req,res){
  var options = { method: 'POST',
  url: 'https://www.instantpay.in/ws/digitalgold/fetchInvoice',
    headers:
      { accept: 'application/json',
        'content-type': 'application/json' },
    body: { token:token,
      request : {
   outlet_id:"19027",
   mobile:"8331094518",
   tx_id:"2440026"
 } },
    json: true };
    request(options, function (error, response, body) {
                    if(error){
                      console.log(error);
                      res.json("Unable to process your request");
                      return false;
                    }else{
                      console.log(body);
                    }
                    res.json(body)
                  });
});
  app.post('/GetChecksum',function(req,res){
    hart.wallets.findOne({ agentId : req.session.id1 },function(err,data){
      if(err){
        console.log(err);
        res.json("Unable to process your request");
        return false;;
      }
      else{
        console.log(data);
        var balance = parseFloat(data.Wallet);
        var Amount = parseFloat(200);
        if(balance > Amount){
          console.log("shshsh")
                hart.panoutlets.findOne({ agentId : req.session.id1 },function(err,data1){
                  if (!err) {
                    console.log(data1.PanNumber)
                    var options = { method: 'GET',
                    url: 'http://test.itdukaan.in/redirect',   //link provide here
                    qs:
                     { pan: data1.PanNumber,
                       allowedservice: "PAN",
                       appid:"157",
                       blockedservices:"",
                       key:"d317e77d94c236dd98d5df6b84efd7c7638c401cfaa01a1d51ecf847fe8eeb3a",
                     },
                    headers:
                     {
                       'cache-control': 'no-cache',
                       'Content-Type': 'application/json' } };

                  request(options, function (error, response, body) {
                    if (error) throw new Error(error);
                    console.log(body);
                    res.json(body)
                  });
                  }else{
                    console.log(err)
                  }
                })
        }
        else{
          res.json("1")
        }
  }
  })
})

  app.post('/forgotPwd', function(req, res) {
    hart.agents.find({ "$or": [{ "Mobile": req.body.phone }, { "email": req.body.email }] }).exec(function(err, doc1) {
      res.end(JSON.stringify(doc1));
    });
  })

  app.post('/ecenterlogout', async function(req, res) {

    var auth = await authService.logout(req.session.auth_token);
    req.session.logout((err) => {
      if (!err) {
        //sessionStorage.removeItem('userId');
        // res.redirect('/');
        res.send({
          retStatus: "Success",
          redirectTo: '/',
          msg: 'Just go there please' // this should help
        });
      }
    });
  })


  app.post('/Agentloginform', function(req, res) {
    hart.agents.count({ email: req.body.userid }, function(err, dof) {
      if (dof == "1") {
        hart.agents.find({ email: req.body.userid }).exec(async function(err, doc1) {
          var x = JSON.parse(JSON.stringify(doc1));
          if (x[0].password == req.body.password) {
            req.session.Agentid = x[0]._id;
            req.session.username = x[0].UserName;
            req.session.useremail = x[0].email;
            req.session.user = req.body.userid;
            req.session.admin = true;
            var auth = await authService.login(req.body.userid, req.body.password);
            req.session.auth_agenttoken = auth.message.token;
        //    console.log(req.session.auth_agenttoken);
            res.end(JSON.stringify(1));
          }
          else {
            res.end(JSON.stringify(0));
          }
        });
      }
      else {
        res.json(0);
      }
    });
  })

  app.post('/ecenterlogin', function(req, res) {

    hart.agents.count({ email: req.body.userid }, function(err, doc) {
      if (doc >= "1") {
        hart.agents.find({ email: req.body.userid }).exec(async function(err, doc1) {
          var x = JSON.parse(JSON.stringify(doc1));
          if (x[0].password == req.body.password) {
            req.session.id1 = x[0]._id;
            req.session.userId = x[0]._id;
            req.session.username = x[0].UserName;
            req.session.Mobile = x[0].Mobile;
            req.session.user = x[0].email;
            req.session.agentimage = x[0].Photo;
            req.session.Remitterid = x[0].RemitterId;
            req.session.password = x[0].password;
            req.session.admin1 = true;
           var auth = await authService.login(req.session.user, req.session.password);   //sso
           req.session.auth_token = auth.message.token;
           res.cookie('token', auth.message.token, { maxAge: 3600000, httpOnly: false });
           console.log("Session Id:"+req.session.id1)
            res.json("1");
          }
          else {
            res.end(JSON.stringify(0));
          }
        });
      }
      else {
        res.end(JSON.stringify(2));
      }
    });
  })


  app.post('/niharloginform', function(req, res) {
    hart.nihar_admins.count({ Email: req.body.userid }, function(err, dof) {
      if (dof == "1") {
        hart.nihar_admins.find({ Email: req.body.userid }).exec(async function(err, doc1) {
          var x = JSON.parse(JSON.stringify(doc1));
          if (x[0].Password == req.body.password){
            req.session.niharid = x[0]._id;
            req.session.user = req.body.userid;
            req.session.admin2 = true;
           var auth = await authService.login(req.body.userid, req.body.password);
           req.session.auth_nihartoken = auth.message.token;
           console.log(req.session.auth_nihartoken);
            res.end(JSON.stringify(1));
          }
          else {
            res.end(JSON.stringify(0));
          }
        });
      }
      else {
        res.json(0);
      }
    });
  })

  app.post('/checkotp', function(req, res) {
    if (req.session.otp == req.body.inputotp) {
      hart.agents.findOne({ email: req.session.otpuser }, function(err, data) {
        if (err) {
          console.log(err);
        }
        else {
          req.session.id1 = data._id;
          req.session.username = data.UserName;
          req.session.Mobile = data.Mobile;
          req.session.user = data.email;
          req.session.agentimage = data.Photo;
          req.session.Remitterid = data.RemitterId;
          req.session.admin1 = true;
          res.json("1");
        }
      });
    }
    else {
      res.json("0");
    }
  })

  app.post('/sendignmessageemovies', function(req, res) {
    TinyURL.shorten('https://drive.google.com/open?id=1Igtz_Dmo9uw5i612M7TLBLlomaIyw3vT', function(res) {
      var txt = "You can view purchased movies in our eMovies NM App. For downloading the app :"+res;
      sendotpItdukaan(req.body.cust_number, txt)
    });
  })

  function sendotpItdukaan(mobile, txt) {
    var msg = txt;
    var options = {
      method: 'GET',
      url: 'http://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
      qs: {
        type: 'smsquicksend',
        user: 'niharinfo',
        pass: 'Nihar@123',
        sender: 'NIHAAR',
        to_mobileno: mobile,
        sms_text: msg
      },
      headers: {
        'postman-token': 'eda138b3-9000-ff1c-56a6-adb5438d46f4',
        'cache-control': 'no-cache'
      }
    };
    request(options, function(error, response, body) {
      if (error) throw new Error(error);
      console.log(body);
    });
  }
// Create Api for webhooks
//here we getting transaction data from instantpay and this data will be storing in our database
    app.get('/Webhook',function(req,res){
      var url_parts = urlget.parse(req.url, true);
      var query = url_parts.query;
      var body = JSON.stringify(query)
      var data = JSON.parse(body);
        utlity.loggermessage(data.agent_id,"Nani Kn","WebHookUrl","","",""+url_parts);
      var instreports = hart.webhookshistory;
      var inreports = new instreports({
        ipay_id:data.ipay_id,
        agent_id:data.agent_id,
        opr_id:data.opr_id,
        status:data.status,
        res_code:data.res_code,
        res_msg:'' ,
      });
      inreports.save(function (err, results) {
        if(err){
          console.log(err);
        }else{
          console.log("Webhooks report saved");

            if(data.status=="REFUND"){
                  hart.wallets.findOne({ agentId :data.agent_id },function(err,data1){
                    if(err){
                      console.log(err);
                    }else {
                      console.log(data.ipay_id);
                      hart.transaction_reports.findOne({pay_id: data.ipay_id},function(err,tdata){
                        if(!err){
                          console.log(tdata)
                          console.log("Order Data:"+tdata.Amount);
                          console.log("Wallet:"+data1.Wallet);
                          var balanceamount = parseFloat(data1.Wallet)+parseFloat(tdata.Amount);
                          console.log("Update Balance is :"+balanceamount)
                          hart.wallets.updateOne({ agentId :data.agent_id },{ $set:{ Wallet: balanceamount }}, function(err, response) {
                            if(err){
                              console.log(err);
                            }else{
                              console.log('refunded amount updated into Bp wallet');
                            }
                        });
                        hart.transaction_reports.save({})

                        }
                      })

                }
                });
            }
          res.json("Saved");
        }
      });
    });
    app.post('/balanceWebhook',function(req,res){
      console.log(req.body);
      // hart.wallets.findOne({ agentId : req.body.session_id},function(err,data){
      //   if(err){
      //     console.log(err);
      //   }else {
      //     var balanceamount = data.Wallet;
          var response = {"response_code": "TXN",
                          "response_msg": "Transaction Successfull",
                          "transactions": [{
                                "balance": 1000
                                }]
                              }
                              res.json(response);


    //     }
    //   res.json(response);
    // });
  });

    app.post('/transactionWebhook',function(req,res){
        console.log(req.body);
    hart.panoutlets.findOne({ PanNumber : req.body.outlet_pan },function(err,data1){
      console.log(data1)
       var response = {"response_code": "TXN",
                       "response_msg": "Transaction Successfull",
                       "transactions": [{
                             "agent_id": req.body.session_id
                          }]
                        }
          var wallets = hart.wallets;
          wallets.findOne({ agentId : data1.agentId },function(err,data){
            if(!err){
              var balance = parseFloat(data.Wallet);
              var Amount = parseFloat(200);
              savepanreports(data1.agentId,"Test","9876543210","Outlet",req.body.app_id,"PPAN","Test",req.body.transactions[0].amount,200,'',"Success","Success","Test","Test");
              var updamnt = balance - Amount;
              var discount=90;
              var bp_commission=0.8;
              var nihar_commission=0.2;
              var bp_earn=discount*bp_commission;
              var nihar_earn=(discount*nihar_commission)+3; //3rs is instant giving commission for nihar
              // var agentcommision = 60;
              // var niharcommision = 33;
              utlity.transactions(data1.agentId,req.body.session_id,'ecenter','INP','PAN','PAN',updamnt,Amount,discount,bp_commission,nihar_commission,bp_earn,nihar_earn,"Success","",'PAN');
              res.json(response);
            }
          })
        })
    });

    function savepanreports(agentId,app_name,app_number,outletid,ipay_id,opr_id,account_no,trans_amt,charged_amt,datetime,resstatus,res_mesg,applicantname,applicantnumber){
      var Pantokens = hart.pantokens;
      var pantokens = new Pantokens({
        agentId:agentId,
        app_name:app_name,
        app_number:app_number,
        outletid:outletid,
        ipay_id:ipay_id,
        opr_id:opr_id,
        account_no:account_no,
        trans_amt:trans_amt,
        charged_amt:charged_amt,
        datetime:datetime,
        resstatus:resstatus,
        res_mesg:res_mesg,
        applicantname:applicantname,
        applicantnumber:applicantnumber,
        Status:1,
      });
      pantokens.save(function (err, results) {
        if(err){
          console.log(err);
        }
       // res.json(1);
      });

    }
  app.post('/confirmationWebhook',function(req,res){
    var response = {"response_code": "TXN",
                    "response_msg": "Transaction Successfull",
                    "display_params": {
                    "custom_param_key_1": "custom_param_value_1",
                    "custom_param_key_2": "custom_param_value_2",
                    "custom_param_key_3": "custom_param_value_3",
                    "custom_param_key_4": "custom_param_value_4"
                  }
                }
  });
   //   if(req.body.request_id != ""){
   //      hart.transfers.find({"$and" : [{CreateDate:{"$gte": fromdate, "$lte": enddate}}]}).exec(function(err,doc){
   //        if(!err){
   //          res.json(doc)
   //        }else{
   //          console.log(err);
   //    }
   //  });
   // }else{
   //      hart.transfers.find({}).sort({CreateDate:-1}).exec(function(err, data){
   //        if(err){
   //          console.log(err);
   //        }
   //        else{
   //          res.json(data);
   //        }
   //      });
   //     }


  function randotp() {
    var chars = "0123456789";
    var string_length = 6;
    var randomstring = '';
    var charCount = 0;
    var numCount = 0;
    for (var i = 0; i < string_length; i++) {
      if ((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
        var rnum = Math.floor(Math.random() * 10);
        randomstring += rnum;
        numCount += 1;
      }
      else {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
        charCount += 1;
      }
    }
    return randomstring;
  }
}
