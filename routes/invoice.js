const crypto = require('crypto');
const hart= require('../hart');
const moment = require('moment');
//const session = require('express-session');
const mongoose = require('mongoose');
const mongoXlsx = require('mongo-xlsx');
const MongoClient = require('mongodb').MongoClient;
const url = hart.url;
const invoice =hart.invoice;
const request = require('request');
const qs = require("querystring");  //For Otp service
const http = require("http");     //For Otp service
const ObjectId = require('mongodb').ObjectID;
const utlity = require('./utlity');

//check
const movieservices = require('./movieservices');

const logger = require("../_helpers/logger");
const datenow = moment().format('YYYY-MM-DD hh:mm:ss')
const fail_msg = "Fail";
const suc_msg = "Success";
const ser_sart_msg = "Service Started";
const findwalletbal_err = "Error In Find Wallet Balance";
const errorinapireq_err = "Error In Requesting API";
const transactiion_suc_msg = "Transaction Success";
const transactiion_fail_msg = "Transaction Fail";
const Insufficientbalance = "In Insufficient Balance";
const sessionout = "Session Out";

module.exports = function(app){
app.post('/change_stage',function(req,res){
  var doc={};

  if (req.body.status==1) {
    doc.Review='Yes';
    doc.Dispatch='No';
  }else if (req.body.status==2) {
    doc.Review='Yes';
    doc.Dispatch='Yes';
  }else if (req.body.status==3) {
    doc.Review='Yes';
    doc.Dispatch='Returned';
  }
  doc.status_date=new Date().getTime();

    const options = {
      method: 'POST',
      url: hart.invoiceurl+"/"+'change_deliverystatus',
      headers: {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
      },
      body:{doc,data:req.body,Token:req.session.auth_nihartoken,AgentId:req.session.niharid,Store:req.body.Store},
      json:true
    };
    request(options, function (error, response, body) {
      if (error) {console.log(error);}else {
        if (typeof body=='string') {
          res.json(JSON.parse(body));
        }else {
          res.json(body);
        }

      }
    });
  })

  app.post('/Getitdukaan_ts',function(req,res){
    var toodate = utlity.addoneday(req.body.todate);
    const options = {
      method: 'POST',
      url: hart.invoiceurl+"/"+'Getitdukaan_ts',
      headers: {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
      },
      body:{Token:req.session.auth_nihartoken,AgentId:req.session.niharid,Store:req.body.store,from:req.body.fromdate,to:toodate},
      json:true
    };
    request(options, function (error, response, body) {
      if (error) {
        console.log(error);
      }else {
        console.log(body);
        if (typeof body=='string') {
          res.json(JSON.parse(body));
        }else {
          res.json(body);
        }
      }
    });
  })

  app.post('/Getitdukaan_ts_ad',function(req,res){
    var toodate = utlity.addoneday(req.body.todate);
    const options = {
      method: 'POST',
      url: hart.invoiceurl+"/"+'Getitdukaan_ts_ad',
      headers: {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
      },
      body:{id:req.session.Agentid,Token:req.session.auth_agenttoken,Store:req.body.store,from:req.body.fromdate,to:toodate},
      json:true
    };
    request(options, function (error, response, body) {
      if (error) {
        console.log(error);
      }else {
        if (typeof body=='string') {
          res.json(JSON.parse(body));
        }else {
          res.json(body);
        }
      }
    });
  })
}
