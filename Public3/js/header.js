function getNiharAdminDetails() {
  $.ajax({
    type: 'POST',
    data: '',
    contentType: 'application/json',
    url: '/GetNiharAdminDetails',
    async: true,
    crossDomain: true,
    success: function(res) {
      $("#NiharName").html(res.Name);
      $("#NiharEmail").html(res.Email);
      $("#NiharAdminName").html(res.Name);
      $("#exampleInputUser").val(res.Name);
      $("#exampleInputEmail1").val(res.Email);
      $("#exampleInputMobile").val(res.Mobile);
    }
  });
}

function getAgentMessagess() {
  $.ajax({
    type: 'POST',
    data: '',
    contentType: 'application/json',
    url: '/agentmessages',
    async: true,
    crossDomain: true,
    success: function(res) {
      var mes = '';
      if (res.length > 0) {
        $.each(res, function(i, v) {
          console.log(v);
          mes += '<li><a href="#"><div class="pull-left"><img src="Public3/images/user2-160x160.jpg" class="rounded-circle" alt="User Image"></div><div class="mail-contnet"><h4>' + v.SenderName + '<small><i class="fa fa-clock-o"></i> 15 mins</small></h4><span>' + v.Message + '</span></div></a></li>';
        })
      }
      else {
        mes += '<div class="active first_td">No Messages</div>';
      }
      $('#agent_messages').html(mes);
      $('#countmsg').html(res.length);
    }
  });
}
$(document).ready(function(){
  $('#updateAgentBtn1').click(function(e) {
    e.preventDefault();

    var data = {};
    data.exampleInputUser = $("#exampleInputUser").val();
    data.exampleInputEmail1 = $("#exampleInputEmail1").val();
    data.exampleInputMobile = $("#exampleInputMobile").val();
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/UpdateAdminProfile',
      async: false,
      crossDomain: true,
      success: function(res) {
        console.log(res);
        if (res == "1") {
          $("#updateAgentBtn1").notify("Profile Updated..", { className: "success", position: "right" });
          setTimeout(function() {
            $("#myModal").modal("hide");
            getNiharAdminDetails()
          }, 3000);
        }
        else {
          $("#updateAgentBtn1").notify("Couldn't Update...", { className: "error", position: "right" });
          return false;
        }
      }
    })
    return false;
  })

  $("#Password_Form").submit(function(e) {
    e.preventDefault();
    if ($('#oldpassword').val().trim() == '') {
      $('#oldpassword').focus().notify("Please Enter oldpassword...", { className: "error", position: "bottom" });
      return false;
    }
    if (!checkPassword($('#newpassword').val().trim())) {
      $('#newpassword').focus().notify("Please Enter valid password...", { className: "error", position: "bottom" });
      return false;
    }
    if (!checkPassword($('#reenterpassword').val().trim())) {
      $('#reenterpassword').focus().notify("Please Enter valid password...", { className: "error", position: "bottom" });
      return false;
    }

    var newpassword = $('#newpassword').val();
    var password = $('#reenterpassword').val();
    if (newpassword !== password) {
      $('#newpassword').focus().notify("Please Enter Password is not Matched...", { className: "error", position: "bottom" });
      return false;
    }
    var data = $(this).serializeArray();
    console.log(data);
    $.ajax({
      url: '/UpdatePassword',
      type: 'POST',
      data: data,
      dataType: 'JSON',
      async: false,
      crossDomain: true,
      success: function(res) {
        console.log(res);
        if (res == "0") {
          $('#updatedpasswordbtn').focus().notify("please enter correct Password...", { className: "error", position: "bottom" });
        }
        if (res == '1') {
          $('#updatedpasswordbtn').focus().notify("Updated Password Successfully", { className: "success", position: "bottom" });
          setTimeout(function() {
            $("#myModal1").modal("hide");
          }, 2000);
        }

      }
    });
    return false;
  });
})
function signout() {
  $.ajax({
    type: "POST",
    url: "/ecenterlogout",
    dataType: 'JSON',
    async: true,
    crossDomain: true,
    success: function(data) {
      console.log("success" + data);
      console.log(data);
      if (data.retStatus === 'Success') {
        // not sure what did you mean by ('/team' && '/team' !== "")
        // if('/team' && '/team' !== "") {
        if (data.redirectTo && data.msg == 'Just go there please') {
          console.log("inside");
          window.location = data.redirectTo;
        }
      }

    }
  });
}

const _1_MINUTE = 1000 * 60;

var sess_pollinterval = 1 * _1_MINUTE;
const sess_expirationTime = 20 * _1_MINUTE;
const sess_warningTime = 15 * _1_MINUTE;

let sess_intervalID;
let sess_lastActivity;


function initSessionMonitor() {
  sess_lastActivity = new Date();
  sessSetInterval();
  //capturing any keypress or mouse click
  $(document).bind('keypress.session', (ed, e) => {
    sessKeyPressed(ed, e);
  });
  $(document).bind('mousedown.session', (ed, e) => {
    sessKeyPressed(ed, e);
  });
}

function sessSetInterval() {
  sess_intervalID = setInterval(sessInterval, sess_pollinterval);
}

function sessKeyPressed(ed, e) {
  sess_lastActivity = new Date();
}

function sessClearInterval() {
  clearInterval(sess_intervalID);
}

function logout() {
  signout();
  return true;
}

function pingServer() {
  $.ajax({
    type: "GET",
    url: "/ping",
    dataType: 'JSON',
    async: true,
    crossDomain: true,
    success: function(data) {
      console.log("success" + data);
    }
  });
}


function sessInterval() {
  // console.log("sessInterval()");
  let now = new Date();
  let diff = now - sess_lastActivity;
  if (diff >= sess_warningTime) {
    sessClearInterval();
    let diffInMin = parseInt((sess_expirationTime - sess_warningTime) / _1_MINUTE);
    var currentDate = new Date();
    if (confirm("Your session will expire in " + diffInMin + " minutes as of " + now.toLocaleString() + ".\n Press OK to remain loggedin or press cancel to logoff. If you are logged off any changes will be lost")) {
      now = new Date();
      diff = now - sess_lastActivity;
      if (diff > sess_expirationTime) {
        logout();
      }
      else {
        pingServer();
        sessClearInterval();
        sessSetInterval();
        sess_lastActivity = new Date();
      }
    }
    else {
      logout();
    }
  }
  else {
    // pingServer();
  }

}

initSessionMonitor();
// session timout refernce:
// https://www.itworld.com/article/2832447/development/how-to-create-a-session-timeout-warning-for-your-web-application-using-jquery.html
