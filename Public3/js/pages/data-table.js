//[Data Table Javascript]

//Project:	Nihar Admin - Responsive Admin Template
//Version:	1.1
//Primary use:   Used only for the Data Table

$(function () {
    "use strict";

    $('#example1').DataTable();
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
	$('#example3').DataTable();
  $('#example4').DataTable();
  $('#example5').DataTable();

	$('#example').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	} );

  }); // End of use strict
