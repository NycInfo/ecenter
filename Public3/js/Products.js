
function getHistory() {
  //console.log("Hi");
            $.ajax({
                data       : "",
                dataType   : "json",
                type       : "POST",
                url        : "/getproducts",
                success    : function (data) {
                  var mes = '';
                    if(data.length>0){
                         $.each(data,function(i,v){
                            mes += '<tr><td><a href="">'+v.skuid+'</a></td><td>'+v.productName+'</td><td>'+v.subcategory+'</td><td>'+v.subcategory+'</td><td>'+v.productMRP+'<i class="fa fa-inr"></i></td><td>'+v.productPrice+'<i class="fa fa-inr"></i></td><td>'+v.createdAt+'</td><td><div class="row"><div class="col-sm-5"><a href="#">Edit</a></div><div class="col-sm-7"><a onclick = removeproduct("'+v._id+'")><i class="fa fa-trash-o"></i></a></div></div></td></tr>';
                         })
                     } else {
                             mes += '<div class="active first_td">No data available...</div>';
                         }
                       $('#pr_table').html(mes);
                       //$("#pr_table").DataTable();
                }
            });
}

function getEcommerceOrders() {
            $.ajax({
                data       : "",
                dataType   : "json",
                type       : "POST",
                url        : "/getorderecommerce",
                success    : function (data) {
                  console.log(data);
                  var mes = '';
                    if(data.length>0){
                         $.each(data,function(i,v){
                            mes += '<tr><td><a href="">'+v.SkuNumber+'</a></td><td>'+v.ProductName+'</td><td>'+v.Quantity+'</td><td>'+v.Amount+'</td><td>';
                            mes += ''+moment(v.OrderDate).format('LLL')+'</td>';
                            // <td> if(v.DeliveryStatus == 0){
                            //   mes += 'Not Yet Delivered.. If Delivered <a onclick = updatedelivery("'+v._id+'") href>Click Here</a>';
                            // }
                            // else{
                            // mes += v.DeliveryStatus;
                            // } </td>
                            mes += '<td><i class="fa fa-remove text-red fa-2x"></i></td><td><a onclick= GetInvoice("'+v._id+'","'+v.AgentId+'");>Get Invoice</a></td></tr>';
                         })
                     } else {
                             mes += '<div class="active first_td">No data available...</div>';
                         }
                       $('#pr_table1').html(mes);
                       //$("#pr_table").DataTable();
                }

            });
}

function GetInvoice(orderid,AgentId){
  var data = {};
  data.OrderId  = orderid;
  data.AgentId  = AgentId;
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/AgentProfileDetails',
      async:true,
      crossDomain:true,
      success: function(res) {
            //  console.log(res);
            $("#InvliceModal").modal('show');
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            if(dd<10){
              dd='0'+dd;
            }
            if(mm<10){
              mm='0'+mm;
            }
            var today = dd+'/'+mm+'/'+yyyy;
            $("#InvoiceDate").html(today);
            $("#DateOfSupply").html(today);
            $("#AgentName").html(res.UserName);
            $("#Address1").html(res.HNo+","+res.Street);
            $("#Address2").html(res.Village+","+res.City+","+res.State);
            $("#StateName").html(res.State);
            $("#StateCode").html("TS");
            $("#AgentNameA").html(res.UserName);
            $("#Address1A").html(res.HNo+","+res.Street);
            $("#Address2A").html(res.Village+","+res.City+","+res.State);
            $("#StateNameA").html(res.State);
            $("#StateCodeA").html("TS");
    }
  });
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/SingleOrderDetails',
      async:true,
      crossDomain:true,
      success: function(res) {
      //  console.log(res);
        var date = moment(res.OrderDate).format('LLL')
        $("#orderNumber").html(res.BillNumber);
        $("#orderDate").html(date);
        $("#ProductName").html(res.ProductName);
        $("#Quantity").html(res.Quantity);
        var datap = {};
        var quantity = res.Quantity;
        datap.ProductId = res.ProductId;
        $.ajax({
            type: 'POST',
            data: JSON.stringify(datap),
            contentType: 'application/json',
            url: '/GetProductDetails',
            async:true,
            crossDomain:true,
            success: function(ressub) {
              var ActualSalePrice = ressub.productPrice;
              var saleplus = 100 + ressub.TaxRate;
              var salepriceper = 100/saleplus;
              var saleprice = ActualSalePrice*salepriceper;
              var salepricen = saleprice.toFixed(2);
              $("#Rate").html(salepricen);
              var Amount = quantity * salepricen;
              $("#Amount").html(Amount);
              $("#Discount").html(ressub.productDiscount+"%");
              var Disper = Amount/ressub.productDiscount;
              var Taxable = Amount - Disper;
              var sggsg = Taxable.toFixed(2);
              $("#Taxable").html(sggsg);
              var Taxrate =  ressub.TaxRate/2;
              $("#TaxRate1").html(Taxrate+"%");
              var amoutnmu = sggsg*Taxrate;
              var gdgdgd = amoutnmu/100;
              var TaxCgstamont = gdgdgd.toFixed(2);
              $("#CGstAmount").html(TaxCgstamont);
              $("#TaxRate2").html(Taxrate+"%");
              $("#SGstAmount").html(TaxCgstamont);
              $("#TaxRate3").html("-");
              $("#IGstAmount").html("-");
              var Total = parseFloat(gdgdgd)+parseFloat(gdgdgd)+parseFloat(sggsg);
              var Totaln = Total.toFixed(2);
              $("#TotalAmount").html(Totaln);
              $("#TotalAmountBeforeTax").html(sggsg);
              $("#CGstAmount1").html(TaxCgstamont);
              $("#SGstAmount1").html(TaxCgstamont);
              $("#GstAmount1").html("-");
              var totalgstam = TaxCgstamont*2;
              $("#TotalGst").html(totalgstam);
              var TotalAllAmout1 = parseFloat(totalgstam)+parseFloat(sggsg);
              var TotalAllAmout =  TotalAllAmout1.toFixed(2)
              $("#TotalAllAmout").html(TotalAllAmout);
              var words = withDecimal(TotalAllAmout);
              console.log(words);
            }
        });
      }
  });


}

function withDecimal(TotalAllAmout) {
    var nums = n.toString().split('.')
    var whole = convertNumberToWords(nums[0])
    if (nums.length == 2) {
        var fraction = convertNumberToWords(nums[1])
        return whole + 'and ' + fraction;
    } else {
        return whole;
    }
}

// function updatedelivery(id){
//   var data = {};
//   data.id = id;
//   $.ajax({
//                 type: 'POST',
//                 data:  JSON.stringify(data),
//                 contentType: 'application/json',
//                 url: '/UpdateDeliveryStatus',
//                 async:false,
//                 crossDomain:true,
//                 success: function(res) {
//                   console.log(res);
//                   return false;
//                 }
//     })
//
// }
