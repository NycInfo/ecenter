module.exports = errorHandler;


function errorHandler(err, req, res, next) {
    if (typeof(err) === 'string') {
        // custom application error
        return res.status(400).json({ message: err });
    }

    if (err.name === 'UnauthorizedError') {
        // jwt authentication error
        // return res.status(401).json({ success: false, message: 'Invalid Token' });
        return res.redirect('/');
    }
    if (err.type === 'UnauthorizedError') {
        // jwt authentication error
        console.log('UnauthorizedError');
        return res.redirect('/');
        // return res.status(401).json({ success: false, message: 'Invalid Token' });
    }

    if (err.type === 'InputError') {
        // Input error
        console.log(err);
        return res.status(400).json({ success: false, type: err.type, message: err.message });
    }

    // default to 500 server error
    return res.status(500).json({ success: false, message: err.message });

}
