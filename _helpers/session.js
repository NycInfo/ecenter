const session = require('express-session');
var mongoose = require('mongoose');
const mongoStoreFactory = require("connect-mongo");
const MongoStore = mongoStoreFactory(session);

const uuid = require('uuid/v4');

const _1_MINUTE = 1000 * 60;


module.exports = {
    init
}

function init(mongooseConnection) {
    const createdSession = session({
        genid: (req) => {
            // console.log('Inside the session middleware')
            // console.log(req.sessionID)
            return uuid() // use UUIDs for session IDs
        },
        store: new MongoStore({
            mongooseConnection: mongooseConnection
        }),
        // store: new FileStore(),
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: true,
        cookie: {
            name: 'ecenter',
            path: '/',
            httpOnly: false,
            secure: false,
            maxAge: 6000 * _1_MINUTE
            //maxAge: 60 * _1_MINUTE
            //maxAge: 20 * _1_MINUTE
        },
        //  rolling: true
    });
    session.Session.prototype.login = function(user, cb) {
        console.log("inside sessionprotype");
        const req = this.req;
        req.session.regenerate(function(err) {
            if (err) {
                cb(err);
            }
        });
        req.session.auth_token = user;
        req.session.save((err) => {
            console.log(err);
        });
        cb();
    };

    session.Session.prototype.logout = function(callback) {
        const req = this.req;
        req.session.destroy(function(err) {
            if (err) {
                callback(err);
            }
            callback();
        });
    }
    return createdSession;
}
