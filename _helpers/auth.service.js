'use strict';
var request = require('request');
const Querystring = require('querystring');
// const logger = require("./logger");

const config = require('config');
const AUTH_CONFIG = config.get('appFamily').auth;


const status = Object.freeze({
    SUCCESS: true,
    FAILED: false
});

module.exports = {
    login,
    logout,
    authenticate,
    status,
    validateToken,
    userrandomod
};
async function validateToken(token, userId) {

    var options = {
        method: 'POST',
        url: getAuthURL(AUTH_CONFIG.path.validateToken),
        headers: getHeaders(token),
        body: { userId: userId },
        json: true
    };
    console.log(options);
    return await handleResponse(options);
}
function getAuthURL(path) {
    console.log(path);
    console.log("Path : " + AUTH_CONFIG.domain + path);
    return AUTH_CONFIG.domain + path;
}

function getHeaders(token) {
    const headers = {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json',
    };
    if (token) {
        headers['Authorization'] = 'Bearer ' + token;
    }
    return headers;
}

async function login(username, password) {
    var options = {
        method: 'POST',
        url: getAuthURL(AUTH_CONFIG.path.login),
        headers: getHeaders(),
        body: {
            username: username,
            password: password
        },
        json: true
    };
    return await handleResponse(options);
}

async function logout(token) {
    console.log("token to logout: " + token);
    var options = {
        method: 'POST',
        url: getAuthURL(AUTH_CONFIG.path.logout),
        headers: getHeaders(token),
        json: true
    };
    return await handleResponse(options);
}

async function authenticate(token, userId) {
    var options = {
        method: 'POST',
        url: getAuthURL(AUTH_CONFIG.path.authenticate),
        headers: getHeaders(token),
        body: { userId: userId },
        json: true
    };
    return await handleResponse(options);
}


async function handleResponse(options) {
    return await new Promise((resolve) => {
        request(options, (error, response, body) => {
            if (error) {
                console.log(error);
                // logger.error("/api/auth: " + JSON.stringify(error));
                resolve({ success: false, message: error.message });
            }
            else if (response.statusCode != 200) {
                console.log("not 200");
                // logger.info("/api/auth: " + JSON.stringify(response));
                resolve({ success: false, message: response.body.message });
            }
            else {
                console.log("200 statsu");
                // logger.info(response);
                resolve({ success: true, message: response.body });
            }
        });
    });
}

async function userrandomod(mobile) {
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZ0123456789";
  var string_length = 10;
  var randomstring = '';
  var charCount = 0;
  var numCount = 0;
  for (var i=0; i<string_length; i++) {
      if((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
          var rnum = Math.floor(Math.random() * 10);
          randomstring += rnum;
          numCount += 1;
      } else {
          var rnum = Math.floor(Math.random() * chars.length);
          randomstring += chars.substring(rnum,rnum+1);
          charCount += 1;
      }
  }
    return await randomstring;
}
