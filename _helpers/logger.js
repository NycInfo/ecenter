const winston = require('winston');
const { format } = require('winston');
const { combine, timestamp, label, prettyPrint } = format;
const DailyRotateFile = require('winston-daily-rotate-file');



// set default log level.
// var logLevel = 'trace'
var logLevel = 'error'

// Set up logger
var customColors = {
    trace: 'white',
    debug: 'green',
    info: 'blue',
    warn: 'yellow',
    error: 'red',
    fatal: 'red'
}
const level = process.env.LOG_LEVEL || 'debug';

var options = {
    file: {
        level: level,
        filename: `logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,

    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

var logger = winston.createLogger({
    // colors: customColors,
    level: logLevel,
    format: combine(
        timestamp(),
        prettyPrint()
    ),
    levels: {
        fatal: 0,
        error: 1,
        warn: 2,
        info: 3,
        debug: 4,
        trace: 5
    },

    transports: [
        new(winston.transports.Console)({
            colorize: true,
            timestamp: true
        }),
        new(winston.transports.File)(options.file),
        // new winston.transports.Console(options.console)
    ]
})

winston.addColors(customColors);


// Extend logger object to properly log 'Error' types
var origLog = logger.log
logger.stream = {
    write: function(message, encoding) {
        logger.info(message);
    },
};

logger.log = function(level, msg) {
    if (msg instanceof Error) {
        var args = Array.prototype.slice.call(arguments);
        args[1] = msg.stack;
        origLog.apply(logger, args);
    }
    else {
        origLog.apply(logger, arguments);
    }
}
/* LOGGER EXAMPLES
  var log = require('./log.js')
  log.trace('testing')
  log.debug('testing')
  log.info('testing')
  log.warn('testing')
  log.error('testing')
  log.fatal('testing')
 */

module.exports = logger;
