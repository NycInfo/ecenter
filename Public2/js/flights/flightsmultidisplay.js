  var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));
  var data = {};
  if (urldata.form_type=="multi") {
    data.source = urldata.source;
    data.destination = urldata.destination;
    data.departing = urldata.departing_1;
    data.sourcemulti2 = urldata.sourcemulti2;
    data.destinationmulti2 = urldata.destinationmulti2;
    data.departing_2 = urldata.departing_2;
    data.multi_number=2;
    if(urldata.sourcemulti3 != ""){
    data.sourcemulti3 = urldata.sourcemulti3;
    data.destinationmulti3 = urldata.destinationmulti3;
    data.departing_3 = urldata.departing_3;
    data.multi_number=3;
    }
    if(urldata.sourcemulti4 != ""){
    data.sourcemulti4 = urldata.sourcemulti4;
    data.destinationmulti4 = urldata.destinationmulti4;
    data.departing_4 = urldata.departing_4;
    data.multi_number=4;
    }
    if(urldata.sourcemulti5 != ""){
    data.sourcemulti5 = urldata.sourcemulti5;
    data.destinationmulti5 = urldata.destinationmulti5;
    data.departing_5 = urldata.departing_5;
    data.multi_number=5;
    }
    if(urldata.sourcemulti6 != ""){
    data.sourcemulti6 = urldata.sourcemulti6;
    data.destinationmulti6 = urldata.destinationmulti6;
    data.departing_6 = urldata.departing_6;
    data.multi_number=6;
    }
      data.triptype='AirshoppingRQ_multi';
    // url='/getmultitripflights';
  }else if (urldata.form_type=="round") {
    data.source = urldata.source;
    data.destination = urldata.destination;
    data.departing = urldata.departing;
    data.triptype='AirshoppingRQ_round';
    data.returning = urldata.returning;
    // url='/getmultitripflights';
  }else if (urldata.form_type=="single") {
    data.source = urldata.source;
    data.destination = urldata.destination;
    data.departing = urldata.departing;
    data.triptype='AirshoppingRQ';

  }

  data.adults = urldata.adults;
  data.child = urldata.child;
  data.infant = urldata.infant;
  data.class = urldata.class;
  data.currency = urldata.currency;
  $('#multi_number').val(data.multi_number);
  // console.log(data);
  $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      dataType: "xml",
      url: '/getmultitripflights',
      success: function(res) {
        console.log(res);
        if (res==null||res.getElementsByTagName("DataLists").length == 0) {
          alert('Error in Listing, please try again');
          location.href='/SearchFlight';
        }else if (res=='err') {
          alert('Error in Listing');
          location.href='/SearchFlight';
        }else {

        const AirShopingResId = res.getElementsByTagName("ResponseID")[0].innerHTML;
        const DataLists = res.getElementsByTagName("DataLists");
        const PassengerList = [];
        var psl=DataLists[0].children[0].getElementsByTagName("Passenger");
        for (var pl = 0; pl < psl.length; pl++) {
          PassengerList.push({
            ID : psl[pl].getAttribute("PassengerID"),
            type : psl[pl].getElementsByTagName("PTC")[0].innerHTML
          })
        }
        urldata.PassengerList=PassengerList;

        const FlightList = res.getElementsByTagName("FlightList");
        const FlightSegmentList = res.getElementsByTagName("FlightSegmentList");
        const OffersGroup = res.getElementsByTagName("OffersGroup");
        const offers = OffersGroup[0].childNodes[1].children;
        const flightkey = res.getElementsByTagName("FlightList");
        var segmentdetails = [];

        //iterate through each offer
        for (var i = 1; i < offers.length; i++) {
          // const offer = offers[i].children[5];
          const offer = offers[i];
          var remarks = [];

          const OfferID = offers[i].getAttribute("OfferID");

          const onPoint = offers[i].getElementsByTagName("SegmentRefs")[0].getAttribute("ON_Point");
          const offPoint = offers[i].getElementsByTagName("SegmentRefs")[0].getAttribute("OFF_Point");
          // var calc=getDirection(urldata.form_type,onPoint,urldata.source,offPoint,urldata.destination);
          var calc = getDirectionEasy(offers[i].getElementsByTagName("FlightRef")[0].getAttribute("ODRef"));
          //extract flight reference from the offer (could be multiple flights depending on one-way, round or multi-trip)
          const flightrefs = offers[i].getElementsByTagName("FlightRefs")[0].innerHTML.split(" ");
          const noOfStops = offer.getElementsByTagName("SegmentRefs").length-1;
          for (var b=0;b<offer.getElementsByTagName("SegmentRefs").length;b++){
              var flight_components = offer.getElementsByTagName("FareComponent")[b];
              var flight_rules = flight_components.getElementsByTagName("FareRules")[0];
              var flight_remarks=flight_rules.getElementsByTagName("Remarks")[0];
              var rem=[];
              for (var c=0;c<flight_remarks.getElementsByTagName("Remark").length; c++){
                rem[c] = flight_remarks.getElementsByTagName("Remark")[c].innerHTML;
              }
              remarks[b]=rem;
          }
          if (offers[i].getElementsByTagName("FlightRef").length > 1) {
            urldata.bundled='bundled';
          } else {
            urldata.bundled='unbundled';
          }
          var ofr_it_id_array=[];
          for (var fta = 0; fta < offers[i].getElementsByTagName("OfferItem").length; fta++) {
            ofr_it_id_array.push({
                              offeritemid:offers[i].getElementsByTagName("OfferItem")[fta].getAttribute("OfferItemID"),
                              p_ref:offers[i].getElementsByTagName("OfferItem")[fta].getElementsByTagName("Service")[0].getElementsByTagName("PassengerRefs")[0].innerHTML
                            });
          }

          var seglineitem = { offerid:OfferID,
                              offeritemid:ofr_it_id_array,
                              price:offer.getElementsByTagName("Total")[0].innerHTML,
                              pricecode:offer.getElementsByTagName("Total")[0].getAttribute("Code"),
                              Tax:offer.getElementsByTagName("Total")[0].innerHTML,
                              CabinClass:offer.getElementsByTagName("CabinTypeName")[0].innerHTML,
                              PassengerRefs:offer.getElementsByTagName("PassengerRefs")[0].innerHTML,
                              Owner:offers[i].getAttribute("Owner"),
                              PassengerList:PassengerList,
                              AirshoppingResponseID:AirShopingResId,
                              direction:calc,
                              onpoint:onPoint,
                              offpoint:offPoint,
                              stops:noOfStops,
                              flights:[]
                            };
            // var ODref_length=0;
          for (var j = 1; j < flightrefs.length; j++) {
            //get flight details from FlightList
            const flights = GetElementsByAttribute(res, 'Flight', 'FlightKey', flightrefs[j]);
            var x=0;
            for (var k=0; k<flights.length; k++) {
              const segmentIDs = flights[k].getElementsByTagName("SegmentReferences")[0].innerHTML.split(" ");



              //get the offeritem from Offer

              var flight = {
                            flightref:flightrefs[j],
                            segments:[]
                          };
              for (var l = 0; l <segmentIDs.length; l++) {

                //create a javascript json Object
                if(segmentIDs[l] != ""){
                  var segd = GetElementsByAttribute(res, 'FlightSegment', 'SegmentKey', segmentIDs[l]);
                  var str = segd[0].getElementsByTagName("Value")[0].innerHTML;
                  var hours = str.substring(str.indexOf("PT")+2,str.indexOf("H"));
                  var minutes = str.substring(str.indexOf("H")+1,str.indexOf("H")+3);

                  var duration = str.substring(str.indexOf("PT")+2,str.indexOf("H")+1)+
                                  ":"+
                                  str.substring(str.indexOf("H")+1,str.indexOf("H")+4);
                  var seg = { SegmentId:segmentIDs[l],
                                      AirlineID:segd[0].getElementsByTagName("AirlineID")[1].innerHTML,
                                      AirlinesName:segd[0].getElementsByTagName("MarketingCarrier")[0].children[1].innerHTML,
                                      FlightNumber:segd[0].getElementsByTagName("FlightNumber")[0].innerHTML,
                                      AircraftCode:segd[0].getElementsByTagName("AircraftCode")[0].innerHTML,
                                      FlightDuration:duration,
                                      FlightDurationHours: hours,
                                      FlightDurationMinutes: minutes,
                                      DepartureCode:segd[0].getElementsByTagName("AirportCode")[0].innerHTML,
                                      DepartureDate:segd[0].getElementsByTagName("Date")[0].innerHTML,
                                      DepartureTime:segd[0].getElementsByTagName("Time")[0].innerHTML,
                                      DepartureAirportName:segd[0].getElementsByTagName("AirportName")[0].innerHTML,
                                      DepartureTermnal:segd[0].getElementsByTagName("Terminal")[0].children[0].innerHTML,
                                      ArrivalCode:segd[0].getElementsByTagName("AirportCode")[1].innerHTML,
                                      ArrivalDate:segd[0].getElementsByTagName("Date")[1].innerHTML,
                                      ArrivalTime:segd[0].getElementsByTagName("Time")[1].innerHTML,
                                      ArrivalAirportName:segd[0].getElementsByTagName("AirportName")[1].innerHTML,
                                      ArrivalTerminal:segd[0].getElementsByTagName("Terminal")[1].children[0].innerHTML,
                                      carrierSatus: remarks[x++]

                              };
                    //console.log(seg);
                    flight.segments.push(seg);
                }
              }
              seglineitem.flights.push(flight);
            }
           // console.log(flights);
          }
          segmentdetails.push(seglineitem);
          if(flightrefs.length>2){
            // console.log(flightrefs);
            // console.log(seglineitem);
          }
        }
        // console.log(segmentdetails);
        if(DataLists.length > 0) {

          const HighestPrice = res.getElementsByTagName("Highest")[0].childNodes[0].innerHTML;
          const LowestPrice = res.getElementsByTagName("Lowest")[0].childNodes[0].innerHTML;
          $( "#slider-range3" ).slider( "option", "values", [LowestPrice,HighestPrice] );
          $( "#slider-range3" ).slider( "option", "min", LowestPrice );
          $( "#slider-range3" ).slider( "option", "max", HighestPrice );
          $( "#price_range_val" ).val( "$" + $( "#slider-range3" ).slider( "values", 0 ) + " - $" + $( "#slider-range3" ).slider( "values", 1 ) );
          var t='';
          var resultsHtml = {fl:"",flr:""};
          for (var i = 0; i < segmentdetails.length; i++) {
            const offerr = segmentdetails[i];
            taghtml='<div class="airline_list filters" style="background:#f1f1f1;" id="airlinelist'+i+'" data-category="'+offerr.stops+' '+offerr.price+'">';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            // console.log(offerr.flights);
            for (var k = 0; k < offerr.flights.length; k++) {

              const flg =  offerr.flights[k];
              // console.log(flg);
              var flightDurationInMinutes =0;
              taghtml='<div class="airline_list colPSE linkf'+i+'" style="border-top:0px; border-left:0px; border-right:0px;padding:0px; margin:0px; background:#fff;">';
              // console.log(flg.OnPoint,urldata.source,flg.OffPoint,urldata.destination);
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              for (var j = 0; j < flg.segments.length; j++) {

                const s = flg.segments[j];

                //add hours and minutes
                flightDurationInMinutes += parseInt(s.FlightDurationHours)*60+parseInt(s.FlightDurationMinutes);


                taghtml= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 offset-0">';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                var class_refund;
                var string_refund;
                for (var cs = 0; cs < s.carrierSatus.length; cs++) {
                  if (s.carrierSatus[cs]=='Refundable') {
                    class_refund='style="color:green;"';
                    string_refund='Refundable';
                  }else if (s.carrierSatus[cs]=='NonRefundable') {
                    class_refund='style="color:red;"';
                    string_refund='Non-Refundable';
                  }else if (s.carrierSatus[cs]=="NonLcc"||s.carrierSatus[cs]=="Lcc") {
                    taghtml= '<input type="hidden" class="lcc_status'+k+''+j+''+i+'" value='+s.carrierSatus[cs]+'>';
                    resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                  }
                }
                taghtml= '<div class="airline_section">';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml= '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 offset-1 padL20 size12 borderbottom"><img src="Public2/images/flight/'+s.AirlineID.trim()+'.png" alt="British Airways" title="British Airways"><br><span class="flightno size10">'+s.AirlineID+'-'+s.FlightNumber+'</span><br><span class="airlinename size10">'+s.AirlinesName+'</span><br class="visible-xs"></div>';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml= '<div class="col-xs-6 visible-xs offset-1 padL20 borderbottom"><br><span class="size10">Economy Class <br>Duration : <b>'+createDurationString(s.FlightDurationHours,s.FlightDurationMinutes)+'</b></span></div>';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml= '<div class="clearfix visible-xs"></div>';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml= '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 offset-1 padL20">';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml='<span class="size12">'+s.DepartureAirportName+'('+s.DepartureCode+')<br><i class="fa fa-calendar" aria-hidden="true"></i> <span class="datetime">'+s.DepartureDate+'</span><br>';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml='<i class="fa fa-clock-o" aria-hidden="true"></i> <span class="datetime">'+s.DepartureTime+'</span></span></div> <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs offset-1" style="text-align:center;"><br><i class="fa fa-fighter-jet fa-2x" aria-hidden="true"></i></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 offset-1 padL20"><span class="size12">'+s.ArrivalAirportName+'('+s.ArrivalCode+')';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml='<br><i class="fa fa-calendar" aria-hidden="true"></i> <span class="datetime">'+s.ArrivalDate+'</span><br><i class="fa fa-clock-o" aria-hidden="true"></i><span class="datetime"> '+s.ArrivalTime+'</span></span></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 hidden-xs offset-1" style="text-align:center;">';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml='<span '+class_refund+'>'+string_refund+'</span></br><span class="size10"><b>Cabin Class: </b>'+offerr.CabinClass+'<br>Duration :';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
                taghtml='<b>'+createDurationString(s.FlightDurationHours,s.FlightDurationMinutes)+'</b></span></div></div></div>';
                resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);

              }
              //convert total flight time in minutes to hours and minutes
              var flightDurationMinutesComponent = flightDurationInMinutes%60;
              var flightDurationHoursComponent = Math.trunc(flightDurationInMinutes/60);
              var flightDurationDesc = createDurationString(flightDurationHoursComponent,flightDurationMinutesComponent);
              //Section to display number of stops and Duration (this is done once for each flight)
              // var stop_no_inner=flg.segments.length-1;
              taghtml='<div style="width:100%; float:left; padding:0 20px 10px 20px;"><div style="float:left; padding-right:30px;"><span class="stop" id="stops'+i+'"></span><span class="stop"></span></div><div style="float:left;"><span class="size13">Total-Duration <br> <i class="fa fa-clock-o" aria-hidden="true"></i><b> '+flightDurationDesc+'</b></span></div></div></div>';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            }
            taghtml='<div class="col-lg-9 col-md-10 col-sm-10 col-xs-12 offset-0 whiteBac">';
            // console.log(resultsHtml);
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            taghtml= '<div class="airline_section ">';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            taghtml='<div class="clearfix visible-xs"></div>';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            taghtml= '</div></div>';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);

            //PriceDisplay and Book Now Button
            taghtml= '<div class="col-lg-3 col-md-2 col-sm-2 col-xs-12 offset-0 price-section whiteBac">';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            taghtml='<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs offset-0"><p class="price"><i class="fa fa-inr"></i> '+offerr.price+' / INR</p></div><div class="col-lg-12 col-md-12 col-sm-12 offset-0 hidden-xs"></div><div class="col-xs-12 size12 visible-xs"><div class="col-xs-7 offset-0"><p class="price"><i class="fa fa-inr"></i>'+offerr.price+' / INR </p></div></div><div class="clearfix visible-xs"></div><p class="booksection"></p>';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            taghtml='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 offset-0">';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" value="'+offerr.offerid+'" id="OfferID_can'+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" value="'+offerr.offeritemid.length+'" id="OfferItemID_length'+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            for (var op = 0; op < offerr.offeritemid.length; op++) {
              taghtml= '<input type="hidden" name=offeritemid_array value="'+offerr.offeritemid[op].offeritemid+'" ref="'+offerr.offeritemid[op].p_ref+'" id="OfferItemID_can'+op+''+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" name=pass_ref_array value="'+offerr.offeritemid[op].p_ref+'" id="passinfo_can'+op+''+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            }



              taghtml= '<input type="hidden" value="'+offerr.Owner+'" id="Owner_can'+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" value="'+offerr.PassengerList.length+'" id="total_p'+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" value="'+urldata.infant+'" id="infant_can'+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" value="'+urldata.child+'" id="child_can'+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" value="'+urldata.adults+'" id="adults_can'+i+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input type="hidden" id="price_can'+i+'" tax="'+offerr.price+'"  value="'+offerr.price+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
              taghtml= '<input id="ResponseID_can'+i+'" type="hidden" value="'+offerr.AirshoppingResponseID+'">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            const onclickparameters='onclick=book("'+offerr.direction+'","'+offerr.price+'","'+offerr.pricecode+'",'+i+') ';
            const onclickparameters2='onclick=storevalue("'+offerr.direction+'","'+offerr.price+'","'+offerr.pricecode+'",'+i+') ';
            const postonclick='class="btn btn_chk" style="background:#e14443; color:#fff; margin-bottom:11px;" name="btn_chk" value="'+i+'">';
            const postonclick_flr='class="btn btn_chk" style="background:#e14443; color:#fff; margin-bottom:11px;" name="btn_chk1" value="'+i+'">';
            if (urldata.bundled=='unbundled'&&urldata.form_type!=='single') {
              taghtml= '<input type="button" ';
              resultsHtml=highTagHTML2(offerr.direction,resultsHtml,taghtml,onclickparameters,postonclick,onclickparameters2,postonclick_flr);
            }else {
              taghtml= '<input type="button" '+onclickparameters+' class="btn btn_chk" style="background:#e14443; color:#fff; margin-bottom:11px;" name="btn_chk" value="Book Now">';
              resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            }

            taghtml= '</div><br class="hidden-xs"></div>';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            taghtml='</div>';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
            taghtml='</div>';
            resultsHtml=highTagHTML(offerr.direction,resultsHtml,taghtml);
          }
          // console.log(resultsHtml);
          $('.loader').hide();
          $('.AirShoppingRQ').html(resultsHtml.fl);
          console.log(urldata.bundled);
          if (urldata.bundled=="unbundled"&&urldata.form_type!=='single') {
              $('.AirShoppingRQ_r').html(resultsHtml.flr);
              $('#offward').css("display", "block");
              $('#customise_div').removeClass("col-sm-9");
              $('#customise_div').addClass("col-sm-5");
              $('#customise_div_fil').removeClass("col-sm-3");
              $('#customise_div_fil').addClass("col-sm-2");
              // $('.container-fluid.aboutus').css("margin-top","120px");
              // $('.flight-round-fixed').css("display","block");
              $('.btn_chk').attr('type','radio');
          }

        }else{
          console.log("Data Not Found..");
        }
       }
     }
  });
  $('#Confirmationdone').click(function(e) {
    $('#can_mod_FM').submit();
  })
function GetElementsByAttribute(doc, tag, attr, attrValue) {
    //Get elements and convert to array
    var elems = Array.prototype.slice.call(doc.getElementsByTagName(tag), 0);

    //Matches an element by its attribute and attribute value
    var matcher = function(el) {
        return el.getAttribute(attr) == attrValue;
    };

    return elems.where(matcher);
}
//Filters the given array to those which when passed into matcher return true
Array.prototype.where = function(matcher) {
    var result = [];
    for (var i = 0; i < this.length; i++) {
        if (matcher(this[i])) {
            result.push(this[i]);
        }
    }
    return result;
};
function parseCardsXml(xml, lang)
{
  //console.log(lang);
    var $xml = $(xml),
        name = $xml.find('name[FlightKey="' + lang + '"]').text(),
        desc = $xml.find('desc[FlightKey="' + lang + '"]').text();
        console.log($xml);
        // console.log(name);
        // console.log(desc);
}
function storevalue(a,b,c,d) {
  $('#alldata1').val(d);
}
function book(t,a,b,c) {
  console.log(t,a,b,c);
    $('#myModalbook').modal('show');
    var psng='';
    console.log(urldata.PassengerList);
    for(var j=0;j<urldata.PassengerList.length;j++){
      var val=urldata.PassengerList[j];
      console.log(val);
      var ptc;
      var title=[];
      if (val.type.trim()=='ADT') {
        x=1;
        ptc='Adult '+x;
        title=['Mr','Mrs','Ms'];
        x++;
      }else if (val.type.trim()=='CHD') {
        y=1;
        ptc='Child '+y;
        title=['Master','Ms'];
        y++;
      }else if (val.type.trim()=='INF') {
        z=1;
        ptc='Infant '+z;
        title=['Master','Ms'];
        z++;
      }
    psng+='<div class="form-group row">';
    psng+='<div class="col-sm-1">'+ptc+'</div><input type="hidden" id="pass_loop'+j+'" ref="'+val.type+'" value="'+val.ID+'"><div class="col-sm-2">';
    psng+='<select class="form-control" name="atitle'+j+'" required><option value="">Title</option>';
    for (var t = 0; t < title.length; t++) {
      psng+='<option value="'+title[t]+'">'+title[t]+'</option>';
    }
    psng+='</select></div><div class="col-sm-3"><input class="form-control" type="text" value="" name="afname'+j+'" placeholder="First Name/Given Name" required></div><div class="col-sm-3"><input class="form-control" type="text" name="amname'+j+'" placeholder="middle Name"></div><div class="col-sm-3"><input class="form-control" type="text" name="alname'+j+'" placeholder="Last Name/Surname"></div><div class="col-sm-6" style="padding:15px;"><div class="col-sm-4" style="line-height:30px;">Date of Birth</div><input class="form-control date_birth_a" type="date" name="adate'+j+'" style="width:50%;"></div></div>';
    }
    psng+='<div class="form-group row"><input type="hidden" id="alldata" value='+c+'><div class="col-sm-1"></div><div class="col-sm-3"><input class="form-control" type="email" value="" name="email" placeholder="Email Address" required></div><div class="col-sm-3"><input class="form-control" type="tel" placeholder="PhoneNumber" name="phone"><input type="hidden" name="price" value='+a+'/></div></div>';
    $('#can_mod').html(psng);
    var today_validation = new Date().toISOString().split('T')[0];
    $('.date_birth').attr('max',today_validation);

}
function reverseHighTagHTML(direction,resultsHtml,htmltag){
  if (direction=="inreturn") {
    // console.log(resultsHtml.flr);
    resultsHtml.flr=htmltag+resultsHtml.flr;
  }else {
      resultsHtml.fl=htmltag+resultsHtml.fl;
  }
  return resultsHtml;
}
function appendDiv(resultsHtml,taghtml){
  var result=taghtml+resultsHtml+'<div>';
  return result;
}

function getDirectionEasy(ODref) {
  if (ODref=='OD1') {
   var  calc='onwards';
  }else {
   var  calc='inreturn';
  }
  return calc;
}

function getDirection(form_type,onpoint,source,offpoint,destination) {
  var result="onward";
  if (form_type=='round') {
    //check if bundled or unbundled
   if (offpoint==source && onpoint==destination) {
     result="onward";
   }else {
     result="inreturn";
   }
 }
  return result;
}
//logic to add to AirShoppingRQ OR AirshoppingRQ_round
function highTagHTML(direction,resultsHtml,htmltag) {
  if (direction=="inreturn") {
    resultsHtml.flr+=htmltag;
  }else {
      resultsHtml.fl+=htmltag;
  }
  return resultsHtml;
}
function highTagHTML2(direction,resultsHtml,htmltag,onclickparameters,postonclick,onclickparameters2,postonclick_flr) {
  if (direction=="inreturn") {
    resultsHtml.flr+=htmltag+onclickparameters+postonclick_flr;
  }else {
      resultsHtml.fl+=htmltag+onclickparameters2+postonclick;
  }
  return resultsHtml;
}
function tagHTML(resultsHtml,htmltag){
    resultsHtml.fl+=htmltag;
    resultsHtml.flr+=htmltag;
    return resultsHtml;
}
function print() {
  var divToPrint=document.getElementById("final_Tick_div");
  newWin= window.open("");
  newWin.document.write(divToPrint.outerHTML);
  newWin.close();
  newWin.print();
}

function createDurationString(hours,min)  {
  return hours+'H:'+min+'M';
}

function getCarrierStatus(remarks)  {
  return "NonLCC";
}
