$( "#slider-range3" ).slider({
  range: true,
  min: 0,
  max: 500,
  values: [ 75, 300 ],
  slide: function( event, ui ) {
    $( "#price_range_val" ).val( "$" + ui.values[ 0 ] + "   -   $" + ui.values[ 1 ] );
  }
});

function print() {
  var divToPrint=document.getElementById("final_Tick_div");
  newWin= window.open("");
  newWin.document.write(divToPrint.outerHTML);
  newWin.print();
  newWin.close();
}


var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));
var data = {};
console.log(urldata);
data.source = urldata.source;
data.destination = urldata.destination;
data.departing = urldata.departing;
data.adults = urldata.adults;
data.child = urldata.child;
data.infant = urldata.infant;
data.class = urldata.class;
data.currency = urldata.currency;
data.triptype='AirshoppingRQ';
$.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getflights',
    success: function(res) {
      console.log(res);

      if (res.AirShoppingRS.DataLists!==undefined) {
        var s=res.AirShoppingRS.DataLists[0];
        var ofr=res.AirShoppingRS.OffersGroup[0].AirlineOffers[0];
        //console.log(ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0]._);
        $( "#slider-range3" ).slider( "option", "values", [parseInt(ofr.AirlineOfferSnapshot[0].Lowest[0].EncodedCurrencyPrice[0]._),parseInt(ofr.AirlineOfferSnapshot[0].Highest[0].EncodedCurrencyPrice[0]._)] );
        $( "#slider-range3" ).slider( "option", "min", parseInt(ofr.AirlineOfferSnapshot[0].Lowest[0].EncodedCurrencyPrice[0]._) );
        $( "#slider-range3" ).slider( "option", "max", parseInt(ofr.AirlineOfferSnapshot[0].Highest[0].EncodedCurrencyPrice[0]._) );
        $( "#price_range_val" ).val( "$" + $( "#slider-range3" ).slider( "values", 0 ) + " - $" + $( "#slider-range3" ).slider( "values", 1 ) );
      var t='';var fl='';
      for (var i = 0; i < s.FlightList[0].Flight.length; i++) {
        var stops=s.FlightList[0].Flight[i].SegmentReferences[0]._.trim().split(" ");
        var stopno=stops.length-1;
          var flight_key=s.FlightList[0].Flight[i].$.FlightKey;
          for (var j = 0; j < ofr.Offer.length; j++) {
            if (ofr.Offer[j].FlightsOverview[0].FlightRef[0]._ == flight_key) {
              var category=stopno+' '+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0]._;
              fl+='<div class="airline_list filters" id="airlinelist4"  data-category="'+stopno+' '+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0]._+'">';
              for (var col = 0; col < stops.length; col++) {
                for (var segl = 0; segl < s.FlightSegmentList[0].FlightSegment.length; segl++) {
                  if (stops[col]==s.FlightSegmentList[0].FlightSegment[segl].$.SegmentKey) {
                    fl+='<div class="airline_list colPSE linkf'+i+'"><div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 offset-0"><div class="airline_section "><div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 offset-1 padL20 size12 borderbottom"><img src="Public2/images/indigo.png" alt="British Airways" title="British Airways"><br><span class="flightno size10">'+s.FlightSegmentList[0].FlightSegment[segl].MarketingCarrier[0].AirlineID[0]+'-'+s.FlightSegmentList[0].FlightSegment[segl].MarketingCarrier[0].FlightNumber[0]+'</span><br><span class="airlinename size10">'+s.FlightSegmentList[0].FlightSegment[segl].MarketingCarrier[0].Name[0]+'</span><br class="visible-xs"></div><div class="col-xs-6 visible-xs offset-1 padL20 borderbottom"><br><span class="size10">Economy Class <br>Duration : <b>27hr 15min</b></span></div><div class="clearfix visible-xs"></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 offset-1 padL20">';
                    fl+='<span class="size12">'+s.FlightSegmentList[0].FlightSegment[segl].Departure[0].AirportName[0]+'('+s.FlightSegmentList[0].FlightSegment[segl].Departure[0].AirportCode[0]+')<br><i class="fa fa-calendar" aria-hidden="true"></i> <span class="datetime">'+s.FlightSegmentList[0].FlightSegment[segl].Departure[0].Date[0]+'</span><br>';
                    fl+='<i class="fa fa-clock-o" aria-hidden="true"></i> <span class="datetime">'+s.FlightSegmentList[0].FlightSegment[segl].Departure[0].Time[0]+'</span></span></div> <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs offset-1" style="text-align:center;"><br><i class="fa fa-fighter-jet fa-3x" aria-hidden="true"></i></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 offset-1 padL20"><span class="size12">'+s.FlightSegmentList[0].FlightSegment[segl].Arrival[0].AirportName[0]+'('+s.FlightSegmentList[0].FlightSegment[segl].Arrival[0].AirportCode[0]+')';
                    fl+='<br><i class="fa fa-calendar" aria-hidden="true"></i> <span class="datetime">'+s.FlightSegmentList[0].FlightSegment[segl].Arrival[0].Date[0]+'</span><br><i class="fa fa-clock-o" aria-hidden="true"></i><span class="datetime"> '+s.FlightSegmentList[0].FlightSegment[segl].Arrival[0].Time[0]+'</span></span></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 hidden-xs offset-1" style="text-align:center;"><span class="size10">Economy Class<br>Duration :'; fl+='<b>'+s.FlightSegmentList[0].FlightSegment[segl].FlightDetail[0].FlightDuration[0].Value[0].substring(2)+'</b></span></div></div></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 offset-0 price-section"><div class="col-lg-12 col-md-12 col-sm-12 offset-0 hidden-xs airline_section">';
                    // if (ofr.Offer[j].OfferItem[0].FareDetail[0].FareComponent[0].FareRules[0].Penalty[0].$.RefundableInd=='true') {
                    //   fl+='<p class="per_price fiL-xs" style="color:green">Refundable</p>';
                    // }else {
                    //   fl+='<p class="per_price fiL-xs" style="color:red">Refundable</p>';
                    //
                    // }
                    fl+='</div></div></div>';
                  }
                }
                }

                fl+='<div class="col-lg-9 col-md-10 col-sm-10 col-xs-12 offset-0 whiteBac"><div class="airline_section ">';
                fl+='<div class="clearfix visible-xs"></div> <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs offset-1"><span class="stop">'+stopno+' Stop</span></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 hidden-xs offset-1"><span class="size13">Total-Duration <br> <i class="fa fa-clock-o" aria-hidden="true"></i><b>'+s.FlightList[0].Flight[i].Journey[0].Time[0].substring(2)+'</b></span></div></div></div><div class="col-lg-3 col-md-2 col-sm-2 col-xs-12 offset-0 price-section whiteBac">';
                fl+='<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs offset-0"><p class="price"><i class="fa fa-inr"></i> '+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0]._+' / '+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0].$.Code+'</p></div><div class="col-lg-12 col-md-12 col-sm-12 offset-0 hidden-xs"></div><div class="col-xs-12 size12 visible-xs"><div class="col-xs-7 offset-0"><p class="price"><i class="fa fa-inr"></i>'+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0]._+' / '+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0].$.Code+' </p></div></div><div class="clearfix visible-xs"></div><p class="booksection"></p>';
                fl+='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 offset-0">';
                fl += '<input type="hidden" value="'+ofr.Offer[j].$.OfferID+'" id="OfferID_can'+j+'">';
                fl += '<input type="hidden" value="'+ofr.Offer[j].OfferItem[0].$.OfferItemID+'" id="OfferItemID_can'+j+'"><input type="hidden" value="'+ofr.Offer[j].OfferItem[0].Service[0].PassengerRefs[0]+'" id="passinfo_can'+j+'"><input type="hidden" value="'+ofr.Offer[j].$.Owner+'" id="Owner_can'+j+'">';
                fl += '<input type="hidden" value="'+s.PassengerList[0].Passenger.length+'" id="total_p'+j+'"><input type="hidden" value="'+urldata.infant+'" id="infant_can'+j+'"><input type="hidden" value="'+urldata.child+'" id="child_can'+j+'"><input type="hidden" value="'+urldata.adults+'" id="adults_can'+j+'"><input type="hidden" id="price_can'+j+'"  value="'+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0]._+'">';
                fl += '<input id="ResponseID_can'+j+'" type="hidden" value="'+res.AirShoppingRS.ShoppingResponseID[0].ResponseID[0]+'"><button type="submit" onclick=book("'+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0]._+'","'+ofr.Offer[j].TotalPrice[0].DetailCurrencyPrice[0].Total[0].$.Code+'",'+j+') class="btn" style="background:#e14443; color:#fff; margin-bottom:11px;">Book Now</button></div><br class="hidden-xs"></div>';
              fl+='</div></div>';
            }
          }
      }
      $('.loader').hide();
      if (fl=='') {
        alert(''+urldata.class+' class is unavailabe..try other class');
              location.href='/SearchFlight';
      }
      $('.AirShoppingRQ').html(fl);
    }else {
      return false;
      alert('error please try again');
      location.href='/SearchFlight';
    }
  }
});
$('#Confirmationdone').click(function(e) {
  $('#can_mod_FM').submit();
})

function book(a,b,c) {
console.log(a);
console.log(a,b,c);
var urldata = JSON.parse('{"' + decodeURI(location.search.substring(1).replace(/%2C/g, ",").replace(/&/g, "\",\"").replace(/%23/g, "#").replace(/=/g, "\":\"") + '"}'));
$('#myModalbook').modal('show');
var psng='';
for(var j=1;j<=urldata.adults;j++){
psng+='<div class="form-group row"><div class="col-sm-1">Adult '+j+'</div><div class="col-sm-2">';
psng+='<select class="form-control" name="atitle'+j+'" required><option value="">Title</option><option value="Mr">Mr</option><option value="Mrs">Mrs</option><option value="Ms">Ms</option></select></div><div class="col-sm-3"><input class="form-control" type="text" value="" name="afname'+j+'" placeholder="First Name/Given Name" required></div><div class="col-sm-3"><input class="form-control" type="text" name="amname'+j+'" placeholder="middle Name"></div><div class="col-sm-3"><input class="form-control" type="text" name="alname'+j+'" placeholder="Last Name/Surname"></div><div class="col-sm-6" style="padding:15px;"><input class="form-control" type="date" name="adate'+j+'"></div></div>';
}
for(var j=1;j<=urldata.child;j++){
psng+='<div class="form-group row"><div class="col-sm-1">Child '+j+'</div><div class="col-sm-2">';
psng+='<select class="form-control" required name="ctitle'+j+'"><option value="">Title</option><option value="Master" >Master</option><option value="Ms">Ms</option></select></div><div class="col-sm-3"><input class="form-control" type="text" value="" id="" name="cfname'+j+'" placeholder="First Name/Given Name" required></div><div class="col-sm-3"><input class="form-control" type="text" placeholder="middle Name" name="cmname'+j+'"></div><div class="col-sm-3"><input class="form-control" type="text" name="clname'+j+'" placeholder="Last Name/Surname"></div><div class="col-sm-6" style="padding:15px;"><input class="form-control" type="date" name="cdate'+j+'"></div></div>';
}
for(var j=1;j<=urldata.infant;j++){
psng+='<div class="form-group row"><div class="col-sm-1">Infant '+j+'</div><div class="col-sm-2">';
psng+='<select class="form-control" required  name="ititle'+j+'"><option value="">Title</option><option value="Master">Master</option><option value="Ms">Ms</option></select></div><div class="col-sm-3"><input class="form-control" type="text" value="" id="" name="ifname'+j+'" placeholder="First Name/Given Name" required></div><div class="col-sm-3"><input class="form-control" type="text" placeholder="middle Name" name="imname'+j+'"></div><div class="col-sm-3"><input class="form-control" type="text" placeholder="Last Name/Surname" name="ilname'+j+'"></div><div class="col-sm-6" style="padding:15px;"><input class="form-control" name="idate'+j+'" type="date"></div></div>';
}
psng+='<div class="form-group row"><input type="hidden" id="alldata" value='+c+'><div class="col-sm-1"></div><div class="col-sm-3"><input class="form-control" type="email" value="" name="email" placeholder="Email Address" required></div><div class="col-sm-3"><input class="form-control" type="tel" placeholder="PhoneNumber" name="phone"><input type="hidden" name="price" value='+a+'/></div></div>';
$('#can_mod').html(psng);
}
