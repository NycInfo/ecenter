$(document).ready(function() {
$( ".source1" ).autocomplete({
    minLength: 2,
    source: temp,
    autofocus:true,
    select: function( event, ui ) {
      $( ".source1" ).val( ui.item.label );
      $( ".source1_id" ).val( ui.item.value );
      return false;
    },focus: function( event, ui ) {
        event.preventDefault();
      }
  })
  .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li></li>" )
      .data( "item.autocomplete", item )
      .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
      .appendTo( ul );
  };
  $( ".source2" ).autocomplete({
      minLength: 2,
      source: temp,
      autofocus:true,
      select: function( event, ui ) {
        $( ".source2" ).val( ui.item.label );
        $( ".source2_id" ).val( ui.item.value );
        return false;
      },focus: function( event, ui ) {
          event.preventDefault();
        }
    })
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
        .appendTo( ul );
    };
$( ".destination1" ).autocomplete({
      minLength: 2,
      source: temp,
      select: function( event, ui ) {
        $( ".destination1" ).val( ui.item.label );
        $( ".destination1_id" ).val( ui.item.value );
        return false;
    },focus: function( event, ui ) {
        event.preventDefault();
      }
  })
  .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
      .data( "ui-autocomplete-item", item )
      .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
      .appendTo( ul );
  };
$( ".destination2" ).autocomplete({
      minLength: 2,
      source: temp,
      select: function( event, ui ) {
        $( ".destination2" ).val( ui.item.label );
        $( ".destination2_id" ).val( ui.item.value );
        return false;
    },focus: function( event, ui ) {
        event.preventDefault();
      }
  })
  .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
      .data( "ui-autocomplete-item", item )
      .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
      .appendTo( ul );
  };



  $( ".sourcemulti1" ).autocomplete({
      minLength: 2,
      source: temp,
      autofocus:true,
      select: function( event, ui ) {
        $( ".sourcemulti1" ).val( ui.item.label );
        $( ".sourcemulti1_id" ).val( ui.item.value );
        return false;
      },focus: function( event, ui ) {
          event.preventDefault();
        }
    })
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
        .appendTo( ul );
    };

    $( ".destinationmulti1" ).autocomplete({
          minLength: 2,
          source: temp,
          select: function( event, ui ) {
            $( ".destinationmulti1" ).val( ui.item.label );
            $( ".destinationmulti1_id" ).val( ui.item.value );
            return false;
        },focus: function( event, ui ) {
            event.preventDefault();
          }
      })
      .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          return $( "<li></li>" )
          .data( "ui-autocomplete-item", item )
          .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
          .appendTo( ul );
      };

      $( ".sourcemulti2" ).autocomplete({
          minLength: 2,
          source: temp,
          autofocus:true,
          select: function( event, ui ) {
            $( ".sourcemulti2" ).val( ui.item.label );
            $( ".sourcemulti2_id" ).val( ui.item.value );
            return false;
          },focus: function( event, ui ) {
              event.preventDefault();
            }
        })
        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
            .appendTo( ul );
        };

        $( ".destinationmulti2" ).autocomplete({
              minLength: 2,
              source: temp,
              select: function( event, ui ) {
                $( ".destinationmulti2" ).val( ui.item.label );
                $( ".destinationmulti2_id" ).val( ui.item.value );
                return false;
            },focus: function( event, ui ) {
                event.preventDefault();
              }
          })
          .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
              return $( "<li></li>" )
              .data( "ui-autocomplete-item", item )
              .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
              .appendTo( ul );
          };

          $( ".sourcemulti3" ).autocomplete({
              minLength: 2,
              source: temp,
              autofocus:true,
              select: function( event, ui ) {
                $( ".sourcemulti3" ).val( ui.item.label );
                $( ".sourcemulti3_id" ).val( ui.item.value );
                return false;
              },focus: function( event, ui ) {
                  event.preventDefault();
                }
            })
            .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
              return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                .appendTo( ul );
            };

            $( ".destinationmulti3" ).autocomplete({
                  minLength: 2,
                  source: temp,
                  select: function( event, ui ) {
                    $( ".destinationmulti3" ).val( ui.item.label );
                    $( ".destinationmulti3_id" ).val( ui.item.value );
                    return false;
                },focus: function( event, ui ) {
                    event.preventDefault();
                  }
              })
              .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                  return $( "<li></li>" )
                  .data( "ui-autocomplete-item", item )
                  .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                  .appendTo( ul );
              };

              $( ".sourcemulti4" ).autocomplete({
                  minLength: 2,
                  source: temp,
                  autofocus:true,
                  select: function( event, ui ) {
                    $( ".sourcemulti4" ).val( ui.item.label );
                    $( ".sourcemulti4_id" ).val( ui.item.value );
                    return false;
                  },focus: function( event, ui ) {
                      event.preventDefault();
                    }
                })
                .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                  return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                    .appendTo( ul );
                };

                $( ".destinationmulti4" ).autocomplete({
                      minLength: 2,
                      source: temp,
                      select: function( event, ui ) {
                        $( ".destinationmulti4" ).val( ui.item.label );
                        $( ".destinationmulti4_id" ).val( ui.item.value );
                        return false;
                    },focus: function( event, ui ) {
                        event.preventDefault();
                      }
                  })
                  .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                      return $( "<li></li>" )
                      .data( "ui-autocomplete-item", item )
                      .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                      .appendTo( ul );
                  };

                  $( ".sourcemulti5" ).autocomplete({
                      minLength: 2,
                      source: temp,
                      autofocus:true,
                      select: function( event, ui ) {
                        $( ".sourcemulti5" ).val( ui.item.label );
                        $( ".sourcemulti5_id" ).val( ui.item.value );
                        return false;
                      },focus: function( event, ui ) {
                          event.preventDefault();
                        }
                    })
                    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                      return $( "<li></li>" )
                        .data( "item.autocomplete", item )
                        .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                        .appendTo( ul );
                    };

                    $( ".destinationmulti5" ).autocomplete({
                          minLength: 2,
                          source: temp,
                          select: function( event, ui ) {
                            $( ".destinationmulti5" ).val( ui.item.label );
                            $( ".destinationmulti5_id" ).val( ui.item.value );
                            return false;
                        },focus: function( event, ui ) {
                            event.preventDefault();
                          }
                      })
                      .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                          return $( "<li></li>" )
                          .data( "ui-autocomplete-item", item )
                          .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                          .appendTo( ul );
                      };

                      $( ".sourcemulti6" ).autocomplete({
                          minLength: 2,
                          source: temp,
                          autofocus:true,
                          select: function( event, ui ) {
                            $( ".sourcemulti6" ).val( ui.item.label );
                            $( ".sourcemulti6_id" ).val( ui.item.value );
                            return false;
                          },focus: function( event, ui ) {
                              event.preventDefault();
                            }
                        })
                        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                          return $( "<li></li>" )
                            .data( "item.autocomplete", item )
                            .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                            .appendTo( ul );
                        };

                        $( ".destinationmulti6" ).autocomplete({
                              minLength: 2,
                              source: temp,
                              select: function( event, ui ) {
                                $( ".destinationmulti6" ).val( ui.item.label );
                                $( ".destinationmulti6_id" ).val( ui.item.value );
                                return false;
                            },focus: function( event, ui ) {
                                event.preventDefault();
                              }
                          })
                          .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                              return $( "<li></li>" )
                              .data( "ui-autocomplete-item", item )
                              .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
                              .appendTo( ul );
                          };
                          var today_validation = new Date().toISOString().split('T')[0];
                          $('.d_date,.d_date_r,.d1_date_m').attr('min',today_validation);
                          $('.d_date_r,.d1_date_m,.d2_date_m,.d3_date_m,.d4_date_m,.d5_date_m,.d6_date_m').change(function() {
                            $('.r_date_r').attr('min',$('.d_date_r').val());
                            $('.d2_date_m').attr('min',$('.d1_date_m').val());
                            $('.d3_date_m').attr('min',$('.d2_date_m').val());
                            $('.d4_date_m').attr('min',$('.d3_date_m').val());
                            $('.d5_date_m').attr('min',$('.d4_date_m').val());
                            $('.d6_date_m').attr('min',$('.d5_date_m').val());
                          })
});
