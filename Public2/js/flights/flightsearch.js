$('input[type=radio][name=triptype]').change(function() {
        if (this.value == 'oneway') {
          $("#firstsection").show();
          $("#secondsection").hide();
          $("#thirdsection").hide();
        }
        else if (this.value == 'roundtrip') {
          $("#firstsection").hide();
          $("#secondsection").show();
          $("#thirdsection").hide();
        }else{
          $("#firstsection").hide();
          $("#secondsection").hide();
          $("#thirdsection").show();
        }
    });

$("#addbtn_flighttrip").click(function(){
  var val = $("#extrahiddenval").val();
  if(val == "0"){
    $("#extrahiddenval").val("1");
    $("#extradiv3").show();
    return false;
  }
  if(val == "1"){
    $("#extrahiddenval").val("2");
    $("#extradiv4").show();
    return false;
  }
  if(val == "2"){
    $("#extrahiddenval").val("3");
    $("#extradiv5").show();
    return false;
  }
  if(val == "3"){
    $("#extrahiddenval").val("4");
    $("#extradiv6").show();
    return false;
  }
  return false;
})
$("#removebtn_flighttrip").click(function(){
  var val = $("#extrahiddenval").val();
  if(val == "4"){
    $("#extrahiddenval").val("3");
    $("#extradiv6").hide();
    return false;
  }
  if(val == "3"){
    $("#extrahiddenval").val("2");
    $("#extradiv5").hide();
    return false;
  }
  if(val == "2"){
    $("#extrahiddenval").val("1");
    $("#extradiv4").hide();
    return false;
  }
  if(val == "1"){
    $("#extrahiddenval").val("0");
    $("#extradiv3").hide();
    return false;
  }
})
