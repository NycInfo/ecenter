
function getallmovies(){
  $.ajax({
        data : '',
        dataType : 'json',
        type : 'POST',
        url : '/GetMovies',
        success : function (res) {
          console.log(res);
            if(res.success) {
              $.each(res.movies, function(i, genre) {
                let genreElement = $('#genreListing').clone();
                genreElement.css('display','block');
                genreElement.id = genre.title;
                genreElement.children('.heading-inner').html(genre.title);
                $.each(genre.details, function(i, movie){
                    let movieElementHtml = '<div><a href="movie_details?mid='+movie._id+'"><img src="'+movie.thumbnailUrl+'"></a></div>';
                //  let movieElementHtml ='<div><a href="movie_details?mid='+movie._id+'" data-toggle="modal" data-target="#myModal2" title=""><img src="'+movie.thumbnailUrl+'"></div></a></div>';
                    genreElement.find('.responsive').append(movieElementHtml);
                });
                $("#movieListContainer").append(genreElement);
                // $('#movieListContainer').append(genreElement).ready(function() {
                // });
              });
              applySlider();
            } else {
              $('#movieListContainer').focus().notify("fetching movies is failed ", { className: "error", position:"bottom" });
            }
        }
  });
}

function getEmoviesLoginPages(){
  $.ajax({
        //data : '',
        dataType : 'json',
        type : 'POST',
        url : '/GetMovies',
        success : function (res) {
            console.log(res);
            if(res.success) {
              $.each(res.movies, function(i, genre) {
                let genreElement = $('#genreListing').clone();
                genreElement.css('display','block');
                genreElement.id = genre.title;
                genreElement.children('.heading-inner').html(genre.title);
                $.each(genre.details, function(i, movie){
                //  txt2 +='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a data-toggle="modal" data-target="#myModal2" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';

                  //  let movieElementHtml = '<div><a href="movie_details?mid='+movie._id+'"><img src="'+movie.thumbnailUrl+'"></a></div>';
                  let movieElementHtml ='<div><a  data-toggle="modal" data-target="#myModal2" title=""><img src="'+movie.thumbnailUrl+'"></div></a></div>';
                    genreElement.find('.responsive').append(movieElementHtml);
                });
                $("#movieListContainer").append(genreElement);
                // $('#movieListContainer').append(genreElement).ready(function() {
                // });
              });
              applySlider();
            } else {
              $('#movieListContainer').focus().notify("fetching movies is failed ", { className: "error", position:"bottom" });
            }
        }
  });
}

function applySlider() {
  $('.responsive').slick({
    dots: false,
    centerMode: false,
    arrows: true,
    infinite: true,
    speed: 300,
    automatic: true,
    slidesToShow: 7,
    slidesToScroll: 7,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }

    ]
  });
}
function getmoviedetails(mid){
  var data = {};
  data.movieid = mid
  $.ajax({
        data : data,
        dataType : 'json',
        type : 'POST',
        url : '/GetMovieDetails',
        success : function (res) {
          console.log(res.data.isAvailable);
          if (res.data.isAvailable == "false") {
            $("#buynow_btn").hide();
            $("#buynow_btn_notavailable").show();
          }else{
            $("#buynow_btn").show();
            $("#buynow_btn_notavailable").hide();
          }
          var apnd = res.data;
          $("#movieid").val(apnd._id);
          $("#moviename").html(apnd.movie_name);
          $("#directorname").html(apnd.director)
          $("#starname").html(apnd.stars);
          $("#movie_length").html(apnd.movie_length);
          $("#movie_description").html(apnd.description)
          $("#movie_name").html(apnd.movie_name);
          $("#director_name").html(apnd.director)
          $("#star_name").html(apnd.stars);
        }
  });
}
$("#buy_mve_token").click(function(){
  var data = {};
  data.movieid = $("#movieid").val();
  data.cust_name = $("#cust_name").val();
  if (!checkInputAlpha($('#cust_name').val().trim()) || $('#cust_name').val().trim() == '') {
      $('#cust_name').focus().notify("Please Enter Valid Customer Name.", { className: "error", position: "bottom" });
      return false;
  }
  data.cust_number = $("#cust_number").val();
  if(!checkInputPhNum($('#cust_number').val())){
  $('#cust_number').focus().notify("Please Enter valid Customer Mobile Number.", { className: "error", position:"bottom" });
  return false;
  }
  $("#buy_mve_token").prop('disabled', true);
  $("#close_btn").prop('disabled', true);
  $.ajax({
      data : data,
      dataType : 'json',
      type : 'POST',
      url : '/Purchasetokenfor_movie',
      success : function (res) {
        if(res.success == true){
          $('#buy_mve_token').focus().notify("Purchased Movies Successfully", { className: "success", position:"bottom" });
          setTimeout(function(){
          $('#myModal').modal('hide');
          $('#tokenform')[0].reset();
          location.reload();
          getwalletmoney();
          sendmovieapplinkmessage(data.cust_number);
        },2000);
            return false;
        }else{
          $('#buy_mve_token').focus().notify("Token Is Not Purchased ", { className: "error", position:"bottom" });
          setTimeout(function(){
              location.reload();
          },2000);
          return false
        }
      }
  });
  return false;
})


function sendmovieapplinkmessage(number){
  var data  = {};
  data.cust_number = number;
  $.ajax({
        data : data,
        dataType : 'json',
        type : 'POST',
        url : '/sendignmessageemovies',
        success : function (res) {
          console.log("ok");
        }
  });
}



// app.post('/GetMovies',function(req,res){
//   const options = { method: 'GET',
//     url: 'https://learn-angular-mittu-spidey.c9users.io/api/emovies/list',
//     headers:movieservices.headersforapi(),
//     body: movieservices.bodyforapi(req.session.user,req.session.password),
//     json: true };
//
//   request(options, function (error, response, body) {
//     if(!error){
//       res.json(body);
//     }else{
//       res.json("No Getting Results")
//     }
//   });
// })
//
// app.post('/GetMovieDetails',function(req,res){
//   const movieid = req.body.movieid;
//   const options = { method: 'GET',
//   url: 'https://learn-angular-mittu-spidey.c9users.io/api/emovies/'+movieid,
//   headers: movieservices.headersforapi() };
//   request(options, function (error, response, body) {
//     if (error) throw new Error(error);
//     res.json(JSON.parse(body))
//   });
// })
