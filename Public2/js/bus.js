$('#bussubmit').submit(function(e){
	e.preventDefault();
		if($('#boarding').val() == '0'){
				$('#boarding').focus();
				$('#boarding_div').addClass('has-error');
				return false;
			} else {
					$('#boarding_div').removeClass('has-error');
							}
		if($('#dropping').val() == '0'){
			 $('#dropping').focus();
			 $('#dropping_div').addClass('has-error');
			 return false;
		 } else {
					$('#dropping_div').removeClass('has-error');
						}
    if($('.form-group row').hasClass('has-error')){
			  return false;
		}
  	var data = $(this).serializeArray();
		if ($('#seatnumber').val()=='') {
			$('#busform').focus().notify('Select Seat...',{ className: "error", position:"top" });
			return false;
		}
		$('#seats_n_pinfo').addClass('submitted');
		$(".form-control").prop('disabled',true)
		$('#busform').prop('disabled',true);
		var dateofjour=$("#datepicker1").val();
		$("#date_conf").text(moment(dateofjour).format('DD-MM-YYYY'));
		console.log(data);
    $.ajax({
				url:"/BusTicketBooking",
				dataType:"json",
				data : data,
				type : 'POST',
				success : function (res){
        console.log(res);
				if(res.statuscode == "TXN"){
					$("#Bussrch").hide();
          $("#BusAvailability1").hide();
					$("#BusAvailability").hide();
					$("#bookingid").val(res.data.booking_id);
					var ph=$("#phone").val();
					var fare=$("#totalprice").val();
					var seat=$("#seatnumber").val();
					var name=$("input[name=names1]").val();
					var age=$("#age").val();
					var gend=$("#gender").val();
					$("#Name_conf").text(name);
					$("#phone_conf").text(ph);
					$("#fare_conf").text(fare);
					$("#seat_conf").text(seat);
					$("#Age_conf").text(age);
					$("#gen_conf").text(gend);
					$("#confirmtickectmodal").show();
				}else {
					$('#busform').notify('"'+res.status+'"...',{ className: "error", position:"bottom" });
					$('#seats_n_pinfo').removeClass('submitted');
					$('#busform').prop('disabled',false);
					$(".form-control").prop('disabled',false)
					return false;
				}
      }
  })
})
$("#cancelbooking").click(function(e) {
	window.location='/BusSearch';
});
$("#confirmbooking").click(function(e) {
	$('#confirmbooking').prop('disabled',true);
	$('#cancelbooking').prop('disabled',true);
	e.preventDefault();
	var data={};
	data.id=$("#bookingid").val();
	data.Name=$("#Name_conf").text();
	data.Mobile=$("#phone_conf").text();
	data.Fare=$("#fare_conf").text();
	data.Seat=$("#seat_conf").text();
	data.date_jou=$("#date_conf").text();
	data.age=$("#Age_conf").text();
	data.gender=$("#gen_conf").text();
	// data.dep=$("#dep_conf").text();
	// data.Ari=$("#Ari_conf").text();
	data.subservices='BBS';
	data.services='BusBooking';
	$.ajax({
		url:'/confirm_booking',
		type:'POST',
		dataType:"json",
		data : data,
		success: function(res) {

			if (res.statuscode == "TXN") {
				 // console.log('booking success');
				 alert("booking success ...")
				 //$('#confirmbooking').notify('booking success ...',{ className: "success", position:"top" });
				 setTimeout(function(){ window.location="/BusSearch" }, 3000);
			}else if (res=='12') {
					alert("Insufficient balance...");
				// $('#confirmbooking').notify('Insufficient balance...',{ className: "error", position:"top" });
					setTimeout(function(){ window.location="/BusSearch" }, 3000);
			}else {
				alert('"'+res.status+'"...');
				//$('#confirmbooking').notify('"'+res.status+'"...',{ className: "error", position:"top" });
				setTimeout(function(){ window.location="/BusSearch" }, 3000);
			}
		}
	})
	return false;
});


$("#cancelbtn").click(function(){
	window.location="/BusSearch";
})
