$(document).ready(function() {
$( ".source1" ).autocomplete({
    minLength: 2,
    source: temp,
    autofocus:true,
    select: function( event, ui ) {
      $( ".source1" ).val( ui.item.label );
      $( ".source1_id" ).val( ui.item.value );
      return false;
    },focus: function( event, ui ) {
        event.preventDefault();
      }
  })
  .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li></li>" )
      .data( "item.autocomplete", item )
      .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
      .appendTo( ul );
  };
  $( ".source2" ).autocomplete({
      minLength: 2,
      source: temp,
      autofocus:true,
      select: function( event, ui ) {
        $( ".source2" ).val( ui.item.label );
        $( ".source2_id" ).val( ui.item.value );
        return false;
      },focus: function( event, ui ) {
          event.preventDefault();
        }
    })
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
        .appendTo( ul );
    };
$( ".destination1" ).autocomplete({
      minLength: 2,
      source: temp,
      select: function( event, ui ) {
        $( ".destination1" ).val( ui.item.label );
        $( ".destination1_id" ).val( ui.item.value );
        return false;
    },focus: function( event, ui ) {
        event.preventDefault();
      }
  })
  .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
      .data( "ui-autocomplete-item", item )
      .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
      .appendTo( ul );
  };
$( ".destination2" ).autocomplete({
      minLength: 2,
      source: temp,
      select: function( event, ui ) {
        $( ".destination2" ).val( ui.item.label );
        $( ".destination2_id" ).val( ui.item.value );
        return false;
    },focus: function( event, ui ) {
        event.preventDefault();
      }
  })
  .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
      .data( "ui-autocomplete-item", item )
      .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
      .appendTo( ul );
  };



  $( ".sourcemulti1" ).autocomplete({
      minLength: 2,
      source: temp,
      autofocus:true,
      select: function( event, ui ) {
        $( ".sourcemulti1" ).val( ui.item.label );
        $( ".sourcemulti1_id" ).val( ui.item.value );
        return false;
      },focus: function( event, ui ) {
          event.preventDefault();
        }
    })
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
        .appendTo( ul );
    };

    $( ".destinationmulti1" ).autocomplete({
          minLength: 2,
          source: temp,
          select: function( event, ui ) {
            $( ".destinationmulti1" ).val( ui.item.label );
            $( ".destinationmulti1_id" ).val( ui.item.value );
            return false;
        },focus: function( event, ui ) {
            event.preventDefault();
          }
      })
      .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          return $( "<li></li>" )
          .data( "ui-autocomplete-item", item )
          .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
          .appendTo( ul );
      };

      $( ".sourcemulti2" ).autocomplete({
          minLength: 2,
          source: temp,
          autofocus:true,
          select: function( event, ui ) {
            $( ".sourcemulti2" ).val( ui.item.label );
            $( ".sourcemulti2_id" ).val( ui.item.value );
            return false;
          },focus: function( event, ui ) {
              event.preventDefault();
            }
        })
        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
            .appendTo( ul );
        };

        $( ".destinationmulti2" ).autocomplete({
              minLength: 2,
              source: temp,
              select: function( event, ui ) {
                $( ".destinationmulti2" ).val( ui.item.label );
                $( ".destinationmulti2_id" ).val( ui.item.value );
                return false;
            },focus: function( event, ui ) {
                event.preventDefault();
              }
          })
          .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
              return $( "<li></li>" )
              .data( "ui-autocomplete-item", item )
              .append( "<div id='sai'><div><i class='fa fa-plane' style='font-size:24px;padding:10px;'></i><h3 style='display:inline-block;'> "+item.label+" </h3></div><div><span style='padding-left: 45px;margin-bottom:0px;color:grey;'>  "+item.Airport+" </span><span style='float:right;padding-left:5px;color:grey;'>"+item.country+"</span></div></div>" )
              .appendTo( ul );
          };

});
