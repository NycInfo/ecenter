function testSelect(d,id) {
	if (d == '0'){
        $('#'+id).focus();
		$('#'+id+"_div").addClass('has-error');
		return false;
    } else {
    	$('#'+id+"_div").removeClass('has-error');
    }
}
function testEmail(d,id) {
	if (!/^([a-zA-Z])+([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9\-]{2,4})$/g.test(d)){
        $('#'+id).val('');
		$('#'+id).focus();
		$('#'+id+"_div .errspan").remove();
		$('#'+id+"_div").append('<span class="col-sm-9 col-sm-push-3 errspan" style="color:red">Please Enter Correct Email</span>');
		$('#'+id+"_div").addClass('has-error');
		return false;
    } else {
    	$('#'+id+"_div .errspan").remove();
    	$('#'+id+"_div").removeClass('has-error');
    }
}
