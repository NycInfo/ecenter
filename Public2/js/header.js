
function getwalletmoney() {
 $.ajax({
  url: "/getmoney",
  dataType: "json",
  type: 'POST',
  async: true,
  crossDomain: true,
  success: function(data) {
   for (i = 0; i < data.length; i++) {
    var s=data[i].Wallet.toFixed(2);
   }
   $('#walletmoney').html("Rs. " + s);
  }
 });
}
function getToday() {
 $.ajax({
  url: "/getEarnings_today",
  dataType: "json",
  type: 'POST',
  async: true,
  crossDomain: true,
  success: function(data) {
    var mnt=0;
    for (var i = 0; i < data.length; i++) {
      if (data[i].bp_earn==undefined||null) {

      }else {
        mnt+=data[i].bp_earn;
      }
    }
    $('#commission_today').text("Rs. " + mnt.toFixed(2));
  }
 });
}
function getMonth() {
 $.ajax({
  url: "/getEarnings_month",
  dataType: "json",
  type: 'POST',
  async: true,
  crossDomain: true,
  success: function(data) {    
    var mnt=0;
    for (var i = 0; i < data.length; i++) {
      if (data[i].bp_earn==undefined||null) {

      }else {
        mnt+=data[i].bp_earn;
      }
    }
    $('#commission_month').text("Rs. " + mnt.toFixed(2));
  }
 });
}
function getagentdetails() {
 $.ajax({
  url: "/getdetails",
  dataType: "json",
  type: 'POST',
  async: true,
  crossDomain: true,
  success: function(data) {
   $("#AgentIdProdesc").val(data[0]._id)
   $("#AgentName").val(data[0].UserName);
   $("#AgentEmail").val(data[0].email);
   $("#AgentMobile").val(data[0].Mobile);
   $("#product1Description").val(data[0]._id)
   $("#buyerFirstName").val(data[0].UserName);
   $("#buyerEmail").val(data[0].email);
   $("#buyerPhoneNumber").val(data[0].Mobile);
   $('#agentname').html(data[0].UserName);
   if (data[0].UserName != "" || data[0].UserName != "undefined") {
    // $("#cssmenu1").hide();
    // $(".cssmenulogin").show();
   }
   else {
    // $("#cssmenu1").show();
    // $(".cssmenulogin").hide();
   }
  }
 });
}

$(document).ready(function() {
 getwalletmoney();
 getagentdetails();
 getmessages();
 getToday();
 getMonth();
 $(window).focus(function(e) {
  getwalletmoney();
   getToday();
 getMonth();
 });
});

function getmessages() {
 $.ajax({
  url: "/getmessage",
  dataType: "json",
  type: 'POST',
  async: true,
  crossDomain: true,
  success: function(data) {
   for (i = 0; i < data.length; i++) {
    $('#messagename').html(data[i].AgentName);
    $('#message').html(data[i].Message);
    dt1 = moment(data[i].CreateDate).format('YYYY-MM-DD');
    $('#messagetime').html(dt1);
    $('#countmessage').html(data.length);
   }
  }
 });
}
$('#updateForm').submit(function(e) {
 e.preventDefault();
 var formdata = $(this).serializeArray();
 $.ajax({
  type: "POST",
  url: "/getUpdate",
  data: formdata,
  dataType: 'JSON',
  async: true,
  crossDomain: true,
  success: function(data) {
   if (data == 1) {
    $('#myModal').modal('hide');
    getagentdetails();
   }
  }
 });
});


$("#signoutecenter").click(function() {
 signout();
 // return false;
});

function signout() {
 $.ajax({
  type: "POST",
  url: "/ecenterlogout",
  dataType: 'JSON',
  async: true,
  crossDomain: true,
  success: function(data) {
   console.log("success" + data);
   console.log(data);
   if (data.retStatus === 'Success') {
    // not sure what did you mean by ('/team' && '/team' !== "")
    // if('/team' && '/team' !== "") {
    if (data.redirectTo && data.msg == 'Just go there please') {
     console.log("inside");
     window.location = data.redirectTo;
    }
   }

  }
 });
}


const _1_MINUTE = 1000 * 60;

var sess_pollinterval = 1000 * _1_MINUTE;
const sess_expirationTime = 100000 * _1_MINUTE;
const sess_warningTime = 0.500000 * _1_MINUTE;

// var sess_pollinterval = 1 * _1_MINUTE;
// const sess_expirationTime = 20 * _1_MINUTE;
// const sess_warningTime = 15 * _1_MINUTE;


let sess_intervalID;
let sess_lastActivity;


function initSessionMonitor() {
  console.log("1");
 sess_lastActivity = new Date();
 sessSetInterval();
 //capturing any keypress or mouse click
 $(document).bind('keypress.session', (ed, e) => {
  //sessKeyPressed(ed, e);
  console.log("2");
 });
 $(document).bind('mousedown.session', (ed, e) => {
  //sessKeyPressed(ed, e);
  console.log("3");
 });
}

function sessSetInterval() {
  console.log("4");
 sess_intervalID = setInterval(sessInterval, sess_pollinterval);
}

function sessKeyPressed(ed, e) {
  console.log("5");
 sess_lastActivity = new Date();
}

function sessClearInterval() {
  console.log("6");
 clearInterval(sess_intervalID);
}

function logout() {
  console.log("7");
 signout();
 return true;
}

function pingServer() {
  console.log("8");
 $.ajax({
  type: "GET",
  url: "/ping",
  dataType: 'JSON',
  async: true,
  crossDomain: true,
  success: function(data) {
   console.log("success" + data);
  }
 });
}


function sessInterval() {
  console.log("9");
 // console.log("sessInterval()");
 let now = new Date();
 let diff = now - sess_lastActivity;
 if (diff >= sess_warningTime) {
  sessClearInterval();
  let diffInMin = parseInt((sess_expirationTime - sess_warningTime) / _1_MINUTE);
  var currentDate = new Date();
  if (confirm("Your session will expire in " + diffInMin + " minutes as of " + now.toLocaleString() + ".\n Press OK to remain loggedin or press cancel to logoff. If you are logged off any changes will be lost")) {
   now = new Date();
   diff = now - sess_lastActivity;
   if (diff > sess_expirationTime) {
    logout();
   }
   else {
    pingServer();
    sessClearInterval();
    sessSetInterval();
    sess_lastActivity = new Date();
   }
  }
  else {
   logout();
  }
 }
 else {
  // pingServer();
 }

}

//initSessionMonitor();
// session timout refernce:
// https://www.itworld.com/article/2832447/development/how-to-create-a-session-timeout-warning-for-your-web-application-using-jquery.html
