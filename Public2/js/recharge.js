function getallserv() {
  $.ajax({
    type: 'POST',
    url: '/getallserv',
    success: function(res) {
      res.sort(function(a, b){
        var nameA=a.sub_servicesName.toLowerCase(), nameB=b.sub_servicesName.toLowerCase()
        if (nameA < nameB)
        return -1
        if (nameA > nameB)
        return 1
        return 0
      })

      var Prepaid = $.grep(res, function(v) {
          return v.serviceCode === "PRP";
      });
      var txt='';
      $.each(Prepaid,function(i,v) {
         txt+='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a href="/Recharges?aid='+v._id+'" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#Prepaidreul').html(txt);
      var Postpaid = $.grep(res, function(v) {
          return v.serviceCode === "PPD";
      });
      var txt2='';
      $.each(Postpaid,function(i,v) {
         txt2 +='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a href="/Postpaid?aid='+v._id+'" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#Postpaidreul').html(txt2);


      var  dth = $.grep(res, function(v) {
          return v.serviceCode === "DTH";
      });
      var txt3 ='';
      $.each(dth,function(i,v) {
        txt3  +='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a href="/DTHRecharge?aid='+v._id+'" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#DTHreul').html(txt3);

      var  landline = $.grep(res, function(v) {
          return v.serviceCode === "LLN";
      });
         var txt4 ='';
      $.each(landline,function(i,v) {
         txt4 +='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a href="/LandLine?aid='+v._id+'" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#llreul').html(txt4);
    }
  })
}

function getallservlogin() {
  $.ajax({
    type: 'POST',
    url: '/getallserv',
    success: function(res) {
      console.log(res);
      res.sort(function(a, b){
        var nameA=a.sub_servicesName.toLowerCase(), nameB=b.sub_servicesName.toLowerCase()
        if (nameA < nameB)
        return -1
        if (nameA > nameB)
        return 1
        return 0
      })

      var Prepaid = $.grep(res, function(v) {
          return v.serviceCode === "PRP";
      });
      var txt='';
      $.each(Prepaid,function(i,v) {
         txt+='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a data-toggle="modal" data-target="#myModal2" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#Prepaidreul').html(txt);
      var Postpaid = $.grep(res, function(v) {
          return v.serviceCode === "PPD";
      });
      var txt2='';
      $.each(Postpaid,function(i,v) {
         txt2 +='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a data-toggle="modal" data-target="#myModal2" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#Postpaidreul').html(txt2);


      var  dth = $.grep(res, function(v) {
          return v.serviceCode === "DTH";
      });
      var txt3 ='';
      $.each(dth,function(i,v) {
        txt3  +='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a data-toggle="modal" data-target="#myModal2" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#DTHreul').html(txt3);

      var  landline = $.grep(res, function(v) {
          return v.serviceCode === "LLN";
      });
         var txt4 ='';
      $.each(landline,function(i,v) {
         txt4 +='<li class="col-lg-2 col-sm-4 col-md-4 col-xs-6"><a data-toggle="modal" data-target="#myModal2" title=""><div class="" style="background:#fff;"><img src="Public2/images/EImages/'+v.Icon+'" alt="" class="img-responsive" width="100%" height="" /></div><div class="imgthu">'+v.sub_servicesName+'</div></a></li>';
      });
      $('#llreul').html(txt4);
    }
  })
}
