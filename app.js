var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');
var express = require('express');
var app = express();
const helmet = require('helmet');
const mongo = require('./_helpers/mongo.connect');
const session = require('./_helpers/session');
const hart = require("./hart");
var request = require('request');
const authService = require('./_helpers/auth.service');
global.appRoot = path.resolve(__dirname);
var bodyParser = require('body-parser');
const config = require("config");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const errorHandler = require('./_helpers/error.handler');

const mongoose = mongo();
app.use(session.init(mongoose.connection));

var cookieParser = require('cookie-parser');
app.use(cookieParser());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


//logging setup
var morgan = require('morgan');
const logger = require("./_helpers/logger");
app.use(morgan('combined', { stream: logger.stream }));
//TEst
var transactAPI = require('./routes/posttozaakpay');
var response = require('./routes/response');
var responsepayu = require('./routes/responsepayu');
var checkAPI = require('./routes/poststatuschecktozaakpay');
var updateAPI = require('./routes/postmtxupdatetozaakpay');
// app.use('/', index);
//
require('./routes/mainroute')(app);
require('./routes/loginroutes')(app);
require('./routes/addagentRoute')(app);
require('./routes/DMTRoutes')(app);
require('./routes/ECommerceRoutes')(app);
require('./routes/NiharProductRoutes')(app);
require('./routes/NiharAddAgentRoutes')(app);
require('./routes/PanRoutes')(app);
require('./routes/RechargeRoutes')(app);
require('./routes/OnlineRTIRoutes')(app);
require('./routes/WalletRoutes')(app);
require('./routes/dashboard')(app);
require('./routes/redbusservice')(app);
require('./routes/DMTinstantPayRoutes')(app);
require('./routes/expresscartAPI')(app);
require('./routes/flightRoutes')(app);
require('./routes/flightmutitripRoutes')(app);
require('./routes/hotelRoutes')(app);
require('./routes/emoviesRoutes')(app);
require('./routes/electricityRoutes')(app);
require('./routes/invoice')(app);
require('./routes/customercareRoutes')(app);
require('./routes/sampletest')(app);
require('./routes/DigitalGold')(app);


app.use(errorHandler);


hart.mongo.Promise = global.Promise;


hart.mongo.connect(hart.url, { useNewUrlParser: true }, function(err, db) {
  if (!err) {
    console.log("mongodb connected successfully...");
  }
  else {
    console.log(err);
  }
});

app.use(helmet());
app.use(express.static(path.join(__dirname, '')));



//app.use(express.static(path.join(__dirname, '')));


// app.use();
app.get('/empty', (req, res) => {
  res.header('X-Frame-Options', 'ALLOWALL');
  res.sendFile(path.resolve(__dirname, 'Public2/empty.html'));
});

app.get('/token/:app', (req, res, next) => {
  logger.info(req.session);
  const appFamily = config.get('appFamily');
  const app = req.params.app;
  const appDomain = appFamily[app].domain;
  if (!appDomain) {
    console.log("App names incorrect");
    next({ type: "InputError", message: "App names incorrect" });
    return;
  }

  const expireTime = 15 * 60000 //1 minute
  // res.cookie('token', req.session.auth_token, { maxAge: expireTime, httpOnly: false });
  // res.cookie('userId', req.session.userId, { maxAge: expireTime, httpOnly: false });
  // res.cookie('username', req.session.username, { maxAge: expireTime, httpOnly: false });

  res.cookie('token', req.session.auth_token, { httpOnly: false });
  res.cookie('userId', req.session.userId, { httpOnly: false });
  res.cookie('username', req.session.username, { httpOnly: false });

  res.status(200).json({ success: true, appDomain: appDomain,token: req.session.auth_token });
})
app.get('/', function(req, res) {
  delete req.session.Niharinfo;
  res.sendFile(path.resolve(__dirname, 'Public/login.html'));
});
app.get('/logout', function(req, res) {
  req.session.logout((err) => {
    res.sendFile(path.resolve(__dirname, 'Public/login.html'));
  });
});

// app.get('*', function(req, res) {
//     res.redirect('https://' + req.headers.host + req.url);
// })
// Listen for requests\


logger.info("Using environment file: " + config.get("environment"));
app.listen(hart.port, function(e) {
  if (!e) {
    console.log('Server running at localhost:' + hart.port);
  }
})
